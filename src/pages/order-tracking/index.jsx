import CloseIcon from '@mui/icons-material/Close';
import { Box, Button, Container, IconButton, InputAdornment, OutlinedInput, Typography, styled } from "@mui/material";
import bgImg from "assets/images/delivery-banner.jpg";
import { COLORS } from 'assets/styles/colors/colors';
import { withPublicPage } from "components/HOC/PublicPage";
import { isEmpty } from 'helpers/functions';
import Head from 'next/head';
import { useRouter } from "next/router";
import { useEffect, useState } from 'react';

const CssTextField = styled(OutlinedInput)({
    '& .MuiInputBase-colorPrimary': {
      color: COLORS.primary,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: COLORS.primary,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: COLORS.gray
      },
      '&:hover fieldset': {
        border: "none",
      },
      '&.Mui-focused fieldset': {
        borderColor: COLORS.primary,
      },
    },
  });

const OrderTracking = () => {
    const route = useRouter();
    const {order_id} = route.query ?? "";
    const [id, setId] = useState("");
    

    useEffect(()=>{
        if(order_id){
            setId(order_id);
        }
    }, [order_id]);

  return (
      <Box>
          <Box
              sx={{
                  padding: "120px 0",
                  background: `url(${bgImg?.src})`,
                  backgroundSize: "cover",
                  backgroundPosition: "center center",
                  backgroundRepeat: "no-repeat",
                  position: "relative",
                  textAlign:"center"
              }}
          >
              <Box
                  sx={{
                      background: "black",
                      opacity: 0.7,
                      position: "absolute",
                      height: "100%",
                      width: "100%",
                      top: 0,
                      left: 0,
                     
                  }}
              />
                  
                  <Typography sx={{ color: "#fff", fontSize: 40, fontWeight: 600, position: "relative" }}>Track Your Order</Typography>
          </Box>
          <Container>
              <Head>
                  <title>Order tracker - Top Brand Outlet UK</title>
              </Head>
              <Box sx={{ padding: "50px 20px", minHeight: 300, display:"flex", alignItems: "center" }}>
                  <Box
                      sx={{
                          width: "100%",
                          display: "flex",
                          height: "100%",
                          gap: "10px",
                          alignItems: "center",
                          justifyContent: "center",
                      }}
                  >
                      <CssTextField
                          sx={{ width: "80%" }}
                          id="outlined-adornment-password"
                          type={"text"}
                          onChange={(e) => setId(e.target.value)}
                          value={id}
                          endAdornment={
                              <InputAdornment position="end">
                                  <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={() => setId("")}
                                      edge="end"
                                  >
                                      {isEmpty(id) ? "" : <CloseIcon />}
                                  </IconButton>
                              </InputAdornment>
                          }
                          label=""
                          placeholder="Enter Your Order ID"
                          size="small"
                      />
                      <Button
                          sx={{
                              padding: "6px 60px",
                              border: "1px solid #800",
                              textTransform: "capitalize",
                              color: "#800",
                              "&:hover": { background: "#800", color: "#fff" },
                          }}
                      >
                          Search
                      </Button>
                  </Box>
              </Box>
          </Container>
      </Box>
  );
}

export default withPublicPage(OrderTracking);
