import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { withPublicPage } from "components/HOC/PublicPage";
import { SlideProductCard } from "components/ProductCard/slideProductCard";
import Sliders from "components/Slider";
import { LAST_VISITED_PRODUCTS } from "helpers/config";
import { appStorage } from "utils/storage";

const Preview = () => {
    const lastVisitedProducts = JSON.parse(
        appStorage.get(LAST_VISITED_PRODUCTS)
    );

    const productSetting = {
        slidesToShow: 5,
        infinite: false,
        centerMode: false,
        rows: 1,
        dots: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    arrows: true,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    arrows: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 414,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 411,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 395,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
        ],
    };
    return (
        <Container>
            {lastVisitedProducts?.length > 1 && (
                <Grid container spacing={2} className="recent-view">
                    <Box sx={{ width: "100%" }}>
                        <Box className="product-slider">
                            <Typography component={"h2"}>
                                Recently viewed items
                            </Typography>
                            <Divider />
                            <Box sx={{ width: "100%" }}>
                                <Sliders settings={productSetting}>
                                    {lastVisitedProducts
                                        ?.map((data, index) => (
                                            <Box key={index}>
                                                <SlideProductCard item={data} />
                                            </Box>
                                        ))}
                                </Sliders>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            )}
        </Container>
    );
};

export default withPublicPage(Preview);
