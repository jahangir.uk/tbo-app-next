import { Box, Container } from "@mui/material";
import FaqImg from "assets/images/back.jpg";
import { useFaqStyle } from "assets/stylesheets/faq/faqStylesheet";
import { Accordions } from "components/Accordion";
import { withPublicPage } from "components/HOC/PublicPage";
import { HeroBanner } from "components/HeroBanner";
import { Faqs } from "components/dummy-data/FAQ";

 function Faq() {
  const faq_list = Faqs;
  const classes = useFaqStyle();
  return (
    <Box>
        <HeroBanner title={'FAQ'} image={FaqImg} />
        <Container>
            <Box>
                <Accordions data={faq_list?.returns} />
            </Box>
        </Container>
    </Box>
  );
};

export default withPublicPage(Faq);
