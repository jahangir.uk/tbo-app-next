import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import MailIcon from "@mui/icons-material/Mail";
import PhoneIcon from "@mui/icons-material/Phone";
import PlaceIcon from "@mui/icons-material/Place";
import {
    Box,
    Card,
    CardContent,
    Container,
    FormControl,
    Grid,
    ListItemIcon,
    ListItemText,
    TextField,
    TextareaAutosize,
    Typography,
    styled,
} from "@mui/material";
import heroImg from "assets/images/contact-header.jpg";
import { COLORS } from "assets/styles/colors/colors";
import { useContactUsStyle } from "assets/stylesheets/contact-us/contactUsStylesheet";
import { CustomButton } from "components/Button";
import { withPublicPage } from "components/HOC/PublicPage";
import { HeroBanner } from "components/HeroBanner";
import { GoogleMap } from "components/Map";
import Head from "next/head";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.primary,
    },
    "& .MuiOutlinedInput-root": {
        "& fieldset": {
            borderColor: COLORS.gray,
        },
        "&:hover fieldset": {
            borderColor: COLORS.primary,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.primary,
        },
    },
});

function ContactUs() {
    const classes = useContactUsStyle();
    return (
        <Box className={classes.root}>
            <Head>
                <title>Contact us - Top Brand Outlet UK</title>
                <meta property="og:title" content="Contact us - Top Brand Outlet UK" />
                <meta
                    property="og:description"
                    content="Contact us - Top Brand Outlet UK"
                />
                <meta
                    property="og:image"
                    content="https://example.com/images/cool-page.jpg"
                />
            </Head>
            <HeroBanner title={"Contact Us"} image={heroImg} />
            <Container>
                <Grid container className="form">
                    <Grid item md={4} sm={6} xs={12} className="quick-contact">
                        <Box className="quick-contact"></Box>
                    </Grid>
                    <Grid item md={8} sm={6} xs={12}>
                        <Box className="contact-form">
                            <h2>Quick Contact</h2>
                            <Box component="form" noValidate sx={{ mt: 1 }}>
                                <FormControl fullWidth>
                                    <CssTextField
                                        id="standard-basic"
                                        label="Full Name"
                                        variant="outlined"
                                    />
                                </FormControl>

                                <FormControl
                                    variant="standard"
                                    fullWidth
                                    sx={{ mt: 2 }}
                                >
                                    <CssTextField
                                        id="standard-basic"
                                        label="Email"
                                        variant="outlined"
                                        type="email"
                                    />
                                </FormControl>
                                <FormControl
                                    variant="standard"
                                    fullWidth
                                    sx={{ mt: 2 }}
                                >
                                    <CssTextField
                                        id="standard-basic"
                                        label="Order Number"
                                        variant="outlined"
                                    />
                                </FormControl>
                                <FormControl
                                    variant="standard"
                                    fullWidth
                                    sx={{ mt: 2 }}
                                >
                                    <TextareaAutosize
                                        placeholder="Description"
                                        minRows={4}
                                    />
                                </FormControl>
                                <FormControl
                                    variant="standard"
                                    fullWidth
                                    sx={{ mt: 2 }}
                                >
                                    <CssTextField
                                        id="standard-basic"
                                        variant="outlined"
                                        type="file"
                                    />
                                </FormControl>
                                <CustomButton height={40} text={"Send"} />
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
                <Grid container className="card-list" spacing={1}>
                    <Grid item md={4} sm={6} xs={12}>
                        <Card sx={{ minHeight: 130 }}>
                            <CardContent>
                                <Typography
                                    sx={{ fontSize: 14 }}
                                    color="text.secondary"
                                    gutterBottom
                                >
                                    Address
                                </Typography>
                                <Box
                                    sx={{
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <ListItemIcon>
                                        <PlaceIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="35 Knockhall Rd, Greenhithe DA9 9EZ, United Kingdom" />
                                </Box>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item md={4} sm={6} xs={12}>
                        <Card sx={{ minHeight: 130 }}>
                            <CardContent>
                                <Typography
                                    sx={{ fontSize: 14 }}
                                    color="text.secondary"
                                    gutterBottom
                                >
                                    Quick Contact
                                </Typography>
                                <Box
                                    sx={{
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <ListItemIcon>
                                        <PhoneIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="0330 133 2599" />
                                </Box>
                                <Box
                                    sx={{
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <ListItemIcon>
                                        <MailIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="contactus@topbrandoutlet.co.uk" />
                                </Box>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item md={4} sm={6} xs={12}>
                        <Card sx={{ width: "100%", minHeight: 130 }}>
                            <CardContent>
                                <Typography
                                    sx={{ fontSize: 14 }}
                                    color="text.secondary"
                                    gutterBottom
                                >
                                    Social Link
                                </Typography>
                                <Box className="social-list">
                                    <Box component="a" href="#">
                                        <FacebookIcon />
                                    </Box>
                                    <Box component="a" href="#">
                                        <InstagramIcon />
                                    </Box>
                                </Box>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Box sx={{ paddingBottom: "50px" }}>
                    <GoogleMap />
                </Box>
            </Container>
        </Box>
    );
}

export default withPublicPage(ContactUs);
