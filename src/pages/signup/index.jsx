import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Box, FormControl, Grid, InputLabel } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import OutlinedInput from "@mui/material/OutlinedInput";
import Typography from "@mui/material/Typography";
import LoginImage from "assets/images/Sign-up.png";
import BackImg from "assets/images/back.jpg";
import AppleIcon from "assets/images/social-icon/Appleicon.png";
import TwitterIcon from "assets/images/social-icon/Twitter.png";
import FacebookIcon from "assets/images/social-icon/facebook.png";
import GoogleIcon from "assets/images/social-icon/google.png";
import InstaIcon from "assets/images/social-icon/instra.png";
import LinkedinIcon from "assets/images/social-icon/linkedinicon.png";
import axios from "axios";
import { CustomButton } from "components/Button";
import { SocialLoginButton } from "components/SocialLoginButton";
import { API_URL } from "helpers/apiUrl";
import useSnackbar from "hooks/useSnackbar";
import useToken from "hooks/useToken";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { login } from "redux/action/authActions";

export default function SignUp  () {
  const [showPassword, setShowPassword] = useState(false);
  const snackbar = useSnackbar();
  const dispatch = useDispatch();
  const isAuthenticated = useToken();
  const router = useRouter();
  const handleClickShowPassword = () => setShowPassword((show) => !show);
//   const navigate = useNavigate();
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const {
    handleSubmit,
    register,
    formState: { errors },
    reset
  } = useForm();

  const onSubmit = async (data) => {
    axios
      .post(`${API_URL}/customer-registration`, data, {})
      .then((res) => {
        console.log("Response", res);
        if (res?.data.status === "success") {
          reset();
          snackbar("Registration Successfully", {
            variant: "success"
          });
          const res =  dispatch(login({username: data?.email, password: data.password}));
        //   navigate("/login");
        } else if (res?.data?.status === "error") {
          if (res?.data.fieldErrors.username) {
            snackbar(res?.data?.fieldErrors.username?.message, {
              variant: "error"
            });
          }
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
    // console.log(data);
  };

  useEffect(() => {
    if(isAuthenticated){
        router.push('/my-account');
    }
   
  }, [isAuthenticated]);

  return (
    <Box
      sx={{
        minHeight: "90vh",
        height: "100%",
        background: `url(${BackImg?.src})`,
        padding: "50px 80px",
        backgroundSize: "cover",
        display: "flex",
        alignItems: "center"
      }}
    >
        <Head>
            <title>Sign up  - Top Brand Outlet UK</title>
        </Head>
      <Container component="main" width={1100}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            background: "#efefef",
            padding: 3,
            borderRadius: 2.5
          }}
        >
          <Grid
            container
            spacing={2}
            sx={{ display: "flex", alignItems: "center" }}
          >
            <Grid item md={6} sm={12} sx={{ '& img': {width:'100%', height:'auto'} }}>
              <Image
                src={LoginImage}
                alt={"Login"}
              />
            </Grid>
            <Grid item md={6} sm={12}>
              {/* <Box component={"img"} sx={{ m: 1 }} src={TopLogo} alt="Logo" /> */}

              <Typography
                component="h1"
                variant="h2"
                sx={{
                  fontWeight: 600,
                  paddingBottom: 2,
                  fontSize: { md: "3.75rem", sm: "1.75rem", xs: "1.75rem" }
                }}
              >
                Sign up
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit(onSubmit)}
                noValidate
                sx={{ mt: 1 }}
              >
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    First Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    type="text"
                    label="First Name"
                    {...register("first_name", {
                      type: "text"
                    })}
                    error={!!errors.first_name}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Last Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Last Name"
                    {...register("last_name", {
                      type: "text"
                    })}
                    error={!!errors.last_name}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Username
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Username"
                    {...register("username", {
                      type: "text"
                    })}
                    error={!!errors.username}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Email
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Email"
                    type="email"
                    {...register("email", {
                      type: "email"
                    })}
                    error={!!errors.email}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Nick Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Nick Name"
                    type="text"
                    {...register("nickname", {
                      type: "nickname"
                    })}
                    error={!!errors.nickname}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                    label="Password"
                    {...register("password", {
                      type: "password"
                    })}
                    error={!!errors.password}
                  />
                </FormControl>
                <FormControl sx={{ marginTop: 2, width: "100%" }}>
                  <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label={
                      <Box sx={{ fontSize: { md: "1rem", xs: "14px" } }}>
                        Click to agree Terms of Use & Privacy Policy
                      </Box>
                    }
                    fullWidth
                  />
                </FormControl>
                <CustomButton type="submit" fullWidth text={"Sign up"} />
                <Grid container>
                  <Grid item xs></Grid>
                  <Grid item>
                    <Link href="/login" variant="body2">
                      {"Already have account? Log in"}
                    </Link>
                  </Grid>
                </Grid>
              </Box>
              <Divider
                sx={{
                  margin: "24px 0px",
                  flexShrink: 0,
                  display: "flex",
                  whiteSpace: "nowrap",
                  textAlign: "center",
                  border: "0px"
                }}
              >
                Or
              </Divider>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  gap: 2
                }}
              >
                <SocialLoginButton icon={GoogleIcon} color={"#df3e30"} />
                <SocialLoginButton icon={TwitterIcon} color={"#1877f2"} />
                <SocialLoginButton icon={InstaIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={FacebookIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={LinkedinIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={AppleIcon} color={"#1c9cea"} />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};
