import LogoutIcon from '@mui/icons-material/Logout';
import { Box, Container, Tab, Tabs, useTheme } from "@mui/material";
import { useMyAccountStyle } from "assets/stylesheets/my-account/myAccountStyleSheet";
import { withPublicPage } from "components/HOC/PublicPage";
import { Address } from 'components/myAccount/address';
import { MyDetails } from 'components/myAccount/myDetails';
import { Orders } from 'components/myAccount/orders';
import useAuthUser from "hooks/useAuthUser";
import Head from 'next/head';
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserDetails, logout } from "redux/action/authActions";
import OrderServiceProvider from 'services/OrderServiceProvider';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

function MyAccount() {
    const userData = useAuthUser();
    const {userDetails, isAuthenticated } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    // const [user, setUser] = useState();
    const [orderList, setOrderList] = useState([]);
    const router = useRouter();
    const classes = useMyAccountStyle();

    const logoutClick = () => {
        dispatch(logout());
        router.push('/login');
    };
    
    const [value, setValue] = useState(0);
    const theme = useTheme();
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const handleGetOrderList = () => {
        OrderServiceProvider.Commands.getOrderList().then((res) => {
            console.log(res);
            setOrderList(res?.allOrder);
        }).catch((error) => {
            console.log(error);
        })
    }

    // useEffect(()=>{
    //     if(userData){
    //         setUser(userData)
    //     }
    // }, [userData])

    useEffect(() => {
        dispatch(getUserDetails());
        handleGetOrderList();
    }, []);

    useEffect(() => {
        if(!isAuthenticated){
            router.push('/login');
        }
    }, []);

    // console.log(isAuthenticated);

    return (
        <Container className={classes.root} >
            <Head>
                <title>My account - Top Brand Outlet UK</title>
            </Head>
            <Box sx={{ width: "100%", display: "flex", minHeight: "60vh" }}>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={value}
                    onChange={handleChange}
                    aria-label="Vertical tabs example"
                    sx={{ borderRight: 1, borderColor: "divider", width: 200 }}
                    
                >
                    <Tab label="My Details" {...a11yProps(0)} />
                    <Tab label="Orders" {...a11yProps(1)} />
                    <Tab label="Address" {...a11yProps(2)} />
                    <Tab icon={<LogoutIcon/>} iconPosition="end" label="Logout" {...a11yProps(3)} onClick={logoutClick} />
                </Tabs>
                <TabPanel value={value} index={0} style={{ width: '100%' }}>
                   <MyDetails/>
                </TabPanel>
                <TabPanel value={value} index={1} style={{ width: '100%' }}>
                    <Orders orders={orderList} />
                </TabPanel>
                <TabPanel value={value} index={2} style={{ width: '100%' }}>
                    <Address/>
                </TabPanel>
            </Box>
        </Container>
    );
}

export default withPublicPage(MyAccount);
