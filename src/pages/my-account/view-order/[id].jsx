import {
    Box,
    Card,
    CardContent,
    CardHeader,
    Container,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import { useViewOrderStyle } from "assets/stylesheets/my-account/orderViewStyleSheet";
import { withPublicPage } from "components/HOC/PublicPage";
import { singleOrder } from "components/dummy-data/singleProduct";
import { IMAGE_URL } from "helpers/config";
import getCountryName from "helpers/countries";
import { capitalize, formatDate, isEmpty } from "helpers/functions";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";
import OrderServiceProvider from "services/OrderServiceProvider";

const ViewOrder = () => {
    const classes = useViewOrderStyle();
    // const [item, setItem] = useState(null);
    const router = useRouter();
    const { id } = router.query;
    const item = singleOrder;
    const getOrderById = () => {
        OrderServiceProvider.Commands.getOrderByCode(id).then((res) => {
            console.log(res);
        });
    };

    const getStatus = (code) => {
        switch(code){
            case 1:
                return "Cancel";
            case 2:
                return "Processing";
            case 3:
                return "Complete";
            default:
                return "Shipping";
        }
    }

    useEffect(() => {
        getOrderById();
    }, [id]);

    return (
        <Container>
            <Box className={classes.root}>
                {isEmpty(item) ? (
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "center",
                            minHeight: 400,
                        }}
                    >
                        <Typography
                            sx={{ fontWeight: 600, fontSize: 30, color: "red" }}
                        >
                            Invalid Order ID:{" "}
                            <span style={{ color: "green" }}>{id}</span>
                        </Typography>
                        <Box
                            sx={{
                                textAlign: "center",
                                mt: 3,
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "space-between",
                                gap: "20px",
                            }}
                        >
                            <Box
                                sx={{
                                    background: "#891d1d",
                                    padding: "10px 20px",
                                    borderRadius: "5px",
                                    "& a": {
                                        textDecoration: "none",
                                        fontWeight: 600,
                                        color: "#ffffff",
                                    },
                                }}
                            >
                                <Link href={"/my-account"}>Go to Profile</Link>
                            </Box>

                            <Box
                                sx={{
                                    background: "#2c2c07",
                                    padding: "7px 20px",
                                    borderRadius: "5px",
                                    cursor: "pointer",
                                }}
                            >
                                <Typography
                                    sx={{
                                        textDecoration: "none",
                                        fontWeight: 600,
                                        color: "#ffffff",
                                        padding: 0,
                                        margin: 0,
                                        "& a": {
                                            color: "#ffffff",
                                            textDecoration: "none",
                                        },
                                    }}
                                >
                                    <Link href={"/shop"}>
                                        Continue Shopping
                                    </Link>
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                ) : (
                    <Box sx={{ marginBottom: 2 }}>
                        <Link
                            style={{
                                background: "#cccccc",
                                padding: "5px 15px",
                                textDecoration: "none",
                                marginBottom: "10px",
                            }}
                            href="/my-account"
                        >
                            Back To Profile
                        </Link>
                        <Typography
                            sx={{ color: "#6c6c6c", padding: "10px 0" }}
                        >
                            Order <strong>#{item?.invoice}</strong> was placed
                            on{" "}
                            <strong>
                                {formatDate(item?.updatedAt, "MMM DD, YYYY")}
                            </strong>{" "}
                            and is currently <strong>{getStatus(item?.status)}</strong>.
                        </Typography>
                        <Link
                            href={`/order-tracking?order_id=${item?.invoice}`}
                            style={{
                                background: "#800",
                                display: "inline-block",
                                padding: "7px 20px",
                                borderRadius: "5px",
                                color: "#fff",
                            }}
                        >
                            Track Your Order
                        </Link>
                        <Typography
                            sx={{
                                color: "#000000",
                                fontSize: 28,
                                fontWeight: 600,
                            }}
                        >
                            Order Details
                        </Typography>
                        <Box
                            sx={{
                                background: "#e7ebf0",
                                padding: "15px",
                                borderRadius: "5px",
                                "& th": { fontWeight: 600 },
                            }}
                        >
                            <TableContainer component={Paper}>
                                <Table
                                    sx={{
                                        minWidth: 650,
                                        borderColor: "#e7e5e5",
                                    }}
                                    aria-label="simple table"
                                    border={1}
                                >
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Product</TableCell>
                                            <TableCell align="right">
                                                Total
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {item?.orderitem?.map((row) => (
                                            <TableRow key={row.id}>
                                                <TableCell
                                                    component="th"
                                                    scope="row"
                                                    sx={{ width: "75%" }}
                                                >
                                                    <Link
                                                        style={{
                                                            color: "#800",
                                                        }}
                                                        href={`/product/${row?.productObj?.slug}`}
                                                    >
                                                        <img
                                                            alt="img"
                                                            width={50}
                                                            src={
                                                                IMAGE_URL +
                                                                "variant-product-image/" +
                                                                row
                                                                    ?.vatiantProductObj
                                                                    ?.featured_image
                                                            }
                                                        />{" "}
                                                        {
                                                            row
                                                                ?.vatiantProductObj
                                                                ?.name
                                                        }
                                                    </Link>{" "}
                                                    x {row?.quantity}
                                                    <br />
                                                    <Typography>
                                                        <strong>SKU:</strong>{" "}
                                                        {
                                                            row
                                                                ?.vatiantProductObj
                                                                ?.sku
                                                        }
                                                    </Typography>
                                                </TableCell>
                                                <TableCell
                                                    align="right"
                                                    component="th"
                                                >
                                                    {
                                                        row?.vatiantProductObj
                                                            ?.sale_price
                                                    }
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                        <TableRow>
                                            <TableCell
                                                sx={{ width: "75%" }}
                                                component={"th"}
                                            >
                                                Subtotal:
                                            </TableCell>
                                            <TableCell
                                                align="right"
                                                component={"th"}
                                            >
                                                {Number(
                                                    item?.sub_total
                                                ).toFixed(2)}
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell
                                                sx={{ width: "75%" }}
                                                component={"th"}
                                            >
                                                Shipping:
                                            </TableCell>
                                            <TableCell
                                                align="right"
                                                component={"th"}
                                            >
                                                {item?.shipping_method}
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell
                                                sx={{ width: "75%" }}
                                                component={"th"}
                                            >
                                                Payment method:
                                            </TableCell>
                                            <TableCell
                                                align="right"
                                                component={"th"}
                                            >
                                                {capitalize(
                                                    item?.payment_method
                                                )}
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell
                                                sx={{ width: "75%" }}
                                                component={"th"}
                                            >
                                                Total:
                                            </TableCell>
                                            <TableCell
                                                align="right"
                                                component={"th"}
                                            >
                                                {Number(
                                                    item?.grand_total
                                                ).toFixed(2)}
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Box>
                        <Box className="details">
                            <Grid container spacing={2}>
                                <Grid item md={6}>
                                    <Card className="personal-info">
                                        <CardHeader
                                            title={
                                                <Box>
                                                    <Typography
                                                        component={"h1"}
                                                    >
                                                        Shipping address
                                                    </Typography>
                                                </Box>
                                            }
                                        />
                                        <CardContent>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Street</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {
                                                        item?.shipping_street_address
                                                    }
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>City</strong>
                                                </Box>{" "}
                                                <Box>{item?.shipping_city}</Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Post Code</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {item?.shipping_post_code}
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Country</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {getCountryName(
                                                        item?.shipping_country
                                                    )}
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Email</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {getCountryName(
                                                        item?.shipping_email
                                                    )}
                                                </Box>{" "}
                                            </Box>
                                        </CardContent>
                                    </Card>
                                </Grid>
                                <Grid item md={6}>
                                    <Card className="personal-info">
                                        <CardHeader
                                            title={
                                                <Box>
                                                    <Typography
                                                        component={"h1"}
                                                    >
                                                        Billing address
                                                    </Typography>
                                                </Box>
                                            }
                                        />
                                        <CardContent>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Street</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {
                                                        item?.billing_street_address
                                                    }
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>City</strong>
                                                </Box>{" "}
                                                <Box>{item?.billing_city}</Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Post Code</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {item?.billing_post_code}
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Country</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {getCountryName(
                                                        item?.billing_country
                                                    )}
                                                </Box>{" "}
                                            </Box>
                                            <Box className="item">
                                                <Box className="heading">
                                                    <strong>Email</strong>
                                                </Box>{" "}
                                                <Box>
                                                    {getCountryName(
                                                        item?.billing_email
                                                    )}
                                                </Box>{" "}
                                            </Box>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                )}
            </Box>
        </Container>
    );
};

export default withPublicPage(ViewOrder);

// export async function getServerSideProps(context) {
//     const { params } = context;
//     const { id } = params;
//     const response = await OrderServiceProvider.Commands.getOrderByCode(id);

//     console.log(response, id);
//     return {
//         props: {
//             item: response?.data ?? null,
//             id: id,
//         },
//     };
// }
