import FacebookOutlinedIcon from "@mui/icons-material/FacebookOutlined";
import InstagramIcon from "@mui/icons-material/Instagram";
import StarIcon from "@mui/icons-material/Star";
import TwitterIcon from "@mui/icons-material/Twitter";
import {
    Box,
    Button,
    Container,
    Divider,
    Grid,
    Tab,
    Tabs,
    Typography,
    useTheme,
} from "@mui/material";
import { useSingleProductStyle } from "assets/stylesheets/singleProduct/singleProductStylesheet";
import { CustomButton } from "components/Button";
import { withPublicPage } from "components/HOC/PublicPage";
import { ImageSliderWithLeftThumbnail } from "components/ImageSlider/ImageSliderWithLeftThumbnail";
import { Loader } from "components/Loader";
import { AfterAddToCartModal } from "components/Modals/AfterAddToCartModal";
import { SlideProductCard } from "components/ProductCard/slideProductCard";
import Sliders from "components/Slider";
import { IMAGE_URL, LAST_VISITED_PRODUCTS } from "helpers/config";
import { camelCaseToText, capitalize, isEmpty } from "helpers/functions";
import useSnackbar from "hooks/useSnackbar";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import SwipeableViews from "react-swipeable-views";
import { addToCart } from "redux/action/cart";
import ProductService from "services/ProductService";
import { appStorage } from "utils/storage";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

function Product({ item, variantProducts, productImages }) {
    const snackbar = useSnackbar();
    const classes = useSingleProductStyle();
    const [loading, setLoading] = useState(false);
    const [terms, setTerms] = useState([]);
    // const [item, setItem] = useState(null);
    const [cartModalOpen, setCartModalOpen] = useState(false);
    const router = useRouter();
    // const { slug } = router.query;
    const [qty, setQty] = useState(1);
    const [value, setValue] = useState(0);
    const theme = useTheme();
    const dispatch = useDispatch();
    const [selectedVariant, setSelectedVariant] = useState([]);
    // const [variantProducts, setVariantProducts] = useState([]);
    // const [productImages, setProductImages] = useState([]);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const [selectedProduct, setSelectedProduct] = useState(null);

    const lastVisitedProducts = JSON.parse(
        appStorage.get(LAST_VISITED_PRODUCTS)
    );

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const slideSetting = {
        slidesToShow: 1,
        infinite: true,
        centerMode: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 414,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 411,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                },
            },
            {
                breakpoint: 395,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                },
            },
        ],
    };

    const productSetting = {
        slidesToShow: 5,
        infinite: false,
        centerMode: false,
        rows: 1,
        dots: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                },
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    arrows: true,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                    dots: false,
                    arrows: true,
                },
            },
            {
                breakpoint: 414,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 411,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 395,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
        ],
    };
 
    const selectTerms = (attrId) => (e) => {
        console.log(attrId, e.target.value);
        const selectTedTerms = item?.producttermsArr.find(
            (i) => i.termsId === e.target.value && i.attributeId === attrId
        );
        if (isEmpty(e.target.value)) {
            setTerms((prev) => prev.filter((i) => i.attributeId !== attrId));
        }
        console.log(selectTedTerms);
        const findIndex = terms.find(
            (data) =>
                data?.attributeId === selectTedTerms?.attributeId &&
                data?.termsId === selectTedTerms?.termsId
        );
        const hasAttr = terms.find(
            (data) => data?.attributeId === selectTedTerms?.attributeId
        );
        if (!findIndex && !hasAttr) {
            setTerms([...terms, selectTedTerms]);
        } else if (hasAttr) {
            setTerms((prev) =>
                prev.map((i) =>
                    i?.attributeId === hasAttr?.attributeId ? selectTedTerms : i
                )
            );
        }
    };

    const lastVisiteProduct = (item) => {
        const data = [];
        data.unshift(item);
        const items = isEmpty(lastVisitedProducts) ? data : lastVisitedProducts;
        const findIndex = items?.find((data) => data.id === item.id);

        if (!findIndex) {
            items.unshift(item);
            if (lastVisitedProducts?.length >= 9) {
                items.pop();
            }
        }

        // console.log(items);

        appStorage.set(LAST_VISITED_PRODUCTS, JSON.stringify(items));
    };

    const onChangeQty = (e) => {
        const q = e.target.value;
        if (selectedProduct?.inventory >= q) {
            if (q > 0) {
                setQty(q);
            } else {
                setQty(1);
            }
        } else {
            snackbar(
                `Please select qty less then or equal ${selectedProduct?.inventory}`,
                {
                    variant: "error",
                }
            );
        }
    };

    // const getProductDetails = (slug) => {
    //     setLoading(true);
    //     ProductService.Commands.getProductDetails({ slug })
    //         .then((res) => {
    //             if (res.status === "success") {
    //                 setItem(res.data);
    //                 setVariantProducts(res.variant_product);
    //                 setProductImages(res.product_images);
    //                 setLoading(false);
    //             } else {
    //                 setLoading(false);
    //             }
    //         })
    //         .catch((error) => {
    //             console.log(error);
    //             setLoading(false);
    //         });
    // };

    function createMarkup(text) {
        return { __html: text };
    }

    // useEffect(() => {
    //     if (!isEmpty(slug)) {
    //         getProductDetails(slug);
    //     }
    // }, [slug]);

    useEffect(() => {
        if (!isEmpty(item)) {
            lastVisiteProduct(item);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [item]);

    const handleAddToCart = () => {
        console.log(selectedProduct?.inventory, qty);
        if (parseInt(qty) < 0) {
            snackbar(`Please select quantity minimum 1`, {
                variant: "error",
            });

            return false;
        }
        if (selectedProduct?.inventory >= qty) {
            dispatch(
                addToCart({
                    ...selectedProduct,
                    quantity: Number(qty),
                    name: item?.name,
                    image: item?.featured_image,
                    slug: item?.slug,
                    unit_price: Number(
                        (selectedProduct?.sale_price * 100).toFixed(2)
                    ),
                    total_amount: Number(
                        (
                            Number(qty) *
                            selectedProduct?.sale_price *
                            100
                        ).toFixed(2)
                    ),
                    product_id: item?.id,
                    item_total: Number(
                        (Number(qty) * selectedProduct?.sale_price).toFixed(2)
                    ),
                    category_id: item?.category,
                })
            );
            setQty(1);

            setCartModalOpen(true);
        } else {
            snackbar(
                `Please select qty less then or equal ${selectedProduct?.inventory}`,
                {
                    variant: "error",
                }
            );
        }
    };

    useEffect(() => {
        if (
            Number(terms?.length) === Number(item?.productattributeArr?.length)
        ) {
            //   console.log("Equal");
            const matchedProducts = variantProducts?.find((product) => {
                // check if the product has the same number of attributes as the selected options
                if (product.producttermsArr.length !== terms.length) {
                    return false;
                }
                // check if all selected options exist in the product
                return terms.every((option) => {
                    return product.producttermsArr.some((productOption) => {
                        return (
                            productOption.attributeId === option.attributeId &&
                            productOption.termsId === option.termsId
                        );
                    });
                });
            });
            setSelectedProduct(matchedProducts);
        } else {
            setSelectedProduct(null);
        }
    }, [terms]);

    console.log("Product", item);
    return (
        <Box>
            <Head>
                <title>{item?.name} - Top Brand Outlet UK</title>
                <meta name="description" content={item?.description} />
                <meta property="og:title" content={item?.name} />
                <meta property="og:description" content={item?.name} />
                <meta
                    property="og:image"
                    content={
                        IMAGE_URL +
                        "product-fetured-image/" +
                        item?.featured_image
                    }
                />
            </Head>

            {loading ? (
                <Loader />
            ) : (
                <Container className={classes.root} maxWidth="xl">
                    {item && (
                        <Grid container>
                            <Grid item md={9} sm={7} xs={12}>
                                <Box
                                    className="image"
                                    sx={{
                                        padding: "20px",
                                        relative: "relative",
                                        margin: "0 auto",
                                        width: { md: 700, sm: 500, xs: 300 },
                                    }}
                                >
                                    {productImages?.length > 0 ? (
                                        //   <ImageSlider images={productImages} brand={item?.brandObj} />
                                        // <ImageSlickSlider images={productImages} />
                                        // <LinkedListSlider
                                        //     images={productImages}
                                        // />
                                        <ImageSliderWithLeftThumbnail
                                            images={productImages}
                                            brand={item?.brandObj}
                                        />
                                    ) : (
                                        <Sliders settings={slideSetting}>
                                            <Box
                                                component={"img"}
                                                src={
                                                    IMAGE_URL +
                                                    "product-fetured-image/" +
                                                    item?.featured_image
                                                }
                                            />
                                        </Sliders>
                                    )}
                                </Box>
                                {item?.groupProductArr?.length > 0 && (
                                    <Box className="color">
                                        <Box className="color-container">
                                            <Box className="color-content">
                                                <Typography>
                                                    {Number(
                                                        item?.groupProductArr
                                                            ?.length
                                                    )}{" "}
                                                    {item?.groupProductArr
                                                        ?.length > 1
                                                        ? "colors"
                                                        : "color"}{" "}
                                                    are available
                                                </Typography>
                                                <Box className="img-list" sx={{ flexWrap: "wrap" }}>
                                                    {item?.groupProductArr.filter(i => i.id != item?.id).map(
                                                        (image, index) => (
                                                            <Box
                                                                component={"a"}
                                                                href={`/product/${image?.slug}`}
                                                                key={index}
                                                            >
                                                                <Box
                                                                    component={
                                                                        "img"
                                                                    }
                                                                    src={
                                                                        IMAGE_URL +
                                                                        "product-fetured-image/" +
                                                                        image?.featured_image
                                                                    }
                                                                />
                                                            </Box>
                                                        )
                                                    )}
                                                </Box>
                                            </Box>
                                        </Box>
                                    </Box>
                                )}
                                <Box>
                                <Grid container>
                                    <Grid item md={12}>
                                        <Box sx={{ width: "100%" }}>
                                            <Box
                                                sx={{
                                                    borderBottom: 1,
                                                    borderColor: "divider",
                                                }}
                                            >
                                                <Tabs
                                                    value={value}
                                                    onChange={handleChange}
                                                    aria-label="basic tabs example"
                                                    sx={{
                                                        flexWrap: {
                                                            md: "nowrap",
                                                            xs: "wrap",
                                                        },
                                                    }}
                                                >
                                                    <Tab
                                                        iconPosition="start"
                                                        label="Description"
                                                        {...a11yProps(0)}
                                                    />
                                                    <Tab
                                                        iconPosition="start"
                                                        label="Size & Specs"
                                                        {...a11yProps(1)}
                                                    />
                                                    <Tab
                                                        iconPosition="start"
                                                        label="Delivery"
                                                        {...a11yProps(2)}
                                                    />
                                                    <Tab
                                                        iconPosition="start"
                                                        label="Returns"
                                                        {...a11yProps(3)}
                                                    />
                                                    <Tab
                                                        iconPosition="start"
                                                        label="Reviews"
                                                        {...a11yProps(4)}
                                                    />
                                                </Tabs>
                                            </Box>
                                            <SwipeableViews
                                                axis={
                                                    theme.direction === "rtl"
                                                        ? "x-reverse"
                                                        : "x"
                                                }
                                                index={value}
                                                onChangeIndex={
                                                    handleChangeIndex
                                                }
                                            >
                                                <TabPanel
                                                    value={value}
                                                    index={0}
                                                >
                                                    <Box
                                                        dangerouslySetInnerHTML={createMarkup(
                                                            item?.description
                                                        )}
                                                    />
                                                </TabPanel>
                                                <TabPanel
                                                    value={value}
                                                    index={1}
                                                >
                                                    <h2>Size & Specs</h2>
                                                </TabPanel>
                                                <TabPanel
                                                    value={value}
                                                    index={2}
                                                >
                                                    <h2>Delivery</h2>
                                                </TabPanel>
                                                <TabPanel
                                                    value={value}
                                                    index={3}
                                                >
                                                    <h2>Returns</h2>
                                                </TabPanel>
                                                <TabPanel
                                                    value={value}
                                                    index={4}
                                                >
                                                    <h2>Reviews</h2>
                                                </TabPanel>
                                            </SwipeableViews>
                                        </Box>
                                    </Grid>
                                </Grid>
                                </Box>
                                
                                <Box>
                                <Container>
                                    {lastVisitedProducts?.length > 1 && (
                                        <Grid container spacing={2} className="recent-view">
                                            <Box sx={{ width: "100%" }}>
                                                <Box className="product-slider">
                                                    <Typography component={"h2"}>
                                                        Recently viewed items
                                                    </Typography>
                                                    <Divider />
                                                    <Box sx={{ width: "100%" }}>
                                                        <Sliders settings={productSetting}>
                                                            {lastVisitedProducts
                                                                ?.map((data, index) => (
                                                                    <Box key={index}>
                                                                        <SlideProductCard item={data} />
                                                                    </Box>
                                                                ))}
                                                        </Sliders>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </Grid>
                                    )}
                                </Container>
                                </Box>
                                                            
                            </Grid>
                            <Grid
                                item
                                md={3}
                                sm={5}
                                xs={12}
                                sx={{
                                    borderLeft: {
                                        md: "1px solid #0000002e",
                                        xs: "none",
                                    },
                                }}
                            >
                                <Box className="content">
                                    <Typography component="h2">
                                        {camelCaseToText(item?.name)}
                                    </Typography>
                                    <Box
                                        sx={{
                                            display: "flex",
                                            gap: 1,
                                            alignItems: "center",
                                        }}
                                    >
                                        <Box className="review">
                                            <StarIcon />
                                            <StarIcon />
                                            <StarIcon />
                                            <StarIcon />
                                            <StarIcon />
                                            <Typography>Review(1)</Typography>
                                        </Box>
                                        <Divider
                                            orientation="vertical"
                                            flexItem
                                        />
                                        <Box className="share-box">
                                            <Typography>Share</Typography>
                                            <Box className="shareIcon">
                                                <FacebookOutlinedIcon />
                                                <TwitterIcon />
                                                <InstagramIcon />
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box
                                        sx={{
                                            display: "flex",
                                            alignItems: "center",
                                            gap: 1,
                                            marginBottom: 2,
                                        }}
                                    >
                                        <Typography className="regular_price">
                                            £{item?.regular_price}
                                        </Typography>
                                        <Typography className="sale_price">
                                            £{item?.sale_price}
                                        </Typography>
                                    </Box>
                                    <Typography>
                                        <strong>Availability: </strong>
                                        {item?.outofstock
                                            ? "Stock out"
                                            : "In Stock"}
                                    </Typography>
                                    <Typography>
                                        <strong>SKU: </strong>
                                        {selectedProduct?.sku ?? item?.sku}
                                    </Typography>
                                    <Box sx={{ minHeight: 42 }}>
                                        <Typography>
                                            <strong style={{ color: "green" }}>
                                                {selectedProduct?.sale_price
                                                    ? `£${selectedProduct?.sale_price}`
                                                    : ""}
                                            </strong>
                                        </Typography>
                                        <Typography>
                                            <strong style={{ color: "green" }}>
                                                {selectedProduct?.inventory
                                                    ? `Qty:${selectedProduct?.inventory}`
                                                    : ""}
                                            </strong>
                                        </Typography>
                                    </Box>
                                    <Box className="variant">
                                        {item?.productattributeArr?.length >
                                            0 &&
                                            item?.productattributeArr?.map(
                                                (data, index) => (
                                                    <Box
                                                        sx={{
                                                            display: "flex",
                                                            flexDirection:
                                                                "column",
                                                        }}
                                                        key={index}
                                                    >
                                                        <label>
                                                            {capitalize(
                                                                data?.name
                                                            )}
                                                        </label>
                                                        <select
                                                            onChange={selectTerms(
                                                                data?.id
                                                            )}
                                                            style={{
                                                                textTransform:
                                                                    "uppercase",
                                                            }}
                                                        >
                                                            <option value="0">
                                                                Select{" "}
                                                                {data?.name}
                                                            </option>
                                                            {item.producttermsArr
                                                                .filter(
                                                                    (i) =>
                                                                        i.attributeId ===
                                                                        data.id
                                                                )
                                                                .map(
                                                                    (
                                                                        data,
                                                                        index
                                                                    ) => (
                                                                        <option
                                                                            value={
                                                                                data?.termsId
                                                                            }
                                                                            key={
                                                                                index
                                                                            }
                                                                        >
                                                                            {capitalize(
                                                                                data?.termsName
                                                                            )}
                                                                        </option>
                                                                    )
                                                                )}
                                                        </select>
                                                    </Box>
                                                )
                                            )}
                                        <Box className="quantity">
                                            <label>QTY</label>
                                            <input
                                                className="qty"
                                                type="number"
                                                onChange={onChangeQty}
                                                value={qty}
                                                min="1"
                                                onWheel={(e) => e.target.blur()}
                                                disabled={isEmpty(
                                                    selectedProduct
                                                )}
                                            />
                                        </Box>
                                    </Box>
                                    <Box>
                                        {typeof selectedProduct ===
                                            "undefined" ||
                                            (selectedProduct?.inventory ===
                                                0 && (
                                                <Typography
                                                    sx={{ color: "red" }}
                                                >
                                                    Out of stock!
                                                </Typography>
                                            ))}
                                        <CustomButton
                                            text={"Add To Bag"}
                                            disabled={
                                                Number(
                                                    item?.productattributeArr
                                                        .length
                                                ) !== Number(terms.length) ||
                                                typeof selectedProduct ===
                                                    "undefined" ||
                                                selectedProduct?.inventory === 0
                                            }
                                            onClick={handleAddToCart}
                                        />
                                        <Button
                                            sx={{
                                                background: "#CDCDCD",
                                                padding: "10px",
                                                color: "#000",
                                                fontWeight: 600,
                                                marginLeft: 2,
                                                textTransform: "capitalize",
                                            }}
                                        >
                                            Add To Wish
                                        </Button>
                                    </Box>
                                </Box>
                            </Grid>
                        </Grid>
                    )}
                    <AfterAddToCartModal
                        open={cartModalOpen}
                        handleClose={() => setCartModalOpen(false)}
                        item={item}
                        addedItem={selectedProduct}
                    />
                </Container>
            )}
        </Box>
    );
}

export default withPublicPage(Product);

export async function getServerSideProps(context) {
    const { params } = context;
    const { slug } = params;
    const response = await ProductService.Commands.getProductDetails({ slug });

    console.log(response, slug);
    return {
        props: {
            item: response?.data,
            variantProducts: response?.variant_product,
            productImages: response?.product_images,
        },
    };
}
