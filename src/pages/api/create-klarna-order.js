import { KLARNA_PASSWORD, KLARNA_USERNAME } from 'helpers/apiUrl';
import fetch from 'node-fetch';

export default async function handler(req, res) {
  const { authorization_token, customer, cartTotal, cartItems, uu_id } = req.body;
  const username = KLARNA_USERNAME;
  const password = KLARNA_PASSWORD;
  const authToken = Buffer.from(`${username}:${password}`).toString('base64');
  
  console.log(authorization_token);

  try {
    const response = await fetch(`https://api.playground.klarna.com/payments/v1/authorizations/${authorization_token}/order`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${authToken}`,
        'X-Klarna-Reference': uu_id,
      },
      body: JSON.stringify({
            purchase_country: "GB",
            purchase_currency: "GBP",
            locale: "en-UK",
            billing_address: {
              given_name: customer?.first_name ?? "John",
              family_name: customer?.last_name ??"Doe",
              email: customer?.email ?? "john@doe.com",
              title: "Mr",
              street_address: customer?.street_address ?? "Lombard St 10",
              street_address2: customer?.street_address2 ?? "Apt 214",
              postal_code: customer?.post_code ?? "90210",
              city: customer?.city ?? "Beverly Hills",
              region: "CA",
              phone: customer?.phone ?? "07700 900508",
              country: customer?.country ?? "GB"
            },
            
            order_amount: cartTotal,
            order_tax_amount: 0,
            order_lines: cartItems,
            customer: {
                date_of_birth: "1970-01-01",
            },
      }),
    });

    const data = await response.json();

    res.status(200).json({
        data
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: `An error occurred creating the Klarna session ${error}` });
  }
}