const stripe = require("stripe")("sk_test_UsLRveHKteIC5ygTJkvsDV7B");

export default async function createPaymentIntent(
    req,
    res
) {
    let { amount, id } = req.body;

    console.log(amount);

    try {
        const payment = await stripe.paymentIntents.create({
            amount: Number(amount),
            currency: "gbp",
            description: "Top Brand Outlet",
            payment_method: id,
            confirm: true,
        });

        console.log("Payment", payment);

        res.json({
            message: "Payment successful",
            success: true,
            data: payment
        });
    } catch (error) {
        console.log(error);
        res.json({
            message: "Payment failed",
            success: false,
        });
    }
}
