import { KLARNA_PASSWORD, KLARNA_PLAYGROUND_URL, KLARNA_USERNAME } from 'helpers/apiUrl';
import fetch from 'node-fetch';

export default async function handler(req, res) {
  const { order_amount, order_tax_amount, order_lines, uu_id } = req.body;
  const username = KLARNA_USERNAME;
  const password = KLARNA_PASSWORD;
  const authToken = Buffer.from(`${username}:${password}`).toString('base64');
  
  

  try {
    const response = await fetch(KLARNA_PLAYGROUND_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${authToken}`,
        'X-Klarna-Reference': uu_id,
      },
      body: JSON.stringify({
        purchase_country: 'GB',
        purchase_currency: 'GBP',
        intent: "buy",
        locale: "en-US",
        order_amount,
        order_tax_amount,
        order_lines,
      }),
    });

    const data = await response.json();

    res.status(200).json({
        data
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: `An error occurred creating the Klarna session ${authToken}` });
  }
}