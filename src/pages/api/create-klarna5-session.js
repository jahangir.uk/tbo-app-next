const stripe = require("stripe")("sk_test_UsLRveHKteIC5ygTJkvsDV7B");
// import { NextApiRequest, NextApiResponse } from "next";

export default async function createKlarnaSession(
    req,
    res
) {
    let { amount } = req.body;
    const session = await stripe.checkout.sessions.create({
        payment_method_types: ["card", "klarna"],
        line_items: [
            {
                price_data: {
                    // To accept `klarna`, all line items must have currency: eur, dkk, gbp, nok, sek, usd, czk, aud, nzd, cad, pln, chf
                    currency: "gbp",
                    product_data: {
                        name: "T-shirt",
                    },
                    unit_amount: amount,
                },
                quantity: 1,
            },
        ],
        mode: "payment",
        success_url: "https://localhost:3000/success",
        cancel_url: "https://localhost:3000/cancel",
    });

    console.log(session);
    res.json({
        message: "Payment successful",
        success: true,
        data: session,
    });
}
