import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import PersonIcon from "@mui/icons-material/Person";
import PublicIcon from "@mui/icons-material/Public";
import ReplyIcon from "@mui/icons-material/Reply";
import SendIcon from "@mui/icons-material/Send";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Box, Container, Tab, Tabs, useTheme } from "@mui/material";
import { useCustomerCareStyle } from "assets/stylesheets/customerCare/useCustomerCareStylesheet";
import { Accordions } from "components/Accordion";
import { withPublicPage } from "components/HOC/PublicPage";
import { HeroBanner } from "components/HeroBanner";
import { Faqs } from "components/dummy-data/FAQ";
import { useState } from "react";
import SwipeableViews from "react-swipeable-views";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

 function CustomerCare() {
    const classes = useCustomerCareStyle();
    const [value, setValue] = useState(0);
    const theme = useTheme();
    const faq_list = Faqs;
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };
    return (
        <Box >
            <HeroBanner title={"Customer Care"} />
            <Container className={classes.root}>
                <Box sx={{ width: "100%" }}>
                    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="basic tabs example"
                            centered
                            variant="fullWidth"
                        >
                            <Tab
                                icon={<LocalShippingIcon />}
                                iconPosition="start"
                                label="Delivery"
                                {...a11yProps(0)}
                            />
                            <Tab
                                icon={<ShoppingCartIcon />}
                                iconPosition="start"
                                label="Orders"
                                {...a11yProps(1)}
                            />
                            <Tab
                                icon={<ReplyIcon />}
                                iconPosition="start"
                                label="Returns"
                                {...a11yProps(2)}
                            />
                            <Tab
                                icon={<PersonIcon />}
                                iconPosition="start"
                                label="My Account"
                                {...a11yProps(3)}
                            />
                            <Tab
                                icon={<PublicIcon />}
                                iconPosition="start"
                                label="Website"
                                {...a11yProps(4)}
                            />
                            <Tab
                                icon={<SendIcon />}
                                iconPosition="start"
                                label="Contact Us"
                                {...a11yProps(5)}
                            />
                        </Tabs>
                    </Box>
                    <SwipeableViews
                        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                        index={value}
                        onChangeIndex={handleChangeIndex}
                    >
                        <TabPanel value={value} index={0}>
                            <Accordions data={faq_list.delivery} />
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <Accordions data={faq_list.orders} />
                        </TabPanel>
                        <TabPanel value={value} index={2}>
                            <Accordions data={faq_list.returns} />
                        </TabPanel>
                        <TabPanel value={value} index={3}>
                            <Accordions data={faq_list?.myAccounts} />
                        </TabPanel>
                        <TabPanel value={value} index={4}>
                            <Accordions data={faq_list?.website} />
                        </TabPanel>
                        <TabPanel value={value} index={5}>
                            <Accordions data={faq_list?.contactUs} />
                        </TabPanel>
                    </SwipeableViews>
                </Box>
            </Container>
        </Box>
    );
}

export default withPublicPage(CustomerCare);
