/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/accessible-emoji */
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { Box, Container, Divider, Grid, Typography } from '@mui/material';
import kidC from "assets/images/girl-fashion-1.jpg";
import jeansImg from "assets/images/jeans.jpg";
import mensC from "assets/images/mens-fashion.jpg";
import newC from "assets/images/new-arraival.jpg";
import womenC from "assets/images/womens-accessories.jpg";
import { useHomePageStyle } from "assets/stylesheets/home-page/homePageStylesheet";
import { withPublicPage } from 'components/HOC/PublicPage';
import { HeroBanner } from "components/HeroBanner";
import { BrandSectionLoader } from "components/Loader/brand-section";
import { HomeSliderLoader } from "components/Loader/home-slider";
import { CardLoader } from "components/Loader/product-card";
import { ProductVariation } from "components/Modals/ProductVariation";
import { ProductCard } from 'components/ProductCard';
import Sliders from 'components/Slider';
import { brands } from 'components/dummy-data/Brand';
import { sliders } from 'components/dummy-data/homeSliders';
import { IMAGE_URL, LAST_VISITED_PRODUCTS } from "helpers/config";
import { Inter } from 'next/font/google';
import Head from "next/head";
import Image from 'next/image';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchShopPageProduct } from "redux/action/productAction";
import HomePageService from "services/HomePageService";
import ProductService from "services/ProductService";
import { appStorage } from "utils/storage";

const inter = Inter({ subsets: ['latin'] })

function Home() {
    const dispatch = useDispatch();
    const slide_data = sliders;
    const brand_slider = brands;
    const classes = useHomePageStyle();
    const { items } = useSelector((state) => state.product);
    const [homeData, setHomeData] = useState({});
    const [open, setOpen] = useState(false);
    const [data, setData] = useState(null);
    const [brandLoader, setBrandLoader] = useState(true);
    const [menItems, setMenItems] = useState([]);
    const [womenItems, setWomenItems] = useState([]);
    const lastVisitedProducts = JSON.parse(
        appStorage.get(LAST_VISITED_PRODUCTS)
    );
    
    const brandSlideSetting = {
        slidesToShow: 7,
        infinite: true ,
        autoplay: false,
        slidesToScroll: 7,
        centerMode: false,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                  slidesToShow: 6,
                  slidesToScroll: 6,
                  infinite: true
                }
              },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 5,
              infinite: true
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              initialSlide: 2
            }
          },
          {
            breakpoint: 850,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              initialSlide: 2
            }
          },
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              initialSlide: 2
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              initialSlide: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              dots: false,
              arrows: true,
            }
          },
          {
            breakpoint: 434,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              dots: false,
              arrows: true,
              centerMode: false,
            }
          },
          {
            breakpoint: 411,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              centerMode: false,
              dots: false,
              arrows: true,
            }
          },
          {
            breakpoint: 395,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              centerMode: false,
              dots: false,
              arrows: true,
            }
          }
        ]
      };

      
    const homeSlideSetting = {
        slidesToShow: 1,
        infinite: true,
        centerMode: false,
        arrows: false,
        autoplay: true,
      };


      const getHomePageData = () => {
        setBrandLoader(true);
        HomePageService.Commands.getHomePageData().then((res) => {
          // console.log("Home Page Data", res);
          if (res.status === "success") {
            setHomeData({
              brands: res?.brand,
              categories: res?.category,
              sub_categories: res?.sub_category,
              sub_sub_categories: res?.sub_sub_category,
              home_sliders: res?.home_slider
            });
            setBrandLoader(false);
          }
        });
      };

    const mensClothing = () => {
        ProductService.Commands.getProductByCategory({slug: "men", pageSize: 4}).then((res) => {
            setMenItems(res?.data)
        });
    }  
    const womenClothing = () => {
        ProductService.Commands.getProductByCategory({slug: "women", pageSize: 4}).then((res) => {
            setWomenItems(res?.data)
        });
    }
    
      useEffect(() => {
        dispatch(fetchShopPageProduct());
        getHomePageData();
      }, [dispatch]);

      useEffect(() => {
        mensClothing();
        womenClothing();
      }, []);
    //   console.log('Rendard');

  return (
      <Box className={classes.root}>
        <Head>
            <title>Top Brand Outlet UK - Cheap Branded Clothes up to 80% Off</title>
        </Head>
        {brandLoader ? (
           <Box sx={{ minHeight: '100vh' }}>
                <HomeSliderLoader />
                <BrandSectionLoader />
           </Box>
        ) : (
            <>
                <Sliders settings={homeSlideSetting}>
                {homeData?.home_sliders?.length > 0 &&
                    homeData?.home_sliders.map((item, index) => (
                        <Box
                            key={index}
                            sx={{
                            background: `url(${
                            IMAGE_URL + 'image-gallery/' +  item?.image
                            })`,
                            height: { md: 550, sm: 300, xs: 200 },
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                            position: "relative"
                            }}
                        >
                        
                        </Box>
                    ))}
                </Sliders>
             <Container maxWidth="xl">
           
             <Grid container className="brand" spacing={2}>
                 <Box className="title">
                     <Typography component={"h2"}>
                         Featured <strong>Brands</strong>
                     </Typography>
                     <Divider />
                 </Box>
 
                 <Box sx={{ width:{md: "100%", xs: "80%"}, margin: "0 auto" }}>
                    <Sliders settings={brandSlideSetting}>
                    {homeData?.brands?.length > 0 &&
                        homeData?.brands?.map((item, index) => (
                        <Box component={"a"} href={`/${item?.slug}`} key={index}>
                            {/* <Box
                                sx={{ width: 150 }}
                                component={'img'}
                                src={IMAGE_URL + 'brand-image/' + item?.image}
                                alt={item?.title}
                            /> */}
                            <Image src={IMAGE_URL + "brand-image/" + item?.image} alt={item?.title} width={150} height={100} />
                        </Box>
                        ))}
                    </Sliders>
                 </Box>
             </Grid>
             

           <Box className="top-category">
             <Typography component="h2">
               UK's Cheap Branded Clothes Collection With Online Brand Outlet
             </Typography>
             <Divider />
 
             <Grid container spacing={2} mt={2}>
               <Grid item md={3} sm={6} xs={12}>
                 <Box className="image-box">
                   <Image src={mensC} srcSet={mensC} alt={"Test"} loading="lazy"  />
                   <Box className="image-content">
                     <Typography>Men's Collection</Typography>
                     <Box component="a" href="/shop/men">
                         Shop Now
                       </Box>
                   </Box>
                 </Box>
               </Grid>
               <Grid item md={3} sm={6} xs={12}>
               <Box className="image-box">
                   <Image src={womenC} srcSet={womenC} alt={"Test"} loading="lazy"  />
                   <Box className="image-content">
                     <Typography>Women's Accessories</Typography>
                     <Box component="a" href="/shop/women">
                         Shop Now
                       </Box>
                   </Box>
                 </Box>
               </Grid>
               <Grid item md={3} sm={6} xs={12}>
               <Box className="image-box">
                   <Image src={kidC} srcSet={kidC} alt={"Test"} loading="lazy"  />
                   <Box className="image-content">
                     <Typography>Kid's Collection</Typography>
                     <Box component="a" href="/shop/boys">
                         Shop Now
                       </Box>
                   </Box>
                 </Box>
               </Grid>
               <Grid item md={3} sm={6} xs={12}>
               <Box className="image-box">
                   <Image src={newC} srcSet={newC} alt={"Test"} loading="lazy"  />
                   <Box className="image-content">
                     <Typography>New Arrival</Typography>
                     <Box component="a" href="/shop/new-arrival">
                         Shop Now
                       </Box>
                   </Box>
                 </Box>
               </Grid>
             </Grid>
           </Box>
           <Box className="product-section">
             <Typography component="h2">Men's Clothing</Typography>
             <Divider />
             {brandLoader ? (
                 <Box sx={{ padding: '30px 0' }}><CardLoader width={"100%"} /></Box>
             ) : (
                 <Grid container mt={3}>
                 {menItems?.length > 0 &&
                   menItems?.map((item, index) => (
                     <Grid
                       key={index}
                       item
                       md={3}
                       sm={4}
                       xs={6}
                       sx={{ marginTop: 1 }}
                     >
                       <ProductCard
                         item={item}
                         setData={setData}
                         open={open}
                         setOpen={setOpen}
                         size={346}
                       />
                     </Grid>
                   ))}
               </Grid>
             )}
            
             <Box className="all-product-btn">
               <Box component={"a"} href="/shop/men">
                 All Products <ArrowForwardIcon />
               </Box>
             </Box>
           </Box>
           <Box sx={{ padding: "70px 0" }}>
             <HeroBanner image={jeansImg} title={"JEANS COLLECTION FOR HER"} />
           </Box>
           <Box className="product-section">
             <Typography component="h2">Women's Clothing</Typography>
             <Divider />
             <Grid container mt={3}>
               {womenItems?.length > 0 &&
                 womenItems?.map((item, index) => (
                   <Grid
                     key={index}
                     item
                     md={3}
                     sm={4}
                     xs={6}
                     sx={{ marginTop: 1 }}
                   >
                     <ProductCard
                       item={item}
                       setData={setData}
                       open={open}
                       setOpen={setOpen}
                       size={346}
                     />
                   </Grid>
                 ))}
             </Grid>
             <Box className="all-product-btn">
               <Box component={"a"} href="/shop/women">
                 All Products <ArrowForwardIcon />
               </Box>
             </Box>
           </Box>
           <Box sx={{ padding: "70px 0" }}>
             <HeroBanner image={jeansImg} title={"JEANS COLLECTION FOR HER"} />
           </Box>
         </Container>
            </>
           
        )}
        
       
        <ProductVariation
        open={open}
        handleClose={() => {
          setOpen(false);
          setData(null);
        }}
        item={data}
      />
      </Box>
      
  );
}

export default withPublicPage(Home);
