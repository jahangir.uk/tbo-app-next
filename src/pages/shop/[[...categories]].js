import CloseIcon from "@mui/icons-material/Close";
import TuneIcon from "@mui/icons-material/Tune";
import {
    Box,
    Button,
    Container,
    Drawer,
    Grid,
    IconButton,
    Typography,
} from "@mui/material";
import { COLORS } from "assets/styles/colors/colors";
import { withPublicPage } from "components/HOC/PublicPage";
import { CardLoader } from "components/Loader/product-card";
import { ProductVariation } from "components/Modals/ProductVariation";
import { ProductCard } from "components/ProductCard";
import { ProductTopBanner } from "components/ProductTopBanner";
import { bannerData } from "components/dummy-data/productTopBanner";
import { ProductFilter } from "components/productFilter";
import { capitalize, isEmpty } from "helpers/functions";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFilterData } from "redux/action/filterDataAction";
import { getFilterProductList } from "redux/action/productAction";

function CategoryPage() {
    const router = useRouter();
    const dispatch = useDispatch();
    const { categories = [] } = router?.query;
    const length = categories?.length;
    const { brand, gender, product_type, tag_group } = useSelector(
        (state) => state?.filterData
    );
    const [catDetails, setCatDetails] = useState("");
    const {items, category_details, sub_category_details, sub_sub_category_details} = useSelector((state) => state.product);
    const [open, setOpen] = useState(false);
    const [data, setData] = useState(null);
    const div = useRef();
    const [categoryData, setCategoryData] = useState({});
    const [anchor, setAnchor] = useState(false);
    const [sortBy, setSortBy] = useState("most-popular");
    const [loading, setLoading] = useState(false);
    const toggleDrawer = (open) => {
        setAnchor(open);
    };

    const [searchItems, setSearchItems] = useState({
        items: [],
    });
    const [brands, setBrands] = useState([]);
    const [genders, setGenders] = useState([]);
    const [productType, setProductType] = useState([]);
    const [tagGroup, setTagGroup] = useState([]);

    const handleCheckItem = (e, value) => {
        const { checked } = e.target;
        const { items } = searchItems;
        // console.log(`${value} is ${checked}`);
        console.log(value);
        // Case 1 : The user checks the box
        if (checked) {
            setSearchItems({
                items: [...items, value],
            });

            if (value?.type === "brand") {
                const brandIndex = brands.find((data) => data === value?.id);
                if (!brandIndex) {
                    setBrands([...brands, value?.id]);
                }
            } else if (value?.type === "gender") {
                const genderIndex = genders.find((data) => data === value?.id);
                if (!genderIndex) {
                    setGenders([...genders, value?.id]);
                }
            } else if (value?.type === "productType") {
                const productTypeIndex = productType.find(
                    (data) => data === value?.id
                );
                if (!productTypeIndex) {
                    setProductType([...productType, value?.id]);
                }
            } else if (value?.type === "tagGroup") {
                const tagIndex = tagGroup.find((data) => data === value?.id);
                if (!tagIndex) {
                    setTagGroup([...tagGroup, value?.id]);
                }
            }
        }

        // Case 2  : The user unchecks the box
        else {
            setSearchItems({
                items: items.filter((e) => e.id !== value?.id),
            });

            if (value?.type === "brand") {
                setBrands((prev) => prev?.filter((i) => i !== value?.id));
            } else if (value?.type === "gender") {
                setGenders((prev) => prev?.filter((i) => i !== value?.id));
            } else if (value?.type === "productType") {
                setProductType((prev) => prev?.filter((i) => i !== value?.id));
            } else if (value?.type === "tagGroup") {
                setTagGroup((prev) => prev?.filter((i) => i !== value?.id));
            }
        }
    };

    const handleProductFilter = () => {
        setLoading(true);
        const payload = {
            brand: brands,
            gender: genders,
            product_type: productType,
            tag: tagGroup,
            category_slug: categories?.[0] ?? "",
            sub_category_slug: categories?.[1] ?? "",
            sub_sub_category_slug: categories?.[2] ?? "",
        };
        dispatch(getFilterProductList(payload));
        setTimeout(() => {
            setLoading(false);
        }, 300);
    };

    const handleRemoveItem = (value) => {
        const { items } = searchItems;
        setSearchItems({
            items: items.filter((e) => e.id !== value?.id),
        });

        if (value?.type === "brand") {
            setBrands((prev) => prev?.filter((i) => i !== value?.id));
        } else if (value?.type === "gender") {
            setGenders((prev) => prev?.filter((i) => i !== value?.id));
        } else if (value?.type === "productType") {
            setProductType((prev) => prev?.filter((i) => i !== value?.id));
        } else if (value?.type === "tagGroup") {
            setTagGroup((prev) => prev?.filter((i) => i !== value?.id));
        }
    };

    useLayoutEffect(() => {
        const divAnimate = div?.current?.getBoundingClientRect().top;
        // console.log(divAnimate);
        const onScroll = () => {
            if (divAnimate < window.scrollY) {
                div.current.style.position = "sticky";
                div.current.style.background = "#ffffff";
                div.current.style.zIndex = "1";
                if (window.matchMedia("(max-width: 426px)").matches) {
                    div.current.style.position = "relative";
                }

                div.current.style.top = "0px";
            } else {
                div.current.style.position = "relative";
            }
        };
        window.addEventListener("scroll", onScroll);
        return () => window.removeEventListener("scroll", onScroll);
    }, []);

    useEffect(() => {
        setCategoryData(bannerData);
    }, []);

    useEffect(() => {
        handleProductFilter();
    }, [brands, genders, productType, tagGroup]);

    useEffect(() => {
        dispatch(getFilterData());
    }, []);

    useEffect(() => {
        if (!isEmpty(categories)) {
            handleProductFilter();
        }
    }, [categories]);

    // console.log(brands, genders);
    
    useEffect(() => {
        if(!isEmpty(sub_sub_category_details)){
            setCatDetails(sub_sub_category_details)
        } else if(!isEmpty(sub_category_details)){
            setCatDetails(sub_category_details)
        } else{
            setCatDetails(category_details)
        }
    },[sub_sub_category_details, sub_category_details, category_details]);

    console.log(sub_sub_category_details);

    return (
        <Box>
            <Head>
                <title>
                    {isEmpty(categories)
                        ? "Shop"
                        : capitalize(
                              categories[length - 1]?.replaceAll("-", " ")
                          )}{" "}
                    - Top Brand Outlet UK
                </title>
            </Head>
            <Container sx={{ padding: "40px 0" }}>
                <Grid container spacing={2} sx={{ display: "flex" }}>
                    <Grid item md={12} xs={12}>
                        {length > 0 && (
                            <Box>
                                <ProductTopBanner data={catDetails} />
                            </Box>
                        )}

                        <Box
                            ref={div}
                            sx={{
                                padding: "20px 0",
                                display: "flex",
                                justifyContent: "space-between",
                                alignItems: "center",
                            }}
                        >
                            <Box>
                                <Button
                                    onClick={() => toggleDrawer(true)}
                                    sx={{
                                        // display: { md: "none", xs: "block" },
                                        background: COLORS.primary,
                                        color: COLORS.white,
                                        marginRight: 1,
                                        border: "none",
                                        borderRadius: "3px",
                                        display: "flex",
                                        gap: 1,
                                        alignItems: "center",
                                        "&:hover": {
                                            background: COLORS.primary,
                                            color: COLORS.white,
                                        },
                                    }}
                                >
                                    Filter & Sort <TuneIcon />
                                </Button>
                                {/* <Pagination
                    sx={{ display: { md: "block", xs: "none" } }}
                    siblingCount={0}
                    variant="text"
                    count={99}
                    size="small"
                    color="secondary"
                  /> */}
                            </Box>
                            <Typography
                                sx={{
                                    color: "#363636",
                                    fontWeight: 600,
                                    fontSize: 12,
                                }}
                            >
                                {items?.filter(i=> i.product_type === 1 || i.product_type === 2).length} Styles
                            </Typography>
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: 2,
                                    fontSize: 12,
                                    fontWeight: 600,
                                    padding: 1,
                                }}
                            >
                                <Box
                                    sx={{
                                        display: { md: "block", xs: "none" },
                                    }}
                                >
                                    Sort by
                                </Box>
                                <select
                                    name=""
                                    id=""
                                    style={{
                                        height: 32,
                                        border: "1px solid #dbd7d7",
                                    }}
                                    value={"lowest-price"}
                                >
                                    <option selected value="most-popular">
                                        Most Popular
                                    </option>
                                    <option value="lowest-price">
                                        Lowest Price
                                    </option>
                                    <option value="highest-price">
                                        Highest Price
                                    </option>
                                    <option value="new">{`What's New`}</option>
                                </select>
                            </Box>
                        </Box>

                        {loading ? (
                            <CardLoader />
                        ) : (
                            <>
                                {items?.length > 0 && (
                                    <Grid container>
                                        {items.map(
                                            (item, index) => (
                                                <>
                                                    {item?.product_type === 1 ||
                                                        (item?.product_type ===
                                                            2 && (
                                                            <Grid
                                                                key={index}
                                                                item
                                                                md={3}
                                                                sm={4}
                                                                xs={6}
                                                                sx={{
                                                                    marginTop: 1,
                                                                }}
                                                            >
                                                                <ProductCard
                                                                    item={item}
                                                                    setData={
                                                                        setData
                                                                    }
                                                                    open={open}
                                                                    setOpen={
                                                                        setOpen
                                                                    }
                                                                    size={262}
                                                                />
                                                            </Grid>
                                                        ))}
                                                </>
                                            )
                                        )}
                                    </Grid>
                                )}
                            </>
                        )}
                    </Grid>
                </Grid>
            </Container>
            <ProductVariation
                open={open}
                handleClose={() => {
                    setOpen(false);
                    setData(null);
                }}
                item={data}
            />
            <Drawer
                anchor={"left"}
                open={anchor}
                onClose={() => toggleDrawer(false)}
            >
                <IconButton
                    onClick={() => toggleDrawer(false)}
                    sx={{ position: "absolute", right: 0 }}
                >
                    <CloseIcon />
                </IconButton>
                <Box sx={{ padding: "10px 20px" }}>
                    <ProductFilter
                        width={"370px"}
                        brand={brand}
                        brands={brands}
                        gender={gender}
                        genders={genders}
                        product_type={product_type}
                        productTypes={productType}
                        tag_group={tag_group}
                        tagGroup={tagGroup}
                        searchItems={searchItems}
                        handleCheckItem={handleCheckItem}
                        handleRemoveItem={handleRemoveItem}
                    />
                </Box>
            </Drawer>
        </Box>
    );
}

export default withPublicPage(CategoryPage);

export async function getServerSideProps(context) {
    const { params, query } = context;

    console.log(query, params);
    return {
        props: {
            categories: params,
        },
    };
}
