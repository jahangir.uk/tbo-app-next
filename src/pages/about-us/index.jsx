
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import CheckroomIcon from "@mui/icons-material/Checkroom";
import GroupIcon from "@mui/icons-material/Group";
import StarIcon from "@mui/icons-material/Star";
import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import AboutImg from "assets/images/about-us.png";
import heroImg from "assets/images/back1.jpg";
import missionImg from "assets/images/mission-vision.png";
import { useAboutUsStyle } from "assets/stylesheets/about-us/aboutusStylesheet";
import { withPublicPage } from 'components/HOC/PublicPage';
import { HeroBanner } from "components/HeroBanner";
import { RightSideImageCard } from "components/RightSideImageCard";
import Sliders from "components/Slider";
import { brands } from "components/dummy-data/Brand";
import { LeftSideImageCard } from "components/leftSideImageCard";
import Image from 'next/image';
import CountUp from "react-countup";

 function AboutUs() {
    const classes = useAboutUsStyle();
    const brand_slider = brands;
  return (
    <Box>
        <HeroBanner title={"About us"} image={heroImg} />
        <Box className={classes.root}>
        <Container>
          <RightSideImageCard
            image={AboutImg?.src}
            title={"About the Company"}
            description={`Our buyers are always looking for opportunities to buy quality
              brands and on-trend Clothing, Footwear & Accessories. Over time
              we've built long-term relationships with many companies and
              currently stock over 150 brands, ranging from big household names
              to up-and-coming labels. Our buyers are always looking for
              opportunities to buy quality brands and on-trend Clothing,
              Footwear & Accessories. Over time we've built long-term
              relationships with many companies and currently stock over 150
              brands, ranging from big household names to up-and-coming labels.`}
          />
        </Container>
        <Container>
          <Grid container className="counter" spacing={2}>
            <Grid item md={12}>
              <Typography component={"h2"}>Our Achivement</Typography>
              <Divider />
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Box className="counter-card">
                <GroupIcon />
                <Typography>Total Customers</Typography>
                <CountUp
                  className="number"
                  start={0}
                  end={50}
                  suffix="K+"
                  duration={5}
                />
              </Box>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Box className="counter-card">
                <CheckroomIcon />
                <Typography>Total Products</Typography>
                <CountUp
                  className="number"
                  start={0}
                  end={30}
                  suffix="K+"
                  duration={5}
                />
              </Box>
            </Grid>

            <Grid item md={3} sm={6} xs={12}>
              <Box className="counter-card">
                <StarIcon />
                <Typography>Total Reviews</Typography>
                <CountUp
                  className="number"
                  start={0}
                  end={10}
                  suffix="K+"
                  duration={10}
                />
              </Box>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Box className="counter-card">
                <AddShoppingCartIcon />
                <Typography>Total Sales</Typography>
                <CountUp
                  className="number"
                  start={0}
                  end={50}
                  suffix="K+"
                  duration={5}
                />
              </Box>
            </Grid>
          </Grid>
        </Container>
        <Container sx={{ padding: "50px 0" }}>
          <LeftSideImageCard
            image={missionImg?.src}
            title={"Mission & Vision"}
            description={`Our mission is to empower individuals and businesses to thrive in the digital economy by providing them with a seamless and convenient online shopping experience. We strive to create a platform that offers a wide range of high-quality products at competitive prices, while providing exceptional customer service and fostering a sense of community among our users. Our vision is to be the leading eCommerce platform, providing our customers with an unparalleled shopping experience that is both convenient and personalized. We aim to leverage the latest technology and innovation to anticipate and exceed our customers' expectations, and to continually improve our operations to make online shopping faster, more secure, and more enjoyable for everyone. Ultimately, we hope to make eCommerce a positive force for economic growth and social change, enabling individuals and businesses of all sizes to thrive in the digital economy`}
          />
        </Container>
        <Container>
          <Grid container className="brand" spacing={2}>
            <Box className="title">
              <Typography component={"h2"}>
                Featured <strong>Brands</strong>
              </Typography>
              <Divider />
            </Box>

            <Sliders slidesToShow={7} infinite={true} centerPadding="10px">
              {brand_slider.length > 0 &&
                brand_slider.map((item, index) => (
                  <Box component={"a"} href={item?.link} key={index}>
                    <Image
                      src={item?.image}
                      alt={item?.title}
                    />
                  </Box>
                ))}
            </Sliders>
          </Grid>
        </Container>
      </Box>
    </Box>
  )
}

export default withPublicPage(AboutUs);
