import { Box, Container, List, ListItem, Typography } from "@mui/material";
import heroImg from "assets/images/back1.jpg";
import { useTermsAndConditionStyle } from "assets/stylesheets/terms&Condition/terms&ConditionStytlesheet";
import { withPublicPage } from "components/HOC/PublicPage";
import { HeroBanner } from "components/HeroBanner";
import Link from "next/link";

function TermsAndCondition() {
    const classes = useTermsAndConditionStyle();
    return (
        <Box>
            <HeroBanner title={"Terms and Condition"} image={heroImg} />
            <Container>
                <Box className={classes.root}>
                    <Typography>
                        We know it might seem like a lot of information, but
                        it’s important, so please make sure that you read and
                        understand these conditions before you agree to them.
                        This page, along with the documents referred to on it,
                        tells you about the legal terms and conditions on which
                        we sell any of the products listed on our website to
                        you. Any personal information that you provide on a
                        Notice of Non-Delivery, Denial of Receipt Letter, Notice
                        of Error or Cancellation Form will be subject to the
                        provisions described in our Privacy Policy, once we have
                        received those documents.
                    </Typography>
                    <Box>
                        <Typography component={"h2"}>1. THESE TERMS</Typography>
                        <List>
                            <ListItem>
                                <strong>1.1 What these terms cover.</strong>
                                These are the terms and conditions on which we
                                supply products to you.
                            </ListItem>
                            <ListItem>
                                <strong>1.2 Why you should read them.</strong>
                                Please read these terms carefully before you
                                submit your order to us. These terms tell you
                                who we are, how we will provide products to you,
                                how you and we may change or end the contract,
                                what to do if there is a problem and other
                                important information. If you think that there
                                is a mistake in these terms, please contact us
                                to discuss.
                            </ListItem>
                        </List>
                    </Box>

                    <Box>
                        <Typography component={"h2"}>
                            2. INFORMATION ABOUT US AND HOW TO CONTACT US
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>2.1Who we are. </strong>We are Top brand
                                Outlet Ltd, a company registered in England and
                                Wales. Our company registration number is
                                11761282 and our registered office is at 35
                                Knockhall Road, Greenhithe, Kent, DA9 9EZ. Our
                                registered VAT number is GB 32 4746 791.
                            </ListItem>
                            <ListItem>
                                <strong>2.2How to contact us. </strong>You can
                                contact us by writing to us at 35 Knockhall
                                Road, Greenhithe, Kent, DA9 9EZ or by sending us
                                an email to{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>
                                .
                            </ListItem>
                            <ListItem>
                                <strong>2.3How we may contact you. </strong>If
                                we have to contact you we will do so by
                                telephone or by writing to you at the email
                                address or postal address you provided to us in
                                your order.
                            </ListItem>
                            <ListItem>
                                <strong>2.4“Writing” includes emails. </strong>
                                When we use the words “writing” or “written” in
                                these terms, this includes emails.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            3. OUR CONTRACT WITH YOU
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    3.1 How we will accept your order.{" "}
                                </strong>
                                Our acceptance of your order will take place
                                when we email you to accept it, at which point a
                                contract will come into existence between you
                                and us.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    3.2 If we cannot accept your order.{" "}
                                </strong>
                                If we are unable to accept your order, we will
                                inform you of this and will not charge you for
                                the product. There are a number of reasons we
                                might not accept your order, for example:
                                <List>
                                    <ListItem>
                                        (a) a credit reference we have obtained
                                        for you does not meet our minimum
                                        requirements;
                                    </ListItem>
                                    <ListItem>
                                        (b) we have identified an error in the
                                        price or description of the product;
                                    </ListItem>
                                    <ListItem>
                                        (c) your account with us has been
                                        suspended or terminated for any reason;
                                    </ListItem>
                                    <ListItem>
                                        (d) we reasonably believe that you are
                                        buying products from us with the
                                        intention of reselling them;
                                    </ListItem>
                                    <ListItem>
                                        (e) we are unable to meet a delivery
                                        deadline you have specified; or
                                    </ListItem>
                                    <ListItem>
                                        (f) because of unexpected limits on our
                                        resources which we could not reasonably
                                        plan for.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>3.3 Your order number. </strong>We will
                                assign an order number to your order and tell
                                you what it is when we accept your order. It
                                will help us if you can tell us the order number
                                whenever you contact us about your order.
                            </ListItem>
                            <ListItem>
                                <strong>3.4 Minimum age requirements. </strong>
                                We do not accept orders from persons under the
                                age of 16. By placing an order with us, you
                                confirm that you are at least 16 years old.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    3.5 We only deliver to the UK and certain
                                    other countries{" "}
                                </strong>
                                WOur website is primarily for the promotion of
                                our products in the UK but for delivery outside
                                the UK please view our International Delivery
                                policy (by clicking here) – You will be
                                responsible for all customs requirements and
                                duties arising where we agree to deliver the
                                products outside the UK. Our Unlimited Delivery
                                service is not available to customers located
                                outside the UK mainland.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            4. OUR PRODUCTS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    4.1 Products may vary slightly from their
                                    pictures
                                </strong>
                                The images of the products on our website are
                                for illustrative purposes only. Although we have
                                made every effort to display the colours
                                accurately, we cannot guarantee that a device’s
                                display of the colours accurately reflects the
                                colour of the products. Your product may vary
                                slightly from those images. And although we have
                                made every effort to be as accurate as possible,
                                all sizes, weights, capacities, dimensions and
                                measurements indicated on our website are
                                approximate only.
                            </ListItem>
                            <ListItem>
                                <strong>4.2 Product packaging may vary.</strong>
                                The packaging of the product may vary from that
                                shown in images on our website.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    4.3 Making sure you order the correct size.
                                </strong>
                                TYou are responsible for taking any measurements
                                and ensuring that those measurements are
                                correct. You can find further information on our
                                website.
                            </ListItem>
                            <ListItem>
                                <strong>4.4 Product Authenticity.</strong>All
                                products we provide are sourced from the
                                manufacturer and are thus the legitimate and
                                authentic product.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            5. RESALE OF OUR PRODUCTS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    5.1 Products are provided for personal use
                                    only
                                </strong>
                                We only supply the products for your own
                                domestic and private use.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    5.2 Our rights before we accept your order.
                                </strong>
                                If we reasonably believe that you are buying
                                products from us with the intention of reselling
                                them, we may:
                                <List>
                                    <ListItem>
                                        (a) refuse to accept your order (see
                                        section 3.2); or
                                    </ListItem>
                                    <ListItem>
                                        (b) accept your order subject to
                                        agreeing additional or alternative terms
                                        with you.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    5.3 Our rights after we have accepted your
                                    order.
                                </strong>
                                If we can demonstrate that you are buying
                                products from us with the intention of reselling
                                them:
                                <List>
                                    <ListItem>
                                        (a) we may cancel your order (see
                                        section 11.1); or
                                    </ListItem>
                                    <ListItem>
                                        (b) treat your order as though you are
                                        buying products in the course of your
                                        business (see section 5.4).
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    5.4 If we can demonstrate that you are
                                    buying products from us with the intention
                                    of reselling them.
                                </strong>
                                You will not be a ‘consumer’ for the purposes of
                                the Consumer Contracts Regulations 2013 or the
                                Consumer Rights Act 2015. As a result:
                                <List>
                                    <ListItem>
                                        (a) any rights that are granted only to
                                        a ‘consumer’ under the Consumer
                                        Contracts Regulations 2013 or the
                                        Consumer Rights Act 2015 shall not apply
                                        to you;
                                    </ListItem>
                                    <ListItem>
                                        (b) different terms will apply to any
                                        orders for products, namely:
                                        <List>
                                            <ListItem>
                                                (i) section 9 will not apply and
                                                if the products are faulty or
                                                misdescribed, you may return
                                                them to us within 7 days after
                                                the day you (or someone you
                                                nominate) receives the products
                                                in accordance with the returns
                                                procedure available by clicking
                                                here. Please note that our
                                                returns address is Top Brand
                                                Outlet Ltd, Unit 10 Howletts
                                                Hall Farm, Mill Lane,
                                                Nevestock,Romford, RM4 1ET,
                                                United Kingdom. You must pay the
                                                costs of postage, and we may
                                                charge you a reasonable handling
                                                fee for processing your return;
                                            </ListItem>
                                            <ListItem>
                                                (ii) section 10.3 will not
                                                apply, and you must pay the
                                                costs of returning any products
                                                to us (even where those products
                                                are faulty or misdescribed);
                                            </ListItem>
                                            <ListItem>
                                                (iii) you must pay for the
                                                products in full at the time of
                                                placing your order, and we will
                                                charge your credit or debit card
                                                immediately;
                                            </ListItem>
                                            <ListItem>
                                                (iv) if you do not make any
                                                payment to us by the due date we
                                                may charge interest to you on
                                                the overdue amount at the rate
                                                of 6% a year above the Bank of
                                                England’s base rate from time to
                                                time (section 13.5 will be
                                                amended accordingly);
                                            </ListItem>
                                            <ListItem>
                                                (v) you will not be entitled to
                                                join our Unlimited Delivery
                                                service and, if you are an
                                                existing member, your membership
                                                will be terminated (see section
                                                22); and
                                            </ListItem>
                                            <ListItem>
                                                (vi) our ‘no-quibble 30-day
                                                returns policy’ and the
                                                paragraph headed ‘What if the
                                                item is faulty?’ in our returns
                                                procedure will not apply.
                                            </ListItem>
                                        </List>
                                    </ListItem>
                                </List>
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            6. YOUR RIGHTS TO MAKE CHANGES
                        </Typography>
                        <Typography>
                            If you wish to make a change to the product you have
                            ordered before we have delivered it to you, please
                            contact us. We will let you know if the change is
                            possible. If it is possible we will let you know
                            about any changes to the price of the product, the
                            timing of delivery or anything else which would be
                            necessary as a result of your requested change and
                            ask you to confirm whether you wish to go ahead with
                            the change. If we cannot make the change or the
                            consequences of making the change are unacceptable
                            to you, you may want to cancel your order (see
                            section 9 – Your rights to end the contract).
                        </Typography>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            7. OUR RIGHTS TO MAKE CHANGES
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    7.1 Minor changes to the products
                                </strong>
                                We may change the product to reflect changes in
                                relevant laws and regulatory requirements, or to
                                implement minor technical adjustments and
                                improvements. These changes will not affect your
                                use of the product.
                            </ListItem>
                            <ListItem>
                                <strong>7.2 Changes to these terms.</strong>We
                                may make changes to these terms from time to
                                time, for example, to reflect changes in
                                relevant laws and regulatory requirements, or to
                                implement ‘good practice’ or to improve the
                                efficiency of our order and delivery process. We
                                will always display the current version of these
                                terms on our website. The version of these terms
                                displayed on our website at the time you place
                                your order will be the terms that apply to your
                                order.
                            </ListItem>
                            <ListItem>
                                <strong>7.3 Withdrawing special offers.</strong>
                                We may withdraw any offer, special promotions or
                                promotional code without notice at any time.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    7.4 If all products that you order are out
                                    of stock.
                                </strong>
                                We will cancel your order (see section 0) and
                                provide you with a full refund.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    7.5 If only some of the products that you
                                    order are out of stock.
                                </strong>
                                We will notify you in writing of those products
                                that are out of stock, but we will continue to
                                deliver the rest of your order. We will provide
                                you with a full refund for those products that
                                are out of stock (see sections 10.4 and 10.6).
                                You have the right to cancel your order for the
                                rest of the products that we deliver to you (see
                                section 9).
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            8. DELIVERING THE PRODUCTS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>8.1 Delivery costs. </strong>The costs
                                of delivery will be as displayed to you on our
                                website by{" "}
                                <Link href="/terms-and-condition">
                                    clicking here
                                </Link>
                                .
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.2 When we will provide the products.{" "}
                                </strong>
                                During the order process, we will let you know
                                when we will expect to be able to provide the
                                products to you. We will deliver them to you as
                                soon as reasonably possible and in any event
                                within 30 days after the day on which we accept
                                your order.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.3 We are not responsible for delays
                                    outside our control.{" "}
                                </strong>
                                If our delivery of the products is delayed by an
                                event outside our control then we will contact
                                you as soon as possible to let you know and we
                                will take steps to minimise the effect of the
                                delay. Provided we do this we will not be liable
                                for delays caused by the event, but if there is
                                a risk of substantial delay you may contact us
                                to cancel your order and receive a refund for
                                any products you have paid for but not received.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.4 Delivery of your first order.{" "}
                                </strong>
                                When you place your first order with us, we may
                                insist that we deliver the products to the
                                address registered to the cardholder of the
                                credit or debit card that you used to make
                                payment, regardless of any alternative address
                                you provide us.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.5 Delivery of all subsequent orders.{" "}
                                </strong>
                                Apart from your first order, you may ask us to
                                deliver the products to an alternative address.
                                We may attempt to deliver the products either to
                                that alternative address or to the address
                                registered to the cardholder of the credit or
                                debit card that you used to make payment. As
                                long as we deliver the products to one of those
                                addresses, you agree that we will have delivered
                                the products to you.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.6 If you are not at the property when the
                                    product is delivered.{" "}
                                </strong>
                                .If no one is available at the address to take
                                delivery and the products cannot be posted
                                through your letterbox, we will leave you a note
                                informing you of how to rearrange delivery or
                                collect the products from a local depot. Please
                                note that we will not comply with any
                                instructions that you leave at your property,
                                for example, a note on your door asking us to
                                deliver to a next-door neighbour, because we
                                would not be able to verify that this was a
                                genuine note left by you.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.7 If you do not re-arrange delivery.{" "}
                                </strong>
                                .If, after three failed delivery attempts or
                                after 90 days (whichever happens sooner), you do
                                not re-arrange delivery or collect them from a
                                delivery depot, we will contact you for further
                                instructions and may charge you for storage
                                costs and any further delivery costs. If despite
                                our reasonable efforts, we are unable to contact
                                you or re-arrange delivery or collection we may
                                cancel your order and section 11 will apply.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.8 If we cannot gain access to the
                                    property.{" "}
                                </strong>
                                If you do not allow us access to the property in
                                order to deliver the products to you (and you do
                                not have a good reason for this) we may charge
                                you additional costs incurred by us as a result.
                                If despite our reasonable efforts, we are unable
                                to contact you or re-arrange access to the
                                property (or an alternative property) we may end
                                the contract and section 11 will apply.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.9 If you do not receive the products from
                                    us.{" "}
                                </strong>
                                .If you have not received delivery of the
                                products within 14 days of the date we notify
                                you by email that the products have been
                                dispatched, please inform us by sending an email
                                to{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>{" "}
                                <strong>Notice of Non-Delivery</strong> To Top
                                Brand Outlet Ltd,
                                Emailcontactus@topbrandoutlet.co.uk I/We* hereby
                                give notice that I/we* have not received my/our*
                                order for the following products: Ordered on:
                                Order number: Products not received: Name of
                                customer(s): Address of customer(s): Email
                                address of customer(s): Telephone number of
                                customer(s): Signature of customer(s) (only if
                                this form is notified on paper): Date: * Delete
                                as appropriate
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.10 What we will do when we receive a
                                    Notice of Non-Delivery.{" "}
                                </strong>
                                .We will send you a Denial of Receipt Letter to
                                complete and return to us. We will make
                                reasonable efforts to locate the missing
                                products and may contact you for further
                                information. We will update you within 14 days
                                of receipt of the Notice of Non-Delivery.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.11 If we locate the missing products.{" "}
                                </strong>
                                .We will arrange to re-deliver them to you.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.12 If we cannot locate the missing
                                    products.{" "}
                                </strong>
                                .We will offer you one or more of the following
                                options:
                                <List>
                                    <ListItem>
                                        (a) where we still have products in
                                        stock, we may offer to deliver the
                                        missing products to you; or
                                    </ListItem>
                                    <ListItem>
                                        (b) we will offer you the option to
                                        cancel the order (see section 10);and
                                        notify you of the time by which we will
                                        need to receive confirmation of which
                                        offer you wish to accept, failing which,
                                        we will assume you wish to take up the
                                        option to cancel the order.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.13 If you receive the wrong products from
                                    us.{" "}
                                </strong>
                                In the unlikely event of an error with your
                                order, please inform us within 48 hours of your
                                receipt of the order by sending an email{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>{" "}
                                . We will send you a Notice of Error form to
                                complete and return to us to provide us with
                                details of the error.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.14 What we will do when we receive a
                                    Notice of Error.{" "}
                                </strong>
                                We will investigate the issue with your order
                                and may contact you for further information. We
                                will update you within 14 days of receipt of the
                                Notice of Error.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.15 What we will do if there has been an
                                    error with your order.{" "}
                                </strong>
                                If our investigations establish that there has
                                been an error with your order, we will offer you
                                one or more of the options in section 8.12
                                (a)-(b) above, and notify you of the time by
                                which we will need to receive confirmation of
                                which offer you wish to accept, failing which,
                                we will assume you wish to take up the option to
                                cancel the order.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.16 When you become responsible for the
                                    products.{" "}
                                </strong>
                                The products will be your responsibility from
                                the time we deliver the product into your
                                physical possession, or to another person you
                                have notified us is permitted to accept delivery
                                of the products.
                            </ListItem>
                            <ListItem>
                                <strong>8.17 When you own goods. </strong>You
                                own the Products once we have received payment
                                in full.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.18 What will happen if you do not give
                                    required information to us.{" "}
                                </strong>
                                We may need certain information from you so that
                                we can supply the products to you, for example,
                                your choice of size and color. If so, this will
                                have been stated in the description of the
                                products on our website. If you give us
                                incomplete or incorrect information, we may
                                either cancel your order (and section 11 will
                                apply) or make an additional charge of a
                                reasonable sum to compensate us for any extra
                                work that is required as a result. We will not
                                be responsible for supplying the products late
                                or not supplying any part of them if this is
                                caused by you giving us incomplete or incorrect
                                information.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.19 Reasons we may suspend the supply of
                                    products to you.{" "}
                                </strong>
                                We may suspend the supply of a product:
                                <List>
                                    <ListItem>
                                        (a) to deal with technical problems or
                                        make minor technical changes;
                                    </ListItem>
                                    <ListItem>
                                        (b) to update the product to reflect
                                        changes in relevant laws and regulatory
                                        requirements;
                                    </ListItem>
                                    <ListItem>
                                        (c) if we can demonstrate that you are
                                        buying products from us with the
                                        intention of reselling them (see section
                                        5);
                                    </ListItem>
                                    <ListItem>
                                        (d) if we reasonably believe that you
                                        have failed to comply with these terms;
                                        or
                                    </ListItem>
                                    <ListItem>
                                        (e) to make changes to the product as
                                        requested by you or notified by us to
                                        you (see section 7).
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.20 Your rights if we suspend the supply of
                                    products.{" "}
                                </strong>
                                We will contact you in advance to tell you we
                                will be suspending supply of the product, unless
                                the problem is urgent or an emergency. You may
                                contact us to cancel your order for a product if
                                we suspend it, or tell you we are going to
                                suspend it, in each case for a period of more
                                than 30 days and we will refund any sums you
                                have paid in advance for the product in respect
                                of the period after you cancel the order.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    8.21 We may also suspend supply of the
                                    products if you do not pay.{" "}
                                </strong>
                                If you do not pay us for the products when you
                                are supposed to (see section 13.4), for example,
                                your payment is declined by your credit card
                                provider or your bank, we may suspend supply of
                                the products until you have paid us the
                                outstanding amounts. We will contact you to tell
                                you we are suspending supply of the products. As
                                well as suspending the products we can also
                                charge you interest on your overdue payments
                                (see section 13.5).
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            9. YOUR RIGHTS TO CANCEL YOUR ORDER
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    9.1 When you can cancel your order with us.{" "}
                                </strong>
                                Your rights to cancel an order will depend on
                                what you have bought, whether there is anything
                                wrong with it and when you decide to end the
                                contract:
                                <List>
                                    <ListItem>
                                        (a)
                                        <strong>
                                            If what you have bought is faulty or
                                            misdescribed you may have a legal
                                            right to cancel your order
                                        </strong>
                                        (or to ask us to repair or replace the
                                        product),{" "}
                                        <strong>see section 12;</strong>{" "}
                                    </ListItem>
                                    <ListItem>
                                        (b)
                                        <strong>
                                            If you want to cancel your order
                                            because of something we have done or
                                            have told you we are going to do,
                                            see section 9.2;
                                        </strong>
                                    </ListItem>
                                    <ListItem>
                                        (c)
                                        <strong>
                                            If you have just changed your mind
                                            about the product, see sections 9.3
                                            and 9.4.
                                        </strong>
                                        You may be able to get a refund if you
                                        are within the cooling-off period or the
                                        ‘no-quibble 28-day returns’ period, but
                                        this may be subject to deductions and
                                        you will have to pay the costs of return
                                        of the products unless you are a VIP
                                        Premier member and we have provided you
                                        with a free returns label;
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    9.2 Cancelling an order because of something
                                    we have done or are going to do.{" "}
                                </strong>
                                If you are cancelling an order for a reason set
                                out at (a) to (e) below, the order will be
                                cancelled immediately and we will refund you in
                                full for any products which have not been
                                provided and you may also be entitled to
                                compensation. The reasons are:
                                <List>
                                    <ListItem>
                                        (a) we have told you about an upcoming
                                        change to the product which you do not
                                        agree to (see section 0);
                                    </ListItem>
                                    <ListItem>
                                        (b) we have told you about a change to
                                        these terms which you do not agree to
                                        (see section 7.2);
                                    </ListItem>
                                    <ListItem>
                                        (c) we have told you about an error in
                                        the price or description of the product
                                        you have ordered and you do not wish to
                                        proceed;
                                    </ListItem>
                                    <ListItem>
                                        (d) there is a risk that the supply of
                                        the products may be significantly
                                        delayed because of events outside our
                                        control;
                                    </ListItem>
                                    <ListItem>
                                        (e) we have suspended supply of the
                                        products for technical reasons, or
                                        notify you we are going to suspend them
                                        for technical reasons, in each case for
                                        a period of more than 30 days; or
                                    </ListItem>
                                    <ListItem>
                                        (f) you have a legal right to cancel the
                                        order because of something we have done
                                        wrong.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    9.3 Exercising your right to change your
                                    mind (Consumer Contracts Regulations 2013){" "}
                                </strong>
                                For most products bought online, you have a
                                legal right to change your mind within 14 days
                                and receive a refund. These rights, under the
                                Consumer Contracts Regulations 2013, are
                                explained in more detail in these terms.
                            </ListItem>
                            <ListItem>
                                <strong>9.4 Our ’no-quibble</strong>30-day
                                returns policy’. Please note, these terms
                                reflect the goodwill guarantee offered by Top
                                Brand Outlet Ltd to its UK customers, which is
                                more generous than your legal rights under the
                                Consumer Contracts Regulations in the ways set
                                out below. This goodwill guarantee does not
                                affect your legal rights in relation to faulty
                                or misdescribed products (see section 12.2).
                                Further details of our policy are available by
                                clicking here. Rights under the Consumer
                                Contracts Regulations 2013 How our ‘no-quibble
                                30-day returns policy’ is a more generous 14-day
                                period to change your mind. The 30-day period to
                                change your mind.
                            </ListItem>
                            <ListItem>
                                9.5. When you don’t have the right to change
                                your mind. You do not have a right to change
                                your mind in respect of:
                                <List>
                                    <ListItem>
                                        (a) products sealed for health
                                        protection or hygiene purposes, once
                                        these have been unsealed after you
                                        receive them (for example, underwear,
                                        swimwear, pierced earrings, toiletries
                                        and fragrances); and
                                    </ListItem>
                                    <ListItem>
                                        (b) any products which become mixed
                                        inseparably with other items after their
                                        delivery.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                9.6 How long do I have to change my mind? Where
                                you wish to take advantage of the rights under
                                the Consumer Contracts Regulations 2013, you
                                have 14 days after the day you (or someone you
                                nominate) receives the products. Where your
                                products are split into several deliveries over
                                different days, you have until 14 days after the
                                day you (or someone you nominate) receives the
                                last delivery to change your mind about the
                                products. Where you wish to take advantage of
                                the rights under our ‘no-quibble 30-day returns
                                policy’, you have 30 days after the day you (or
                                someone you nominate) receives the products.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            10. HOW TO CANCEL YOUR ORDER (INCLUDING IF YOU HAVE
                            CHANGED YOUR MIND)
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    10.1 Tell us you want to cancel your order.{" "}
                                </strong>
                                To cancel your order, please let us know by
                                doing the following: Email us at{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>
                                . Please provide your name, home address,
                                details of the order and, where available, your
                                phone number and email address.{" "}
                                <strong>Model cancellation form</strong> To Top
                                Brand Outlet Ltd, I/We* hereby give notice that
                                I/we* cancel my/our* order for the following
                                products: Ordered/received* on: Order number:
                                Name of customer(s): Address of customer(s):
                                Signature of customer(s) (only if this form is
                                notified on paper): Date: * Delete as
                                appropriate
                            </ListItem>
                            <ListItem>
                                <strong>
                                    10.2 Returning products after cancelling
                                    your order.{" "}
                                </strong>
                                If you cancel an order you must return the
                                products to us in accordance with the returns
                                procedure available by clicking here. Please
                                note that our returns address is Returns
                                Department, Top Brand Outlet Ltd, 35 Knockhall
                                Road, Greenhithe, Kent, DA9 9EZ, United Kingdom.
                                If you are exercising your right to change your
                                mind, you must send off the products within 30
                                days of telling us you wish to cancel the order.
                            </ListItem>
                            <ListItem>
                                10.3 When we will pay the costs of return. We
                                will pay the costs of return:
                                <List>
                                    <ListItem>
                                        (a) if the products are faulty or
                                        misdescribed;
                                    </ListItem>
                                    <ListItem>
                                        (b) if you are cancelling an order
                                        because we have told you of an upcoming
                                        change to the product or these terms, an
                                        error in pricing or description, a delay
                                        in delivery due to events outside our
                                        control or because you have a legal
                                        right to do so as a result of something
                                        we have done wrong; orIn all other
                                        circumstances (including where you are
                                        exercising your right to change your
                                        mind) you must pay the costs of return.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                10.4 How we will refund you. We will refund you
                                the price you paid for the products (including
                                delivery costs, where appropriate), by the
                                method you used for payment. However, we may
                                make deductions from the price, as described
                                below.
                            </ListItem>
                            <ListItem>
                                10.5. Deductions from refunds if you are
                                exercising your right to change your mind. If
                                you are exercising your right to change your
                                mind:
                                <List>
                                    <ListItem>
                                        (a) We may reduce your refund of the
                                        price (excluding delivery costs) to
                                        reflect any reduction in the value of
                                        the products if this has been caused by
                                        your excessive handling of them in a way
                                        which would not be permitted in a shop.
                                        If we refund you the price paid before
                                        we are able to inspect the products and
                                        later discover you have handled them in
                                        an unacceptable way, you must pay us an
                                        appropriate amount.
                                    </ListItem>
                                    <ListItem>
                                        (b) The maximum refund for delivery
                                        costs will be the costs of delivery by
                                        the least expensive delivery method we
                                        offer (namely our ‘standard UK delivery’
                                        cost).
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    10.6 When your refund will be made.{" "}
                                </strong>
                                We will make any refunds due to you as soon as
                                possible. Your refund will be made within 14
                                days from the day on which we receive the
                                product back from you or, if earlier, within 14
                                days from the day on which you provide us with
                                evidence that you have sent the product back to
                                us. For information about how to return a
                                product to us, see section 10.2.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            11. OUR RIGHTS TO END THE CONTRACT
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    1.1 We may cancel an order if you do not
                                    comply with these terms.{" "}
                                </strong>
                                We may cancel an order for a product at any time
                                by writing to you if:
                                <List>
                                    <ListItem>
                                        (a) you do not pay us for the products
                                        when you are supposed to (see section
                                        13.4), for example, your payment is
                                        declined by your credit card provider or
                                        your bank;
                                    </ListItem>
                                    <ListItem>
                                        (b) you do not, within a reasonable time
                                        of us asking for it, provide us with
                                        information that is necessary for us to
                                        provide the products (see section 8.18);
                                    </ListItem>
                                    <ListItem>
                                        (c) you do not, within a reasonable
                                        time, allow us to deliver the products
                                        to you;
                                    </ListItem>
                                    <ListItem>
                                        (d) we can demonstrate that you are
                                        buying products from us with the
                                        intention of reselling them (see section
                                        5);
                                    </ListItem>
                                    <ListItem>
                                        (e) you are under 16 years old;
                                    </ListItem>
                                    <ListItem>
                                        (f) you ask us to deliver to an address
                                        outside the UK which is not in a country
                                        that we deliver to (see section 3.5);
                                    </ListItem>
                                    <ListItem>
                                        (g) you use our website, or any content,
                                        images or material on our website, in a
                                        way that is not permitted by section 19;
                                    </ListItem>
                                    <ListItem>
                                        (h) you behave in an abusive or
                                        offensive manner towards our staff;
                                    </ListItem>
                                    <ListItem>
                                        (i) we are unable to verify your address
                                        or other details in order to satisfy our
                                        security procedure for delivery or card
                                        processing purposes.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    11.2 If all products that you order are out
                                    of stock.{" "}
                                </strong>
                                We will cancel your order and provide you with a
                                full refund.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    11.3 You must compensate us if you do not
                                    comply with these terms.{" "}
                                </strong>
                                If we cancel an order in the situations set out
                                in section 11.1 we will refund any money you
                                have paid in advance for products we have not
                                provided but we may deduct or charge you
                                reasonable compensation for the net costs we
                                will incur as a result of you not complying with
                                these terms.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    11.4 We may withdraw the product.{" "}
                                </strong>
                                We may write to you to let you know that we are
                                going to stop providing the product. In some
                                circumstances, we may not be able to provide you
                                with advance notice, for example, where a
                                product is withdrawn from sale by reason of us
                                complying with relevant laws and regulatory
                                requirements. In all other cases, we will let
                                you know in advance of our stopping the supply
                                of the product and will refund any sums you have
                                paid in advance for products which will not be
                                provided.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            12. IF THERE IS A PROBLEM WITH THE PRODUCT
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    12.1 How to tell us about problems.
                                </strong>{" "}
                                .If you have any questions or complaints about
                                the product, please contact us. You can send us
                                an email to{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>{" "}
                                or write to us at Top Brand Outlet Ltd, 35
                                Knockhall Road, Greenhithe, Kent, DA9 9EZ,
                                United Kingdom.
                            </ListItem>
                            <ListItem>
                                <strong>12.2 Your legal rights.</strong> We are
                                under a legal duty to supply products that are
                                in conformity with this contract. Nothing in
                                these terms will affect your legal rights. For
                                more information about your legal rights please
                                visit the Citizens Advice Bureau website{" "}
                                <Link
                                    target="_blank"
                                    href="www.citizensadvice.org.uk"
                                >
                                    www.citizensadvice.org.uk
                                </Link>{" "}
                                or call 03454 04 05 06.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    12.3Your obligation to return rejected
                                    products.
                                </strong>
                                If you wish to exercise your legal rights to
                                reject products, you should not attempt to
                                repair the products and you must return them to
                                us in accordance with the returns procedure
                                available by clicking here. Please note that our
                                returns address is Top Brand Outlet Ltd, 35
                                Knockhall Road, Greenhithe, Kent, DA9 9EZ,
                                United Kingdom. We will only pay the costs of
                                postage if there is a problem with the product.
                                Please email us at{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>{" "}
                                for a return label.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            13. PRICE AND PAYMENT
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    13.1 Where to find the price for the
                                    product.
                                </strong>{" "}
                                The price of the product (which includes VAT)
                                will be the price indicated on the order pages
                                when you placed your order. We take all
                                reasonable care to ensure that the price of the
                                product advised to you is correct. However
                                please see section 13.3 for what happens if we
                                discover an error in the price of the product
                                you order.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.2 We will pass on changes in the rate of
                                    VAT.{" "}
                                </strong>
                                If the rate of VAT changes between your order
                                date and the date we supply the product, we will
                                adjust the rate of VAT that you pay, unless you
                                have already paid for the product in full before
                                the change in the rate of VAT takes effect.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.3 What happens if we got the price wrong.{" "}
                                </strong>
                                It is always possible that, despite our best
                                efforts, some of the products we sell may be
                                incorrectly priced in our catalogues and/or on
                                our website. We will normally check prices
                                before accepting your order so that, where the
                                product’s correct price at your order date is
                                less than our stated price at your order date,
                                we will charge the lower amount. If the
                                product’s correct price at your order date is
                                higher than the price stated to you, we will
                                contact you for your instructions before we
                                accept your order. If we accept and process your
                                order where a pricing error is obvious and
                                unmistakeable and could reasonably have been
                                recognised by you as a mispricing, we may cancel
                                your order, refund you any sums you have paid
                                and require the return of any products provided
                                to you.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.4 When you must pay and how you must pay.
                                </strong>{" "}
                                We accept payment with PayPal, Mastercard,
                                Maestro, Visa and Visa Debit, and you must
                                provide your credit or debit card details at the
                                time of placing your order. You must pay for the
                                products before we dispatch them.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.5 We can charge interest if you pay late.
                                </strong>{" "}
                                If you do not make any payment to us by the due
                                date we may charge interest to you on the
                                overdue amount at the rate of 3% a year above
                                the Bank of England’s base rate from time to
                                time. This interest shall accrue on a daily
                                basis from the due date until the date of actual
                                payment of the overdue amount, whether before or
                                after judgment. You must pay us interest
                                together with any overdue amount.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.6 If your payment is declined by your
                                    credit card provider or your bank.
                                </strong>{" "}
                                Your credit card provider, your bank or our debt
                                collection company (Transax or Certegy) may
                                charge you a fee. The amount of that fee is
                                beyond our control. Where we incur any fee as a
                                result of a declined payment, you must reimburse
                                you for the fee we incur.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    13.7 What to do if you think a payment is
                                    wrong.
                                </strong>{" "}
                                If you think a payment is wrong, please contact
                                us promptly to let us know and we will not
                                charge you interest until we have resolved the
                                issue.
                            </ListItem>
                            <ListItem>
                                <strong>13.8 Recommended retail prices.</strong>{" "}
                                We generally publish recommended retail prices
                                (‘RRPs’) based on our brands’/suppliers’ normal
                                UK retail prices. However, some products have
                                never previously been sold in the UK and are
                                exclusive to us. For these products, we
                                calculate our RRP by converting the product’s
                                RRP for EU sales into pounds sterling in
                                accordance with currency exchange rates at the
                                time we first purchase that product from our
                                brand/supplier.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            14. MULTI-BUY OFFERS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    14.1 How we apply multi-buy offers.
                                </strong>{" "}
                                From time to time, we may issue multi-buy
                                offers, for example, ‘buy one get one free’ or
                                ‘buy three products and get the lowest priced
                                product for half-price’. Multi-buy offers cannot
                                be used on permanently-reduced products, sale
                                products or brand concession products. If you
                                require a refund or replacement of products that
                                were subject to a multi-buy offer, then all
                                products (including those that you would
                                otherwise want to keep) must be returned to us.
                                The free or discounted product cannot be
                                refunded on its own for money or credit.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    14.2 What other terms apply to multi-buy
                                    offers.
                                </strong>{" "}
                                When we issue a multi-buy offer, we reserve the
                                right to apply additional terms and conditions
                                to the use of that offer which we will make
                                clear to you at the time. We also reserve the
                                right to withdraw or amend an offer at any time
                                without giving you prior notice. There is no
                                cash alternative available.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            15.OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY
                            YOU
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    15.1 We are responsible to you for
                                    foreseeable loss and damage caused by us.
                                </strong>{" "}
                                If we fail to comply with these terms, we are
                                responsible for loss or damage you suffer that
                                is a foreseeable result of our breaking this
                                contract or our failing to use reasonable care
                                and skill, but we are not responsible for any
                                loss or damage that is not foreseeable. Loss or
                                damage is foreseeable if either it is obvious
                                that it will happen or if, at the time the
                                contract was made, both we and you knew it might
                                happen, for example, if you discussed it with us
                                during the order process.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    15.2 We do not exclude or limit in any way
                                    our liability to you where it would be
                                    unlawful to do so.
                                </strong>{" "}
                                This includes liability for death or personal
                                injury caused by our negligence or the
                                negligence of our employees, agents or
                                subcontractors; for fraud or fraudulent
                                misrepresentation; for breach of your legal
                                rights in relation to the products including the
                                right to receive products which are: as
                                described and match the information we provided
                                to you and any sample or model seen or examined
                                by you; of satisfactory quality; fit for any
                                particular purpose made known to us and supplied
                                with reasonable skill and care and for defective
                                products under the Consumer Protection Act 1987.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    15.3 We are not liable for any other loss
                                    arising from unauthorised access to your
                                    account.
                                </strong>{" "}
                                It is your responsibility to keep your email
                                address, account log-in details and password
                                safe and secure. We shall be entitled to treat
                                any orders that we receive using your account
                                and your stored credit or debit card details as
                                coming from you or being authorised by you, and
                                we will process those orders in accordance with
                                these terms. Unless we are responsible under
                                section 15.1 or 15.2, we have no liability to
                                you for any loss or damage you suffer as a
                                result of any person gaining unauthorised access
                                to your account and placing an order with us.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    15.4 We are not liable for business losses.
                                </strong>{" "}
                                We only supply the products for domestic and
                                private use. If you use the products for any
                                commercial, business or re-sale purpose we will
                                have no liability to you for any loss of profit,
                                loss of business, business interruption, or loss
                                of business opportunity.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            16. HOW WE MAY USE YOUR PERSONAL INFORMATION
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    16.1 We explain how we collect, store and
                                    use your personal information in our privacy
                                    policy, available{" "}
                                    <Link href="/privacy-policy">here</Link>
                                </strong>
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            17. HOW TO MAKE A COMPLAINT
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    17.1 How to contact us in the first
                                    instance.
                                </strong>{" "}
                                We want you to be pleased with any purchase you
                                make from us, so if there’s something you’re not
                                happy with please let us know as soon as
                                possible by sending an email to{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>{" "}
                                and we will try and resolve the problem for you.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    17.2 If you are not happy with our initial
                                    response.
                                </strong>{" "}
                                Please send us a letter addressed to Top Brand
                                Outlet Ltd, 35 Knockhall Road, Greenhithe, Kent,
                                DA9 9EZ, United Kingdom, providing us with your
                                order number and full details of your complaint,
                                or alternatively by email at{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>
                                . We will respond to you within 14 days of
                                receiving your letter with a suggested course of
                                action to try and resolve the problem.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    17.3 If we cannot resolve your complaint.{" "}
                                </strong>{" "}
                                If we are unable to resolve your complaint via
                                our internal complaints procedure above, you may
                                wish to use the European Online Dispute
                                Resolution Platform (ODR platform), available at{" "}
                                <Link href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage">
                                    here
                                </Link>
                                .
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            18. OTHER IMPORTANT TERMS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    18.1 We may transfer this agreement to
                                    someone else.
                                </strong>{" "}
                                We may transfer our rights and obligations under
                                these terms to another organisation. We will
                                always tell you in writing if this happens and
                                we will ensure that the transfer will not affect
                                your rights under the contract. If you are
                                unhappy with the transfer, you may contact us to
                                cancel any outstanding orders within 30 days of
                                us telling you about it and we will refund you
                                any payments you have made in advance for
                                products not provided.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    18.2 You need our consent to transfer your
                                    rights to someone else.
                                </strong>{" "}
                                You may only transfer your rights or your
                                obligations under these terms to another person
                                if we agree to this in writing. However, you may
                                transfer your rights under our ‘no quibble
                                30-day returns policy’ at section 9.4 to a
                                person who has acquired the product from you. We
                                may require the person to whom your rights are
                                transferred to provide reasonable evidence that
                                they are now the owner of the relevant product.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    18.3 Nobody else has any rights under this
                                    contract.
                                </strong>{" "}
                                This contract is between you and us. No other
                                person shall have any rights to enforce any of
                                its terms, except as explained in section 18.2
                                in respect of our ‘no quibble 30-day returns
                                policy’.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    18.4 If a court finds part of this contract
                                    illegal, the rest will continue in force
                                </strong>{" "}
                                Each of the sections of these terms operates
                                separately. If any court or relevant authority
                                decides that any of them are unlawful, the
                                remaining sections will remain in full force and
                                effect.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    18.5 Even if we delay in enforcing this
                                    contract, we can still enforce it later.
                                </strong>{" "}
                                If we do not insist immediately that you do
                                anything you are required to do under these
                                terms, or if we delay in taking steps against
                                you in respect of your breaking this contract,
                                that will not mean that you do not have to do
                                those things and it will not prevent us taking
                                steps against you at a later date. For example,
                                if we can demonstrate that you are buying
                                products from us with the intention of reselling
                                them and we accept your order, we can still take
                                any of the action permitted by section 5 at a
                                later date.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    18.6 Which laws apply to this contract and
                                    where you may bring legal proceedings.
                                </strong>{" "}
                                These terms are governed by English law and you
                                can bring legal proceedings in respect of the
                                products in the English courts. If you live in
                                Scotland, you can bring legal proceedings in
                                respect of the products in either the Scottish
                                or the English courts. If you live in Northern
                                Ireland, you can bring legal proceedings in
                                respect of the products in either the Northern
                                Irish or the English courts.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            19. USE OF OUR WEBSITE
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    19.1 Terms on which we make our website
                                    available.
                                </strong>{" "}
                                By using our website, you confirm that you
                                accept the terms set out in section 19. If you
                                do not agree to the terms set out in section 19,
                                you must not use our website.
                            </ListItem>
                            <ListItem>
                                <strong>19.2 We may update our website.</strong>{" "}
                                We may update our website from time to time and
                                change the content at any time. Any changes to
                                the products or these terms will be dealt with
                                by section 7.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    19.3 Our website may not always be accurate
                                    or available.
                                </strong>{" "}
                                We take all reasonable care to ensure that our
                                website is accurate, but we cannot guarantee
                                that our website, or any content on it, will be
                                completely free from errors or omissions. We do
                                not guarantee that our website will always be
                                available or you will be able to use it without
                                interruption. Access to our website is permitted
                                on a temporary basis, and we may suspend or
                                withdraw all or any part of our website without
                                notice. We will not be responsible for any loss
                                or damage you suffer if our website is
                                unavailable at any time.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    19.4 You must use our website lawfully.
                                </strong>{" "}
                                You may use our website only for lawful
                                purposes. You may not use our website:
                                <List>
                                    <ListItem>
                                        (a) In any way that breaches any
                                        applicable local, national or
                                        international law or regulation
                                    </ListItem>
                                    <ListItem>
                                        (b) In any way that is unlawful or
                                        fraudulent, or has any unlawful or
                                        fraudulent purpose or effect
                                    </ListItem>
                                    <ListItem>
                                        (c) For the purpose of harming or
                                        attempting to harm minors in any way
                                    </ListItem>
                                    <ListItem>
                                        (d) To transmit, or procure the sending
                                        of, any unsolicited or unauthorised
                                        advertising or promotional material or
                                        any other form of similar solicitation
                                        (spam)
                                    </ListItem>
                                    <ListItem>
                                        (e) To knowingly transmit any data, send
                                        or upload any material that contains
                                        viruses, Trojan horses, worms,
                                        time-bombs, keystroke loggers, spyware,
                                        adware or any other harmful programs or
                                        similar computer code designed to
                                        adversely affect the operation of any
                                        computer software or hardware.
                                    </ListItem>
                                </List>
                            </ListItem>
                            <ListItem>
                                <strong>
                                    19.5 Using our name and the images and
                                    material on our website.
                                </strong>{" "}
                                We are the owner of all intellectual property
                                rights in our website, and in the images and
                                material published on it. Those images and
                                materials are protected by copyright laws around
                                the world. ‘Topbrandoutlet’ and our logo are our
                                trade marks. All rights are reserved. You must
                                not use any images or material from our website
                                for any commercial purpose, for example,
                                advertising products that you have bought from
                                us for resale on other websites.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    19.6 If you want to link another website to
                                    our website.
                                </strong>{" "}
                                You may create a link from your own website to
                                our website home page (but not to any other page
                                of our website) provided that you do so in a way
                                that is fair and legal and does not damage our
                                reputation or take advantage of it. You must not
                                suggest any form of association, approval or
                                endorsement by us of you or your website unless
                                we have agreed with you in writing. You must not
                                ‘frame’ our website on any other website.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    19.7 Use of third party trade marks on our
                                    website.
                                </strong>{" "}
                                We use third party trade marks on our website
                                for the purpose of describing and identifying
                                the products that we sell on our website. We are
                                not associated, linked or affiliated with the
                                owners of any third party trade marks we use on
                                our website, and do not endorse any of their
                                businesses or products.
                            </ListItem>
                        </List>
                    </Box>
                    <Box>
                        <Typography component={"h2"}>
                            20. COMPETITION TERMS AND CONDITIONS
                        </Typography>
                        <List>
                            <ListItem>
                                <strong>
                                    20.1 We may run competitions from time to
                                    time.
                                </strong>{" "}
                                Every competition we run will be accompanied by
                                the terms and conditions that apply to each
                                competition at the time. Unless those terms and
                                conditions contain alternative provisions, the
                                following provisions in section 19 will also
                                apply to all competitions that we run.
                            </ListItem>
                            <ListItem>
                                <strong>20.2 Who the promoter is.</strong> The
                                promoter will be Top Brand Outlet Ltd, a company
                                registered in England and Wales. Our company
                                registration number is 11761282 and our
                                registered office is at Top Brand Outlet Ltd, 35
                                Knockhall Road, Greenhithe, Kent, DA9 9EZ,
                                United Kingdom.. You can contact us by writing
                                to us at Top Brand Outlet Ltd, 35 Knockhall
                                Road, Greenhithe, Kent, DA9 9EZ, United Kingdom.
                                or by sending us an email to{" "}
                                <Link href="mailto:contactus@topbrandoutlet.co.uk">
                                    contactus@topbrandoutlet.co.uk
                                </Link>
                                .
                            </ListItem>
                            <ListItem>
                                <strong>
                                    20.3 You must be eligible to enter the
                                    competition.
                                </strong>{" "}
                                The competition is open to all residents in the
                                UK aged 18 years or over except employees of Top
                                Brand Outlet Ltd and members of their immediate
                                families. We may ask you to provide proof that
                                you are eligible to enter the competition, and
                                we may reject your entry if you do not provide
                                that proof.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    20.4 How to enter our competitions.
                                </strong>{" "}
                                The competition will run until a specified
                                closing date. All competition entries must be
                                received by us by no later than 5:00pm on the
                                closing date. All entries received after the
                                closing date are automatically disqualified. The
                                method of entering the competition will be
                                clearly stated in the terms and conditions that
                                apply to that competition. No purchase is
                                necessary. There is a limit of one entry per
                                person per residential address. By submitting a
                                competition entry, you are agreeing to comply
                                with the terms and conditions that apply to that
                                competition, and also that we may use your name,
                                photograph and competition entry in connection
                                with publicity of the competition.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    20.5 Our liability for entries we do not
                                    receive or we reject.
                                </strong>{" "}
                                We do not accept responsibility for competition
                                entries that are lost, mislaid, damaged or
                                delayed, regardless of the cause, for example,
                                as a result of any postal failure, equipment
                                failure, technical malfunction, systems,
                                satellite, network, server, computer hardware or
                                software failure of any kind. We will reject any
                                entries that have been forged or tampered with
                                or are automatically generated by computer,
                                completed in bulk, illegible or incomplete. We
                                will not accept proof of posting or transmission
                                as proof of receipt of entry to the competition.
                            </ListItem>
                            <ListItem>
                                <strong>20.6 Choosing the winner.</strong> The
                                winner(s) will be decided in accordance with the
                                terms and conditions that apply to that
                                competition. Our decision as to the winner is
                                final. We will send the name and county of the
                                winner to anyone who writes within one month of
                                the closing date requesting details of the
                                winner and who encloses a self-addressed
                                envelope.
                            </ListItem>
                            <ListItem>
                                <strong>
                                    20.7 Winning and claiming the prize.
                                </strong>{" "}
                                Prizes are subject to availability, and we
                                reserve the right to substitute the prize with a
                                prize of equal or greater value. The prize is
                                not negotiable or transferable. We will contact
                                the winner as soon as practicable after the
                                closing date using the email address provided
                                with the competition entry. If the winner cannot
                                be contacted or is not available, or has not
                                claimed their prize within 28 of days of the
                                closing date, we reserve the right to offer the
                                prize to the next eligible entrant selected from
                                the correct entries that were received before
                                the closing date. We do not accept any
                                responsibility if you are not able to take up
                                the prize.
                            </ListItem>
                        </List>
                    </Box>
                </Box>
            </Container>
        </Box>
    );
}

export default withPublicPage(TermsAndCondition);
