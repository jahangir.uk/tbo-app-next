
import styled from "@emotion/styled";
import { Box, Grid, TextField } from "@mui/material";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { createTheme } from "@mui/material/styles";
import BackImg from "assets/images/back.jpg";
import LoginImage from 'assets/images/forget-password.png';
import { COLORS } from "assets/styles/colors/colors";
import { CustomButton } from "components/Button";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
const theme = createTheme();



const CssTextField = styled(TextField)({
    '& label.Mui-focused': {
      color: COLORS.primary,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: COLORS.primary,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: COLORS.gray
      },
      '&:hover fieldset': {
        borderColor: COLORS.black,
      },
      '&.Mui-focused fieldset': {
        borderColor: COLORS.primary,
      },
    },
  });

export default function ForgetPassword ()  {
    
    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        console.log({
          email: data.get("email"),
          password: data.get("password"),
        });
      };

     

    return (
      
        <Box sx={{ 
          minHeight: '90vh',
          height: '100%',
          background: `url(${BackImg?.src})`,
          padding: '50px 80px',
          backgroundSize: 'cover',
          display: 'flex',
          alignItems: 'center',
       }}>
         <Head>
            <title>Forgot password - Top Brand Outlet UK</title>
        </Head>
         <Container component="main" width={1100}>
          <Box
            sx={{
              
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              background: "#efefef",
              padding: 3,
              borderRadius: 2.5,
            }}
          >
            <Grid container spacing={2} sx={{ display: 'flex', alignItems: 'center' }}>
              <Grid item md={6} sm={6} sx={{ '& img': {width:'100%', height:'auto'} }}>
                <Image src={LoginImage} alt={"Login"} />
              </Grid>
              <Grid item md={6} sm={6}>
                {/* <Box component={"img"} sx={{ m: 1 }} src={TopLogo} alt="Logo" /> */}
  
                <Typography component="h1" variant="h2" sx={{ fontWeight:600 }}>
                  Forgot Password
                </Typography>
                <Box
                  component="form"
                  onSubmit={handleSubmit}
                  noValidate
                  sx={{ mt: 1 }}
                >
                  <CssTextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                  
                
                  <CustomButton
                    type="submit"
                    fullWidth
                    variant="contained"
                    text={'Change Password'}
                    
                  />
                    
                  <Grid container>
                   
                    <Grid item>
                      <Link href="/login" variant="body2">
                        {"Back to login"}
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
               
              </Grid>
            </Grid>
          </Box>
        </Container>
       </Box>
       
     
    );
}
