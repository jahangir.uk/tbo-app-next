import AddIcon from "@mui/icons-material/Add";
import ClearIcon from "@mui/icons-material/Clear";
import RemoveIcon from "@mui/icons-material/Remove";
import {
    Box,
    Button,
    Container,
    Grid,
    IconButton,
    TextField,
    Typography,
} from "@mui/material";
import PaymentGateway from "assets/images/payment.png";
import { COLORS } from "assets/styles/colors/colors";
import { useCartStyle } from "assets/stylesheets/cart/cartStylesheet";
import { withPublicPage } from "components/HOC/PublicPage";
import { ForgotPassModal } from "components/Modals/ForgotPassModal";
import { LoginModal } from "components/Modals/LoginModal";
import { ProductAvailability } from "components/Modals/ProductAvailability";
import { SignModal } from "components/Modals/SignupModal";
import { COUPON_KEY, IMAGE_URL } from "helpers/config";
import { capitalize, isEmpty, limitWords } from "helpers/functions";
import useSnackbar from "hooks/useSnackbar";
import useToken from "hooks/useToken";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import {
    applyCoupon,
    removeCoupon,
    removeToCart,
    updateToCart,
} from "redux/action/cart";
import ProductService from "services/ProductService";
import { secureStorage } from "utils/storage";

function Cart() {
    const { cartItems, total, grandTotal, couponApply, discount } = useSelector(
        (state) => state.cart
    );
    const classes = useCartStyle();
    const snackbar = useSnackbar();
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [checkProduct, setCheckProduct] = useState(false);
    const [shippingCost, setShippingCost] = useState(0);
    const isAuthenticated = useToken();
    const [data, setData] = useState(null);
    const [signOpen, setSignOpen] = useState(false);
    const [forgotOpen, setForgotOpen] = useState(false);
    const [cartTotal, setCartTotal] = useState(0);
    const coupon = JSON.parse(secureStorage.get(COUPON_KEY));
    const {
        handleSubmit,
        register,
        reset,
        formState: { errors },
    } = useForm();

    const incrementQty = (id, qty, inventory) => {
        // console.log(qty, inventory);
        if(inventory >= qty){
            dispatch(updateToCart(id, Number(qty)));
        } else {
            snackbar(`Please select qty less then or equal ${inventory}`, {
                variant: "error"
            });
        }
    };

    const decrementQty = (id, qty) => {
        
        if (qty > 0) {
            dispatch(updateToCart(id, Number(qty)));
        } else {
            dispatch(removeToCart(id));
            
        }
    };

    const demo_data = {
        order_id: "48484fsda478",
    }

    const updateItemQuantity = (id, qty, inventory) => {
        
        if (qty > 0) {
            if(inventory >= qty){
                dispatch(updateToCart(id, Number(qty)));
            } else{
                snackbar(`Please select qty less then or equal ${inventory}`, {
                    variant: "error"
                });
            }
        }else {
            dispatch(removeToCart(id));
        }
    };

    // const addShippingType = (amount) => {
    //     dispatch(shippingTypeSelect(amount));
    // }

    const onHandleApplyCoupon = async (data) => {
        const payload = {
            coupon_code: data?.coupon,
            cart_items: cartItems,
            cart_total: Number(cartTotal).toFixed(2),
        };
        ProductService.Commands.getCouponData(payload).then((response) => {
            console.log(response);
            if (response?.status === "success") {
                dispatch(applyCoupon(response));
                secureStorage.set(COUPON_KEY, JSON.stringify(data?.coupon));
            }
        });
        console.log(payload); // TODO Call coupon api
        // setLoading(true);
        reset();
    };

    const checkAvailability = async () => {
       const res = await ProductService.Commands.checkProductAvailability(cartItems);
       console.log(res);
       if(res?.status === "error"){
            setData(res?.cart);
            setCheckProduct(true);
       }
    }

    const removeItem = (id) => {
        if (!isEmpty(id)) {
            dispatch(removeToCart(id));
        }
    };

    useEffect(() => {
        setCartTotal(total);
    }, [total]);

    useEffect(() => {
        if(!isEmpty(cartItems)){
            checkAvailability();
        }
    }, [cartItems])


    return (
        <Box>
            <Head>
                <title>Cart Page | Top Brand Outlet UK</title>
            </Head>
            <Container className={classes.root} maxWidth="xl">
                {isEmpty(cartItems) ? (
                    <Box className="empty-cart">
                        <Typography component={"h2"}>
                            Your cart is empty
                        </Typography>
                        <Typography>
                            Fill me up with some unbeatable bargains! <br />
                            {`We're adding new styles daily from the biggest brands`}
                            <br />
                            All with up to 75% off the RRP.
                        </Typography>
                        <Box component={"a"} href="/">
                            Continue Shopping
                        </Box>
                    </Box>
                ) : (
                    <Box className="cart">
                        <Typography
                            component={"h2"}
                            sx={{
                                display: "flex",
                                justifyContent: "space-between",
                            }}
                        >
                            <span>Your Cart</span>{" "}
                            <Box
                                component={"span"}
                                sx={{ cursor: "pointer" }}
                                onClick={() => setOpen(!open)}
                            >
                                {isAuthenticated
                                    ? ""
                                    : "Login and checkout faster"}
                            </Box>
                        </Typography>
                        <Box className="list-item">
                            <Box
                                className="heading"
                                sx={{
                                    display: {
                                        sm: "flex !important",
                                        xs: "none !important",
                                    },
                                }}
                            >
                                <Box sx={{ width: { sm: "50%", xs: "60%" } }}>
                                    Item Description
                                </Box>
                                <Box sx={{ width: { sm: "10%", xs: "25%" } }}>
                                    Size & Color
                                </Box>
                                <Box sx={{ width: { sm: "15%", xs: "30%" } }}>
                                    Quantity
                                </Box>
                                <Box sx={{ width: { sm: "10%", xs: "20%" } }}>
                                    Item total
                                </Box>
                                <Box sx={{ width: { sm: "10%", xs: "20%" } }}>
                                    Remove
                                </Box>
                            </Box>

                            {cartItems?.length > 0 &&
                                cartItems.map((item, index) => (
                                    <Box className="item" key={index}>
                                        <Box
                                            sx={{
                                                display: "flex",
                                                alignItems: "center",
                                                gap: "5px",
                                                width: { sm: "50%", xs: "60%" },
                                            }}
                                        >
                                            <Box
                                                component={"img"}
                                                src={
                                                    item?.featured_image
                                                        ? IMAGE_URL +
                                                          "variant-product-image/" +
                                                          item?.featured_image
                                                        : IMAGE_URL +
                                                          "product-fetured-image/" +
                                                          item?.image
                                                }
                                                sx={{
                                                    width: {
                                                        md: 100,
                                                        sm: 60,
                                                        xs: 30,
                                                    },
                                                }}
                                            />
                                            <Typography
                                                sx={{
                                                    "& a": {
                                                        color: item?.quantity >= item?.inventory ? "red" : "#000",
                                                        textDecoration: "none",
                                                    },
                                                }}
                                            >
                                                <Link
                                                    href={`/product/${item?.slug}`}
                                                >
                                                    {limitWords(capitalize(item?.name), 10)}, {item?.sku}
                                                </Link>
                                            </Typography>
                                        </Box>
                                        <Box
                                            sx={{
                                                width: { sm: "10%", xs: "25%" },
                                            }}
                                        >{`${
                                            !isEmpty(
                                                item?.producttermsArr[0]
                                                    ?.termsName
                                            )
                                                ? item?.producttermsArr[0]
                                                      ?.termsName
                                                : ""
                                        }, ${
                                            !isEmpty(
                                                item?.producttermsArr[1]
                                                    ?.termsName
                                            )
                                                ? item?.producttermsArr[1]
                                                      ?.termsName
                                                : ""
                                        } `}</Box>
                                        <Box
                                            sx={{
                                                width: { sm: "15%", xs: "30%" },
                                            }}
                                        >
                                            <Box
                                                sx={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    gap: 1,
                                                }}
                                            >
                                                <IconButton
                                                    sx={{
                                                        width: 20,
                                                        height: 20,
                                                        background: "#df2828",
                                                        color: "#fff",
                                                        "&:hover": {
                                                            background:
                                                                "#df2828",
                                                        },
                                                    }}
                                                    onClick={() =>
                                                        decrementQty(
                                                            item?.id,
                                                            item?.quantity - 1
                                                        )
                                                    }
                                                >
                                                    <RemoveIcon />
                                                </IconButton>
                                                <input
                                                    onChange={(e) =>
                                                        updateItemQuantity(
                                                            item?.id,
                                                            e.target.value,
                                                            item?.inventory
                                                        )
                                                    }
                                                    value={item?.quantity}
                                                    type="number"
                                                    style={{ width: 20, color: item?.quantity >= item?.inventory ? "red" : "#000" }}
                                                    onWheel={(e) =>
                                                        e.target.blur()
                                                    }
                                                />
                                                <IconButton
                                                    sx={{
                                                        width: 20,
                                                        height: 20,
                                                        background: "#499761",
                                                        color: "#fff",

                                                        "&:hover": {
                                                            background:
                                                                "#499761",
                                                        },
                                                    }}
                                                    onClick={() =>
                                                        incrementQty(
                                                            item?.id,
                                                            item?.quantity + 1,
                                                            item?.inventory
                                                        )
                                                    }
                                                >
                                                    <AddIcon />
                                                </IconButton>
                                            </Box>
                                        </Box>
                                        <Box
                                            sx={{
                                                width: { sm: "10%", xs: "20%" },
                                            }}
                                        >
                                            £{Number((item?.sale_price * item?.quantity)).toFixed(2)}
                                        </Box>
                                        <Box
                                            sx={{
                                                width: { sm: "10%", xs: "20%" },
                                            }}
                                        >
                                            <IconButton
                                                onClick={() =>
                                                    removeItem(item?.id)
                                                }
                                            >
                                                <ClearIcon />
                                            </IconButton>
                                        </Box>
                                    </Box>
                                ))}
                        </Box>
                        <Grid container>
                            <Grid item md={8} sm={6}>
                                <Box className="coupon">
                                    <Typography sx={{ fontWeight: 600 }}>
                                        Use Coupon Code?
                                    </Typography>

                                    <Box
                                        sx={{
                                            display: "flex",
                                            alignItems: "center",
                                            gap: 1,
                                            marginTop: 1,
                                        }}
                                        component={"form"}
                                        onSubmit={handleSubmit(
                                            onHandleApplyCoupon
                                        )}
                                    >
                                        <TextField
                                            size="small"
                                            label="Offer Code"
                                            color="secondary"
                                            {...register("coupon", {
                                                required: "Enter your coupon!",
                                                type: "text",
                                            })}
                                            error={!!errors.coupon}
                                            disabled={couponApply}
                                        />
                                        <Button
                                            sx={{
                                                textTransform: "capitalize",
                                                background: COLORS.primary,
                                                color: COLORS.white,
                                                padding: "6px 15px",
                                                fontWeight: 600,
                                                width: 114,
                                                border: `1px solid ${COLORS.primary}`,
                                                "&:hover": {
                                                    background: COLORS.primary,
                                                    color: COLORS.white,
                                                },
                                            }}
                                            type="submit"
                                            disabled={couponApply}
                                        >
                                            {couponApply
                                                ? "Applied"
                                                : "Apply Code"}
                                        </Button>
                                    </Box>
                                    <Typography
                                        sx={{ color: "red", fontSize: 12 }}
                                    >
                                        {errors?.coupon?.message}
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item md={4} sm={6} xs={12}>
                                <Box className="total-box">
                                    <table border={1}>
                                        <tr>
                                            <th>Total Items</th>
                                            <th>{cartItems?.length}</th>
                                        </tr>
                                        <tr>
                                            <th>Sub Total:</th>
                                            <th>£{Number(total).toFixed(2)}</th>
                                        </tr>
                                        {couponApply && (
                                            <tr>
                                                <th style={{ color: "red" }}>Coupon: {coupon?.coupon_code}</th>
                                                <th>
                                                    <Box
                                                        sx={{
                                                            display: "flex",
                                                            alignItems:
                                                                "center",
                                                            justifyContent:
                                                                "space-between",
                                                            color: 'red'
                                                        }}
                                                    >
                                                        -£{coupon?.coupon_value}{" "}
                                                        <IconButton
                                                            title="Remove Coupon"
                                                            onClick={() =>
                                                                dispatch(
                                                                    removeCoupon()
                                                                )
                                                            }
                                                        >
                                                            <ClearIcon color="error" />
                                                        </IconButton>
                                                    </Box>
                                                </th>
                                            </tr>
                                        )}
                                        <tr>
                                            <th>Shipping</th>
                                            <th>
                                                <select
                                                    onChange={(e) => {
                                                        setShippingCost(
                                                            e.target.value
                                                        );
                                                    }}
                                                >
                                                    <option value="0">
                                                        Select Shipping Type
                                                    </option>
                                                    <option value="0">
                                                        Free shipping via Royal
                                                        Mail 48
                                                    </option>
                                                    <option value="2.99">
                                                        Royal Mail 24 Tracked:
                                                        £2.99
                                                    </option>
                                                    <option value="6.99">
                                                        Next day Delivery: £6.99
                                                    </option>
                                                </select>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <th>
                                                £
                                                {(
                                                    Number(grandTotal) +
                                                    Number(shippingCost)
                                                ).toFixed(2)}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th
                                                colSpan="2"
                                                style={{ padding: 0 }}
                                            >
                                                <Box
                                                    component="a"
                                                    href="/checkout"
                                                    sx={{
                                                        background:
                                                            COLORS.primary,
                                                        color: COLORS.white,
                                                        textDecoration: "none",
                                                        padding: "15px 40px",
                                                        fontSize: 20,
                                                        fontWeight: 600,
                                                        transition: ".8s",
                                                        display: "block",

                                                        "&:hover": {
                                                            background:
                                                                "#580d11",
                                                        },
                                                    }}
                                                >
                                                    Proceed to checkout
                                                </Box>
                                            </th>
                                        </tr>
                                        <tr style={{ background: "#ffffff" }}>
                                            <th
                                                colSpan="2"
                                                style={{ padding: 0 }}
                                            >
                                                <Box className="payment-method">
                                                    <Typography>
                                                        ACCEPTED PAYMENT METHODS
                                                    </Typography>
                                                    <Image
                                                        src={PaymentGateway}
                                                        alt="payment"
                                                    />
                                                </Box>
                                            </th>
                                        </tr>
                                    </table>
                                    {/* <Box className="list">
                                            <Typography component={"h4"}>Total Items:</Typography>
                                            <Typography>{cartItems?.length}</Typography>
                                        </Box>
                                        <Box className="list">
                                            <Typography component={"h4"}>Sub Total:</Typography>
                                            <Typography>£{total}</Typography>
                                        </Box>
                                        <Box className="list">
                                            <Typography component={"h4"}>Grand Total:</Typography>
                                            <Typography>£{total}</Typography>
                                        </Box> */}
                                </Box>
                                {/* <Box sx={{ textAlign: "center", padding: "20px 10px" }}>
                                        <Box
                                            component="a"
                                            href="/checkout"
                                            sx={{
                                            background: COLORS.primary,
                                            color: COLORS.white,
                                            textDecoration: "none",
                                            padding: "15px 40px",
                                            fontSize: 20,
                                            fontWeight: 600,
                                            transition: ".8s",
                                            display: "block",

                                            "&:hover": {
                                                background: "#580d11"
                                            }
                                            }}
                                        >
                                            Proceed to checkout
                                        </Box>
                                        </Box> */}
                                {/* <Box className="payment-method">
                                        <Typography>ACCEPTED PAYMENT METHODS</Typography>
                                        <Box component={"img"} src={PaymentGateway} alt="payment" />
                                    </Box> */}
                            </Grid>
                        </Grid>
                    </Box>
                )}
                <LoginModal
                    open={open}
                    handleClose={() => setOpen(false)}
                    setOpen={setOpen}
                    setSignOpen={setSignOpen}
                    setForgotOpen={setForgotOpen}
                />
                <SignModal
                    open={signOpen}
                    handleSignUpClose={() => setSignOpen(false)}
                    setOpen={setOpen}
                    setSignOpen={setSignOpen}
                />
                <ForgotPassModal
                    handleClose={() => setForgotOpen(false)}
                    open={forgotOpen}
                    setOpen={setOpen}
                    setForgotOpen={setForgotOpen}
                />
                <ProductAvailability open={checkProduct} data={data} handleClose={() => setCheckProduct(false)} isCartPage={true} />
            </Container>
        </Box>
    );
}

export default withPublicPage(Cart);
