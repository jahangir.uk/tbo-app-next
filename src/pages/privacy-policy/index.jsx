import { Box, Container, List, ListItem, Typography } from "@mui/material";
import BackImg from "assets/images/back.jpg";
import { usePolicyStyle } from "assets/stylesheets/policy/policyStylesheet";
import { withPublicPage } from 'components/HOC/PublicPage';
import { HeroBanner } from 'components/HeroBanner';

function PrivacyPolicy() {
    const classes = usePolicyStyle();
  return (
    <Box>
        <HeroBanner title={"Privacy Policy"} image={BackImg} />
        <Container>
          <Box className={classes.root}>
              <Typography component={'h2'}>WHO ARE WE?</Typography>
              <Typography>In this privacy policy, the terms “we”, “our”, and “us” are used to refer to the Top Brand Outlet Ltd. The controller of your information for the purposes of the Data Protection Laws will depend on which channel(s) of the Top Brand Outlet Ltd you are dealing with.</Typography>
              <Typography><strong>Company name: Top Brand Outlet Ltd Company number: 11761282 Country of Reg: United Kingdom.</strong></Typography>
              <Typography>Your Data is Protected 24/7 Top Brand Outlet is about you! Things change, but it’s nothing to worry about. We take our customers privacy and security very seriously and we are 100% committed to protecting it so you can purchase without a single worry.</Typography>
              <Typography>If you have any questions about how we protect your privacy. Please get in touch with us by letter addressed to: Top Brand Outlet Ltd, 35 Knockhall Road, Greenhithe, Kent, DA9 9EZ ; by email at <a href='mailto:contactus@topbrandoutlet.co.uk'>contactus@topbrandoutlet.co.uk</a>; or by phone on 0330 133 2599.</Typography>
              <Typography component={'h2'}>HOW WE USE YOUR DATA?</Typography>
              <Typography>We are committed to protecting your privacy. It is important that you understand how we look after your personal data and how we make sure that we meet our legal obligations to you under the data protection rules and regulations in the relevant territory (including associated guidance) (the “Data Protection Laws”).</Typography>
              <Typography>This privacy policy outlines how Top Brand Outlet will collect, use, store and share your personal data. If you have any questions in relation to this policy or generally how your personal data is processed by us, please contact our customer care team by letter addressed to: Top Brand Outlet Ltd, 35 Knockhall Road, Greenhithe, Kent, DA9 9EZ ; by email at <a href='mailto:contactus@topbrandoutlet.co.uk'>contactus@topbrandoutlet.co.uk</a>; or by phone on 0330 133 2599.</Typography>
              <Typography>This policy applies to any personal data that we collect about you when you: Visit our one of our websites or channels; Purchase products from us; Visit us onsite; Contact us, for example by telephone, email, post or through submitting a form on our websites</Typography>
              <Typography component={'h2'}>WHY DO WE NEED YOUR INFORMATION?</Typography>
              <Typography>The main purpose for which we use your information is to provide you with the products that you purchase from us and to send you offers and promotions that you might be interested in.</Typography>
              <Typography>In particular, we use your information: to provide you with the products that you have purchased from us and any receipts of purchase, including to deliver your products, and to send you order status updates; to take payments for the products that you have purchased and to give refunds or exchanges; to provide customer service and support to you; to develop and improve our products and services; to develop and improve our websites and set default options for you (such as language and currency) and to ensure that content is presented in the most effective manner; to provide you with information that you request from us or which we feel may interest you.</Typography>
              <Typography>We may send you marketing information by email, SMS or post. This can include information about new products and offers that you may find interesting; to research our customers’ preferences and trending products; to run surveys and competitions; to notify you about changes to our products; as part of our efforts to keep our online stores safe and secure; to prevent and detect fraud and crime (for example, Credit Card thefts).</Typography>
              <Typography component={'h2'}>WHAT INFORMATION DO WE COLLECT ABOUT YOU?</Typography>
              <Typography>If you have an account with us or made a purchase via website, or enter a social media competition or respond to marketing survey with us, we collect your name and contact details (including your address, telephone number, details about your order, and your billing information (where you made a purchase with us)).</Typography>
              <Typography>We also collect details of your interactions with us through our customer service via online or email. For example, when you telephone one of our customer service, we collect notes of our conversations with you and details of your query and from time to time we may record your calls for training and monitoring purposes. We also collect information about the purchases that you make, your saved items, your payment information, any complaints and comments that you make and your shopping preferences.</Typography>
              <Typography>Each time you visit one of our websites, we will also automatically collect information and personal data about your computer for system administration including, where available, your IP address, operating system and browser type. We do this to help us analyse how users use the websites (including behaviour patterns and the tracking of visits across multiple devices), to establish more about our website users and to assist us in managing your account and improving your online experience. We also collect information about your activities on our websites, for example what device you are using and what products you’re looking at.</Typography>
              <Typography>By the way of example, our website uses Microsoft clarity for analysis purposes and specifically to identify any technical issues or difficulties the users of our websites are having during the customer journey. To do this, Microsoft clarity records your mouse clicks, mouse movements and scrolling of pages on our websites. The information that we collect about you through Microsoft clarity does not include your bank details or any special categories of personal data, but could include your name, address, telephone number, email address and anything else you may insert when completing an order. The information collected by us through Microsoft clarity is used for our internal purposes only and is used to improve our websites’ usability and is stored and used only for the purposes of collecting aggregates and statistics relating to the functional performance of our websites.</Typography>
              <Typography>Please see our cookies policy for further information about what information may be automatically collected when you visit our website: <a href='https://www.topbrandoutlet.co.uk/cookie-policy'>https://www.topbrandoutlet.co.uk/cookie-policy</a> </Typography>
              <Typography>Please note that, from time to time, we will combine personal data that we receive from other sources with personal data you give to us and personal data we collect about you.</Typography>
              <Typography component={'h2'}>WHO DO WE SHARE YOUR INFORMATION WITH?</Typography>
              <Typography>We do not, and will not, sell any of your information to any third party, including your name, address, email address or credit card information. However, we do share your information with a number of select third parties to enable us to provide our products and services to you, to send marketing information and to improve our business operations as set out below.</Typography>
              <Typography>We will share your information to selected third parties to help us to provide you with products, services and to market to you. In particular, we disclose your information to:</Typography>
              <List>
                <ListItem>Companies that do things to get your purchases to you, such as payment service providers, warehouses, order packers and delivery companies</ListItem>
                <ListItem>Our trusted service providers, such as marketing agencies, advertising partners, website hosts and other third parties who provide services to help us to tailor our marketing to you.</ListItem>
                <ListItem>Credit reference agencies, law enforcement and fraud prevention agencies</ListItem>
                <ListItem>Companies approved by you, such as social media sites, including companies such as Facebook and Google; and</ListItem>
                <ListItem>We will provide third parties with aggregated but anonymised information and analytics about our customers and, before we do so, we will make sure that it does not identify you.</ListItem>
                <ListItem>We will only share your information with third party suppliers where it is necessary for them to provide us with the services we need.</ListItem>
                <ListItem>We may disclose your information to other companies in connection with any merger, acquisition, insolvency situation or otherwise, in which case we will only disclose your information so far as is necessary.</ListItem>
                <ListItem>We may also need to disclose personal data to third parties to comply with a legal or regulatory obligation, or if necessary for legal proceedings.</ListItem>
              </List>
              <Typography component={'h2'}>WHAT RIGHTS DO YOU HAVE?</Typography>
              <Typography>You have a number of rights under the Data Protection Laws in relation to the way we process your personal data, which are set out below. You may contact us using the details at the beginning of this privacy policy to exercise any of these rights.</Typography>
              <Typography>In some instances, we may be unable to carry out your request, in which case we will contact you to explain why.</Typography>
              <List>
                <ListItem>You have the right to request access to your personal data – You have the right to request confirmation that your personal data is being processed, access to your personal data (through us providing a copy) and other information about how we process your personal data.</ListItem>
                <ListItem>You have the right to ask us to rectify your personal data – You have the right to request that we rectify your personal data if it is not accurate or not complete.</ListItem>
                <ListItem>You have the right to ask us to erase your personal data – You have the right to ask us to erase or delete your personal data where there is no reason for us to continue to process your personal data. This right would apply if we no longer need to use your personal data to provide products to you, where you withdraw your consent for us to send you marketing information, or where you object to the way we process your personal data (see right 6 below).</ListItem>
                <ListItem>You have the right to ask us to restrict or block the processing of your personal data – You have the right to ask us to restrict or to block the processing of your personal data that we hold about you. This right applies where you believe the personal data is not accurate, you would rather we block the processing of your personal data rather than erase your personal data, where we don’t need to use your personal data for the purpose we collected it for but you may require it to establish, exercise or defend legal claims.</ListItem>
                <ListItem>You have the right to port your personal data – You have the right to obtain and reuse your personal data from us to reuse for your own purposes across different services. This allows you to move personal data easily to another organisation, or to request us to do this for you.</ListItem>
                <ListItem>You have the right to object to our processing of your personal data – You have the right to object to our processing of your personal data on the basis of our legitimate business interests, unless we are able to demonstrate that, on balance, our legitimate interests override your rights or we need to continue processing your personal data for the establishment, exercise or defence of legal claims.</ListItem>
                <ListItem>You have the right not to be subject to automated decisions – You have the right to object to any automated decision making, including profiling, where the decision has a legal or significant impact on you.</ListItem>
                <ListItem>You have the right to withdraw your consent- You have the right to withdraw your consent where we are relying on it to use your personal data, for example where we are relying on your consent to send you marketing information</ListItem>
              </List>
              <Typography component={'h2'}>WHAT IF YOU HAVE A COMPLAINT?</Typography>
              <Typography>If you have any concerns regarding our processing of your personal data, or are not satisfied with our handing of any request made by you, or would otherwise like to make a complaint, please contact our <a href="#">customer service</a>. So that we can do our very best to sort out the problem.</Typography>
              <Typography>You can also contact us via Post |Phone | Email at Top Brand Outlet Ltd, 35 Knockhall Road, Greenhithe, Kent, DA9 9EZ ; by email at <a href='mailto:contactus@topbrandoutlet.co.uk'>contactus@topbrandoutlet.co.uk</a>; or by phone on 0330 133 2599</Typography>
              <Typography component={'h2'}>CHANGES TO THIS PRIVACY POLICY</Typography>
              <Typography>We will take all measures necessary to communicate any changes to this privacy policy to you, and will post any updated privacy policies on this page.</Typography>
              <Typography>This policy was last reviewed and updated in Nov 2020.</Typography>
          </Box>
        </Container>
    </Box>
  )
}

export default withPublicPage(PrivacyPolicy);
