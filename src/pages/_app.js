import 'assets/styles/global.css';
import AppServiceProvider from 'services/AppServiceProvider';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

export default function App({ Component, pageProps }) {
  return (
     
      <AppServiceProvider>
        <Component {...pageProps} />  
      </AppServiceProvider>
     
  )
}
