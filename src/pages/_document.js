import { Head, Html, Main, NextScript } from 'next/document'
import Script from 'next/script'


export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <Script src='https://www.paypalobjects.com/api/checkout.js' />
        <Script type="text/javascript" src="https://x.klarnacdn.net/kp/lib/v1/api.js"></Script>
        <Script type="text/javascript" src="https://js.afterpay.com/afterpay-1.x.js"></Script>
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
