import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import { Box, Container, Typography } from "@mui/material";
import { COLORS } from "assets/styles/colors/colors";
import { withPublicPage } from "components/HOC/PublicPage";
import Link from "next/link";
import { useRouter } from "next/router";

const ThankYou = () => {
    const router = useRouter();
    const { order_id } = router?.query;
    console.log(order_id);
    return (
        <Container
            sx={{
                minHeight: "70vh",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
            }}
        >
            <Box>
                <Box sx={{ marginBottom: "20px", textAlign: "center" }}>
                    <Typography
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            marginBottom: "20px",
                            gap: 1,
                            fontSize: 50,
                            fontWeight: 600,
                            "& svg": {
                                color: "green",
                                width: "2.5em",
                                height: "2.5em",
                            },
                        }}
                    >
                        Thank You <CheckCircleOutlineIcon />
                    </Typography>
                    <Typography sx={{ fontSize:14 }}>
                        Your Order Invoice is <strong>{order_id}</strong>
                    </Typography>
                    <Typography>
                        It will be delivered in{" "}
                        <strong>2-3 business </strong>days
                    </Typography>
                </Box>
                <Box>
                    <Typography>Email us at <Link href="mailto:contactus@topbrandoutlet.co.uk">contactus@topbrandoutlet.co.uk</Link> with any questions , suggestions.</Typography>
                </Box>
                <Box sx={{ marginTop: '40px', textAlign: "center" }}>
                    <Box sx={{ 
                        textDecoration: 'none',
                        backgroundColor: COLORS.primary,
                        color: COLORS.white,
                        padding: '15px 40px',
                        borderRadius: '7px',
                        fontSize: '20px',
                        fontWeight: 600,
                        transition: '1s',
        
                        "&:hover":{
                            backgroundColor: COLORS.black
                        }
                    }} component={"a"} href="/">
                                Continue Shopping
                    </Box>
                </Box>
            </Box>
        </Container>
    );
};

export default withPublicPage(ThankYou);
