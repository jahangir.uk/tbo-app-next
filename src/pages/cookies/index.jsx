import { Box, Container, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import Paper from "@mui/material/Paper";
import heroImg from "assets/images/back1.jpg";
import { useCookiesStyle } from "assets/stylesheets/cookies/cookiesStylesheet";
import { withPublicPage } from 'components/HOC/PublicPage';
import { HeroBanner } from 'components/HeroBanner';
import Head from "next/head";
import Link from "next/link";

function Cookies() {
    const classes = useCookiesStyle();
  return (
    <Box>
        <Head>
            <title>Cookies - Top Brand Outlet UK</title>
            <meta name='description' content='You can block cookies by activating the setting on your browser
              that allows you to refuse the setting of all or some cookies.
              However, if you use your browser settings to block all cookies
              (including essential cookies) please note that some parts of this
              website may become inaccessible or not function properly.'/>
        </Head>
        <HeroBanner title={"Cookie Policy"} image={heroImg} />
        <Box className={classes.root}>
        <Container>
          <Box>
            <Typography component={"h2"}>MANAGING COOKIES</Typography>
            <Typography>
              You can block cookies by activating the setting on your browser
              that allows you to refuse the setting of all or some cookies.
              However, if you use your browser settings to block all cookies
              (including essential cookies) please note that some parts of this
              website may become inaccessible or not function properly.
            </Typography>
            <Typography>
              <strong>We use the following cookies:</strong>
            </Typography>
            <TableContainer>
              <Table size="small" sx={{ border: 1 }}>
                <TableHead>
                  <TableRow>
                    <TableCell sx={{ width: 280 }}>Type</TableCell>
                    <TableCell>Information</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <strong>Strictly Necessary Cookies</strong>
                    </TableCell>
                    <TableCell>
                      <Typography>
                        <strong>Duration: </strong>12 Months
                      </Typography>
                      <Typography>
                        <strong>Purpose:</strong>These are cookies that are
                        required for the operation of our website. These
                        include, for example, cookies that enable you to log
                        into secure areas of our website, use a shopping cart or
                        make use of billing services.
                      </Typography>
                      <Typography>
                        <strong>Example: </strong>TopBrandOutlet Website
                        Cookies, <Link href="#">Google Tag Manager</Link>
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <strong>
                        Analytical / Performance / Functionality Cookies
                      </strong>
                    </TableCell>
                    <TableCell>
                      <Typography>
                        <strong>Duration: </strong>12 Months
                      </Typography>
                      <Typography>
                        <strong>Purpose:</strong>They allow us to recognise and
                        count the number of visitors and to see how visitors
                        move around our website when they are using it. This
                        helps us to identify issues with our site and improve
                        the way our website works.
                      </Typography>
                      <Typography>
                        <strong>Example: </strong>TopBrandOutlet Website
                        Cookies, <Link href="#">Google Analytics</Link>,{" "}
                        <Link href="#">Heap</Link> , <Link href="#">Clarity</Link>
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <strong>Targeting And Remarketing Cookies</strong>
                    </TableCell>
                    <TableCell>
                      <Typography>
                        <strong>Duration: </strong>24 Months
                      </Typography>
                      <Typography>
                        <strong>Purpose:</strong>These cookies record your visit
                        to our website, the pages you have visited, and the
                        links you’ve followed. We’ll use this information to
                        make our website and the advertising displayed on it
                        more relevant to your interests. We may also share this
                        information with third parties for this purpose.
                      </Typography>
                      <Typography>
                        <strong>Example: </strong>TopBrandOutlet Website
                        Cookies, <Link href="#">Google Analytics</Link>,{" "}
                        <Link href="#">Facebook</Link> , <Link href="#">Instagram</Link>,{" "}
                        <Link href="#">Facebook Pixel</Link>
                      </Typography>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <Typography sx={{ mt: 3 }}>
              Please note that the exact names may change from time to time and
              there may be a slight delay in the table being updated. Third
              parties (including, for example, advertising networks and
              providers of external services like web traffic analysis services)
              may also use cookies, over which we have no control. These cookies
              are likely to be analytical / performance cookies or targeting
              remarketing cookies.
            </Typography>
            <Typography component={"h2"}>
              FOR A FULL LIST OF OUR COOKIES:
            </Typography>
            <TableContainer component={Paper}>
              <Table size="small" sx={{ border: 1 }}>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <strong>Name</strong>
                    </TableCell>
                    <TableCell>
                      <strong>Type</strong>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover>
                    <TableCell>Facebook</TableCell>
                    <TableCell>Social Media</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>eKomi</TableCell>
                    <TableCell>Analytical / Performance</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>Clarity</TableCell>
                    <TableCell>Analytical / Performance</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>Google Tag Manager</TableCell>
                    <TableCell>Essential</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>Google Analytics</TableCell>
                    <TableCell>Analytical / Performance</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>Google Adwords Remarketing</TableCell>
                    <TableCell>Targeting</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell>Google Adwords Conversion</TableCell>
                    <TableCell>Analytical / Performance</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <Typography sx={{ mt: 4 }}>
              You have the ability to accept or decline cookies by modifying the
              settings in your browser. The majority of browsers will allow the
              user to alter the settings used for cookies. The help menu on your
              browser will have further details. You may also notice that some
              cookies are not related to Top Brand Outlet, and are in fact
              third-party cookies. As we are unable to control these cookies, we
              would encourage you to look at the third-party’s website for more
              information about their cookies, you will find the links to the
              companies in the table above.
            </Typography>
            <Typography>
              To manage cookies, you can use your browser to do. Each browser is
              different, so check the ‘Help’ menu of your particular browser to
              learn how to change your cookie preferences.
            </Typography>

            <Typography component={"h2"}>SALE OF BUSINESS</Typography>
            <Typography>
              In the event that Top Brand Outlet is sold or integrated with
              another business, your details may be disclosed to our advisers
              and any prospective purchasers and their advisers and will be
              passed on to the new owners of the business.
            </Typography>
            <Typography component={"h2"}>UPDATING YOUR DETAILS</Typography>
            <Typography>
              If any of the information that you’ve provided to us has changed,
              e.g. you change your email address, name, or payment details, let
              us know by emailing contactus@topbrandoutlet.co.uk, or sending a
              letter to Top Brand Outlet Ltd, 35 Knockhall Road, Greenhithe,
              Kent, DA9 9EZ, UK. If you have an account with us, you can edit
              your details by logging in to My Account.
            </Typography>
            <Typography component={"h2"}>DISCLOSURE OF INFORMATION</Typography>
            <Typography>
              We comply with and are registered under the Data Protection laws
              in the United Kingdom. We take all reasonable care to prevent any
              unauthorized access to and use of your personal data. We maintain
              a responsibility to keep your information confidential and will
              only use it to offer relevant products and services to you. We’d
              also like to send you information about special features of our
              website or any other service or products we think may be of
              interest to you. If you do not wish to receive any mailings, email
              contactus@topbrandoutlet.co.uk with your email address, customer
              number or name, and address details.
            </Typography>
            <Typography component={"h2"}>
              CHANGES TO THIS COOKIES POLICY
            </Typography>
            <Typography>
              Any changes to this cookies policy in the future will be posted on
              this page and we will take all measures necessary to communicate
              any changes to this cookies policy to you. Please check back
              frequently to see any updates or changes to this cookies policy.
            </Typography>
            <Typography component={"h2"}>MORE INFORMATION</Typography>
            <Typography>
              Advertisements on our site are provided by another organization.
              Our advertising partner will serve ads that it believes are most
              likely to be of interest to you, based on information about your
              visit to this and other websites.
            </Typography>
          </Box>
        </Container>
      </Box>
    </Box>
  )
}

export default withPublicPage(Cookies);
