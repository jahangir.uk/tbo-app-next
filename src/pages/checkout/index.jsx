import {
    Box,
    Button,
    Checkbox,
    Container,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    Radio,
    RadioGroup,
    TextField,
    Typography,
    styled,
} from "@mui/material";
import { PayPalButtons, PayPalScriptProvider } from "@paypal/react-paypal-js";
import klarnaIcon from "assets/images/klarna-logo.svg";
import clearpayIcon from "assets/images/logo-clearpay-colour-131x25.png";
import { COLORS } from "assets/styles/colors/colors";
import { useCheckoutStylesheet } from "assets/stylesheets/checkout/checkoutStylesheet";
import axios from "axios";
import { CustomButton } from "components/Button";
import { withPublicPage } from "components/HOC/PublicPage";
import { ForgotPassModal } from "components/Modals/ForgotPassModal";
import { LoginModal } from "components/Modals/LoginModal";
import { ProductAvailability } from "components/Modals/ProductAvailability";
import { SignModal } from "components/Modals/SignupModal";
import AfterpayButton from "components/PaymentsForm/AfterpayButton";
import KlarnaPaymentsWidget from "components/PaymentsForm/KlarnaPaymentsWidget";
import { StripeContainer } from "components/PaymentsForm/StripeContainer";
import { countries } from "components/dummy-data/countries";
import { CART_ITEMS, COUPON_KEY, IMAGE_URL } from "helpers/config";
import { capitalize, isEmpty, limitWords } from "helpers/functions";
import useAuthUser from "hooks/useAuthUser";
import useSnackbar from "hooks/useSnackbar";
import useToken from "hooks/useToken";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { applyCoupon, removeCoupon, updateCartData } from "redux/action/cart";
import OrderServiceProvider from "services/OrderServiceProvider";
import ProductService from "services/ProductService";
import { secureStorage } from "utils/storage";
import { v4 as uuidv4 } from "uuid";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.primary,
    },
    "& .MuiOutlinedInput-root": {
        "& fieldset": {
            borderColor: COLORS.gray,
        },
        "&:hover fieldset": {
            borderColor: COLORS.black,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.primary,
        },
    },
});

const initialOptions = {
    "client-id":
        "AcXitTbBdeholTyib8UUm3p--eKm_F2ms7TECOSkeEZgrI2U3ia96JUXzePWkrVQMhVDa7JkNUP0U5To",
    currency: "GBP",
    intent: "capture",
    components: "buttons",
};

const validation = (customer, setError, clearErrors) => {
    if (
        isEmpty(customer?.first_name) ||
        isEmpty(customer?.last_name) ||
        isEmpty(customer?.email) ||
        isEmpty(customer?.street_address) ||
        isEmpty(customer?.post_code) ||
        isEmpty(customer?.phone) ||
        isEmpty(customer?.city)
    ) {
        if (isEmpty(customer?.first_name)) {
            setError("first_name", {
                type: "custom",
                message: "Billing First name is a required field.",
            });
        } else {
            clearErrors("first_name");
        }

        if (isEmpty(customer?.last_name)) {
            setError("last_name", {
                type: "custom",
                message: "Billing Last name is a required field.",
            });
        } else {
            clearErrors("last_name");
        }

        if (isEmpty(customer?.email)) {
            setError("email", {
                type: "custom",
                message: "Billing Last name is a required field.",
            });
        } else {
            clearErrors("email");
        }
        if (isEmpty(customer?.street_address)) {
            setError("street_address", {
                type: "custom",
                message: "Billing address is a required field.",
            });
        } else {
            clearErrors("street_address");
        }

        if (isEmpty(customer?.post_code)) {
            setError("post_code", {
                type: "custom",
                message: "Billing post code is a required field.",
            });
        } else {
            clearErrors("post_code");
        }

        if (isEmpty(customer?.phone)) {
            setError("phone", {
                type: "custom",
                message: "Billing phone is a required field.",
            });
        } else {
            clearErrors("phone");
        }

        if (isEmpty(customer?.city)) {
            setError("city", {
                type: "custom",
                message: "Billing city is a required field.",
            });
        } else {
            clearErrors("city");
        }

        return false;
    } else {
        return true;
    }
};

function Checkout() {
    const classes = useCheckoutStylesheet();
    const isAuthenticated = useToken();
    const dispatch = useDispatch();
    const snackbar = useSnackbar();
    const userDetails = useSelector(state=>state.auth.userDetails);
    const router = useRouter();
    const [open, setOpen] = useState(false);
    const userData = useAuthUser();
    const [signOpen, setSignOpen] = useState(false);
    const [forgotOpen, setForgotOpen] = useState(false);
    const country_list = countries;
    const { cartItems, total, grandTotal, couponApply, discount } = useSelector(
        (state) => state.cart
    );
    const [cartTotal, setCartTotal] = useState(10);
    const [loading, setLoading] = useState(false);
    const [scriptLoaded, setScriptLoaded] = useState(false);
    const [paymentType, setPaymentType] = useState(null);
    const [paidFor, setPaidFor] = useState(false);
    const [shippingCost, setShippingCost] = useState({
        value: 0,
        name: "Free Delivery"
    });
    const [agreement, setAgreement] = useState(false);
    const [sessionId, setSessionId] = useState(null);
    const [clientToken, setClientToken] = useState(null);
    const [checkProduct, setCheckProduct] = useState(false);
    const [data, setData] = useState(null);

    const [customerInfo, setCustomerInfo] = useState({
        first_name: userDetails?.first_name ?? "",
        last_name: userDetails?.last_name ?? "",
        email: userDetails?.email ?? "",
        street_address: userDetails?.street_address ?? "",
        street_address2: userDetails?.street_address2 ?? "",
        street_address3: userDetails?.street_address3 ?? "",
        post_code: userDetails?.post_code ?? "",
        phone: userDetails?.phone ?? "",
        country: userDetails?.country ?? "GB",
        city: userDetails?.city ?? "",
        county: userDetails?.county ?? "",
        agreement: agreement,
    });

    const coupon = JSON.parse(secureStorage.get(COUPON_KEY));

    const handleOrderPlace = async (paymentInfo) => {
        const payload = {
            cart_data: {
                items: cartItems,
                total,
                grand_total: grandTotal,
            },
            coupon_data: {
                coupon_code: coupon?.coupon_code ?? null,
                coupon_value: coupon?.coupon_value ?? null,
            },
            payment_info: paymentInfo,
            customer_info: customerInfo,
            billing_address: {
                email: userDetails?.billing_email ?? customerInfo?.email,
                street_address: userDetails?.billing_street_address ?? customerInfo?.street_address,
                street_address2: userDetails?.billing_street_address2 ?? customerInfo?.street_address2,
                street_address3: userDetails?.billing_street_address3 ?? customerInfo?.street_address3,
                postal_code: userDetails?.billing_post_code ?? customerInfo?.post_code,
                city: userDetails?.billing_city ?? customerInfo?.city,
                phone: userDetails?.billing_phone ?? customerInfo?.phone,
                country: userDetails?.billing_country ?? customerInfo?.country,
            },
            shipping_address: {
                email: userDetails?.shipping_email ?? customerInfo?.email,
                street_address: userDetails?.shipping_street_address ?? customerInfo?.street_address,
                street_address2: userDetails?.shipping_street_address2 ?? customerInfo?.street_address2,
                street_address3: userDetails?.shipping_street_address3 ?? customerInfo?.street_address3,
                postal_code: userDetails?.shipping_post_code ?? customerInfo?.post_code,
                city: userDetails?.shipping_city ?? customerInfo?.city,
                phone: userDetails?.shipping_phone ?? customerInfo?.phone,
                country: userDetails?.shipping_country ?? customerInfo?.country,
                shipping_method: shippingCost?.name
            },
        };

        const res = await OrderServiceProvider.Commands.orderPlace(payload);
        console.log(res);

        if(res?.status === "success"){
            const data = {
                cartItems: [],
                total: 0,
                grandTotal: 0,
                discount:0 ,
                couponApply: false
            }
            router.push({
                pathname: "/thank-you",
                query: {order_id: res?.order?.invoice}
            });
            secureStorage.remove(CART_ITEMS);
            dispatch(updateCartData(data));
        } else{
            snackbar(res?.data?.fieldErrors.username?.message, {
                variant: "error"
              });
        }
   
    };

    const loadPaypalScript = () => {
        if (window.paypal) {
            setScriptLoaded(true);
            return;
        }
        const script = document.createElement("script");
        script.src =
            "https://www.paypal.com/sdk/js?client-id=AcXitTbBdeholTyib8UUm3p--eKm_F2ms7TECOSkeEZgrI2U3ia96JUXzePWkrVQMhVDa7JkNUP0U5To&currency=GBP";
        script.type = "text/javascript";
        script.async = true;
        script.onload = () => setScriptLoaded(true);
        document.body.appendChild(script);
    };

    const onHandleApplyCoupon = async (data) => {
        const payload = {
            coupon_code: data?.coupon,
            cart_items: cartItems,
            cart_total: Number(cartTotal).toFixed(2),
        };
        ProductService.Commands.getCouponData(payload).then((response) => {
            console.log(response);
            if (response?.status === "success") {
                const coupon_value = {
                    coupon_code: data?.coupon ?? null,
                    coupon_value: response?.discount_val ?? null,
                };
                dispatch(applyCoupon(response));
                secureStorage.set(COUPON_KEY, JSON.stringify(coupon_value));
            }
        });
        console.log(payload); // TODO Call coupon api
        // setLoading(true);
        reset();
    };

    const {
        handleSubmit,
        register,
        formState: { errors },
        reset,
        clearErrors,
        setError,
    } = useForm();

    const PaypalButton = ({ amount }) => {
        return (
            <Box sx={{ width: "100%" }}>
                <PayPalScriptProvider options={initialOptions}>
                    <PayPalButtons
                        createOrder={(data, actions) => {
                            if(!isAuthenticated){
                                setOpen(true);
                                return false;
                            }
                            const isValid = validation(
                                customerInfo,
                                setError,
                                clearErrors
                            );

                            if (!isValid) {
                                return false;
                            }

                            

                            if (!agreement) {
                                setError("agreement", {
                                    type: "custom",
                                    message:
                                        "Billing agreement is a required field.",
                                });
                                return false;
                            }

                            return actions.order.create({
                                purchase_units: [
                                    {
                                        amount: {
                                            value: amount,
                                        },
                                    },
                                ],
                            });
                        }}
                        onApprove={(data, actions) => {
                            return actions.order.capture().then((details) => {
                                const payment_info = {
                                    transition_id: details?.id,
                                    status:
                                        details?.status === "COMPLETED"
                                            ? "success"
                                            : "failed",
                                    payer_name:
                                        details?.payer?.name?.given_name,
                                    email: details?.payer?.email_address,
                                    payer_id: details?.payer?.payer_id,
                                    payment_method: "paypal",
                                };
                                handleOrderPlace(payment_info);
                            });
                        }}
                    />
                </PayPalScriptProvider>
            </Box>
        );
    };

    const handleApprove = (orderId) => {
        // Call backend function to fulfill order

        // if response is success
        setPaidFor(true);
        // Refresh user's account or subscription status

        // if response is error
        // alert("Your payment was processed successfully. However, we are unable to fulfill your purchase. Please contact us at support@designcode.io for assistance.");
    };

    const discountObj = {
        name: "Discount",
        quantity: 1,
        reference: "12-345-ABC-discount",
        tax_rate: 0,
        total_amount: -Number(discount).toFixed(2) * 100,
        total_tax_amount: 0,
        type: "physical",
        unit_price: -Number(discount).toFixed(2) * 100,
        producttermsArr: [],
    };

    const klarnaPaymentSubmit = () => {
        const data = {
            purchase_country: "GB",
            purchase_currency: "GBP",
            locale: "en-UK",
            billing_address: {
                given_name: customerInfo?.first_name ?? "John",
                family_name: customerInfo?.last_name ?? "Doe",
                email: customerInfo?.email ?? "john@doe.com",
                title: "Mr",
                street_address: customerInfo?.street_address ?? "Lombard St 10",
                street_address2: customerInfo?.street_address2 ?? "Apt 214",
                postal_code: customerInfo?.post_code ?? "90210",
                city: customerInfo?.city ?? "Beverly Hills",
                region: "CA",
                phone: customerInfo?.phone ?? "07700 900508",
                country: customerInfo?.country ?? "GB",
            },

            order_amount: cartTotal * 100,
            order_tax_amount: 0,
            order_lines: [
                {
                    type: "physical",
                    reference: "19-402-USA",
                    name: "Battery Power Pack",
                    quantity: 3,
                    unit_price: 11.99 * 100,
                    tax_rate: 0,
                    total_amount: 11.99 * 100,
                    total_discount_amount: 0,
                    total_tax_amount: 0,
                    product_url: "https://www.estore.com/products/f2a8d7e34",
                    image_url: "https://www.exampleobjects.com/logo.png",
                },
            ],
        };

        const isValid = validation(customerInfo, setError, clearErrors);

        if (!isValid) {
            return false;
        }

        window.Klarna.Payments.authorize(
            {
                payment_method_category: "klarna",
            },
            {
                purchase_country: "GB",
                purchase_currency: "GBP",
                locale: "en-UK",
                billing_address: {
                    given_name: customerInfo?.first_name ?? "John",
                    family_name: customerInfo?.last_name ?? "Doe",
                    email: customerInfo?.email ?? "john@doe.com",
                    title: "Mr",
                    street_address:
                        customerInfo?.street_address ?? "Lombard St 10",
                    street_address2: customerInfo?.street_address2 ?? "Apt 214",
                    postal_code: customerInfo?.post_code ?? "90210",
                    city: customerInfo?.city ?? "Beverly Hills",
                    region: "CA",
                    phone: customerInfo?.phone ?? "07700 900508",
                    country: customerInfo?.country ?? "GB",
                },

                order_amount: cartTotal * 100,
                order_tax_amount: 0,
                order_lines: cartItems.concat(discountObj),
                customer: {
                    date_of_birth: "1970-01-01",
                },
            },
            async function (res) {
                console.log(res);

                const response = await axios.post(`/api/create-klarna-order`, {
                    authorization_token: res?.authorization_token,
                    customer: customerInfo,
                    cartTotal: Number(cartTotal * 100),
                    cartItems: cartItems.concat(discountObj),
                });

                console.log(response);
            }
        );
    };

    async function handleCheckout() {
        const amount = cartTotal * 100;

        const { data } = await axios.post("/api/create-klarna-session", {
            order_amount: Number(amount.toFixed(2)),
            order_tax_amount: 0,
            uu_id: uuidv4(),
            order_lines: cartItems.concat(discountObj),
        });
        const { session_id, client_token } = data?.data;
        // console.log(client_token)
        setSessionId(session_id);
        setClientToken(client_token);
    }

    const checkAvailability = async () => {
        const res = await ProductService.Commands.checkProductAvailability(
            cartItems
        );
        console.log(res);
        if (res?.status === "error") {
            setData(res?.cart);
            setCheckProduct(true);
        }
    };

    useEffect(() => {
        loadPaypalScript();
        if (!isAuthenticated) {
            setOpen(!open);
        }
    }, []);

    useEffect(() => {
        const script = document.createElement("script");
        script.src = "https://x.klarnacdn.net/kp/lib/v1/api.js";
        script.async = true;
        document.body.appendChild(script);
    }, []);

    useEffect(() => {
        if (!isEmpty(cartItems)) {
            checkAvailability();
        }
    }, [cartItems]);

    useEffect(() => {
        setCartTotal(total);
    }, [total]);

    useEffect(() => {
        if(userDetails){
            setCustomerInfo({
                first_name: userDetails?.first_name ?? "",
                last_name: userDetails?.last_name ?? "",
                email: userDetails?.email ?? "",
                street_address: userDetails?.street_address ?? "",
                street_address2: userDetails?.street_address2 ?? "",
                street_address3: userDetails?.street_address3 ?? "",
                post_code: userDetails?.post_code ?? "",
                phone: userDetails?.phone ?? "",
                country: userDetails?.country ?? "GB",
                city: userDetails?.city ?? "",
                county: userDetails?.county ?? "",
                agreement: agreement,
            })
        }
       
    }, [userDetails]);

  

    return (
        <Box className={classes.root}>
            <Head>
                <title>Checkout page - Top Brand Outlet UK</title>
            </Head>

            <Container>
                {isEmpty(cartItems) ? (
                    <Box className="empty-cart">
                        <Typography component={"h2"}>
                            Your cart is empty
                        </Typography>
                        <Typography> 
                            Fill me up with some unbeatable bargains! <br />
                            {`We're adding new styles daily from the biggest brands`}
                            <br />
                            All with up to 75% off the RRP.
                        </Typography>
                        <Box component={"a"} href="/">
                            Continue Shopping
                        </Box>
                    </Box>
                ) : (
                    <>
                        <Box
                            component={"span"}
                            sx={{
                                cursor: "pointer",
                                textDecoration: "underline",
                                fontWeight: 600,
                                textTransform: "uppercase",
                                padding: 1,
                                "&:hover": {
                                    background: COLORS.primary,
                                    color: COLORS.white,
                                },
                            }}
                            onClick={() => setOpen(!open)}
                        >
                            {isAuthenticated ? "" : "Login and checkout faster"}
                        </Box>
                        <Grid container>
                            <Grid item md={7} sm={6} xs={12}>
                                <Box className="form">
                                    <Grid container>
                                        <Grid item md={12}>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "49%",
                                                }}
                                            >
                                                <CssTextField
                                                    id="first-name"
                                                    required
                                                    label="First Name"
                                                    placeholder="Jhon"
                                                    fullWidth
                                                    value={
                                                        customerInfo?.first_name
                                                    }
                                                    size="small"
                                                    {...register("first_name", {
                                                        type: "text",
                                                    })}
                                                    error={!!errors.first_name}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            first_name:
                                                                e.target.value,
                                                        });
                                                        clearErrors(
                                                            "first_name"
                                                        );
                                                    }}
                                                    helperText={
                                                        errors?.first_name
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginLeft: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "49%",
                                                }}
                                            >
                                                <CssTextField
                                                    required
                                                    label="Last Name"
                                                    placeholder=" Due"
                                                    fullWidth
                                                    value={customerInfo?.last_name}
                                                    size="small"
                                                    {...register("last_name", {
                                                        type: "text",
                                                    })}
                                                    error={!!errors.last_name}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            last_name:
                                                                e.target.value,
                                                        });
                                                        clearErrors(
                                                            "last_name"
                                                        );
                                                    }}
                                                    helperText={
                                                        errors?.last_name
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    required
                                                    label="Email"
                                                    placeholder="example@gmail.com"
                                                    fullWidth
                                                    value={
                                                        customerInfo?.email
                                                    }
                                                    size="small"
                                                    {...register("email", {
                                                        type: "email",
                                                    })}
                                                    error={!!errors.email}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            email: e.target
                                                                .value,
                                                        });
                                                        clearErrors("email");
                                                    }}
                                                    helperText={
                                                        errors?.email
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>

                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    label="Street address"
                                                    placeholder="House number and street name"
                                                    fullWidth
                                                    required
                                                    value={
                                                        customerInfo?.street_address
                                                    }
                                                    multiline
                                                    size="small"
                                                    {...register(
                                                        "street_address",
                                                        {
                                                            type: "text",
                                                        }
                                                    )}
                                                    error={
                                                        !!errors.street_address
                                                    }
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            street_address:
                                                                e.target.value,
                                                        });
                                                        clearErrors(
                                                            "street_address"
                                                        );
                                                    }}
                                                    helperText={
                                                        errors?.street_address
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    multiline
                                                    label=""
                                                    placeholder="Apartment, suite, unit, etc.(optional)"
                                                    fullWidth
                                                    value={
                                                        customerInfo?.street_address2
                                                    }
                                                    size="small"
                                                    {...register(
                                                        "street_address2",
                                                        {
                                                            type: "text",
                                                        }
                                                    )}
                                                    error={
                                                        !!errors.street_address2
                                                    }
                                                    onChange={(e) =>
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            street_address2:
                                                                e.target.value,
                                                        })
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    multiline
                                                    label=""
                                                    placeholder="Apartment, suite, unit, etc.(optional)"
                                                    fullWidth
                                                    value={
                                                        customerInfo?.street_address3
                                                    }
                                                    size="small"
                                                    {...register(
                                                        "street_address3",
                                                        {
                                                            type: "text",
                                                        }
                                                    )}
                                                    error={
                                                        !!errors.street_address3
                                                    }
                                                    onChange={(e) =>
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            street_address3:
                                                                e.target.value,
                                                        })
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    required
                                                    label="Postcode"
                                                    fullWidth
                                                    size="small"
                                                    value={
                                                        customerInfo?.post_code
                                                    }
                                                    {...register("post_code", {
                                                        type: "text",
                                                    })}
                                                    error={!!errors.post_code}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            post_code:
                                                                e.target.value,
                                                        });
                                                        clearErrors(
                                                            "post_code"
                                                        );
                                                    }}
                                                    helperText={
                                                        errors?.post_code
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    label="Phone"
                                                    required
                                                    fullWidth
                                                    value={
                                                        customerInfo?.phone
                                                    }
                                                    size="small"
                                                    {...register("phone", {
                                                        type: "text",
                                                    })}
                                                    error={!!errors.phone}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            phone: e.target
                                                                .value,
                                                        });
                                                        clearErrors("phone");
                                                    }}
                                                    helperText={
                                                        errors?.phone
                                                            ?.message ?? ""
                                                    }
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "99%",
                                                }}
                                            >
                                                <CssTextField
                                                    required
                                                    select
                                                    label="Country/Region"
                                                    placeholder="House number and street name"
                                                    fullWidth
                                                    size="small"
                                                    {...register(
                                                        "country_code",
                                                        {
                                                            type: "text",
                                                        }
                                                    )}
                                                    error={
                                                        !!errors.country_code
                                                    }
                                                    value={customerInfo?.country_code ?? "GB"}
                                                    SelectProps={{
                                                        native: true,
                                                    }}
                                                    onChange={(e) =>
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            country_code:
                                                                e.target.value,
                                                        })
                                                    }
                                                >
                                                    {country_list?.length > 0 &&
                                                        country_list.map(
                                                            (item, index) => (
                                                                <option
                                                                    value={
                                                                        item?.code
                                                                    }
                                                                    key={index}
                                                                >{`${item?.name} (${item?.code})`}</option>
                                                            )
                                                        )}
                                                </CssTextField>
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "49%",
                                                }}
                                            >
                                                <CssTextField
                                                    required
                                                    label="Town / City"
                                                    fullWidth
                                                    value={
                                                        customerInfo?.city
                                                    }
                                                    size="small"
                                                    {...register("city", {
                                                        type: "text",
                                                    })}
                                                    error={!!errors.city}
                                                    onChange={(e) => {
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            city: e.target
                                                                .value,
                                                        });
                                                        clearErrors("city");
                                                    }}
                                                />
                                            </FormControl>
                                            <FormControl
                                                variant="outlined"
                                                sx={{
                                                    marginRight: {
                                                        md: "4px",
                                                        xs: 0,
                                                    },
                                                    marginTop: 2,
                                                    width: "49%",
                                                }}
                                            >
                                                <CssTextField
                                                    label="County (optional)"
                                                    placeholder=""
                                                    value={
                                                        customerInfo?.county
                                                    }
                                                    fullWidth
                                                    size="small"
                                                    onChange={(e) =>
                                                        setCustomerInfo({
                                                            ...customerInfo,
                                                            county: e.target
                                                                .value,
                                                        })
                                                    }
                                                />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12}>
                                <Box className="total-box">
                                    <Typography component={"h2"}>
                                        Order Summary
                                    </Typography>
                                    <Box className="list-item">
                                        {cartItems?.length > 0 &&
                                            cartItems.map((item, index) => (
                                                <Box
                                                    className="item"
                                                    key={index}
                                                >
                                                    <Box
                                                        sx={{
                                                            display: "flex",
                                                            alignItems:
                                                                "center",
                                                            gap: "5px",
                                                            width: {
                                                                sm: "55%",
                                                                xs: "60%",
                                                            },
                                                        }}
                                                    >
                                                        <Box
                                                            component={"img"}
                                                            src={
                                                                item?.featured_image
                                                                    ? IMAGE_URL +
                                                                      "/variant-product-image/" +
                                                                      item?.featured_image
                                                                    : IMAGE_URL +
                                                                      "/product-fetured-image/" +
                                                                      item?.image
                                                            }
                                                            sx={{
                                                                width: {
                                                                    md: 50,
                                                                    sm: 30,
                                                                    xs: 30,
                                                                },
                                                            }}
                                                        />
                                                        <Typography
                                                            sx={{
                                                                "& a": {
                                                                    color: "#000000",
                                                                    fontSize: 13,
                                                                    textDecoration:
                                                                        "none",
                                                                },
                                                            }}
                                                        >
                                                            <Link
                                                                href={`/product/${item?.slug}`}
                                                            >
                                                                {capitalize(
                                                                    limitWords(
                                                                        item?.name,
                                                                        4
                                                                    )
                                                                )}
                                                                <br />
                                                                <span
                                                                    style={{
                                                                        fontSize: 11,
                                                                    }}
                                                                >{`${
                                                                    item
                                                                        ?.producttermsArr[0]
                                                                        ?.termsName ??
                                                                    ""
                                                                }, ${
                                                                    item
                                                                        ?.producttermsArr[1]
                                                                        ?.termsName ??
                                                                    ""
                                                                } `}</span>
                                                            </Link>
                                                        </Typography>
                                                    </Box>
                                                    <Box>
                                                        {item?.sale_price}
                                                    </Box>

                                                    <Box
                                                        sx={{
                                                            width: {
                                                                sm: "15%",
                                                                xs: "30%",
                                                            },
                                                        }}
                                                    >
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                gap: 1,
                                                            }}
                                                        >
                                                            <input
                                                               
                                                                value={
                                                                    item?.quantity
                                                                }
                                                                type="number"
                                                                style={{
                                                                    width: 20,
                                                                }}
                                                                onWheel={(e) =>
                                                                    e.target.blur()
                                                                }
                                                                disabled
                                                            />
                                                        </Box>
                                                    </Box>
                                                    <Box
                                                        sx={{
                                                            width: {
                                                                sm: "10%",
                                                                xs: "20%",
                                                            },
                                                        }}
                                                    >
                                                        £
                                                        {item?.sale_price *
                                                            item?.quantity}
                                                    </Box>
                                                </Box>
                                            ))}
                                    </Box>
                                    <Box className="coupon">
                                        <Box
                                            sx={{
                                                display: "flex",
                                                alignItems: "center",
                                                gap: 1,
                                                margin: "10px 0",
                                                padding: 0,
                                            }}
                                            component={"form"}
                                            onSubmit={handleSubmit(
                                                onHandleApplyCoupon
                                            )}
                                        >
                                            <TextField
                                                sx={{
                                                    width: {
                                                        md: 326,
                                                        sm: 250,
                                                        xs: 130,
                                                    },
                                                }}
                                                size="small"
                                                label="Offer Code"
                                                color="secondary"
                                                {...register("coupon", {
                                                    required:
                                                        "Enter your coupon!",
                                                    type: "text",
                                                })}
                                                error={!!errors.coupon}
                                                disabled={couponApply}
                                            />
                                            <Button
                                                sx={{
                                                    textTransform: "capitalize",
                                                    background: COLORS.primary,
                                                    color: COLORS.white,
                                                    padding: "6px 15px",
                                                    fontWeight: 600,
                                                    width: 114,
                                                    border: `1px solid ${COLORS.primary}`,
                                                    "&:hover": {
                                                        background:
                                                            COLORS.primary,
                                                        color: COLORS.white,
                                                    },
                                                }}
                                                type="submit"
                                                disabled={couponApply}
                                            >
                                                {couponApply
                                                    ? "Applied"
                                                    : "Apply Code"}
                                            </Button>
                                        </Box>
                                        <Typography
                                            sx={{ color: "red", fontSize: 12 }}
                                        >
                                            {errors?.coupon?.message}
                                        </Typography>
                                    </Box>

                                    <table border={1} style={{ marginTop: 5 }}>
                                        <tbody>
                                            <tr>
                                                <th>Total Items</th>
                                                <th>{cartItems?.length}</th>
                                            </tr>
                                            <tr>
                                                <th>Sub Total:</th>
                                                <th>£{total}</th>
                                            </tr>
                                            {couponApply && (
                                                <tr>
                                                    <th>
                                                        Coupon:{" "}
                                                        {coupon?.coupon_code}
                                                    </th>
                                                    <th>
                                                        -£{coupon?.coupon_value}{" "}
                                                        <span
                                                            style={{
                                                                cursor: "pointer",
                                                                textDecoration:
                                                                    "underline",
                                                            }}
                                                            onClick={() =>
                                                                dispatch(
                                                                    removeCoupon()
                                                                )
                                                            }
                                                        >
                                                            Remove
                                                        </span>
                                                    </th>
                                                </tr>
                                            )}

                                            <tr>
                                                <th>Shipping</th>
                                                <th>
                                                    <select
                                                        onChange={(e) => {
                                                            setShippingCost({
                                                                name: e.target.value === "6.99" ? "Next Day" : e.target.value === "2.99" ? "24 Tracked" : "Free Shipping",
                                                                value: e.target?.value,
                                                            });
                                                        }}
                                                    >
                                                        <option value="">
                                                            Select Shipping Type
                                                        </option>
                                                        <option value="0">
                                                            Free shipping via
                                                            Royal Mail 48
                                                        </option>
                                                        <option value="2.99">
                                                            Royal Mail 24
                                                            Tracked: £2.99
                                                        </option>
                                                        <option value="6.99">
                                                            Next day Delivery:
                                                            £6.99
                                                        </option>
                                                    </select>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Total</th>
                                                <th>
                                                    £
                                                    {(
                                                        Number(grandTotal) +
                                                        Number(shippingCost?.value)
                                                    ).toFixed(2)}
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Box>
                            </Grid>
                            <Grid item md={12}>
                                <Box className="payment-box">
                                    <Typography component={"h2"}>
                                        Payment Option
                                    </Typography>
                                    <Box sx={{ width: "100%" }}>
                                        <FormControl fullWidth>
                                            <RadioGroup
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                value=""
                                                name="radio-buttons-group"
                                            >
                                                <FormControlLabel
                                                    onChange={(e) =>
                                                        setPaymentType(
                                                            e.target.value
                                                        )
                                                    }
                                                    value="stripe"
                                                    control={
                                                        <Radio size="small" checked={paymentType === "stripe"} />
                                                    }
                                                    label="Credit Card (Stripe)"
                                                />
                                                {paymentType === "stripe" && (
                                                    // <MyCheckout amount={cartTotal} />
                                                    <StripeContainer
                                                        amount={cartTotal}
                                                    />
                                                )}
                                                <Divider />
                                                <FormControlLabel
                                                    onChange={(e) =>
                                                        setPaymentType(
                                                            e.target.value
                                                        )
                                                    }
                                                    value="clearpay"
                                                    control={
                                                        <Radio size="small" checked={paymentType === "clearpay"} />
                                                    }
                                                    label={
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                gap: 1,
                                                            }}
                                                        >
                                                            <Typography>
                                                                Clearpay
                                                            </Typography>
                                                            <Image
                                                                src={
                                                                    clearpayIcon.src
                                                                }
                                                                alt="Clear Pay"
                                                                width={120}
                                                                height={25}
                                                            />
                                                        </Box>
                                                    }
                                                />
                                                {paymentType === "clearpay" && (
                                                    <Box sx={{ padding: "10px", height: 150 }}>
                                                        <AfterpayButton total={cartTotal} />
                                                    </Box>
                                                )}
                                                <Divider />
                                                <FormControlLabel
                                                    onChange={(e) => {
                                                        setPaymentType(
                                                            e.target.value
                                                        );
                                                        handleCheckout();
                                                    }}
                                                    value="pay30"
                                                    control={
                                                        <Radio size="small" checked={paymentType === "pay30"} />
                                                    }
                                                    label={
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                gap: 1,
                                                            }}
                                                        >
                                                            <Typography>
                                                                Pay in 30 days
                                                            </Typography>
                                                            <Image
                                                                src={
                                                                    klarnaIcon.src
                                                                }
                                                                alt="Klarna"
                                                                width={40}
                                                                height={40}
                                                            />
                                                        </Box>
                                                    }
                                                />
                                                {paymentType === "pay30" && (
                                                    <Box>
                                                        {sessionId && (
                                                            <KlarnaPaymentsWidget
                                                                sessionId={
                                                                    sessionId
                                                                }
                                                                token={
                                                                    clientToken
                                                                }
                                                            />
                                                        )}
                                                    </Box>
                                                )}
                                                <Divider />
                                                <FormControlLabel
                                                    onChange={(e) =>
                                                        setPaymentType(
                                                            e.target.value
                                                        )
                                                    }
                                                    value="paypal"
                                                    control={
                                                        <Radio size="small" checked={paymentType === "paypal"} />
                                                    }
                                                    label="PayPal"
                                                />
                                            </RadioGroup>
                                        </FormControl>
                                        <Divider />
                                        <Box className="terms">
                                            <Typography>
                                                Your personal data will be used
                                                to process your order, support
                                                your experience throughout this
                                                website, and for other purposes
                                                described in our{" "}
                                                <Link href="/privacy-policy">
                                                    privacy policy
                                                </Link>
                                                .
                                            </Typography>
                                            <FormControlLabel
                                                onClick={() => {
                                                    setAgreement(!agreement);
                                                    clearErrors("agreement");
                                                }}
                                                {...register("agreement")}
                                                error={!!errors.agreement}
                                                control={
                                                    <Checkbox
                                                        size="small"
                                                        checked={agreement}
                                                    />
                                                }
                                                label={
                                                    <Box>
                                                        I have read and agree to
                                                        the website{" "}
                                                        <Link
                                                            href={
                                                                "/terms-and-condition"
                                                            }
                                                        >
                                                            terms and conditions
                                                        </Link>{" "}
                                                        *
                                                    </Box>
                                                }
                                            />
                                            <FormHelperText
                                                sx={{ color: "#d32f2f", marginTop: "-7px", marginLeft: "25px" }}
                                            >
                                                {errors?.agreement?.message ??
                                                    ""}
                                            </FormHelperText>
                                        </Box>
                                        {paymentType === "paypal" && (
                                            // <PaypalButton  amount={cartTotal} />

                                            <PaypalButton amount={cartTotal} />
                                        )}
                                        {paymentType === "pay30" && (
                                            <CustomButton
                                                text={"Place Order"}
                                                onClick={klarnaPaymentSubmit}
                                            />
                                        )}
                                    </Box>
                                </Box>
                            </Grid>
                        </Grid>
                    </>
                )}
            </Container>
            <LoginModal
                open={open}
                handleClose={() => setOpen(false)}
                setOpen={setOpen}
                setSignOpen={setSignOpen}
                setForgotOpen={setForgotOpen}
            />
            <SignModal
                open={signOpen}
                handleSignUpClose={() => setSignOpen(false)}
                setOpen={setOpen}
                setSignOpen={setSignOpen}
            />
            <ForgotPassModal
                handleClose={() => setForgotOpen(false)}
                open={forgotOpen}
                setOpen={setOpen}
                setForgotOpen={setForgotOpen}
            />
            <ProductAvailability
                open={checkProduct}
                data={data}
                handleClose={() => setCheckProduct(false)}
                isCartPage={false}
            />
        </Box>
    );
}

export default withPublicPage(Checkout);
