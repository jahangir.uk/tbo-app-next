import {
    css
} from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useScrollToTopStyle = () => ({
    root: css ` 
        position: relative;

        .icon-position{
            position: fixed;
            bottom: 40px;
            right: 25px;
            z-index: 20;
          }
          .icon-style{
            background-color: ${COLORS.primary};
            border: 2px solid ${COLORS.black};
            border-radius: 50%;
            height: 30px;
            width: 30px;
            color: #fff;
            cursor: pointer;
            animation: movebtn 3s ease-in-out infinite;
            transition: all .5s ease-in-out;
          }
          .icon-style:hover{
            animation: none;
            background: #fff;
            color: #551B54;
            border: 2px solid #551B54;
          }

    `,
});