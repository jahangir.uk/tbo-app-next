import {
    css
} from "@emotion/css";

export const useFilterStyle = (width) => ({
    root: css `
       width: ${width ?? '100%'};
        .filter{
            h2{
                color: #000000;
                font-weight: bold;
                line-height: normal;
                margin: 0 0 10px;
                font-size: 14px;
                font-family: "Open Sans",sans-serif!important;
            }

            p{
                font-size: 12px;
                font-weight: normal;
                font-family: "Open Sans",sans-serif!important;
                margin-bottom: 10px;
            }

            .css-1elwnq4-MuiPaper-root-MuiAccordion-root{
                box-shadow: none;

                .MuiAccordionSummary-root{
                    padding: 0;
                    min-height: 40px;
                    .MuiAccordionSummary-content{
                        p{
                            color: #363636;
                            font-size: 1em;
                            line-height: 10px;
                        }
                    }
                }
            }

            .MuiCheckbox-root{
                svg{
                    fill: #76b3ef;
                }
            }

            .MuiTypography-body1{
                font-size: 12px;
                color: #363636;
            }

            .css-15v22id-MuiAccordionDetails-root{
                padding: 0;
            }

            .css-o4b71y-MuiAccordionSummary-content{
                margin: 0;

                p{
                    margin-bottom: 0;
                }
            }

            .css-o4b71y-MuiAccordionSummary-content.Mui-expanded{
                margin: 10px 0;
            }
        }
    `
})