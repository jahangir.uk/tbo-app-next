import { css } from "@emotion/css";

export const useMyAccountStyle = () => ({
    root: css `
        padding: 50px 0;

        .css-1h9z7r5-MuiButtonBase-root-MuiTab-root, .css-1q2h7u5{
           
            min-height: 30px;
            border-radius: 5px;
            margin-right: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
            text-transform: capitalize;
            font-weight: 600;
        }

        .css-1ocpf3a-MuiButtonBase-root-MuiTab-root{
            min-height: 30px;
            text-transform: capitalize;
            font-weight: 600;
        }

        .Mui-selected{
            background: #017aff;
            color: #ffffff;
            font-weight: 600;
        }

        .css-19kzrtu{
            width: 100%;
        }

        th{
            font-weight: 600;
        }
        
    `,

    details: css `
        
        .form{

            button{
                background:#017aff;
                color: #ffffff;
            }
        }
        
        .details{
           background-color: rgb(231, 235, 240);
           width: 100%;
           padding: 10px; 
           border-radius: 10px;

            h1{
                display: flex;
                justify-content: space-between;
                align-items: center;
                font-size: 30px;
                line-height: .5;

                span{
                    text-decoration: underline;
                    color: #017aff;
                    cursor: pointer;
                    font-size: 18px;
                }
            }

            .personal-info{
                
                .item{
                    display: flex;
                    padding: 10px 0;

                    .heading{
                        width: 100px;
                    }
                }
            }
        }
    `,
});