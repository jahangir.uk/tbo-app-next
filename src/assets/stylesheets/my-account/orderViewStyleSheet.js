import { css } from "@emotion/css";

export const useViewOrderStyle = () => ({
    root: css `
        padding: 50px 0;

        .details{
            background-color: rgb(231, 235, 240);
            width: 98%;
            padding: 15px; 
            margin-top: 20px;
            border-radius: 10px;
 
             h1{
                 display: flex;
                 justify-content: space-between;
                 align-items: center;
                 font-size: 30px;
                 line-height: .5;
 
                 span{
                     text-decoration: underline;
                     color: #017aff;
                     cursor: pointer;
                     font-size: 18px;
                 }
             }
 
             .personal-info{
                 
                 .item{
                     display: flex;
                     padding: 10px 0;
 
                     .heading{
                         width: 100px;
                     }
                 }
             }
         }
        
    `,

});