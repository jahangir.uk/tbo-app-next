import {
    css
} from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useCheckoutStylesheet = () => ({
    root: css `
        
        padding: 50px 30px;

        .contact-form{
            .css-wgai2y-MuiFormLabel-asterisk{
                color: red !important;
            }
        }

        .empty-cart{
            min-height: 600px;
            display: flex;
            border: 2px solid ${COLORS.primary};
            padding: 20px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center;

            h2{
                font-size: 36px;
                font-weight: 800;
            }

            p{
                margin-top: 20px;
                line-height: 2;
                font-size: 13px;
            }

            a{
                margin-top: 20px; 
                text-decoration: none;
                background-color: ${COLORS.primary};
                color: ${COLORS.white};
                padding: 15px 40px;
                border-radius: 7px;
                font-size: 20px;
                font-weight: 600;
                transition: 1s;

                :hover{
                    background-color: ${COLORS.black};
                }
            }
        }

        .list-item{
            height: 244px;
            overflow: hidden;  
            display: flex;
            flex-direction: column;
            background: #f3f3f3;

            :hover{
                overflow: auto;
            }

            .heading{
                display: flex;
                align-items: stretch;
                justify-content: space-between;
                text-align: left;
                background: ${COLORS.black};
                color: ${COLORS.white};
                margin: 10px 10px 0;
                height: 40px;
                line-height: 40px;
                padding: 0 10px;
            }

            .item{
                align-items: center;
                margin: 0px 10px 0;
                display: flex;
                justify-content: space-between;
                padding: 5px 10px;
                border-bottom: 1px solid ${COLORS.gray};
                flex-flow: wrap;
            }
        }

        .coupon{
            
            text-align: center;
            display: flex;
            justify-content: center;
            
            p{
                cursor: pointer;
                display: inline-block;
            }
        }

        .total-box{
            margin: 10px;
            padding: 5px;
            border: 1px solid #e5e5e5;

            h2{
                font-size: 30px;
            }

            table{
                width: 100%;
                text-align: left;
                border-collapse: collapse;

                tr:nth-child(even) {
                    background-color: #D6EEEE;
                  }

                th, td {
                    padding: 5px;
                    width: 50%;
                  }
                
                
                  select{
                    outline: 0;
                    padding: 5px 0;
                  }
            }
            .list{
                display: flex;
                align-items: center;
                gap: 15px;
                h4{
                    width: 30%;
                    font-weight: 600;
                    @media ( max-width: 768px) {
                        width: 70%;
                    }
                }
            }
            
        }

        .payment-method{
            padding: 10px;

            p{
                font-size: 12px;
                line-height: 16px;
                letter-spacing: 2px;
                font-weight: 600;
            }

            img{
                width: 80%;
            }
        }

        .payment-box{
            margin-top: 20px;
            padding: 10px;
            border: 2px solid #87161b59;

            h2{
                font-size: 30px;
            }

            .terms{
                padding-top: 20px;

                a{
                    color: #87161b;
                    text-decoration: none;
                    font-weight: 600;
                }
            }
        }
    `,
});