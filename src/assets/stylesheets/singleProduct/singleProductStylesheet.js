import {
    css
} from "@emotion/css";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useSingleProductStyle = () => ({
    root: css `
        padding: 50px 30px;

        
        .color{
            width: 100%;
            justify-content: center;
            position: sticky;
            bottom: 50px;
            z-index: 55;
            margin-bottom: 40px;
            pointer-events: none; 
            

            .color-container{
                display: flex;
                flex-direction: column;
                padding: 0 20px;
                max-width: 100%;
                width: fit-content;
                pointer-events: all;
                margin: 0 auto;

                .color-content{
                    background: #ffffff;
                    padding: 10px 20px;
                    border-radius: 5px;

                    p{
                        font-size: 16px;
                        font-weight: 600;
                    }

                    .img-list{
                        display: flex;
                        gap: 8px;
                        margin-top: 8px;

                        img{
                            width: 60px;
                            height: 60px;
                        }
                    }
                }
            }
        }

        .content{
            padding: 20px;
            position: sticky;
            top: 0;
            h2{
                font-size: 28px;
                font-weight:600;
                color: ${COLORS.black};
                text-transform: capitalize;
                @media ( max-width: 768px) {
                    font-size: 20px;
                }
            }
    
            p{
                font-size: 14px;
                text-align: justify;
            }
    
            a{
                color: ${COLORS.primary};
            }

            hr{
                border-color: #000000;
            }
            .review{
               display: flex;
               align-items: center;
               
               svg{
                 color: #efce4a;
                 width: 20px;
                 height: 20px;
               }

               p{
                    padding-left: 10px;
                    font-size: 14px;
               }
            }

            .share-box{
                display: flex;
                align-items: center;
                gap: 5px;

                p{
                    font-size: 14px;
                }

                .shareIcon{
                    display: flex;
                    gap: 5px;
                    
                    svg{
                        cursor: pointer;
                        width: 20px;
                        height: 20px;
                      }
                }
            }

            .regular_price{
                font-size: 20px;
                text-decoration: line-through;
                color: ${COLORS.gray}
            }

            .sale_price{
                font-size: 30px;
                font-weight: 600;
            }

            .variant{
                display: flex;
                gap: 20px;
                margin-top: 20px;
                align-items: center;

                label{
                    font-weight: 600;
                }

                select{
                    padding: 5px 10px;
                    border: 1px solid #ddd;
                    margin-top: 6px;
                }

                .quantity{
                    .qty{
                        margin-top: 6px;
                        background-color: #ededed;
                        border: 0px;
                        width: 60px;
                        font-size: 16px;
                        padding: 6px 0px;
                        text-align: center;
                        outline: 0;
                        display: flex;
                        justify-content: center;
                        border-radius: 2px;

                       
                        
                    }
                    input[type='number']::-webkit-inner-spin-button{
                        opacity: 1
                    }
                }

                
            }

            
        }

        .Mui-selected{
            background: ${COLORS.black};
            color: ${COLORS.white}
            
        }

        .MuiTabs-flexContainer{
            flex-wrap: wrap;
        }

        .css-oeaouy{
            color: #ffffff !important;
        }

        .MuiTabs-fixed{
            background: ${COLORS.primary};
            color: #ffffff;
        }

        .css-nwrfgk-MuiButtonBase-root-MuiTab-root.Mui-selected{
            color: #ffffff !important
        }

        .css-nwrfgk-MuiButtonBase-root-MuiTab-root{
            color: #ffffff !important 
        }

        .css-1nppm96-MuiButtonBase-root-MuiTab-root.Mui-selected{
            color: #ffffff !important
        }

        .css-1nppm96-MuiButtonBase-root-MuiTab-root{
            color: #ffffff !important
        }

        .recent-view{
            .product-slider{
                padding: 30px 5px;
    
                h2{
                    font-size: 30px;
                    font-weight: 600;
                    text-transform: uppercase;

                    @media ( max-width: 450px) {
                        font-size: 24px;
                    }
                }
    
                hr{
                    border-color: #dbdbdb;
                    margin-bottom: 20px;
                }

                .slick-slider{ 
                    .slick-list{
                        .slick-track{
                            .slick-slide{
                                width: auto;
                            }
                        }
                    }
                }
            }
            
           
            
        }

   
    `,
});