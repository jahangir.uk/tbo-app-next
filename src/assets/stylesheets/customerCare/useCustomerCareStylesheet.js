import {
    css
} from "@emotion/css";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useCustomerCareStyle = () => ({
    root: css `
        padding: 50px 0;
        .css-1ujykiq-MuiButtonBase-root-MuiTab-root{
            color: #ffffff;
        }

        .MuiTabs-fixed{
            background: ${COLORS.primary};
            color: #ffffff;
        }

        .css-1ujykiq-MuiButtonBase-root-MuiTab-root.Mui-selected{
            color: #ffffff;
        }

        .css-1nppm96-MuiButtonBase-root-MuiTab-root.Mui-selected{
            color: #ffffff;
        }

        .css-1nppm96-MuiButtonBase-root-MuiTab-root{
            color: #ffffff;
        }

        .Mui-selected{
            background: ${COLORS.black};
            
        }
    `,
})