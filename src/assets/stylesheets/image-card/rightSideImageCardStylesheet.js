import { css } from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useRightSideImageStyle = () => ({
    root: css`
        .right-image-card{
            padding: 50px 0px;
            display: flex;
            align-items: center;
            h2{
                font-size: 40px;
                font-weight:600;
                margin-bottom: 1rem;
                color: ${COLORS.primary};
                text-transform: uppercase;

                @media ( max-width: 420px) {
                    font-size: 20px;
                }
            }

            p{
                font-size: 16px;
                margin-bottom: 14px;
                text-align: justify;
            }

            .right-img{
                width: 450px;
                margin-left: auto;
                box-shadow: 0px 0px 5px 4px rgba(0,0,0,0.05) inset;
                -webkit-box-shadow: 0px 0px 5px 4px rgba(0,0,0,0.05) inset;
                -moz-box-shadow: 0px 0px 5px 4px rgba(0,0,0,0. 05) inset;
                padding: 10px;

                @media ( max-width: 450px) {
                    width: 100%;
                }
                
                img{
                   width: 100%;
                   display: flex;
                   justify-content: end
                    
                }
                
            }
        }

    `,
})