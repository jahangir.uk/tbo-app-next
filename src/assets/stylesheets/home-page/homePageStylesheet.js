import {
    css
} from "@emotion/css";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useHomePageStyle = () => ({
    root: css `
    
        .slider{
            display: flex;
            height: 100%;
            width: 100%;
            align-items: center;
            justify-content: center;

            .content{
                display: flex;
                flex-direction: column;
                justify-content: center;
                text-align: center;
                h2{
                    color: #ffffff;
                    font-size: 30px;
                }
        
                p{
                    color: #ffffff;
                    font-size: 14px;
                    margin-bottom: 10px;
                }
                a{
                    text-decoration: none;
                    background-color: ${COLORS.primary};
                    color: ${COLORS.white};
                    padding: 10px 30px;
                    margin: 0 auto;
                    width: 110px;
                }
            }

            
        }
   
        .brand{
            padding: 50px 20px;
           

            .title{
                width: 100%;
                text-align: center;
                margin-bottom: 30px; 

                h2{
                    font-size: 40px;

                    @media ( max-width: 420px) {
                        font-size: 30px;
                    }
                }
                .MuiDivider-root{
                    background: ${COLORS.primary};
                    width: 20%;
                    margin: 0 auto;
                    height:2px;
                }
            }

            .slick-slider{
                
                .slick-list{
                    .slick-track{
                        .slick-slide{
                            width: auto;
                            img{
                                width: 95%;
                                height: auto;
                                border: 1px solid gray;
                            }
                        }
                    }
                }
            }

            .brand-slide{
                height: 120px;

                .slick-slide {
                    height:120px;
                 }
                 
                 .slick-slide img {
                    height:120px;
                    border: 1px solid gray;
                 }
            }
            
        }

        .top-category{
            padding: 50px 0;
            h2{
                font-size: 30px;
                text-align: center;

                @media ( max-width: 420px) {
                    font-size: 20px;
                }
            }

            .image-box{
                position: relative;
                img{
                    width: 100%;
                }

                .image-content{
                    position: absolute;
                    top:0;
                    bottom: 0;
                    left:0;
                    right: 0;
                    background: #000000a6;
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;

                    :hover{
                        background: #0000004f;
                    }

                    p{
                        color: #ffffff;
                        font-size: 20px;
                        margin-bottom: 10px;
                    }

                    a{
                        background: ${COLORS.primary};
                        color: ${COLORS.white};
                        padding: 10px 30px;
                        text-decoration: none;
                    }
                }
            }
            .MuiDivider-root{
                background: ${COLORS.primary};
                width: 20%;
                margin: 0 auto;
                height:2px;
            }

            .css-ztrqiq-MuiImageListItemBar-root{
                top:0 !important;
                text-align: center;
                cursor: pointer;

                .css-dasnyc-MuiImageListItemBar-title{
                    font-size: 20px;
                    margin-bottom: 10px;
                }

                a{
                    background: ${COLORS.primary};
                    color: ${COLORS.white};
                    padding: 7px 20px;
                    display: inline-block;
                    text-decoration: none;
                }

                :hover{
                    background: #00000047;
                }
                
            }
        }

        .product-section{
            text-align: center;
            h2{
                font-size: 30px;
                text-align: center;
                @media ( max-width: 420px) {
                    font-size: 24px;
                }
            }
            .MuiDivider-root{
                background: ${COLORS.primary};
                width: 20%;
                margin: 0 auto;
                height:2px;
            }

            .all-product-btn{
                text-align: center;
                margin-top: 50px;
                a{
                    border: 1px solid #000000;
                    padding: 10px 30px;
                    text-decoration: none;
                    font-weight: 600;
                    display: inline-flex;
                    align-items: center;
                    gap: 10px;
                    transition: all 500ms cubic-bezier(0.39,0.5,0.15,1.36);
                    color: #000000;

                    :hover{
                        background: ${COLORS.black};
                        color: ${COLORS.white}
                    }
                }
            }
        }

        
    `,
});