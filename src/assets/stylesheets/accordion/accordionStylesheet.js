import { css } from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useAccordionStyle = () => ({
    root: css `
        
        .single-item{
            margin: 10px 0;
 
        }

        .MuiAccordion-root{
            border: 1px solid ${COLORS.gray};
            border-radius: 0;
        }

        .MuiAccordionSummary-root{
            background: ${COLORS.lightGray};
            color: ${COLORS.black};
            //border-radius: 5px 5px 0 0;
            
            p{
                font-weight: bold;
                color: #5c4d4d;
            }

            svg{
                color: #5c4d4d;
            }
        }

        .css-sh22l5-MuiButtonBase-root-MuiAccordionSummary-root.Mui-expanded{
            background: ${COLORS.primary};
             p{
                color: ${COLORS.white};
             }

             svg{
                color: ${COLORS.white};
             }
        }

        
        .css-1elwnq4-MuiPaper-root-MuiAccordion-root{
            box-shadow: none;
        }

        .css-sh22l5-MuiButtonBase-root-MuiAccordionSummary-root.Mui-expanded{
            min-height: 50px;

            .css-o4b71y-MuiAccordionSummary-content.Mui-expanded{
                margin: 10px 0;
            }
        }

        .MuiAccordionDetails-root{
            p{
                font-size: 15px;
            }
        }

        
    `})