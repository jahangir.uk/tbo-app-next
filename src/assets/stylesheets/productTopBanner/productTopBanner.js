import {
    css
} from "@emotion/css";

export const useProductTopBannerStyle = () => ({
    root: css `
        font-family: "Open Sans", sans-serif !important;
        h2{
            text-align: center;
            align-items: center;
            justify-content: center;
            font-family: "Open Sans", sans-serif !important;
            font-weight: 400;
            font-size: 21px;
            color: #363636;
        }

        .content{
            border: 1px solid #ddd;
            text-align: center;
            padding: 10px;
            border-radius: 10px;
            margin: 0 3px;
    
            h2{
                font-size: 21px;
                color: #363636;
                font-weight: 400;
                font-family: "Open Sans", sans-serif !important;
            }
    
            p{
                font-size: 16px;
                padding-top: 10px;
                text-align: justify;
            }
    
            .sub-items {
                text-align: left;
                display: flex;
                flex-wrap: wrap;
    
               
                .item{
                    width: 25%;
                    text-align: center;
                    font-weight: 700;
                    line-height: 20px;
                    margin-top: 20px;
                    font-size: 12px;
                    text-decoration: none;
                    color: #363636;
    
                    :hover{
                        text-decoration: underline;
                    }
    
                    @media ( max-width: 420px) {
                        width: 100%;
                        text-align: left;
                    }
                }
            }
        }
        

        
  `
});