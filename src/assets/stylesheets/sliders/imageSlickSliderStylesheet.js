import { css } from "@emotion/css";

export const useImageSlickSliderStyle = () => ({
    root: css `
        .slider {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 400px;

            .slider__thumbnails {
                display: flex;
                align-items: center;
                justify-content: center;
                position: relative;
                width: 200px;
                height: 350px;
                flex-direction: column;
                overflow: hidden;

                :hover{
                    overflow: auto;
                }

                .slider__thumbnail {
                    cursor: pointer;
                    margin-right: 10px;
                    border: 1px solid #ccc;
                    width: 150px;

                    img {
                        width: 100%;
                        height: 100%;
                        object-fit: cover;
                    } 
                }

                .is-selected {
                    width: 150px;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    z-index: 1;

                    img {
                        width: 100%;
                        height: 100%;
                        object-fit: cover;
                    } 
                }
            }
        }

        .slider__image {
            flex: 1;
            height: 100%;

            img {
                height: 100%;
                object-fit: contain;
              }
        }

        .linked-list-slider {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;

            
            .thumbnail-list {
                display: flex;
                flex-direction: column;
                align-items: center;
                height: 400px;
                overflow: auto;
                position: relative;

                .thumbnail {
                    width: 100%
                    font-size: 1.2rem;
                    padding: 0.5rem;
                    margin-bottom: 0.5rem;
                    cursor: pointer;
                    opacity: .6;

                    img{
                        width: 100%;
                    }
                }

                .current {
                    width: 100%
                    font-weight: bold;
                    opacity: 1;
                    // position: absolute;
                    // margin: auto;
                    // max-width: 100%;
                    // max-height: 100%;
                    // transform: translate(-1%, -43%);
                    // top: 48%;

                    img{
                        width: 100%;
                    }
                }
            }

            .slider-wrapper {
                width: 78%;
                display: flex;
                flex-direction: row;
                align-items: center;

                .current-item {
                    font-size: 2rem;
                    margin: 0 1rem;

                    img{
                        width: 100%;
                    }
                }

                .prev-button, .next-button {
                    font-size: 1.5rem;
                    padding: 0.5rem 1rem;
                    cursor: pointer;
                }

                .prev-button {
                    margin-right: 1rem;
                }

                .next-button {
                    margin-left: 1rem;
                }
            }
        }
          
          
        
    `,

    slickSlider: css `
    
    .slider {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 400px;
        flex-direction: column;

        .slider__thumbnails {
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
            width: 100%;
            overflow: hidden;

            :hover{
                overflow: auto;
            }

            .slider__thumbnail {
                cursor: pointer;
                margin-right: 10px;
                border: 1px solid #ccc;
                width: 150px;

                img {
                    width: 100%;
                    height: 100%;
                    object-fit: cover;
                } 
            }

            
        }
    }

    .slider__image {
        flex: 1;
        height: 80%;

        img {
            height: 100%;
            object-fit: contain;
          }
    }
    `,

    bottomThumb: css `
        
        .slider-wrapper{
            position: relative;
            overflow: hidden;

            
            .slick-list{
                .slick-track{
                    .slick-slide{
                        img{
                            width: 100%;
                        }
                    }
                }
            }
        }

        /* the slides */
// .slick-slide {
//     margin: 2px 0;
// }


        .thumbnail-slider-wrap {
            margin-top: 15px;
            height: 90px;
            padding: 0 40px;
            
            .slick-slider{
                .slick-prev, .slick-next{
                    top: 50%;
                }
                .slick-next{
                    right: -33px;
                }
                .slick-prev{
                    left: -33px;
                }
                .slick-list{
                    margin: 0 -10px;
                    .slick-track{
                        .slick-slide{
                            div{
                                margin: 0 10px;
                            }
                            img{
                                margin:0 5px !important;
                                width: 100%;
                                border: 1px solid #d3d0d0;
                            }
                        }

                        .slick-current img{
                            border: 1px solid green;
                            zoom: 2.5;
                            transform: scale(1.2);
                        }
                    }
                }
            }
        }
        
        .thumbnail-slider-wrap img {
            width: 70%;
        }
    `,

    leftThumb: css `
    .slider-wrapper{
        display: flex;    
        .slick-list{
            .slick-track{
                .slick-slide{
                    img{
                        width: 100%;
                    }
                }
            }
        }

        .thumbnail-slider-wrap {
            padding: 0 20px;
            width: 90px;
            
            .slick-slider{
                margin-top: 40px;
                .slick-list{
                    margin: 10px 0;
                    height: 170px !important;
                    .slick-track{
                        .slick-slide{
                            div{
                                margin: 4px 0;
                            }
                            img{
                                width: 100%;
                            }
                        }
    
                        .slick-current img{
                            border: 1px solid green;
                            zoom: 2.5;
                        }
                    }
                }
            }

            .slick-prev{
                bottom: -70px;
                left: 28%;
                top: unset;
                height: 50px;
                width: 50px;
                border-radius: 50%;
                display: flex !important;
                align-items: center;
                justify-content: center;
                color: #000000;
            }

          
            .slick-next{
                top: -20px;
                left: 28%;
                height: 50px;
                width: 50px;
                border-radius: 50%;
                display: flex !important;
                align-items: center;
                justify-content: center;
                color: #000000;

            }

            
        }
        
        .thumbnail-slider-wrap img {
            width: 70%;
        }
    }

    
    `,
    zoomImage: css `
    .imageContainer {
        position: relative;
        width: 300px;
        height: 300px;
        overflow: hidden;

        .image {
            width: 100%;
            height: 100%;
            transition: transform 0.3s;
        }
    }
    
    `,
    imageContainer: css `

            position: relative;
            overflow: hidden;
           
            
            .zoomLens {
                position: absolute;
                top: 0;
                left: 0;
                width: 120px;
                height: 120px;
                opacity: 1;
                pointer-events: none;
                background-repeat: no-repeat;
                background-size: 600px 600px;
                z-index: 1;

                .image {
                    width: 100%;
                    height: 100%;
                    object-fit: cover;
                }
            }

        // .imageWrapper {
        //     flex: 1;
        //     position: relative;
        //     overflow: hidden;

        //     .image {
        //         width: 100%;
        //         height: auto;
        //         transition: transform 0.3s;
        //     }
        // }

        // .zoomedWrapper {
        //     position: absolute;
        //     top: 0;
        //     left: 0;
        //     width: 300%;
        //     height: 100%;
        //     background-repeat: no-repeat;
        //     background-size: 600% 100%;
        //     z-index: 9999;
        //     transition: left 0.3s;

            
        // }

        // :hover{
        //     .image {
        //         transform: scale(1.1);
        //     }

        //     .zoomedWrapper {
        //         left: 0;
        //     }
        // }
    `,
});