import { css } from "@emotion/css";
import backImg from "assets/images/back.jpg";
import quickPic from "assets/images/quick.png";
import { COLORS } from "assets/styles/colors/colors";
export const useContactUsStyle = () => ({
    root: css `
        
        background: url(${backImg?.src});
        background-position: center center;
        background-size: cover;

        .form {
            margin: 50px 0;
            background: #ffffff;
            border: 1px solid #b9b9b9;

            .quick-contact{
                background: url(${quickPic?.src});
                background-size: cover;
                padding: 10px;
                background-repeat: no-repeat;
            }
            .contact-form{
                margin-bottom: 20px;
                
                margin: 20px;
                padding: 10px;
                
    
               textarea{
                    outline: none;
                    border-color: #dddddd;
                    padding: 10px;
                    font-size: 18px;
               }
            }
        }

        .card-list{
            margin-bottom: 50px;

            .css-1dx2aqu-MuiPaper-root-MuiCard-root{
                box-shadow: none;
            }
            .css-jht8kn-MuiPaper-root-MuiCard-root{
                box-shadow: none;
            }
            .social-list{
                margin-top: 15px;
                display: flex;
                align-items: center;
                gap: 10px;
                justify-content: start;

                a{
                    background: ${COLORS.primary};
                    display: flex;
                    border-radius: 5px;
                    svg{
                        color: #ffffff;
                        font-size: 30px;
                    }
                }

                
            }
        }
        
    `,
});