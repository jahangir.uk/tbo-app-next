import {
    css
} from "@emotion/css";

export const useProductCardStyle = () => ({
    root: css `
    
        margin: 4px 4px 0px;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        flex-direction: column;
        flex-wrap: wrap;
        -webkit-box-flex: 1;
        flex: 1 1 auto;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        height: 100%;
        min-height: 0;
        background-color: #fff;
        border: 1px solid #dbdbdb;
        border-radius: 7px;


        .content-box{
            position: relative;
            height: 90%;
            .image{
                padding: 8px;
                width: 90%;
                img{
                    width: 100%;
                }
            }
    
            .content{
                padding: 0.5rem 0.5rem 1.25rem;
                text-align: center;
                h3{
                    color: #0a0a0a;
                    font-size: 13px;
                
                    line-height: 16px;
                    margin: 1rem 0 0.75rem;
                }
            }

            .overlay {
                position: absolute;
                top: 0;
                left: 0;
                bottom:0 ;
                width: 100%;
                height: 100%;
                background: #00000085;
                display: none;
                transition: 0.4s ease-out;
                border-radius: 5px 5px 0 0;
                cursor: pointer;
                
            }

            :hover {
                
                .overlay{
                    display: inline-block;
                    transition: 2s;

                    .icons{
                        display: flex;
                        align-items: center;
                        height: 100%;
                        width: 100%;
                        justify-content: center;
                        gap: 10px
    
                        
                    }
                    svg{
                        width: 40px !important;
                        height: 40px !important;
                        font-size: 40px !important;
                        color: #fff;
                    }
                }
            }
        }

        

        .button{
            padding: 0;
            margin-top: auto;
            flex-grow: 1;
            justify-content: flex-end;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
        }

    `,
})