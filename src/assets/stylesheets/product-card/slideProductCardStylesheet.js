import {
    css
} from "@emotion/css";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useSlideProductCardStyle = () => ({
    root: css `
    
    margin: 4px 4px 0px;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    flex-wrap: wrap;
    -webkit-box-flex: 1;
    flex: 1 1 auto;
    -webkit-box-align: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
    height: 100%;
    min-height: 0;
    background-color: #fff;
    border: 1px solid #dbdbdb;
    border-radius: 7px;

        :hover{
            border: 1px solid #000000;
            border-radius:0;
        }

        a{
            text-decoration: none;
        }

        .content-box{
            text-align: center;
            padding: 10px 20px;

            .price{
                font-weight: 600;
                color: ${COLORS.black};
            }

            h3{
                color: #0a0a0a;
                font-size: 13px !important;
                line-height: 16px; 
            }
        }

        img{
            width: 100%;
        }
       

        .css-46bh2p-MuiCardContent-root:last-child{
            padding-bottom: 0;
        }
        .css-46bh2p-MuiCardContent-root{
            padding: 0;
        }

        

    `,
})