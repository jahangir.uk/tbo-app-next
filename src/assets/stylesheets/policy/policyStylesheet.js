import { css } from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const usePolicyStyle = () => ({
    root: css `
        
        padding: 50px 30px;

        h2{
            font-size: 32px;
            font-weight:600;
            margin-bottom: 1rem;
            color: ${COLORS.black};

            @media ( max-width: 420px) {
                font-size: 20px;
            }
        }

        p{
            font-size: 16px;
            margin-bottom: 14px;
            text-align: justify;
        }

        a{
            color: ${COLORS.primary};
        }
    `,
});