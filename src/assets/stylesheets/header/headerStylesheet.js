import { css } from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useHeaderStyle = () => ({
    root: css `
        .MuiAppBar-root{
            background: ${COLORS.white};
        }

        .main-menu{
            background: ${COLORS.gray};
            padding: 10px 0;
            a{
                text-decoration: none;
                color: ${COLORS.black};
            }

            svg{
                color: ${COLORS.black};
            }

            .mega-menu{
                background: black;

                .menu-items{
                    background: blue;
        
                    a{
                        text-decoration: none;
                    }
                }
            }

            
        }

       

        

        

        .menu-icon{
            display:'flex', 
            alignItems: 'center', 
            gap: '20px',
            padding: '5px 10px',
            cursor: 'pointer',
            &:hover{
                background: '#e9e9e9',
                borderRadius: '5px'
            } 
                
        }

        .mobile-menu{
            background: red;
        }

        .menu-bottom{
            
            .menu{
                padding-right: 20px;
                a{
                    color: ${COLORS.black};
                    text-decoration: none;
                    display: flex;
                    align-items: center;
                    font-size: 15px;
                    font-weight: 600;
    
                    svg{
                        font-size: 20px;
                    }
    
                    &:hover{
                        color: #707070;
                    }
    
                    .MuiBadge-badge{
                        font-weight: 500;
                        font-size: 12px;
                        min-width: 13px;
                        line-height: 1;
                        padding: 0 3px;
                        height: 14px;
                        border-radius: 10px;
                    }
                }
            }
        }

    `,
});