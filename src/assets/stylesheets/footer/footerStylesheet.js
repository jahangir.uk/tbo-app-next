import {
    css
} from "@emotion/css";
import FooterImg from "assets/images/footer.jpg";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useFooterStyle = () => ({
    root: css `
        .footer-top{
            // background: url(${FooterImg});
            background-position: center center;
            background-size: cover;
            position: relative;
            height: 100%;
            overflow: hidden;
            background-color: #3a3a3a;
            padding: 0 10px;

            .newsletter{
                text-align: center;
                align-items: center;
                justify-content: center;
                display: flex;
                flex-direction: column;
                width: 100%;
                padding: 10px 0;

                h2{
                    color: #ffffff;
                    margin: 10px 0;
                }
                .form{
                    display: flex;
                    align-items: center;
                    width: 50%;
                    @media ( max-width: 440px) {
                        width: 90%;
                    }
                    input{
                        display: block;
                        width: 100%;
                        height: auto;
                        padding: 0.375rem 0.75rem;
                        font-size: 1rem;
                        font-weight: 400;
                        line-height: 1.5;
                        color: #000000;
                        background-color: #fff;
                        background-clip: padding-box;
                        border: 1px solid #ced4da;
                        border-radius: 5px 0px 0px 5px;
                        background: #ffffff;
                        outline: none;
    
                        ::placeholder{
                            color: #000;
                            font-weight: 500;
                        }
                    }

                    button{
                        width: 150px;
                        border-radius: 0px 5px 5px 0px;
                        padding: 5px 20px;
                        margin-left: -4px;
                    }
                }
            }
            .footer-menu{
                padding-top: 10px;
                color: #ffffff;

                @media ( max-width: 420px) {
                    padding: 5px 20px;
                }

                h2{
                    font-size: 24px;
                    font-weight: 600;
                    latter-spacing: 1.4px;
                }

                .css-9mgopn-MuiDivider-root{
                    border-color: #ffffff;
                }

                .css-16ac5r2-MuiButtonBase-root-MuiListItemButton-root{
                    padding: 4px 0;
                }

                

                .social-list{
                    margin-top: 15px;
                    display: flex;
                    align-items: center;
                    gap: 10px;
                    justify-content: start;

                    a{
                        background: ${COLORS.primary};
                        display: flex;
                        border-radius: 5px;
                        svg{
                            color: #ffffff;
                        }
                    }

                    
                }

                .sponsor{
                    margin-top: 20px;
                    display: flex;
                    align-items: center;
                    gap: 10px;
                    
                }

                
            }

            .copyright{
                color: #ffffff;
                text-align: center;
                padding: 10px 0;
                p{
                    font-size: 13px;

                    a{
                        color: #ffffff;
                        text-decoration: none;
                        font-weight: 600;
                    }
                }
            }
            
            .payment{
                padding-top: 20px;
                padding-bottom: 10px;
                text-align: left;

                h2{
                    color: #ffffff;
                    text-align: left;
                }
                img{
                    width: 85%;
                }       
            }

            .emi-method{
                display: flex;
                gap: 10px;
                align-items: center;
                justify-content: flex-end;

               
                img{
                    cursor: pointer;
                    border-radius: 5px;
                    @media ( max-width: 420px) {
                        width: 90px;
                    }
                }
            }

            }
        }
    `,
});