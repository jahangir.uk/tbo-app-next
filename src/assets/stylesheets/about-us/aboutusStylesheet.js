import { css } from "@emotion/css";
import { COLORS } from "assets/styles/colors/colors";

export const useAboutUsStyle = () => ({
    root: css ` 
        padding: 50px 30px; 
        .counter{
            padding: 50px 0px;
            h2{
                text-align: center;
                font-size: 50px;
                font-weight: 600;
                
                color: ${COLORS.primary};
                
                @media ( max-width: 420px) {
                    font-size: 30px;
                }
            }

            .MuiDivider-root{
                background: ${COLORS.black};
                width: 20%;
                margin: 0 auto;
                height:2px;
                margin-bottom: 30px;
            }

            .counter-card{

                box-shadow: 0px 0px 5px 4px rgba(94,73,73,0.12) inset;
                -webkit-box-shadow: 0px 0px 5px 4px rgba(94,73,73,0.12) inset;
                -moz-box-shadow: 0px 0px 5px 4px rgba(94,73,73,0.12) inset;
                padding: 30px 10px;
                cursor: pointer;
                text-align: center;

                p{
                    font-weight: 400;
                    font-size: 20px;
                }

                .number{
                    font-weight: 900;
                }

                svg{
                    color: ${COLORS.primary};
                    border-radius: 50px;
                    border: 2px solid ${COLORS.primary};
                    padding: 10px;
                    font-size: 35px;

                    :hover{
                        background: ${COLORS.primary};
                        color: ${COLORS.white};
                    }
                }
            }
        }

           

        .brand{
            padding: 50px 0;
           

            .title{
                width: 100%;
                text-align: center;
                margin-bottom: 30px; 

                h2{
                    font-size: 40px;

                    @media ( max-width: 420px) {
                        font-size: 30px;
                    }
                }
                .MuiDivider-root{
                    background: ${COLORS.primary};
                    width: 20%;
                    margin: 0 auto;
                    height:2px;
                }
            }
            
        }

        
    `,
});