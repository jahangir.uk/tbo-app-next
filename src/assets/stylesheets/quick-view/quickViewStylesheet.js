import {
    css
} from "@emotion/css";
import {
    COLORS
} from "assets/styles/colors/colors";

export const useQuickViewStyle = () => ({
    root: css `
    min-width: 880px;
    min-height: 590px;
    @media ( max-width: 450px) {
        min-width: 100%;
        min-height: 100%;
    }
    .content{
        padding: 0 10px;
        position: sticky;
        top: 0;
        h2{
            font-size: 18px;
            font-weight:600;
            color: ${COLORS.black};
            text-transform: capitalize;
            margin-bottom: 10px;
            a{
                text-decoration: none;
            }
            @media ( max-width: 450px) {
                font-size: 26px;
            }
        }

        p{
            font-size: 14px;
            text-align: justify;
        }

        a{
            color: ${COLORS.primary};
        }

        hr{
            border-color: #000000;
        }
        .review{
           display: flex;
           align-items: center;
           
           svg{
             color: #efce4a;
             width: 20px;
             height: 20px;
           }

           p{
                padding-left: 10px;
                font-size: 14px;
           }
        }

        .share-box{
            display: flex;
            align-items: center;
            gap: 5px;

            p{
                font-size: 14px;
            }

            .shareIcon{
                display: flex;
                gap: 5px;
                
                svg{
                    cursor: pointer;
                    width: 20px;
                    height: 20px;
                  }
            }
        }

        .regular_price{
            font-size: 20px;
            text-decoration: line-through;
            color: ${COLORS.gray}
        }

        .sale_price{
            font-size: 30px;
            font-weight: 600;
        }

        .variant{
            display: flex;
            gap: 20px;
            margin-top: 20px;
            align-items: center;

            label{
                font-weight: 600;
            }

            select{
                padding: 5px 10px;
                border: 1px solid #ddd;
                margin-top: 6px;
            }

            .quantity{
                .qty{
                    margin-top: 6px;
                    background-color: #ededed;
                    border: 0px;
                    width: 60px;
                    font-size: 16px;
                    padding: 6px 0px;
                    text-align: center;
                    outline: 0;
                    display: flex;
                    justify-content: center;
                    border-radius: 2px;

                   
                    
                }
                input[type='number']::-webkit-inner-spin-button{
                    opacity: 1
                }
            }

            
        }

        
    }

        

        
    `,
});