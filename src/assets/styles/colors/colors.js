export const COLORS = {
    primary: '#87161b',
    secondary: '#dfdfsd',
    black:'#000000',
    white: '#ffffff',
    gray: '#cccccc',
    lightGray: '#e7e7e7',
}