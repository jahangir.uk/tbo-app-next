import { useSelector } from "react-redux";

const useAuthUser = () => {
    const user = useSelector((state) => state.auth.userDetails);
    return user;
};

export default useAuthUser;
