import swal from 'sweetalert';

const useConfirmation = (defaultConfig = {}) => {
    const confirm = (confirmProps, { onConfirm, onError, callback, ...config }) =>
        swal({
            title: 'Are you sure?',
            icon: 'warning',
            text: 'Confirm your action.',
            buttons: ['Cancel', 'Ok'],
            ...defaultConfig,
            ...config,
        })
            .then((confirmed) => {
                if (confirmed) {
                    if (typeof onConfirm === 'function') {
                        onConfirm(confirmProps);
                    }
                }
                if (typeof callback === 'function') {
                    callback(confirmed, confirmProps);
                }
            })
            .catch((error) => {
                if (typeof onError === 'function') {
                    onError(error, confirmProps);
                }
            });

    return confirm;
};

export default useConfirmation;
