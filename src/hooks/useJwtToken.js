import { isEmpty } from 'helpers/functions';
import jwtDecode from 'jwt-decode';

const useJwtToken = (token) => {
    const hasToken = !isEmpty(token);
    if (!hasToken) {
        return {
            data: null,
            isExpired: true,
            remaining: {
                seconds: 0,
                minutes: 0,
            },
            error: 'No token provided.',
        };
    }
    try {
        const jwtToken = jwtDecode(token);
        const isExpired = Date.now() >= jwtToken.exp * 1000;
        const seconds = Math.floor((jwtToken.exp * 1000 - Date.now()) / 1000);
        const minutes = Math.floor(seconds / 60);
        const expiringSoon = minutes <= 3;

        return {
            data: jwtToken,
            isExpired,
            expiringSoon,
            remaining: {
                seconds,
                minutes,
            },
        };
    } catch (error) {
        return {
            data: null,
            isExpired: true,
            remaining: {
                seconds: 0,
                minutes: 0,
            },
            error,
        };
    }
};

export default useJwtToken;
