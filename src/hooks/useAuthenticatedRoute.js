/* eslint-disable */
import { Navigate, useLocation } from "react-router-dom";
import useAuthUser from "./useAuthUser";
import useToken from "./useToken";

const ignorePaths = [
    "/login",
    "/register",
    "/forget-password",
    "/reset-password",
];

const useAuthenticatedRoute = () => {
    const isAuthenticated = useToken();
    const location = useLocation();
    const user = useAuthUser();

    const protectedPath = (Component, options = {}) => {
        if (ignorePaths.includes(location.pathname)) {
            return <Component from={location} {...options} />;
        }

        if (!isAuthenticated) {
            return (
                <Navigate
                    state={{
                        from: location,
                    }}
                    {...options}
                    to="/login"
                />
            );
        }

        return <Component from={location} {...options} />;
    };

    return protectedPath;
};

export default useAuthenticatedRoute;
