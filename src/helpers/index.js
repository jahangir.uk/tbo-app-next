import { Avatar } from '@mui/material';
import ProfileIcon from 'src/assets/images/avatar.png';
import { formatDate, isEmpty } from './functions';

export const getStatusFromDate = (end, start) => {
    const isExpired = new Date(end).getTime() < new Date().getTime();
    const isUpcoming = new Date(start).getTime() > new Date().getTime();
    if (isExpired) {
        return 'Closed';
    }
    if (isUpcoming) {
        return 'Upcoming';
    }
    return 'Running';
};

export const isRunning = (status) => status?.toLowerCase() === 'running';
export const isPrevious = (status) => status?.toLowerCase() === 'previous';
export const isUpcoming = (status) => status?.toLowerCase() === 'upcoming';

export const avatarProps = (
    url,
    height = { xs: 80, sm: 100, md: 150 },
    width = { xs: 80, sm: 100, md: 150 },
    name = 'Profile'
) => {
    const common = {
        height,
        width,
        border: '4px solid #ffffff',
    };
    if (url) {
        return {
            src: url,
            alt: name,
            children: (
                <Avatar
                    sx={{
                        height: 100,
                        width: 100,
                        backgroundColor: 'white',
                        objectFit: 'contain',
                    }}
                    src={ProfileIcon}
                    alt={name}
                />
            ),

            sx: {
                ...common,
                backgroundColor: 'white',
                objectFit: 'contain',
            },
        };
    }

    return {
        src: ProfileIcon,
        // children: <img src={ProfileIcon} alt={name} />,

        sx: {
            ...common,
            backgroundColor: 'black',

            '& img': {
                width: '45%',
                height: '55%',
            },
        },
    };
};

export const getDateStr = (start, end) =>
    `${formatDate(start, 'DD MMM')} - ${formatDate(end, 'DD MMM')}`;

export const getUserFullName = (user) => {
    if (!isEmpty(user?.displayName)) return user?.displayName;
    if (!isEmpty(user?.firstName)) return `${user?.firstName} ${user?.lastName ?? ''}`.trim();

    return '';
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getStatusFromDate,
    avatarProps,
    getDateStr,
    getUserFullName,
}; 
