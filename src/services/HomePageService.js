import {
    API_URL
} from 'helpers/apiUrl';
import {
    createApiRequest
} from 'helpers/axios';
import { APP_AUTHORIZE_USER_KEY } from 'helpers/config';
import { secureStorage } from 'utils/storage';
const queriesApi = createApiRequest(API_URL);
const commandApi = createApiRequest(API_URL);
const access_token = JSON.parse(secureStorage.get(APP_AUTHORIZE_USER_KEY)) ?? "V345o52ghvdcgh765dZ";
const Commands = {

    getHomePageData: () => commandApi.post('/home-page', {}, {
       
    }),

};
const Queries = {

};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    Commands,
    Queries,
};