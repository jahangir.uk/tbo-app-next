/* eslint-disable */
import {
    API_URL
} from "helpers/apiUrl";
import {
    createApiRequest
} from "helpers/axios";

// const getAppToken = () => AppService.getSiteToken();
const queries = createApiRequest(API_URL);
const authCommand = createApiRequest(API_URL);

const command = createApiRequest(API_URL);

const Commands = {
  createUser: (data) => command.post("/api/SecurityCommand/CreateUser", data, {

  }),

  verifyUser: ({
      UserName,
      Code
    }) =>
    command.post("/api/SecurityCommand/AccountVerification", {
      UserName,
      Code,
    }),

  changePassword: ({
      OldPassword,
      NewPassword,
      IsLogOutFromAnywhere
    }) =>
    authCommand.post("/api/SecurityCommand/ChangePassword", {
      OldPassword,
      NewPassword,
      IsLogOutFromAnywhere,
    }),
  resetPassword: ({
      UserName,
      NewPassword,
      Code
    }) =>
    command.post("/api/SecurityCommand/ResetPassword", {
      UserName,
      NewPassword,
      Code,
    }),
  resendActivationCode: (email, token = undefined) =>
    command.post(
      "/api/SecurityCommand/ResendActivationCode", {}, {
        params: {
          email
        },
        if (token) {
          headers: (() => {

            return {
              Authorization: `Bearer ${token}`
            }
          })()
        }
      }
    ),

  updateUser: (data) =>
    command.post("/api/SecurityCommand/UpdateUser", data, {
      headers: {
        "content-type": "multipart/form-data",
      },
    }),

  tagPermission: (data) =>
    command.post("/api/SecurityCommand/UserTagPermission", data, {
      headers: {
        "content-type": "multipart/form-data",
      },
    }),
  reportUser: (data) =>
    command.post("/api/SecurityCommand/ReportingUser", data, {

    }),
};

const Queries = {
  getUsers: ({
      page = 1,
      results = 10,
      order_by = "firstname",
      is_ascending = true,
    }) =>
    queries.get("/api/Query/GetUsers", {
      params: {
        Page: page,
        Results: results,
        OrderBy: order_by,
        IsAscending: is_ascending,
      },
      // headers: {
      //   Authorization: `Bearer ${getAppToken()}`,
      // },
    }),
  getUser: (ItemId) =>
    queries.get("/api/Query/GetUserById", {
      params: {
        ItemId
      }
    }),

  getUserDetails: (ItemId) =>
    queries.get("/api/Query/GetUserDetailById", {
      params: {
        ItemId
      }
    }),

  getUserByUserName: (userName) =>
    queries.get("/api/Query/GetUserProfile", {
      params: {
        userName
      },
      // headers: {
      //   Authorization: `Bearer ${getAppToken()}`,
      // },  
    }),

  searchUsers: (is_ascending, name, orderBy = 'FirstName', page = 1, Results = 30) =>
    queries.get("/api/Query/SearchUsers", {
      params: {
        IsAscending: is_ascending,
        Name: name,
        Page: page,
        Results,
        OrderBy: orderBy
      },
    }),

  checkUserName: (username) =>
    queries.get(`/api/Query/checkedusername`, {
      params: {
        // Page: 1,
        // Results: 5,
        // OrderBy: "FirstName",
        // IsAscending: true,
        Name: username,
        // HaveWantsLoadableData: true,
      },
    }),

  getInterests: () => queries.get("/api/Query/GetInterestFeed"),
  getPhotographySkills: () => queries.get("/api/Query/GetPhotographySkills"),

  getSuggestUsers: ({
      page = 1,
      results = 50
    }) =>
    queries.get("/api/Query/suggestusers", {
      params: {
        Page: page,
        Results: results,
      },
    }),
};

export default {
  Commands,
  Queries,
};