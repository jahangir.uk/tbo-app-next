import {
    API_URL
} from 'helpers/apiUrl';
import {
    createApiRequest
} from 'helpers/axios';
import AppService from './Other';
const getAppToken = () => AppService.getSiteToken();
const getAuthToken = () => AppService.getAuthToken();
const queriesApi = createApiRequest(API_URL);
const commandApi = createApiRequest(API_URL);
const Commands = {
    orderPlace: (data) => commandApi.post('/order-create', {
       data
    }),

    getOrderList:() => commandApi.post('/order-list'),

    getOrderByCode: (data) => commandApi.post('/order-details', {
        data: {
            code: data
        }
    }),

};
const Queries = {

};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    Commands,
    Queries,
};