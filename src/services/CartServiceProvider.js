import {
    CART_ITEMS
} from "helpers/config";


export const loadCartFromLocalStorage = () => {
    return (dispatch) => {
        const cartData = localStorage.getItem(CART_ITEMS);
        if (cartData) {
            dispatch({
                type: 'LOAD_CART_FROM_LOCAL_STORAGE',
                payload: JSON.parse(cartData),
            });
        }
    };
};

export const saveCartToLocalStorage = () => {
    return (dispatch, getState) => {
        const cartData = JSON.stringify(getState().cart.items);
        localStorage.setItem(CART_ITEMS, cartData);
    };
};

