// import axios from '';
import axios from 'helpers/axios';

const AuthUserService = {
    getUserDetails: () => axios.get(`/api/v1/get-user`),
};

export default AuthUserService;
