import {
    API_URL
} from 'helpers/apiUrl';
import {
    createApiRequest
} from 'helpers/axios';
import AppService from './Other';
const getAppToken = () => AppService.getSiteToken();
const getAuthToken = () => AppService.getAuthToken();
const queriesApi = createApiRequest(API_URL);
const commandApi = createApiRequest(API_URL);
const Commands = {
    getShopProducts: ({
        pageSize = 48,
        page
    }) => commandApi.post('/page-shop', {
        pageSize,
        page
    }),

    getProductDetails: ({
        slug
    }) => commandApi.post('/product-show', {
        slug
    }),

    getProductByCategory: ({
        slug,
        pageSize = 48,
    }) => commandApi.post('/product-by-category', {
        slug,
        page_size : pageSize
    }), 
    
    getProductByBrand: ({
        pageSize = 48,
        slug
    }) => commandApi.post('/product-by-brand', {
        page_size: pageSize,
        slug
    }),

    getFilterProduct:(data) => commandApi.post('/filter-panel-one-result', {
        data
    }),

    getCouponData:(data)=> commandApi.post("/apply-coupon", {
        data
    }),

    checkProductAvailability:(data)=> commandApi.post("/cart-product-availability-check", {
        data
    }),

    getFilterData:(data)=> commandApi.post("/filter-panel-one", data),

};
const Queries = {

};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    Commands,
    Queries,
};