/* eslint-disable no-case-declarations */

import {
    ADD_TO_CART,
    COUPON_APPLY,
    COUPON_REMOVE,
    REMOVE_CART_ITEM,
    UPDATE_CART_DATA,
    UPDATE_CART_ITEM,
    UPDATE_CART_TOTAL
} from 'redux/action/types';



const initialState = {
    cartItems: [],
    total: 0,
    discount: 0,
    grandTotal: 0,
    couponApply: false,
};


const cartState = (state = initialState, action = {}) => {

    switch (action.type) {
        case ADD_TO_CART:
            console.log(action);
            const existingItemIndex = state.cartItems.findIndex(item => item.id === action.payload.id);
            if (existingItemIndex !== -1) {
                const updatedItem = {
                    ...state.cartItems[existingItemIndex],
                    quantity: state.cartItems[existingItemIndex].quantity + action.payload.quantity,
                    total_amount: ((state.cartItems[existingItemIndex].total_amount + (action.payload.quantity * state.cartItems[existingItemIndex].sale_price)) * 100),
                    item_total: (state.cartItems[existingItemIndex].item_total + (action.payload.quantity * state.cartItems[existingItemIndex].sale_price))
                };
                // console.log(action.payload.quantity);
                const updatedCartItems = [...state.cartItems];
                updatedCartItems[existingItemIndex] = updatedItem;
                const newTotal = parseFloat(state.total) + (action.payload.sale_price * action.payload.quantity);

                return {
                    ...state,
                    cartItems: updatedCartItems,
                    total: Number(newTotal).toFixed(2),
                    grandTotal: Number(newTotal).toFixed(2),
                    discount:0,
                    couponApply: false,
                };

            } else { 
                const newTotal = parseFloat(state.total) + (action.payload.sale_price * action.payload.quantity);
                console.log("Total", newTotal);

                return {
                    ...state,
                    cartItems: [...state.cartItems, action.payload],
                    total: Number(newTotal).toFixed(2),
                    grandTotal: Number(newTotal).toFixed(2),
                    discount:0,
                    couponApply: false,

                };
            }
            case REMOVE_CART_ITEM:
                return {
                    ...state,
                    cartItems: state.cartItems.filter(item => item.id !== action.payload)
                };

            case UPDATE_CART_TOTAL:
                return {
                    ...state,
                    total: action.payload,
                    grandTotal: action?.payload,
                };
            case UPDATE_CART_ITEM:
                const newTotal = state?.total;
                return {
                    ...state,
                    cartItems: state.cartItems.map(item => {
                        if (item.id === action.payload.productId) {
                            return {
                                ...item,
                                quantity: action.payload.quantity,
                                total_amount: Number((Number(action?.payload?.quantity) * Number(item?.sale_price).toFixed(2)) * 100),
                                item_total: Number((item?.sale_price * action.payload.quantity))
                            }
                        } else {
                            return item
                        }
                    }),
                    discount:0,
                    couponApply: false,
                };

            case UPDATE_CART_DATA:
                return {
                    ...state,
                    cartItems: action?.payload?.cartItems,
                    total: action?.payload?.total,
                    grandTotal: action?.payload?.grandTotal,
                    discount: action?.payload?.discount,
                    couponApply: action?.payload?.couponApply,
                    
                };
            case COUPON_APPLY:
                const total = parseFloat(state.total);
                return {
                    ...state,
                    grandTotal: parseFloat(total) - parseFloat(action?.payload?.discount_val),
                    discount: parseFloat(action?.payload?.discount_val),
                    couponApply: true,
                }
            case COUPON_REMOVE:
                const t = parseFloat(state.total);
                return {
                    ...state,
                    grandTotal: t,
                    discount: 0,
                    couponApply:false,
                }
           
            default:
                return state;
    }
};


// const calculateTotal = (cartItems) => {
//     return cartItems.reduce((total, item) => {
//       return total + (item.price * item.quantity);
//     }, 0);
//   };
export default cartState;