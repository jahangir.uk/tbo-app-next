/* eslint-disable no-case-declarations */
import {
    uniqByKeepFirst
} from 'helpers/functions';
import {
    FILTER_PRODUCT_LIST,
    SET_SINGLE_PRODUCT,
    SHOP_PRODUCT_LIST,
    SHOP_PRODUCT_UPDATE_LIST,
    UPDATE_FILTER_PRODUCT_LIST
} from 'redux/action/types';

const initialState = {
    shopProducts: {
        items: [],
        pageSize: 48,
        page: 1
    },
    singleProduct: {
        item: {}
    },

    filterProducts:{
        items: [],
        pageSize: 48,
        page: 1,
        brand_details: {},
        category_details: {},
        sub_category_details: {},
        sub_sub_category_details: {},
    },

   
};

const uniqFunc = (item) => item.id;

const productState = (state = initialState, action = {}) => {

    switch (action.type) {
        case SHOP_PRODUCT_LIST:
            return {

                ...state.shopProducts,
                    items: action.payload.data,
            };

        case SHOP_PRODUCT_UPDATE_LIST:
            return {
                ...state.shopProducts,
                    ...action.payload,
                    items: uniqByKeepFirst(
                        [...state.shopProducts.items, ...action.payload.data], uniqFunc
                    )
            };

        case SET_SINGLE_PRODUCT:
            return {
                ...state.singleProduct,
                    item: action.payload
            };
        case FILTER_PRODUCT_LIST:
            return{
                ...state.filterProducts,
                items: action?.payload?.filter_product,
                brand_details: action?.payload?.brand,
                category_details: action?.payload?.category,
                sub_category_details: action?.payload?.sub_category,
                sub_sub_category_details: action?.payload?.sub_sub_category
            };
        case UPDATE_FILTER_PRODUCT_LIST:
            return {
                ...state.filterProducts,
                ...action?.payload,
                items: uniqByKeepFirst(
                    [...state.filterProducts.items, ...action.payload?.filter_product], uniqFunc
                )
            };
        
        default:
            return state;
    }
};
export default productState;