import {
    combineReducers
} from 'redux';
import app from './app';
import auth from './auth';
import cart from './cartReducer';
import filterData from './filterData';
import menuState from './menuReducer';
import product from './product';

const appReducer = combineReducers({
    app,
    auth,
    product,
    cart,
    filterData,
    menuState,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;