/* eslint-disable no-case-declarations */
import {
    SET_MENU_LIST
} from 'redux/action/types';

const initialState = {
   main_menu : {
        brand_list: [],
        kids_list: [],
        men_menu_list: [],
        women_menu_list: [],
        tags: [],
   }
};


const menuState = (state = initialState, action = {}) => {

    switch (action.type) {
        case SET_MENU_LIST: 
            
            return {
                ...state?.main_menu,
                brand_list: action?.payload?.brand,
                kids_list: action?.payload?.kids_menu_list,
                men_menu_list: action?.payload?.men_menus_list,
                women_menu_list: action?.payload?.women_menus_list,
                tags: action?.payload?.tags,
            }

        default:
            return state;
    }
};
export default menuState;