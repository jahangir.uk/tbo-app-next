/* eslint-disable no-case-declarations */
import {
    FILTER_DATA
} from 'redux/action/types';

const initialState = {
   

    filterData: {
        brand: [],
        gender: [],
        product_type: [],
        tag_group: [],
    }
};


const filterState = (state = initialState, action = {}) => {

    switch (action.type) {
        case FILTER_DATA: 
            return {
                ...state?.filterData,
                brand: action?.payload?.brand,
                gender: action?.payload?.gender,
                product_type: action?.payload?.product_type,
                tag_group: action?.payload?.tag_group,
            }

        default:
            return state;
    }
};
export default filterState;