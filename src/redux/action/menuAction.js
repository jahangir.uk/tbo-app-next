import {
    SET_MENU_LIST
} from 'redux/action/types';
import MenuServiceProvider from 'services/MenuServiceProvider';


export function fetchMenuItems() {
    return (dispatch) => {
        return MenuServiceProvider.Commands.getMenuItem().then((response) => {
            dispatch({
                type:SET_MENU_LIST ,
                payload: response
            });
        })
    }
}



