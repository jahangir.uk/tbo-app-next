import {
    FILTER_PRODUCT_LIST,
    SET_SINGLE_PRODUCT,
    SHOP_PRODUCT_LIST,
    SHOP_PRODUCT_UPDATE_LIST
} from 'redux/action/types';
import ProductService from 'services/ProductService';


export function fetchShopPageProduct(canUpdate = false, page = 1, pageSize = 48) {
    return (dispatch, getState) => {
        const {
            page
        } = getState().product;

        return ProductService.Commands.getShopProducts({
            pageSize,
            page
        }).then((response) => {
            dispatch({
                type: canUpdate ? SHOP_PRODUCT_UPDATE_LIST : SHOP_PRODUCT_LIST,
                payload: response
            });
        })
    }
}

export function setSingleProduct(item) {

    return (dispatch) => {
        return dispatch({
            type: SET_SINGLE_PRODUCT,
            payload: item
        })

    }
}

export function getFilterProductList(data) {
    return (dispatch, getState) => {
        return ProductService.Commands.getFilterProduct(data).then((response) => {
            dispatch({
                type: FILTER_PRODUCT_LIST,
                payload: response
            });
        })
    }
}

