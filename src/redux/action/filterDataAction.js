import {
    FILTER_DATA
} from 'redux/action/types';
import ProductService from 'services/ProductService';



export function getFilterData(data){
    return (dispatch) => {
        return ProductService.Commands.getFilterData(data)
            .then((response) => {
                dispatch({
                    type: FILTER_DATA,
                    payload: response
                });
            })
    }
}