import {
    CART_ITEMS, COUPON_KEY
} from 'helpers/config';
import {
    ADD_TO_CART,
    COUPON_APPLY,
    COUPON_REMOVE,
    REMOVE_CART_ITEM,
    UPDATE_CART_DATA,
    UPDATE_CART_ITEM,
    UPDATE_CART_TOTAL
} from 'redux/action/types';
import { appStorage } from 'utils/storage';


export const updateCartTotal = () => (dispatch, getState) => {
    const {
        cartItems
    } = getState().cart;
    const total = cartItems.reduce((acc, item) => acc + item.quantity * item.sale_price, 0);
    dispatch({
        type: UPDATE_CART_TOTAL,
        payload: total.toFixed(2),
    });
};


export const addToCart = (product) => (dispatch, getState) => {
    dispatch({
        type: ADD_TO_CART, 
        payload: product
    });
    const cartItems = getState().cart;
    appStorage.set(CART_ITEMS, JSON.stringify(cartItems));
}

export const updateToCart = (productId, quantity) => (dispatch, getState) => {
    dispatch({
        type: UPDATE_CART_ITEM,
        payload: {
            productId,
            quantity
        }
    });
    dispatch(updateCartTotal());
    const cartItems = getState().cart;
    appStorage.set(CART_ITEMS, JSON.stringify(cartItems));
}


export const removeToCart = (productId) => (dispatch, getState) => {
    dispatch({
        type: REMOVE_CART_ITEM,
        payload: productId
    });
    dispatch(updateCartTotal());
    const cartItems = getState().cart;
    appStorage.set(CART_ITEMS, JSON.stringify(cartItems));
}

export const updateCartData = (data) => (dispatch) => {
    dispatch({
        type: UPDATE_CART_DATA,
        payload: data
    })
}

export const applyCoupon = (data) => (dispatch,getState) => {
    dispatch({
        type: COUPON_APPLY,
        payload: data,
    });
    const cartItems = getState().cart;
    appStorage.set(CART_ITEMS, JSON.stringify(cartItems));
}

export const removeCoupon = () => (dispatch, getState) => {
    dispatch({
        type: COUPON_REMOVE,
    });
    const cartItems = getState().cart;
    appStorage.set(CART_ITEMS, JSON.stringify(cartItems));
    appStorage.remove(COUPON_KEY);
}
