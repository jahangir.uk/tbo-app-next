const admin = {
    contests: true,
    users: true,
    contents: true,
    notifications: true,
    startup: true,
    analytics: true,
    admins: {
        defaults: true,
        create: true,
        edit: true,
        delete: false,
    },
};

const rolePermissions = {
    admin,
};

export function userCan(permissionName = '', roleName = 'admin') {
    const role = rolePermissions[roleName];

    const [name, action = 'defaults'] = permissionName.split('.');
    const permission = role[name];
    if (typeof permission === 'boolean') {
        return permission;
    }

    if (typeof permission === 'object') {
        return permission[action] ?? false;
    }
    return false;
}
window.userCan = userCan;
window.rolePermissions = rolePermissions;

export default rolePermissions;
