import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import RemoveIcon from "@mui/icons-material/Remove";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormGroup,
    IconButton,
    Typography,
} from "@mui/material";
import { useFilterStyle } from "assets/stylesheets/filter/filterStylesheet";
import { capitalize, isEmpty } from "helpers/functions";
import { useState } from "react";
import { useDispatch } from "react-redux";

export const ProductFilter = ({
    width,
    brand,
    gender,
    product_type,
    productTypes,
    tag_group,
    tagGroup,
    genders,
    brands,
    searchItems,
    handleCheckItem,
    handleRemoveItem,
}) => {
    const classes = useFilterStyle(width);
    const [expanded, setExpanded] = useState(null);
    const dispatch = useDispatch();
    //   const [searchItems, setSearchItems] = useState({
    //     items: []
    //   });
    //   const [brands, setBrands] = useState([]);
    //   const [genders, setGenders] = useState([]);
    //   const [productType, setProductType] = useState([]);
    //   const [tagGroup, setTagGroup] = useState([]);
    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    //   const handleCheckItem = (e, value) => {
    //     const {checked } = e.target;
    //     const { items } = searchItems;
    //     // console.log(`${value} is ${checked}`);

    //     // Case 1 : The user checks the box
    //     if (checked) {
    //       setSearchItems({
    //         items: [...items, value]
    //       });

    //       if(value?.type === "brand"){
    //         setBrands([...brands, value?.id]);
    //       } else if(value?.type === "gender"){
    //         setGenders([...genders, value?.id]);
    //       } else if(value?.type === "productType"){
    //         setProductType([...productType, value?.id]);
    //       }else if(value?.type === "tagGroup"){
    //         setTagGroup([...tagGroup, value?.id]);
    //       }
    //     }

    //     // Case 2  : The user unchecks the box
    //     else {
    //       setSearchItems({
    //         items: items.filter((e) => e.id !== value?.id)
    //       });

    //       if(value?.type === "brand"){
    //         setBrands([...brands.filter(i => i.id != value?.id) ]);
    //       }else if(value?.type === "gender"){
    //         setGenders([...genders.filter(i => i.id != value?.id)]);
    //       } else if(value?.type === "productType"){
    //         setProductType([...productType.filter(i => i.id != value?.id)]);
    //       }else if(value?.type === "tagGroup"){
    //         setTagGroup([...tagGroup.filter(i => i.id != value?.id)]);
    //       }
    //     }
    //   };

    //   const handleProductFilter = () => {
    //     const payload = {
    //         brand: brands,
    //         gender: genders,
    //         product_type: productType,
    //         tag: tagGroup
    //     }
    //     dispatch(getFilterProductList(payload));
    //   }

    //   const handleRemoveItem = (value) => {
    //     const { items } = searchItems;
    //     setSearchItems({
    //       items: items.filter((e) => e.id !== value?.id)
    //     });

    //     setBrands([...brands.filter(i => i.id != value?.id) ]);
    //     setGenders([...genders.filter(i => i.id != value?.id)]);
    //     setProductType([...productType.filter(i => i.id != value?.id)]);
    //     setTagGroup([...tagGroup.filter(i => i.id != value?.id)]);

    //   };

    //   useEffect(() => {
    //     if(!isEmpty(searchItems?.items)){
    //         handleProductFilter();
    //     }
    //   }, [searchItems?.items])


    return (
        <Box className={classes.root}>
            <Box className="filter">
                <Typography component={"h2"}>REFINE BY</Typography>
                <Typography>You have selected:</Typography>
                <Box>
                    {searchItems.items.map((item, index) => (
                        <Typography
                            key={index}
                            sx={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "space-between",
                                border: "1px solid #000000",
                                padding: "4px",
                                height: 30,
                            }}
                        >
                            {capitalize(item?.name)}
                            <IconButton onClick={() => handleRemoveItem(item)}>
                                <CloseIcon />
                            </IconButton>
                        </Typography>
                    ))}
                </Box>

                {isEmpty(genders) && (
                    <Box>
                        <Divider />
                        <Accordion
                            expanded={expanded === 10}
                            onChange={handleChange(10)}
                        >
                            <AccordionSummary
                                expandIcon={
                                    expanded === 10 ? (
                                        <RemoveIcon />
                                    ) : (
                                        <AddIcon />
                                    )
                                }
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography>Gender</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <FormControl component="fieldset">
                                    <FormGroup aria-label="position" row>
                                        {gender?.map((data, index) => (
                                            <FormControlLabel
                                                key={index}
                                                value={data}
                                                checked={genders?.includes(
                                                    data?.id
                                                )}
                                                control={<Checkbox />}
                                                label={data?.name}
                                                labelPlacement="end"
                                                onClick={(e) => {
                                                    handleCheckItem(e, {
                                                        id: data?.id,
                                                        name: data?.name,
                                                        type: "gender",
                                                    });
                                                }}
                                            />
                                        ))}
                                    </FormGroup>
                                </FormControl>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                )}

                <Box>
                    <Divider />
                    <Accordion
                        expanded={expanded === 11}
                        onChange={handleChange(11)}
                    >
                        <AccordionSummary
                            expandIcon={
                                expanded === 11 ? <RemoveIcon /> : <AddIcon />
                            }
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography>Product Type</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <FormControl component="fieldset">
                                <FormGroup aria-label="position" row>
                                    {product_type?.map((data, index) => (
                                        <FormControlLabel
                                            key={index}
                                            value={data}
                                            checked={productTypes?.includes(
                                                data?.id
                                            )}
                                            control={<Checkbox />}
                                            label={capitalize(data?.name)}
                                            labelPlacement="end"
                                            onClick={(e) => {
                                                handleCheckItem(e, {
                                                    id: data?.id,
                                                    name: data?.name,
                                                    type: "productType",
                                                });
                                            }}
                                        />
                                    ))}
                                </FormGroup>
                            </FormControl>
                        </AccordionDetails>
                    </Accordion>
                </Box>
                <Box>
                    <Divider />
                    <Accordion
                        expanded={expanded === 12}
                        onChange={handleChange(12)}
                    >
                        <AccordionSummary
                            expandIcon={
                                expanded === 12 ? <RemoveIcon /> : <AddIcon />
                            }
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography>Brand</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <FormControl component="fieldset">
                                <FormGroup aria-label="position" row>
                                    {brand?.map((data, index) => (
                                        <FormControlLabel
                                            key={index}
                                            value={data}
                                            checked={brands?.includes(data?.id)}
                                            control={<Checkbox />}
                                            label={capitalize(data?.name)}
                                            labelPlacement="end"
                                            onClick={(e) => {
                                                handleCheckItem(e, {
                                                    id: data?.id,
                                                    name: data?.name,
                                                    type: "brand",
                                                });
                                            }}
                                        />
                                    ))}
                                </FormGroup>
                            </FormControl>
                        </AccordionDetails>
                    </Accordion>
                </Box>

                {tag_group?.length > 0 &&
                    tag_group?.map((item, index) => (
                        <Box key={index}>
                            <Divider />
                            <Accordion
                                expanded={expanded === index}
                                onChange={handleChange(index)}
                            >
                                <AccordionSummary
                                    expandIcon={
                                        expanded === index ? (
                                            <RemoveIcon />
                                        ) : (
                                            <AddIcon />
                                        )
                                    }
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography>
                                        {capitalize(item?.name)}
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <FormControl component="fieldset">
                                        <FormGroup aria-label="position" row>
                                            {item?.tag.map((data, index) => (
                                                <FormControlLabel
                                                    key={index}
                                                    value={data}
                                                    control={<Checkbox />}
                                                    label={capitalize(
                                                        data?.name
                                                    )}
                                                    checked={tagGroup?.includes(data?.id)}
                                                    labelPlacement="end"
                                                    onClick={(e) => {
                                                        handleCheckItem(e, {
                                                            id: data?.id,
                                                            name: data?.name,
                                                            type: "tagGroup",
                                                        });
                                                    }}
                                                />
                                            ))}
                                        </FormGroup>
                                    </FormControl>
                                </AccordionDetails>
                            </Accordion>
                        </Box>
                    ))}
            </Box>
        </Box>
    );
};
