const { Box } = require("@mui/material");
const { Footer } = require("components/Footer/Footer");
const { Header } = require("components/Header/Header");

export default function Layout({children}){

    return (
        <Box
            sx={{
                backgroundImage: "#fff", //`url(${backImg})`,
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                backgroundAttachment: "fixed",
                backgroundPosition: "50%",
                minHeight: "100vh",
                margin: 0,
                width: "100% !important"
            }}
            >
            <Box
                sx={{
                display: "flex",
                justifyContent: "space-between",
                // alignItems: 'center',
                flexDirection: "column",
                paddingLeft: 0,
                width: "100%"
                }}
            >
            
                <Header />
                    <Box>{children}</Box>
                <Footer />
            </Box>
        </Box>
    )
}

