import { Box } from '@mui/material';
import { Footer } from 'components/Footer/Footer';
import { Header } from 'components/Header/Header';

export const withPublicPage = (WrapperComponent) => {

    const wrappedPage = (props) => {
        return (
                <Box
                    sx={{
                        backgroundImage: "#fff", //`url(${backImg})`,
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundAttachment: "fixed",
                        backgroundPosition: "50%",
                        minHeight: "100vh",
                        margin: 0,
                        width: "100% !important"
                    }}
                    >
                    <Box
                        sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        // alignItems: 'center',
                        flexDirection: "column",
                        paddingLeft: 0,
                        width: "100%"
                        }}
                    >
                    
                        <Header />
                            <WrapperComponent {...props}  />
                        <Footer />
                    </Box>
                </Box>
        )
    }

    
  return wrappedPage;
}
