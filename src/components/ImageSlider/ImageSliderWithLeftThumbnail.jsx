import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Box, Typography } from "@mui/material";
import { useImageSlickSliderStyle } from "assets/stylesheets/sliders/imageSlickSliderStylesheet";
import { IMAGE_URL } from "helpers/config";
import { isEmpty } from "helpers/functions";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import Slider from "react-slick/lib/slider";

export const ImageSliderWithLeftThumbnail = ({ images, brand }) => {
    const classes = useImageSlickSliderStyle();
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [slider1, setSlider1] = useState(null);
    const [slider2, setSlider2] = useState(null);

    const thumbnailRefs = useRef([]);

    const settingsMain = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: ".slider-nav",
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    arrows:true
                },
            },
        ]
    };

    const settingsThumbs = {
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: ".slider-for",
        dots: false,
        vertical: true,
        verticalSwiping: true,
        centerMode: true,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: "60px",
        className: "center",
        nextArrow: <KeyboardArrowUpIcon />,
        prevArrow: <KeyboardArrowDownIcon />,
        adaptiveHeight: true
    };

    useEffect(() => {
        setNav1(slider1);
        setNav2(slider2);
    });
    return (
        <Box className={classes.leftThumb}>
            <Box className="slider-wrapper">
                <Box className="thumbnail-slider-wrap" sx={{ display: {md: "inline-block", xs: "none"} }}>
                    <Box sx={{ minHeight: 90, display: {md: "inline-block", xs: "none"} }}>
                        {!isEmpty(brand) && (
                            <Box
                                component={Link}
                                href={`/${brand?.slug}`}
                                sx={{
                                    textAlign: "center",
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    paddingBottom: "20px",
                                    color: "#000000"
                                }}
                            >
                                <Box
                                    component={"img"}
                                    src={IMAGE_URL + "brand-image/" + brand?.image}
                                    width={80}
                                />
                                <Typography sx={{ fontSize: 12 }}>
                                    Click to view all {brand?.name}
                                </Typography>
                            </Box>
                        )}
                    </Box>
                    <Slider
                        {...settingsThumbs}
                        asNavFor={nav1}
                        ref={(slider) => setSlider2(slider)}
                    >
                        {images.map((slide) => (
                            <Box className="slick-slide" key={slide.id}>
                                <Box
                                    component={"img"}
                                    className="slick-slide-image"
                                    src={
                                        slide?.title === "master_product"
                                            ? IMAGE_URL +
                                              "product-fetured-image/" +
                                              slide?.image
                                            : IMAGE_URL +
                                              "product-image/" +
                                              slide?.image
                                    }
                                    alt={slide?.title}
                                />
                            </Box>
                        ))}
                    </Slider>
                </Box>

                <Box sx={{ width: "80%" }}>
                    <Slider
                        {...settingsMain}
                        asNavFor={nav2}
                        ref={(slider) => setSlider1(slider)}
                    >
                        {images.map((slide) => (
                            <Box key={slide.id}>
                                <Box component={"img"}
                                    className="slick-slide-image"
                                    src={
                                        slide?.title === "master_product"
                                            ? IMAGE_URL +
                                              "product-fetured-image/" +
                                              slide?.image
                                            : IMAGE_URL +
                                              "product-image/" +
                                              slide?.image
                                    }
                                    alt={slide?.title}
                                />
                            </Box>
                        ))}
                    </Slider>
                </Box>
            </Box>
        </Box>
    );
};
