import { useImageSlickSliderStyle } from 'assets/stylesheets/sliders/imageSlickSliderStylesheet';
import { useState } from 'react';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

import { Box } from '@mui/material';
import { IMAGE_URL } from 'helpers/config';
// import './style.css';

function ImageSlickSlider({ images }) {
    const classes = useImageSlickSliderStyle();
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [stateStyle, setStateStyle] = useState({
        backgroundImage: `url(${
          IMAGE_URL + "product-image/" + images[selectedIndex]?.image
        })`,
        backgroundPosition: "0% 0%"
      });

    console.log(images);

  return (
    <Box className={classes.slickSlider}>
        <div className="slider">           
            <div className="slider__image">
            <img src={images[selectedIndex]?.title === "master_product" ? IMAGE_URL + 'product-fetured-image/' + images[selectedIndex]?.image : IMAGE_URL + 'product-image/' + images[selectedIndex]?.image} alt="" style={stateStyle}  />
            </div>
            <div className="slider__thumbnails">
                {images?.length > 0 && images?.map((item, index) => (
                    <div
                    key={index}
                    className={`slider__thumbnail ${selectedIndex === index ? 'is-selected' : ''}`}
                    onClick={() => setSelectedIndex(index)}
                    >
                    <img src={item?.title === "master_product" ? IMAGE_URL + 'product-fetured-image/' + item?.image : IMAGE_URL + 'product-image/' + item?.image} alt={item?.title} />
                    </div>
                ))}
            </div>
        </div>
    </Box>
  );
}

export default ImageSlickSlider;