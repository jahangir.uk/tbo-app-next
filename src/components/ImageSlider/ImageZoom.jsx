import { Box } from '@mui/material';
import { useImageSlickSliderStyle } from 'assets/stylesheets/sliders/imageSlickSliderStylesheet';
import { useState } from 'react';

const ImageZoom = ({ imageUrl, zoomImageUrl }) => {
  const [isZoomed, setIsZoomed] = useState(false);
  const classes = useImageSlickSliderStyle();
  const handleMouseEnter = () => {
    setIsZoomed(true);
  };

  const handleMouseLeave = () => {
    setIsZoomed(false);
  };

  return (
    <Box className={classes.zoomImage}>
        <div
      className={`imageContainer ${isZoomed ? 'zoomed' : ''}`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <img className={'image'} src={imageUrl} alt="Original Image" />
      {isZoomed && (
        <img className={'zoomedImage'} src={zoomImageUrl} alt="Zoomed Image" />
      )}
    </div>
    </Box>
  );
};

export default ImageZoom;