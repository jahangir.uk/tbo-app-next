import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Box, IconButton } from "@mui/material";
import { useImageSlickSliderStyle } from "assets/stylesheets/sliders/imageSlickSliderStylesheet";
import { IMAGE_URL } from "helpers/config";
import { useEffect, useRef, useState } from "react";

function LinkedListSlider({ images }) {
    const classes = useImageSlickSliderStyle();
    const [currentItemIndex, setCurrentItemIndex] = useState(0);
    const [thumbnailListWidth, setThumbnailListWidth] = useState(0);
    const [stateStyle, setStateStyle] = useState({
        backgroundImage: `url(${
          IMAGE_URL + "product-image/" + images[currentItemIndex]?.image
        })`,
        backgroundPosition: "0% 0%"
      });

  const handlePrevClick = () => {
    setCurrentItemIndex(currentItemIndex === 0 ? images.length - 1 : currentItemIndex - 1);
  };

  const handleNextClick = () => {
    setCurrentItemIndex(currentItemIndex === images.length - 1 ? 0 : currentItemIndex + 1);
  };

  const handleThumbnailClick = (index) => {
    setCurrentItemIndex(index);
  };

  const thumbnailRefs = useRef([]);
  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    // console.log("Left", left);
    const x = ((e.pageX - left) / width) * 100;
    const y = ((e.pageY - top) / height) * 100;
    setStateStyle({ ...stateStyle, backgroundPosition: `${x}% ${y}%` });
  };

  useEffect(() => {
    const thumbnailListElement = document.querySelector('.thumbnail-list');
    setThumbnailListWidth(thumbnailListElement.clientWidth);
  }, []);

  useEffect(() => {
    const container = thumbnailRefs.current.parentElement;
    const selectedThumbnail = thumbnailRefs.current[currentItemIndex];
    if (!container || !selectedThumbnail) return;
    const containerHeight = container.offsetHeight;
    const thumbnailHeight = selectedThumbnail.offsetHeight;
    const thumbnailTop = selectedThumbnail.offsetTop;
    const scrollTop = thumbnailTop - containerHeight / 2 + thumbnailHeight / 2;
    console.log(scrollTop, container.scrollTop);
    selectedThumbnail.scrollTop = 110;
    console.log(selectedThumbnail.scrollTop);

    setStateStyle({
      backgroundImage: `url(${
        IMAGE_URL + "product-image/" + images[currentItemIndex].image
      })`,
      backgroundPosition: "0% 0%"
    });
  }, [currentItemIndex,images]);

  const getThumbnailListTransformValue = () => {
    const thumbnailElementWidth = 100; // width of each thumbnail element in pixels
    const thumbnailListPadding = 20; // padding between each thumbnail element in pixels
    const centerIndex = Math.floor(thumbnailListWidth / (thumbnailElementWidth + thumbnailListPadding)) / 2;
    const difference = currentItemIndex - centerIndex;
    const transformValue = thumbnailElementWidth * difference + thumbnailListPadding * difference;
    return `translateY(-${transformValue}px)`;
  };

    return (
        <Box className={classes.root}>
            <div className="linked-list-slider">
                <Box sx={{ textAlign: 'center', width: '18%' }}>
                    <IconButton className="prev-button" onClick={handlePrevClick}><KeyboardArrowUpIcon /></IconButton>
                    <div className="thumbnail-list" ref={(el) => (thumbnailRefs.current.parentElement = el)}>
                    
                        {images.map((item, index) => (
                        <button
                        style={{ transform: getThumbnailListTransformValue() }}
                            key={index}
                            className={index === currentItemIndex ? 'thumbnail current' : 'thumbnail'}
                            onClick={() => setCurrentItemIndex(index)}
                            ref={(el) => (thumbnailRefs.current[index] = el)}
                        >
                            <img src={item?.title === "master_product" ? IMAGE_URL + 'product-fetured-image/' + item?.image : IMAGE_URL + 'product-image/' + item?.image} alt={item?.title} />
                        </button>
                        ))}
                    </div>
                    <IconButton className="next-button" onClick={handleNextClick}><KeyboardArrowDownIcon /></IconButton>
                </Box>
                
                <div className="slider-wrapper">
                   
                    <div className="current-item" onMouseMove={handleMouseMove} >
                        <img src={images[currentItemIndex].title === "master_product" ? IMAGE_URL + 'product-fetured-image/' + images[currentItemIndex].image : IMAGE_URL + 'product-image/' + images[currentItemIndex].image} alt="" style={stateStyle} />
                    </div>
                    
                </div>
            </div>
        </Box>
    );
}

export default LinkedListSlider;
