import { Box } from "@mui/material";
import { useImageSlickSliderStyle } from "assets/stylesheets/sliders/imageSlickSliderStylesheet";
import { IMAGE_URL } from "helpers/config";
import { useEffect, useState } from "react";
import Slider from "react-slick/lib/slider";

export const ImageSliderWithBottomThumbnail = ({ images }) => {
    const classes = useImageSlickSliderStyle();
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [slider1, setSlider1] = useState(null);
    const [slider2, setSlider2] = useState(null);

    const settingsMain = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: ".slider-nav",
    };

    const settingsThumbs = {
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: ".slider-for",
        dots: false,
        initialSlide: 0,
        centerMode: false,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: "60px",
        className: "center",
    };

    const handleMouseMove = (e) => {
        const { left, top, width, height } = e.target.getBoundingClientRect();
        // console.log("Left", left);
        const x = ((e.pageX - left) / width) * 100;
        const y = ((e.pageY - top) / height) * 100;
        setStateStyle({ ...stateStyle, backgroundPosition: `${x}% ${y}%` });
      };

    useEffect(() => {
        setNav1(slider1);
        setNav2(slider2);
    });

    console.log(nav1, nav2);
    return (
        <Box className={classes.bottomThumb}>
            <Box className="slider-wrapper">
                <Slider
                    {...settingsMain}
                    asNavFor={nav2}
                    ref={(slider) => setSlider1(slider)}
                >
                    {images.map((slide) => (
                        <Box className="main-image" sx={{ background: "#c7c4c4", padding: "5px" }} key={slide.id}>
                            
                            <Box
                                component={"img"}
                                className="slick-slide-image"
                                src={
                                    slide?.title === "master_product"
                                        ? IMAGE_URL +
                                          "product-fetured-image/" +
                                          slide?.image
                                        : IMAGE_URL +
                                          "product-image/" +
                                          slide?.image
                                }
                                alt={slide?.title}
                            />
                           
                        </Box>
                    ))}
                </Slider>
                <Box className="thumbnail-slider-wrap">
                    <Slider
                        {...settingsThumbs}
                        asNavFor={nav1}
                        ref={(slider) => setSlider2(slider)}
                    >
                        {images.map((slide) => (
                            <Box className="slick-slide" key={slide.id}>
                                <Box
                                    component={"img"}
                                    className="slick-slide-image"
                                    src={
                                        slide?.title === "master_product"
                                            ? IMAGE_URL +
                                              "product-fetured-image/" +
                                              slide?.image
                                            : IMAGE_URL +
                                              "product-image/" +
                                              slide?.image
                                    }
                                    alt={slide?.title}
                                />
                            </Box>
                        ))}
                    </Slider>
                </Box>
            </Box>
        </Box>
    );
};
