import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Box, IconButton, Typography } from "@mui/material";
import { IMAGE_URL } from "helpers/config";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";

const ImageSlider = ({ images, brand }) => {
    const [currentIndex, setCurrentIndex] = useState(1);
    const [stateStyle, setStateStyle] = useState({
        backgroundImage: `url(${
            IMAGE_URL + "product-image/" + images[currentIndex].image
        })`,
        backgroundPosition: "0% 0%",
    });
    const thumbnailRefs = useRef([]);

    useEffect(() => {
        const container = thumbnailRefs.current.parentElement;
        const selectedThumbnail = thumbnailRefs.current[currentIndex];
        if (!container || !selectedThumbnail) return;
        const containerHeight = container.offsetHeight;
        const thumbnailHeight = selectedThumbnail.offsetHeight;
        const thumbnailTop = selectedThumbnail.offsetTop;
        const scrollTop =
            thumbnailTop - containerHeight / 2 + thumbnailHeight / 2;
        console.log(scrollTop, container.scrollTop);
        selectedThumbnail.scrollTop = 110;
        console.log(selectedThumbnail.scrollTop);

        setStateStyle({
            backgroundImage: `url(${
                IMAGE_URL + "product-image/" + images[currentIndex].image
            })`,
            backgroundPosition: "0% 0%",
        });
    }, [currentIndex, images]);

    const handleMouseMove = (e) => {
        const { left, top, width, height } = e.target.getBoundingClientRect();
        // console.log("Left", left);
        const x = ((e.pageX - left) / width) * 100;
        const y = ((e.pageY - top) / height) * 100;
        setStateStyle({ ...stateStyle, backgroundPosition: `${x}% ${y}%` });
    };

    const renderThumbnails = () => {
        return images?.map((imageUrl, index) => (
            <Box
                component={"img"}
                key={index}
                src={IMAGE_URL + "product-image/" + imageUrl.image}
                onClick={() => setCurrentIndex(index)}
                style={{
                    width: "100px",
                    height: "auto",
                    margin: "5px",
                    background: "#fff",
                    transition: "1s",
                    border: index === currentIndex ? "2px solid #000" : "none",
                }}
                ref={(el) => (thumbnailRefs.current[index] = el)}
                className={index === currentIndex ? "selected" : ""}
            />
        ));
    };

    const renderImage = () => {
        return (
            <Box
                onMouseMove={handleMouseMove}
                component={"figure"}
                sx={{
                    width: "600px",
                    backgroundRepeat: "no-repeat",
                    margin: 0,
                    height: 600,

                    "& img": {
                        width: "100%",
                    },

                    "&:hover img": {
                        opacity: 0,
                    },
                }}
                style={stateStyle}
            >
                <Box
                    component={"img"}
                    src={
                        IMAGE_URL +
                        "product-image/" +
                        images[currentIndex].image
                    }
                />
            </Box>
        );
    };

    const previous = () => {
        const newIndex =
            currentIndex === 0 ? images.length - 1 : currentIndex - 1;
        setCurrentIndex(newIndex);
    };

    const next = () => {
        const newIndex =
            currentIndex === images.length - 1 ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    };

    return (
        <Box style={{ display: "flex", alignItems: "center" }}>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}
            >
                <Box
                    component={Link}
                    href={`/${brand?.slug}`}
                    sx={{ textAlign: "center" }}
                >
                    <Box
                        component={"img"}
                        src={IMAGE_URL + "brand-image/" + brand?.image}
                        width={80}
                    />
                    <Typography sx={{ fontSize: 12 }}>
                        Click to view all {brand?.name}
                    </Typography>
                </Box>
                <IconButton
                    onClick={previous}
                    style={{
                        padding: "10px",
                        border: "none",
                        cursor: "pointer",
                    }}
                >
                    <KeyboardArrowUpIcon />
                </IconButton>
                <Box
                    sx={{
                        width: "100%",
                    }}
                >
                    <Box
                        className="thumbnail"
                        sx={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            padding: "10px",
                            height: 300,
                            justifyContent: "center",
                            overflowY: "scroll",
                            position: "relative",
                            // transform: `translateY(calc(-50% + (100vw / (2 * ${images?.length})) * ${currentIndex}))`
                        }}
                        ref={(el) => (thumbnailRefs.current.parentElement = el)}
                    >
                        {renderThumbnails()}
                    </Box>
                </Box>
                <IconButton
                    onClick={next}
                    style={{
                        padding: "10px",
                        border: "none",
                        cursor: "pointer",
                    }}
                >
                    <KeyboardArrowDownIcon />
                </IconButton>
            </Box>
            <Box
                style={{
                    marginLeft: "50px",
                    boxShadow: "0px 0px 10px 0px #ddd",
                    padding: "10px",
                    width: 600,
                }}
            >
                {renderImage()}
            </Box>
        </Box>
    );
};

export default ImageSlider;
