import { Box } from '@mui/material';
import { useImageSlickSliderStyle } from 'assets/stylesheets/sliders/imageSlickSliderStylesheet';
import { useRef, useState } from 'react';

const ProductImageZoom = ({ imageUrl }) => {
  const [zoomed, setZoomed] = useState(false);
  const [cursorPosition, setCursorPosition] = useState({ x: 0, y: 0 });
  const classes = useImageSlickSliderStyle();
  const [stateStyle, setStateStyle] = useState({
    backgroundImage: `url(${imageUrl})`,
    backgroundPosition: "0% 0%",
});
const thumbnailRefs = useRef([]);
  
  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    // console.log("Left", left);
    const x = ((e.pageX - left) / width) * 100;
    const y = ((e.pageY - top) / height) * 100;
    setStateStyle({ ...stateStyle, backgroundPosition: `${x}% ${y}%` });
};


 console.log(imageUrl);

  return (
    <Box
        style={{
            marginLeft: "50px",
            boxShadow: "0px 0px 10px 0px #ddd",
            padding: "10px",
            width: 600,
        }}
    >
        <Box
                onMouseMove={handleMouseMove}
                component={"figure"}
                sx={{
                    width: "600px",
                    backgroundRepeat: "no-repeat",
                    margin: 0,
                    height: 600,

                    "& img": {
                        width: "100%",
                    },

                    "&:hover img": {
                        opacity: 0,
                    },
                }}
                style={stateStyle}
            >
                <Box
                    component={"img"}
                    src={imageUrl}
                />
            </Box>
    </Box>
  );
};

export default ProductImageZoom;