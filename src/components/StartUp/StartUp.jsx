import { APP_USER_DETAILS, CART_ITEMS, MAIN_MENU_ITEMS } from "helpers/config";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getUserDetails } from "redux/action/authActions";
import { updateCartData } from "redux/action/cart";
import { fetchMenuItems } from "redux/action/menuAction";
import { appStorage } from "utils/storage";

export const StartUp = () => {
    const dispatch = useDispatch();

    useEffect(()=>{
        const cart_data = appStorage.get(CART_ITEMS);
        const user_details = appStorage.get(APP_USER_DETAILS);
        const main_menu = appStorage.get(MAIN_MENU_ITEMS);
        if(cart_data){
            dispatch(updateCartData(JSON.parse(cart_data)));
        }

        if(user_details){
            dispatch(getUserDetails());
        }
        dispatch(fetchMenuItems());
    }, [])
  return (
    <></>
  )
}
