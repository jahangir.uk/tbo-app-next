import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    FormControl,
    Grid,
    TextField,
    Typography,
    styled,
} from "@mui/material";
import { COLORS } from "assets/styles/colors/colors";
import { useMyAccountStyle } from "assets/stylesheets/my-account/myAccountStyleSheet";
import { countries } from "components/dummy-data/countries";
import getCountryName from "helpers/countries";
import useSnackbar from "hooks/useSnackbar";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { billingAddressUpdate, shippingAddressUpdate } from "redux/action/authActions";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.primary,
    },
    "& .MuiOutlinedInput-root": {
        "& fieldset": {
            borderColor: COLORS.gray,
        },
        "&:hover fieldset": {
            borderColor: COLORS.black,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.primary,
        },
    },
});

export const Address = () => {
    const classes = useMyAccountStyle();
    const [billAddressEdit, setBillAddressEdit] = useState(false);
    const [shipAddressEdit, setShipAddressEdit] = useState(false);
    const dispatch = useDispatch();
    const user = useSelector(state=>state?.auth?.userDetails);
    const snackbar = useSnackbar();
    const country_list = countries;
    const {
        handleSubmit,
        register,
        formState: { errors },
        reset,
        clearErrors,
        setError,
    } = useForm();

    const onShippingSubmit = async (data) => {
        const res = await dispatch(shippingAddressUpdate(data));
        if(res?.status === "success"){
            snackbar("Shipping Address Update Successfully", {
                variant: "success"
            });
        }
        console.log(res);
        setShipAddressEdit(false);
    };

    const onBillingSubmit = async (data) => {
        const res = await dispatch(billingAddressUpdate(data));
        if(res?.status === "success"){
            snackbar("Billing Address Update Successfully", {
                variant: "success"
            });
        }
        console.log(res);
        setBillAddressEdit(false);
    };

    return (
        <Box className={classes.details}>
            {billAddressEdit || shipAddressEdit ? (
                <>
                    {billAddressEdit && (
                        <Box
                            className="form"
                            component="form"
                            onSubmit={handleSubmit(onBillingSubmit)}
                        >
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    paddingRight: "20px",
                                }}
                            >
                                <Typography
                                    onClick={() =>
                                        setBillAddressEdit(!billAddressEdit)
                                    }
                                    sx={{
                                        cursor: "pointer",
                                        display: "inline",
                                        padding: "4px 10px",
                                        "&:hover": {
                                            background: "#e7e7e7",
                                            borderRadius: "5px",
                                        },
                                    }}
                                >
                                    {" "}
                                    Back
                                </Typography>
                                <Typography sx={{ fontSize: 20 }}>
                                    Billing Address
                                </Typography>
                            </Box>
                            <Grid container>
                                <Grid item md={12}>
                                <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Email address"
                                            placeholder="example@gmail.com"
                                            fullWidth
                                            defaultValue={user?.billing_email}
                                            multiline
                                            size="small"
                                            {...register("billing_email", {
                                                type: "billing_email",
                                            })}
                                            error={!!errors.billing_email}
                                            helperText={
                                                errors?.billing_email
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Phone"
                                            placeholder="1524882595"
                                            fullWidth
                                            defaultValue={user?.billing_phone}
                                            multiline
                                            size="small"
                                            {...register("billing_phone", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_phone}
                                            helperText={
                                                errors?.billing_phone
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Street address"
                                            placeholder="House number and street name"
                                            fullWidth
                                            defaultValue={user?.billing_street_address}
                                            multiline
                                            size="small"
                                            {...register("billing_street_address", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_street_address}
                                            helperText={
                                                errors?.billing_street_address
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            multiline
                                            label=""
                                            placeholder="Apartment, suite, unit, etc.(optional)"
                                            fullWidth
                                            defaultValue={user?.billing_street_address2}
                                            size="small"
                                            {...register("billing_street_address2", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_street_address2}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            multiline
                                            label=""
                                            placeholder="Apartment, suite, unit, etc.(optional)"
                                            fullWidth
                                            defaultValue={user?.billing_street_address3}
                                            size="small"
                                            {...register("billing_street_address3", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_street_address3}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Postcode"
                                            fullWidth
                                            size="small"
                                            defaultValue={user?.billing_post_code}
                                            {...register("billing_post_code", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_post_code}
                                            helperText={
                                                errors?.billing_post_code?.message ?? ""
                                            }
                                        />
                                    </FormControl>

                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            select
                                            label="Country/Region"
                                            placeholder="House number and street name"
                                            fullWidth
                                            size="small"
                                            {...register("billing_country", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_country}
                                            defaultValue={user?.billing_country ?? "GB"}
                                            SelectProps={{
                                                native: true,
                                            }}
                                        >
                                            {country_list?.length > 0 &&
                                                country_list.map(
                                                    (item, index) => (
                                                        <option
                                                            value={item?.code}
                                                            key={index}
                                                        >{`${item?.name} (${item?.code})`}</option>
                                                    )
                                                )}
                                        </CssTextField>
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Town / City"
                                            fullWidth
                                            size="small"
                                            defaultValue={user?.billing_city}
                                            {...register("billing_city", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_city}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            label="County (optional)"
                                            placeholder=""
                                            fullWidth
                                            size="small"
                                            defaultValue={user?.billing_county}
                                            {...register("billing_county", {
                                                type: "text",
                                            })}
                                            error={!!errors.billing_county}
                                        />
                                    </FormControl>
                                    <FormControl
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <Button type="submit">
                                            Update Billing
                                        </Button>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Box>
                    )}

                    {shipAddressEdit && (
                        <Box
                            className="form"
                            component="form"
                            onSubmit={handleSubmit(onShippingSubmit)}
                        >
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    paddingRight: "20px",
                                }}
                            >
                                <Typography
                                    onClick={() =>
                                        setShipAddressEdit(!shipAddressEdit)
                                    }
                                    sx={{
                                        cursor: "pointer",
                                        display: "inline",
                                        padding: "4px 10px",
                                        "&:hover": {
                                            background: "#e7e7e7",
                                            borderRadius: "5px",
                                        },
                                    }}
                                >
                                    {" "}
                                    Back
                                </Typography>
                                <Typography sx={{ fontSize: 20 }}>
                                    Shipping Address
                                </Typography>
                            </Box>
                            <Grid container>
                                <Grid item md={12}>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Email address"
                                            placeholder="example@gmail.com"
                                            fullWidth
                                            defaultValue={user?.shipping_email}
                                            multiline
                                            size="small"
                                            {...register("shipping_email", {
                                                type: "shipping_email",
                                            })}
                                            error={!!errors.shipping_email}
                                            helperText={
                                                errors?.shipping_email
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Phone"
                                            placeholder="1524882595"
                                            fullWidth
                                            multiline
                                            size="small"
                                            defaultValue={user?.shipping_phone}
                                            {...register("shipping_phone", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_phone}
                                            helperText={
                                                errors?.shipping_phone
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Street address"
                                            placeholder="House number and street name"
                                            fullWidth
                                            multiline
                                            defaultValue={user?.shipping_street_address}
                                            size="small"
                                            {...register("shipping_street_address", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_street_address}
                                            helperText={
                                                errors?.shipping_street_address
                                                    ?.message ?? ""
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            multiline
                                            label=""
                                            placeholder="Apartment, suite, unit, etc.(optional)"
                                            fullWidth
                                            defaultValue={user?.shipping_street_address2}
                                            size="small"
                                            {...register("shipping_street_address2", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_street_address2}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            multiline
                                            label=""
                                            placeholder="Apartment, suite, unit, etc.(optional)"
                                            fullWidth
                                            defaultValue={user?.shipping_street_address3}
                                            size="small"
                                            {...register("shipping_street_address3", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_street_address3}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Postcode"
                                            fullWidth
                                            size="small"
                                            defaultValue={user?.shipping_post_code}
                                            {...register("shipping_post_code", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_post_code}
                                            helperText={
                                                errors?.shipping_post_code?.message ?? ""
                                            }
                                        />
                                    </FormControl>

                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            select
                                            label="Country/Region"
                                            placeholder="House number and street name"
                                            fullWidth
                                            size="small"
                                            {...register("shipping_country", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_country}
                                            defaultValue={user?.shipping_country ?? "GB"}
                                            SelectProps={{
                                                native: true,
                                            }}
                                        >
                                            {country_list?.length > 0 &&
                                                country_list.map(
                                                    (item, index) => (
                                                        <option
                                                            value={item?.code}
                                                            key={index}
                                                        >{`${item?.name} (${item?.code})`}</option>
                                                    )
                                                )}
                                        </CssTextField>
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            required
                                            label="Town / City"
                                            fullWidth
                                            defaultValue={user?.shipping_city}
                                            size="small"
                                            {...register("shipping_city", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_city}
                                        />
                                    </FormControl>
                                    <FormControl
                                        variant="outlined"
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <CssTextField
                                            label="County (optional)"
                                            placeholder=""
                                            fullWidth
                                            defaultValue={user?.shipping_county}
                                            size="small"
                                            {...register("shipping_county", {
                                                type: "text",
                                            })}
                                            error={!!errors.shipping_county}
                                        />
                                    </FormControl>
                                    <FormControl
                                        sx={{
                                            marginRight: { md: "4px", xs: 0 },
                                            marginTop: 2,
                                            width: "49%",
                                        }}
                                    >
                                        <Button type="submit">
                                            Update Shipping
                                        </Button>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Box>
                    )}
                </>
            ) : (
                <Box className="details">
                    <Grid container spacing={3}>
                        <Grid item md={6}>
                            <Card className="personal-info">
                                <CardHeader
                                    title={
                                        <Box>
                                            <Typography component={"h1"}>
                                                Shipping address{" "}
                                                <span
                                                    onClick={() =>
                                                        setShipAddressEdit(
                                                            !shipAddressEdit
                                                        )
                                                    }
                                                >
                                                    Edit
                                                </span>
                                            </Typography>
                                        </Box>
                                    }
                                />
                                <CardContent>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Street</strong>
                                        </Box>{" "}
                                        <Box>{user?.shipping_street_address}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>City</strong>
                                        </Box>{" "}
                                        <Box>{user?.shipping_city}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Post Code</strong>
                                        </Box>{" "}
                                        <Box>{user?.shipping_post_code}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Country</strong>
                                        </Box>{" "}
                                        <Box>{getCountryName(user?.shipping_country)}</Box>{" "}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item md={6}>
                            <Card className="personal-info">
                                <CardHeader
                                    title={
                                        <Box>
                                            <Typography component={"h1"}>
                                                Billing address{" "}
                                                <span
                                                    onClick={() =>
                                                        setBillAddressEdit(
                                                            !billAddressEdit
                                                        )
                                                    }
                                                >
                                                    Edit
                                                </span>
                                            </Typography>
                                        </Box>
                                    }
                                />
                                <CardContent>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Street</strong>
                                        </Box>{" "}
                                        <Box>{user?.billing_street_address}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>City</strong>
                                        </Box>{" "}
                                        <Box>{user?.billing_city}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Post Code</strong>
                                        </Box>{" "}
                                        <Box>{user?.billing_post_code}</Box>{" "}
                                    </Box>
                                    <Box className="item">
                                        <Box className="heading">
                                            <strong>Country</strong>
                                        </Box>{" "}
                                        <Box>{getCountryName(user?.billing_country)}</Box>{" "}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Box>
            )}
        </Box>
    );
};
