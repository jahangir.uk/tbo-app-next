import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    FormControl,
    Grid,
    TextField,
    Typography,
    styled
} from "@mui/material";
import { COLORS } from "assets/styles/colors/colors";
import { useMyAccountStyle } from "assets/stylesheets/my-account/myAccountStyleSheet";
import { countries } from "components/dummy-data/countries";
import getCountryName from "helpers/countries";
import useSnackbar from "hooks/useSnackbar";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { getUserDetails, userDetailsUpdate } from "redux/action/authActions";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.primary,
    },
    "& .MuiOutlinedInput-root": {
        "& fieldset": {
            borderColor: COLORS.gray,
        },
        "&:hover fieldset": {
            borderColor: COLORS.black,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.primary,
        },
    },
});

export const MyDetails = () => {
    const [isEdit, setIsEdit] = useState(false);
    const user = useSelector(state=>state.auth.userDetails);
    const classes = useMyAccountStyle();
    const country_list = countries;
    const dispatch = useDispatch();
    const snackbar = useSnackbar();
    const {
        handleSubmit,
        register,
        formState: { errors },
        reset,
        clearErrors,
        setError,
    } = useForm();

    const onSubmit = async (data) => {
       
        const res = await dispatch(userDetailsUpdate(data));

        if(res?.status === "success"){
            snackbar("Information Update successfully", {
                variant: "success"
            });
            setIsEdit(false);
            dispatch(getUserDetails());
        }
        // console.log(res);
      };

    //   console.log(user);

    return (
        <Box className={classes.details}>
            {isEdit ? (
                
                    <Box className="form" component="form" onSubmit={handleSubmit(onSubmit)}>
                        <Typography onClick={()=> setIsEdit(!isEdit)} 
                        sx={{ cursor: 'pointer', display: 'inline', padding: '4px 10px', '&:hover':{background: '#e7e7e7',  borderRadius: '5px' } }}> Back</Typography>
                        <Grid container>
                            <Grid item md={12}>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        id="first-name"
                                        required
                                        label="First Name"
                                        placeholder="Jhon"
                                        defaultValue={user?.first_name}
                                        fullWidth
                                        size="small"
                                        {...register("first_name", {
                                            type: "text",
                                        })}
                                        error={!!errors.first_name}
                                        
                                        helperText={
                                            errors?.first_name?.message ?? ""
                                        }
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginLeft: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        required
                                        label="Last Name"
                                        placeholder=" Due"
                                        defaultValue={user?.last_name}
                                        fullWidth
                                        size="small"
                                        {...register("last_name", {
                                            type: "text",
                                        })}
                                        error={!!errors.last_name}
                                        
                                        helperText={
                                            errors?.last_name?.message ?? ""
                                        }
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        InputProps={{
                                            readOnly: true,
                                        }}
                                        label="Email"
                                        placeholder="example@gmail.com"
                                        fullWidth
                                        defaultValue={user?.email}
                                        size="small"
                                        {...register("email", {
                                            type: "email",
                                        })}
                                        error={!!errors.email}
                                        
                                        helperText={
                                            errors?.email?.message ?? ""
                                        }
                                    />
                                </FormControl>

                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        label="Street address"
                                        placeholder="House number and street name"
                                        fullWidth
                                        required
                                        multiline
                                        defaultValue={user?.street_address}
                                        size="small"
                                        {...register("street_address", {
                                            type: "text",
                                        })}
                                        error={!!errors.street_address}
                                        
                                        helperText={
                                            errors?.street_address?.message ??
                                            ""
                                        }
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        multiline
                                        label=""
                                        placeholder="Apartment, suite, unit, etc.(optional)"
                                        fullWidth
                                        defaultValue={user?.street_address2}
                                        size="small"
                                        {...register("street_address2", {
                                            type: "text",
                                        })}
                                        error={!!errors.street_address2}
                                       
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        multiline
                                        label=""
                                        placeholder="Apartment, suite, unit, etc.(optional)"
                                        fullWidth
                                        defaultValue={user?.street_address3}
                                        size="small"
                                        {...register("street_address3", {
                                            type: "text",
                                        })}
                                        error={!!errors.street_address3}
                                        
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        required
                                        label="Postcode"
                                        fullWidth
                                        size="small"
                                        defaultValue={user?.post_code}
                                        {...register("post_code", {
                                            type: "text",
                                        })}
                                        error={!!errors.post_code}
                                        
                                        helperText={
                                            errors?.post_code?.message ?? ""
                                        }
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        label="Phone"
                                        required
                                        fullWidth
                                        size="small"
                                        {...register("phone", {
                                            type: "text",
                                        })}
                                        error={!!errors.phone}
                                        defaultValue={user?.phone}
                                        helperText={
                                            errors?.phone?.message ?? ""
                                        }
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        
                                        select
                                        label="Country/Region"
                                        placeholder="House number and street name"
                                        fullWidth
                                        size="small"
                                        {...register("country", {
                                            type: "text",
                                        })}
                                        error={!!errors.country}
                                        defaultValue={user?.country ?? "GB"}
                                        SelectProps={{
                                            native: true,
                                        }}
                                        
                                    >
                                        {country_list?.length > 0 &&
                                            country_list.map((item, index) => (
                                                <option
                                                    value={item?.code}
                                                    key={index}
                                                >{`${item?.name} (${item?.code})`}</option>
                                            ))}
                                    </CssTextField>
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        required
                                        label="Town / City"
                                        fullWidth
                                        defaultValue={user?.city}
                                        size="small"
                                        {...register("city", {
                                            type: "text",
                                        })}
                                        error={!!errors.city}
                                        
                                    />
                                </FormControl>
                                <FormControl
                                    variant="outlined"
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <CssTextField
                                        label="County (optional)"
                                        placeholder=""
                                        fullWidth
                                        defaultValue={user?.county}
                                        size="small"
                                        {...register("county", {
                                            type: "text",
                                        })}
                                        error={!!errors.county}
                                        
                                    />
                                </FormControl>
                                <FormControl
                                    
                                    sx={{
                                        marginRight: { md: "4px", xs: 0 },
                                        marginTop: 2,
                                        width: "49%",
                                    }}
                                >
                                    <Button type="submit">Update Profile</Button>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Box>
                
            ) : (
                <Box className="details">
                    <Typography component={"h1"}>
                        Account Details{" "}
                        <span onClick={() => setIsEdit(!isEdit)}>Edit</span>
                    </Typography>
                {user && (
                    <Grid container spacing={3} mt={2}>
                        <Grid item md={6}>
                            <Card className="personal-info">
                                <CardHeader title="Personal Info" />
                                <CardContent>
                                   <Box className="item"><Box className="heading"><strong>First Name</strong></Box> <Box>{user?.first_name}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>Last Name</strong></Box> <Box>{user?.last_name}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>Email</strong></Box> <Box>{user?.email}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>Phone</strong></Box> <Box>{user?.phone}</Box> </Box>
                              
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item md={6}>
                        <Card className="personal-info">
                                <CardHeader title="Address Info" />
                                <CardContent>
                                   <Box className="item"><Box className="heading"><strong>Street</strong></Box> <Box>{user?.street_address}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>City</strong></Box> <Box>{user?.city}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>Post Code</strong></Box> <Box>{user?.post_code}</Box> </Box>
                                   <Box className="item"><Box className="heading"><strong>Country</strong></Box> <Box>{getCountryName(user?.country)}</Box> </Box>
                              
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                )}
                    
                </Box>
            )}
        </Box>
    );
};
