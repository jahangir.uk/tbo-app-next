import { Box, Typography } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { formatDate } from "helpers/functions";
import Link from "next/link";

export const Orders = ({ orders }) => {
    const getStatus = (code) => {
        switch(code){
            case 1:
                return "Cancel";
            case 2:
                return "Processing";
            case 3:
                return "Complete";
            default:
                return "Shipping";
        }
    }
    return (
        <>
            {orders?.length > 0 ? (
                <TableContainer component={Paper} sx={{ width: "100%" }}>
                    <Table
                        sx={{ width: "100%" }}
                        size="small"
                        aria-label="a dense table"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell>Order</TableCell>
                                <TableCell align="left">Date</TableCell>
                                <TableCell align="left">Status</TableCell>
                                <TableCell align="left">Total</TableCell>
                                <TableCell align="left">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {orders?.length > 0 &&
                                orders.map((row, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{
                                            "&:last-child td, &:last-child th":
                                                { border: 0 },
                                        }}
                                    >
                                        <TableCell>
                                            <Link
                                                href={`/my-account/view-order/${row?.code}`}
                                            >
                                                #{row.invoice}
                                            </Link>
                                        </TableCell>
                                        <TableCell align="left">
                                            {formatDate(row.updatedAt)}
                                        </TableCell>
                                        <TableCell align="left">
                                            {getStatus(row.status)}
                                        </TableCell>
                                        <TableCell align="left">
                                            {row.grand_total}
                                        </TableCell>
                                        <TableCell align="left">
                                            <Link
                                                style={{
                                                    textDecoration: "none",
                                                    background: "#cccccc",
                                                    color: "#000000",
                                                    padding: "5px 15px",
                                                }}
                                                href={`/my-account/view-order/${row?.code}`}
                                            >
                                                View
                                            </Link>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : (
                <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", minHeight: 400 }}>
                    <Typography sx={{ fontWeight: 600, fontSize: 30, color: "red" }}>Your order list is empty, place your First order</Typography>
                    <Box
                        sx={{
                            textAlign: "center",
                            mt: 3,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                            gap: "20px",
                        }}
                    >
                        <Box
                            sx={{
                                background: "#891d1d",
                                padding: "10px 20px",
                                borderRadius: "5px",
                                "& a":{
                                    textDecoration: "none",
                                    fontWeight: 600,
                                    color: "#ffffff",
                                }
                            }}
                        >
                            <Link href={"/"}>Go to Home</Link>
                        </Box>
                        
                        <Box
                            sx={{
                                background: "#2c2c07",
                                padding: "7px 20px",
                                borderRadius: "5px",
                                cursor: "pointer",
                            }}
                        >
                            <Typography sx={{ 
                                textDecoration: "none",
                                fontWeight: 600,
                                color: "#ffffff",
                                padding: 0,
                                margin: 0,
                                "& a":{
                                    color: "#ffffff",
                                    textDecoration: "none"
                                }
                             }}><Link href={"/shop"}>Continue Shopping</Link></Typography>
                        </Box>
                    </Box>
                </Box>
            )}
        </>
    );
};
