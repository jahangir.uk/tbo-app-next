import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import {
    Box,
    Button,
    Chip,
    Fade,
    IconButton,
    Modal,
    Typography
} from "@mui/material";
import { isEmpty } from "helpers/functions";
import useSnackbar from "hooks/useSnackbar";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addToCart } from "redux/action/cart";
import ProductService from "services/ProductService";
import { AfterAddToCartModal } from "./AfterAddToCartModal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4
};

export const ProductVariation = ({ open, handleClose, item }) => {

    const [terms, setTerms] = useState([]);
    const [variantProducts, setVariantProducts] = useState([]);
    const dispatch = useDispatch();
    const snackbar = useSnackbar();
    const route = useRouter();
    const [cartModalOpen, setCartModalOpen] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const handleSelectVariant = (attrId, termsId) => {
        console.log(attrId, termsId);
       
        const selectTedTerms = item?.producttermsArr.find(
            (i) => i.termsId === termsId && i.attributeId === attrId
        );
        if (isEmpty(termsId)) {
            setTerms((prev) => prev.filter((i) => i.attributeId !== attrId));
        }
        console.log(selectTedTerms);
        const findIndex = terms.find(
            (data) =>
                data?.attributeId === selectTedTerms?.attributeId &&
                data?.termsId === selectTedTerms?.termsId
        );
        const hasAttr = terms.find(
            (data) => data?.attributeId === selectTedTerms?.attributeId
        );
        if (!findIndex && !hasAttr) {
            setTerms([...terms, selectTedTerms]);
        } else if (hasAttr) {
            setTerms((prev) =>
                prev.map((i) =>
                    i?.attributeId === hasAttr?.attributeId ? selectTedTerms : i
                )
            );
        }
    }

    const getProductDetails = (slug) => {
        
        ProductService.Commands.getProductDetails({ slug })
            .then((res) => {
                if (res.status === "success") {
                    
                    setVariantProducts(res.variant_product);
                    
                } else {
                    console.log("Data not found");
                }
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            });
    };

    useEffect(() => {
        if (
            Number(terms?.length) === Number(item?.productattributeArr?.length)
        ) {
            //   console.log("Equal", variantProducts);
            const matchedProducts = variantProducts?.find((product) => {
                // check if the product has the same number of attributes as the selected options
                if (product.producttermsArr.length !== terms.length) {
                    return false;
                }

                console.log(product);
                // check if all selected options exist in the product
                return terms.every((option) => {
                    return product.producttermsArr.some((productOption) => {
                        return (
                            productOption.attributeId === option.attributeId &&
                            productOption.termsId === option.termsId
                        );
                    });
                });
                setSelectedProduct(matchedProducts);
            });
            if(matchedProducts?.inventory > 1) {
                dispatch(
                    addToCart({
                        ...matchedProducts,
                        quantity: Number(1),
                        name: item?.name,
                        image: item?.featured_image,
                        slug: item?.slug,
                        unit_price: Number(
                            (matchedProducts?.sale_price * 100).toFixed(2)
                        ),
                        total_amount: Number(
                            (Number(1) * matchedProducts?.sale_price * 100).toFixed(2)
                        ),
                        product_id: item?.id,
                        item_total: Number(
                            (Number(1) * matchedProducts?.sale_price).toFixed(2)
                        ),
                        category_id: item?.category,
                    })
                );
                snackbar(`This is item added to your bag`, {
                    variant: "success"
                });
                handleClose();
                setCartModalOpen(true);
                setTerms([]);
            } else {
                snackbar(`Out of stock`, {
                    variant: "error"
                });
            }
        } else {
            console.log("Not found");
        }
    }, [terms]);

    useEffect(() => {
        if(!isEmpty(item)){
            getProductDetails(item?.slug);
        }
    }, [item]);
  return (
    <Box>
        <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={() => {
        handleClose();
        setTerms([]);
      }}
      closeAfterTransition
      slotProps={{
        backdrop: {
          timeout: 500
        }
      }}
    >
      <Fade in={open}>
        <Box sx={style}>
          <IconButton
            title="Close"
            onClick={() => {
                handleClose();
                setTerms();
            }}
            sx={{
              position: "absolute",
              right: 10,
              top: 5,
              width: 40,
              height: 40
            }}
          >
            x
          </IconButton>
          <Box sx={{ textAlign: "center" }}>
            <Typography sx={{ textTransform: "capitalize" }}>Add To Bag - {item?.name}</Typography>
            <Typography sx={{ color: "4a4a4a", fontSize: 21 }}>
              <strong style={{ color: "red" }}>SALE</strong>{" "}
              <span style={{ textDecoration: "line-through", fontSize: 12 }}>
                {item?.regular_price}
              </span>{" "}
              <strong>{item?.sale_price}</strong>
            </Typography>
            <Chip
              sx={{
                background: "#d5f5e3",
                borderRadius: 1,
                width: 160,
                margin: "10px 0"
              }}
              icon={<LocalShippingIcon />}
              label="Free Delivery"
            />
          </Box>
          {item?.productattributeArr.length > 0 &&
            item?.productattributeArr?.map((data, index) => (
              <>
                {item.producttermsArr.filter((i) => i.attributeId === data.id)
                  .length > 0 && (
                  <Box key={index}>
                    <Typography>{data?.name}:</Typography>
                    <Box
                      sx={{
                        border: "1px solid #ddd",
                        padding: { md: 2, xs: 1.2 },
                        display: "flex",
                        flexFlow: "wrap",
                        gap: { md: 2, xs: 1.2 },
                        
                      }}
                    >
                      {item?.producttermsArr.length > 0 ? (
                        <>
                          {item.producttermsArr
                            .filter((i) => i.attributeId === data.id)
                            .map((tag, index) => (
                              <Chip
                                sx={{ 
                                    background: terms?.includes(tag) ? "#05520b" : "#87161b",
                                    color: '#ffffff !important',
                                    borderRadius: 1,
                                    width: { md: "22%", xs: "30%" },
                                    textTransform: 'uppercase',

                                    "&:hover": {
                                        background: "#621216 !important"
                                    }
                                 }}
                                key={index}
                                label={tag?.termsName}
                                onClick={() => handleSelectVariant(data?.id, tag?.termsId)}
                              />
                            ))}
                        </>
                      ) : (
                        <></>
                      )}
                    </Box>
                  </Box>
                )}
              </>
            ))}
          <Box sx={{ textAlign: "center", mt: 3 }}>
            <Button onClick={handleClose} fullWidth variant="contained" sx={{ backgroundColor: "#40054a", "&:hover":{backgroundColor: "#40054a",} }}>
              Return
            </Button>
          </Box>
        </Box>
      </Fade>
    </Modal>
        <AfterAddToCartModal 
            open={cartModalOpen} 
            handleClose={()=>setCartModalOpen(false)} 
            item={item}
            addedItem={selectedProduct}
        />
    </Box>
  );
};
