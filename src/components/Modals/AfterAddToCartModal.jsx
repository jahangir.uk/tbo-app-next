import { Box, Fade, IconButton, Modal, Typography } from "@mui/material";
import { IMAGE_URL } from "helpers/config";
import Link from "next/link";
import { useSelector } from "react-redux";


const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    bgcolor: "background.paper",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4,
    
  };

export const AfterAddToCartModal = ({ open, handleClose, addedItem }) => {
    const { cartItems } = useSelector(
        (state) => state.cart
    );

    // console.log(cartItems);
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={open}
            closeAfterTransition
            slotProps={{
                backdrop: {
                    timeout: 500,
                },
            }}
        >
            <Fade in={open}>
                <Box sx={style}>
                    <IconButton
                        title="Close"
                        onClick={handleClose}
                        sx={{
                            position: "absolute",
                            right: 10,
                            top: 5,
                            width: 40,
                            height: 40,
                        }}
                    >
                        x
                    </IconButton>
                    {cartItems?.length > 0 && cartItems?.map((item, index) =>  (
                        <Box sx ={{ textAlign: "center", color: "green", display: 'flex', borderBottom: "1px solid #ccc" }} key={index}>
                        <img src={item?.featured_image ? IMAGE_URL + "/variant-product-image/" + item?.featured_image : IMAGE_URL + "/product-fetured-image/" + item?.image } alt="Img" width={50} />
                        <Typography component={"h4"} sx={{ textTransform: "capitalize", fontSize: 13 }}>{item?.name} added successfully</Typography>
                        </Box>
                    ))}
                   

                    <Box
                        sx={{
                            textAlign: "center",
                            mt: 3,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                        }}
                    >
                        <Box
                            sx={{
                                background: "#891d1d",
                                padding: "10px 20px",
                                borderRadius: "5px",
                                "& a":{
                                    textDecoration: "none",
                                    fontWeight: 600,
                                    color: "#ffffff",
                                }
                            }}
                        >
                            <Link href={"/checkout"}>Go to Checkout({cartItems?.length})</Link>
                        </Box>
                        <Box
                            sx={{
                                background: "green",
                                padding: "10px 20px",
                                borderRadius: "5px",
                                "& a":{
                                    textDecoration: "none",
                                    fontWeight: 600,
                                    color: "#ffffff",
                                }
                            }}
                        >
                            <Link href={"/cart"}>Go to Cart</Link>
                        </Box>
                        <Box
                            sx={{
                                background: "#2c2c07",
                                padding: "7px 20px",
                                borderRadius: "5px",
                                cursor: "pointer",
                            }}
                        >
                            <Typography onClick={handleClose} sx={{ 
                                textDecoration: "none",
                                fontWeight: 600,
                                color: "#ffffff",
                                padding: 0,
                                margin: 0,
                             }}>Continue Shopping</Typography>
                        </Box>
                    </Box>
                </Box>
            </Fade>
        </Modal>
    );
};
