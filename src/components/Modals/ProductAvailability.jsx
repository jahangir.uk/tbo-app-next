import CloseIcon from "@mui/icons-material/Close";
import { Box, Button, Fade, IconButton, Modal, Typography } from '@mui/material';
import { isEmpty } from "helpers/functions";
import Link from 'next/link';
import { useDispatch } from "react-redux";
import { removeToCart } from "redux/action/cart";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    bgcolor: "background.paper",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4
  };

export const ProductAvailability = ({data, open, handleClose, isCartPage = false}) => {

    const dispatch = useDispatch();

    const removeItem = (id) => {
        if (!isEmpty(id)) {
            dispatch(removeToCart(id));
            handleClose();
        }
    };
  return (
    <Modal
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    open={open}
    closeAfterTransition
    slotProps={{
      backdrop: {
        timeout: 500
      }
    }}
  >
    <Fade in={open}>
      <Box sx={style}>
        {isCartPage && (
            <IconButton
          title="Close"
          onClick={handleClose}
          sx={{
            position: "absolute",
            right: 10,
            top: 5,
            width: 40,
            height: 40
          }}
        >
          x
        </IconButton>
        )}
         
        
        <Box>
           {data?.map((item, index) => (
            <>
                {item?.status_code !== 111 && (
                    <Typography sx={{ fontSize:13, display: "flex", alignItems: "center",  }}>{index + 1}.<strong>{item?.sku}</strong> {item?.msg}. <strong style={{ color: "green" }}>Available Qty: {item?.avilable_quantity}</strong> <IconButton onClick={()=> removeItem(item?.id) } title="Remove" sx={{marginLeft: "20px", "& svg":{width: 16, height: 16,  cursor: "pointer"}}}><CloseIcon/></IconButton></Typography>
                )}
            </>
           ))} 
        </Box>
        
        <Box sx={{ textAlign: "center", mt: 3 }}>
            {isCartPage ? (
                <Button onClick={handleClose} fullWidth variant="contained" color="secondary">
                Back
              </Button>
            ) : (
                <Box><Link href={"/cart"}>Back To Cart</Link></Box>
            )}
          
        </Box>
      </Box>
    </Fade>
  </Modal>
  )
}
