import {
    Box,
    Container,
    Fade,
    Grid,
    IconButton,
    Modal,
    TextField,
    Typography,
    styled,
} from "@mui/material";
import { COLORS } from "assets/styles/colors/colors";
import { CustomButton } from "components/Button";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { login } from "redux/action/authActions";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.primary,
    },
    "& .MuiOutlinedInput-root": {
        "& fieldset": {
            borderColor: COLORS.gray,
        },
        "&:hover fieldset": {
            borderColor: COLORS.black,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.primary,
        },
    },
});

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    bgcolor: "background.paper",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4
  };

export const ForgotPassModal = ({
    open,
    handleClose,
    setOpen,
    setForgotOpen,
}) => {
    const [loading, toggleLoading] = useState(false);
    const dispatch = useDispatch();
    const router = useRouter();

    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const {
        handleSubmit,
        register,
        formState: { errors },
    } = useForm();

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const onSubmit = async (data) => {
        toggleLoading(true);
        const res = await dispatch(login(data));
        toggleLoading(false);
        handleClose();
    };
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={open}
            onClose={handleClose}
            closeAfterTransition
            slotProps={{
                backdrop: {
                    timeout: 500,
                },
            }}
            sx={{ opacity: open ? 1 : 0 }}
        >
            <Fade in={open}>
                <Box sx={style}>
                    <IconButton
                        title="Close"
                        onClick={handleClose}
                        sx={{
                            position: "absolute",
                            right: 10,
                            top: 5,
                            width: 40,
                            height: 40,
                        }}
                    >
                        x
                    </IconButton>
                    <Container component="main" width={1100}>
                        <Box
                            sx={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                background: "#efefef",
                                padding: 3,
                                borderRadius: 2.5,
                            }}
                        >
                            <Grid
                                container
                                spacing={2}
                                sx={{ display: "flex", alignItems: "center" }}
                            >
                                <Grid item md={12} sm={12}>
                                    <Typography
                                        component="h2"
                                        sx={{ fontWeight: 600, fontSize: '2rem',paddingBottom: 2 }}
                                    >
                                        Forgot Password
                                    </Typography>
                                    <Box
                                        component="form"
                                        onSubmit={handleSubmit}
                                        noValidate
                                        sx={{ mt: 1 }}
                                    >
                                        <CssTextField
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            label="Email Address"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                        />

                                        <CustomButton
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            text={"Change Password"}
                                        />

                                        <Grid container>
                                            <Grid item>
                                                <Box onClick={() => {
                                                    setForgotOpen(false);
                                                    setOpen(true);
                                                }} sx={{ cursor: 'pointer', textDecoration: 'underline' }} variant="body2">
                                                    {"Back to login"}
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Box>
                    </Container>
                </Box>
            </Fade>
        </Modal>
    );
};
