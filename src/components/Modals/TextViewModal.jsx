import { Box, Dialog, DialogContent } from '@mui/material';

export const TextViewModal = ({open, text, handleClose}) => {
    function createMarkup(text) {
        return { __html: text };
    }
  return (
    <Box>
        <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        
        <DialogContent>
          <Box sx={{ textAlign: "justify", "& p": {fontSize: 16} }} dangerouslySetInnerHTML={createMarkup(
                    text
                )} />
        </DialogContent>
        
      </Dialog>
    </Box>
  )
}
