import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Box, Fade, FormControl, Grid, InputLabel, Modal } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import OutlinedInput from "@mui/material/OutlinedInput";
import Typography from "@mui/material/Typography";
import AppleIcon from "assets/images/social-icon/Appleicon.png";
import TwitterIcon from "assets/images/social-icon/Twitter.png";
import FacebookIcon from "assets/images/social-icon/facebook.png";
import GoogleIcon from "assets/images/social-icon/google.png";
import InstaIcon from "assets/images/social-icon/instra.png";
import LinkedinIcon from "assets/images/social-icon/linkedinicon.png";
import axios from "axios";
import { CustomButton } from "components/Button";
import { SocialLoginButton } from "components/SocialLoginButton";
import { API_URL } from "helpers/apiUrl";
import useSnackbar from "hooks/useSnackbar";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { login } from "redux/action/authActions";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4
};

export const SignModal = ({ open, handleSignUpClose, setOpen, setSignOpen }) => {
    const [showPassword, setShowPassword] = useState(false);
    const [loading, toggleLoading] = useState(false);
    const dispatch = useDispatch();
    const router = useRouter();
    const snackbar = useSnackbar();
    const handleClickShowPassword = () => setShowPassword((show) => !show);
  const {
    handleSubmit,
    register,
    formState: { errors }
  } = useForm();

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSubmit = async (data) => {
    axios
      .post(`${API_URL}/customer-registration`, data, {})
      .then((res) => {
        console.log("Response", res);
        if (res?.data.status === "success") {
            
            const res =  dispatch(login({username: data?.email, password: data.password}));
            handleSignUpClose();
           
        //   navigate("/login");
        } else if (res?.data?.status === "error") {
          if (res?.data.fieldErrors) {
            snackbar(res?.data?.fieldErrors?.username?.message, {
              variant: "error"
            });
            snackbar(res?.data?.fieldErrors?.email?.message, {
                variant: "error"
            });
          }
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
    // console.log(data);
  };

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={handleSignUpClose}
      closeAfterTransition
      slotProps={{
        backdrop: {
          timeout: 500
        }
      }}
      sx={{ opacity: open ? 1 : 0 }}
    >
      <Fade in={open}>
        <Box sx={style}>
          <IconButton
            title="Close"
            onClick={handleSignUpClose}
            sx={{
              position: "absolute",
              right: 10,
              top: 5,
              width: 40,
              height: 40
            }}
          >
            x
          </IconButton>
          <Container component="main" width={1100}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            background: "#efefef",
            padding: 3,
            borderRadius: 2.5
          }}
        >
          <Grid
            container
            spacing={2}
            sx={{ display: "flex", alignItems: "center" }}
          >
            <Grid item md={12} sm={12}>
            <Typography
                component="h1"
                variant="h2"
                sx={{
                  fontWeight: 600,
                  paddingBottom: 2,
                  fontSize: { md: "2rem", sm: "1.75rem", xs: "1.75rem" }
                }}
              >
                Sign up
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit(onSubmit)}
                noValidate
                sx={{ mt: 1 }}
              >
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    First Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    type="text"
                    label="First Name"
                    {...register("first_name", {
                      type: "text"
                    })}
                    error={!!errors.first_name}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Last Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Last Name"
                    {...register("last_name", {
                      type: "text"
                    })}
                    error={!!errors.last_name}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Username
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Username"
                    {...register("username", {
                      type: "text"
                    })}
                    error={!!errors.username}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Email
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Email"
                    type="email"
                    {...register("email", {
                      type: "email"
                    })}
                    error={!!errors.email}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginRight: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Nick Name
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    required
                    fullWidth
                    label="Nick Name"
                    type="text"
                    {...register("nickname", {
                      type: "nickname"
                    })}
                    error={!!errors.nickname}
                  />
                </FormControl>
                <FormControl
                  sx={{
                    marginLeft: { md: "4px", xs: 0 },
                    marginTop: 2,
                    width: { md: "48%", xs: "100%" }
                  }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    margin="normal"
                    id="outlined-adornment-password"
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                    label="Password"
                    {...register("password", {
                      type: "password"
                    })}
                    error={!!errors.password}
                  />
                </FormControl>
                <FormControl sx={{ marginTop: 2, width: "100%" }}>
                  <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label={
                      <Box sx={{ fontSize: { md: "1rem", xs: "14px" } }}>
                        Click to agree Terms of Use & Privacy Policy
                      </Box>
                    }
                    fullWidth
                  />
                </FormControl>
                <CustomButton type="submit" fullWidth text={"Sign up"} />
                <Grid container>
                  <Grid item xs></Grid>
                  <Grid item>
                    <Box onClick={() => {
                        setSignOpen(false);
                        setOpen(true);
                    }} sx={{ cursor: 'pointer', textDecoration: 'underline' }} variant="body2">
                      {"Already have account? Log in"}
                    </Box>
                  </Grid>
                </Grid>
              </Box>
              <Divider
                sx={{
                  margin: "24px 0px",
                  flexShrink: 0,
                  display: "flex",
                  whiteSpace: "nowrap",
                  textAlign: "center",
                  border: "0px"
                }}
              >
                Or
              </Divider>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  gap: 2
                }}
              >
                <SocialLoginButton icon={GoogleIcon} color={"#df3e30"} />
                <SocialLoginButton icon={TwitterIcon} color={"#1877f2"} />
                <SocialLoginButton icon={InstaIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={FacebookIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={LinkedinIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={AppleIcon} color={"#1c9cea"} />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
          
        </Box>
      </Fade>
    </Modal>
  );
};
