import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Box, Fade, FormControl, Grid, InputLabel, Modal } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import OutlinedInput from "@mui/material/OutlinedInput";
import Typography from "@mui/material/Typography";
import AppleIcon from "assets/images/social-icon/Appleicon.png";
import TwitterIcon from "assets/images/social-icon/Twitter.png";
import FacebookIcon from "assets/images/social-icon/facebook.png";
import GoogleIcon from "assets/images/social-icon/google.png";
import InstaIcon from "assets/images/social-icon/instra.png";
import LinkedinIcon from "assets/images/social-icon/linkedinicon.png";
import { CustomButton } from "components/Button";
import { SocialLoginButton } from "components/SocialLoginButton";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { login, updateUserDetailsAfterLogin } from "redux/action/authActions";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4
};

export const LoginModal = ({ open, handleClose, setOpen,setSignOpen,setForgotOpen }) => {
    const [showPassword, setShowPassword] = useState(false);
    const [loading, toggleLoading] = useState(false);
    const dispatch = useDispatch();
    const router = useRouter();
   
    const handleClickShowPassword = () => setShowPassword((show) => !show);
  const {
    handleSubmit,
    register,
    formState: { errors }
  } = useForm();

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSubmit = async (data) => {
    toggleLoading(true);
    const res = await dispatch(login(data));
    toggleLoading(false);
    handleClose();
    dispatch(updateUserDetailsAfterLogin());
  };
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={handleClose}
      closeAfterTransition
      slotProps={{
        backdrop: {
          timeout: 500
        }
      }}
      sx={{ opacity: open ? 1 : 0 }}
    >
      <Fade in={open}>
        <Box sx={style}>
          <IconButton
            title="Close"
            onClick={handleClose}
            sx={{
              position: "absolute",
              right: 10,
              top: 5,
              width: 40,
              height: 40
            }}
          >
            x
          </IconButton>
          <Container component="main" width={1100}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            background: "#efefef",
            padding: 3,
            borderRadius: 2.5
          }}
        >
          <Grid
            container
            spacing={2}
            sx={{ display: "flex", alignItems: "center" }}
          >
            <Grid item md={12} sm={12}>
              {/* <Box component={"img"} sx={{ m: 1 }} src={TopLogo} alt="Logo" /> */}

              <Typography component="h1" variant="h2" sx={{ fontWeight: 600, fontSize: 20 }}>
                Login
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit(onSubmit)}
                noValidate
                width="100%"
                sx={{ mt: 1 }}
              >
                <FormControl
                  sx={{ width: "100%", marginBottom: 2 }}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Email Address
                  </InputLabel>
                  <OutlinedInput
                    margin="none"
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    {...register("username", {
                      required: "Email or username can not be empty!",
                      type: "email"
                    })}
                    error={!!errors.username}
                  />
                  <Typography sx={{ color: "red", fontSize: 12 }}>
                    {errors.username?.message}
                  </Typography>
                </FormControl>
                <FormControl sx={{ width: "100%" }} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-password">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                    label="Password"
                    {...register("password", {
                      required: true,
                      minLength: {
                        value: 6,
                        message: "Password can not be less than 6"
                      },
                      maxLength: {
                        value: 20,
                        message: "Password can not be more than 20"
                      }
                    })}
                  />
                  <Typography sx={{ color: "red", fontSize: 12 }}>
                    {errors.password?.message}
                  </Typography>
                </FormControl>
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <CustomButton
                  type="submit"
                  disabled={loading}
                  fullWidth
                  text={"Login"}
                />
                <Grid container>
                  <Grid item xs>
                    <Box onClick={() => {
                        setOpen(false);
                        setForgotOpen(true);

                    }} sx={{ cursor: 'pointer', textDecoration: 'underline' }} variant="body2">
                      Forgot password?
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box onClick={() => {
                        setOpen(false);
                        setSignOpen(true);
                    }} sx={{ cursor: 'pointer', textDecoration: 'underline' }} variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Box>
                  </Grid>
                </Grid>
              </Box>
              <Divider
                sx={{
                  margin: "24px 0px",
                  flexShrink: 0,
                  display: "flex",
                  whiteSpace: "nowrap",
                  textAlign: "center",
                  border: "0px"
                }}
              >
                Or
              </Divider>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  gap: 2
                }}
              >
                <SocialLoginButton icon={GoogleIcon} color={"#df3e30"} />
                <SocialLoginButton icon={TwitterIcon} color={"#1877f2"} />
                <SocialLoginButton icon={InstaIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={FacebookIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={LinkedinIcon} color={"#1c9cea"} />
                <SocialLoginButton icon={AppleIcon} color={"#1c9cea"} />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
          
        </Box>
      </Fade>
    </Modal>
  );
};
