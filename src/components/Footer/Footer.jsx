import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import {
    Box,
    Button,
    Container,
    Divider,
    Grid,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    TextField,
    Typography
} from "@mui/material";
import clearpay from "assets/images/clearpay.png";
import kiaarna from "assets/images/kiarna.png";
import warranty_img from "assets/images/money-back.png";
import PaymentGateway from "assets/images/payment.png";
import paypal from "assets/images/paypal.png";
import seal_img from "assets/images/seal_image.png";
import { useFooterStyle } from "assets/stylesheets/footer/footerStylesheet";
import { footer_menu } from "components/dummy-data/Menu";
import Image from "next/image";
import Link from "next/link";

export const Footer = () => {
  const classes = useFooterStyle();
  const footer_menus = footer_menu;
  const satisfaction_label = 98;
  return (
    <Box className={classes.root}>
      <Box className="footer-top">
        <Container sx={{ padding: "20px 0" }}>
          <Grid container>
            <Box className="newsletter">
              <Typography component={"h2"}>NEWSLETTER</Typography>
              <Box component={"form"} className="form">
                <TextField
                  type="email"
                  placeholder="Your Email"
                  fullWidth
                  size="small"
                />
                <Button
                  sx={{
                    background: "#87161b",
                    fontSize: 16,
                    color: "#ffffff",
                    "&:hover": { background: "#87161b" }
                  }}
                  fullWidth
                  size="small"
                >
                  Subscribe
                </Button>
              </Box>
            </Box>
          </Grid>
          <Grid container sx={{ display: "flex", alignItems: "center" }}>
            <Grid item md={6} sm={6} xs={12} className="payment">
              <Typography component={"h2"}>Ways To Pay</Typography>
              <Image
                src={PaymentGateway}
                alt="Payment Gateway"
              />
            </Grid>
            <Grid item md={6} sm={6} xs={12}>
              <Typography
                component={"h2"}
                sx={{
                  color: "#ffffff",
                  textAlign: { md: "right", xs: "left" },
                  paddingBottom: "6px"
                }}
              >
                Split The Cost
              </Typography>
              <Box className="emi-method">
                <Image src={paypal} alt="" />
                <Image src={clearpay} alt="" />
                <Image src={kiaarna} alt="" />
              </Box>
            </Grid>
          </Grid>
          <Grid container spacing={2} className="footer-menu">
            <Grid item md={3} sm={6} xs={12}>
              <Typography component={"h2"}>HELP</Typography>
              <Divider />
                <List>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/contact-us"}>
                        <ListItemText primary={'Contact Us'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/return-policy"}>
                        <ListItemText primary={'Return Policy'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/customer-care"}>
                        <ListItemText primary={'Customer Care'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/cookies-policy"}>
                        <ListItemText primary={'Cookies Policy'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} target="__blank" rel="nofollow" href={"https://www.couponupto.com/coupons/top-brand-outlet"}>
                        <ListItemText primary={'Coupon Partner'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/privacy-policy"}>
                        <ListItemText primary={'Privacy Policy'} />
                        </ListItemButton>
                    </ListItem>
                </List>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Typography component={"h2"}>Top Brand Outlet</Typography>
              <Divider />
              <List>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/mens"}>
                        <ListItemText primary={'Men'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/womens"}>
                        <ListItemText primary={'Womens'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/kids"}>
                        <ListItemText primary={'Kids'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/find-us"}>
                        <ListItemText primary={'Find Us'} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/order-tracker"}>
                        <ListItemText primary={'Track Order'} />
                        </ListItemButton>
                    </ListItem>
                    {/* <ListItem disablePadding>
                        <ListItemButton component={"a"} href={"/terms-and-condition"}>
                        <ListItemText primary={'Terms & Conditions'} />
                        </ListItemButton>
                    </ListItem> */}
              </List>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Typography component={"h2"}>OUTLET ADDRESS</Typography>
              <Divider />
              <Typography sx={{ marginTop: 2 }}>
                35 Knockhall Road, Greenhithe DA9 9EZ, United Kingdom
                Contactus@topbrandoutlet.co.uk Phone: 0330 133 2599
              </Typography>
              <Box className="social-list">
                <Box component="a" href="#">
                  <FacebookIcon />
                </Box>
                <Box component="a" href="#">
                  <InstagramIcon />
                </Box>
              </Box>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <Typography sx={{ fontSize: 24, fontWeight: 400 }}>
                Overall {satisfaction_label}%
              </Typography>
              {/* <Divider /> */}
              <Typography sx={{ marginBottom: 2.4 }}>
                of the customers are happy with their experience of shopping
                online with <b>Top Brand Outlet</b>
              </Typography>
              <Typography
                component={"a"}
                href="https://www.ekomi.co.uk/review-topbrandoutlet.co.uk.html"
                target="__blank"
                sx={{
                  fontSize: 20,
                  color: "#ffffff",
                  textDecoration: "underline"
                }}
              >
                READ OUR REVIEWS
              </Typography>

              <Box className="sponsor">
                <Image
                  
                  src={warranty_img}
                  alt="Warranty"
                  width={80}
                />
                <Image
                  
                  src={seal_img}
                  alt="Warranty"
                  height={50}
                />
              </Box>
            </Grid>
          </Grid>
          <Divider color="#ccc" />
          <Grid
            container
            spacing={2}
            sx={{ display: "flex", alignItems: "center" }}
          >
            <Grid item md={12} xs={12}>
              <Box className="copyright">
                <Typography>
                  <Link href="/terms-and-condition">Terms & Conditions</Link> |{" "}
                  <Link href="/privacy-policy">Privacy</Link> |{" "}
                  <Link href="/cookies">Cookies</Link>{" "}
                </Typography>
                <Typography>
                  Copyright ©2021 Top Brand Outlet. All Rights Reserved. | Reg:
                  11761282 | VAT: 324746791 | Develop by{" "}
                  <Link href="#">COMBOSOFT</Link>
                </Typography>
              </Box>
            </Grid>
          </Grid>
          <Divider color="#ccc" />
        </Container>
      </Box>
    </Box>
  );
};
