
export const KlarnaPay = ({amount}) => {

    const handleKlarnaPay = async () => {
        try{
            
            const response = await fetch("http://localhost:3000/api/create-klarna-session", {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },

                body: JSON.stringify({
                    amount: 10 * 100,
                }),
            })

            console.log(response)
            if(response.data.success) {
                setSuccess(true);
            }
        }catch(error) {
            console.log(error)
        }
    }
  return (
    <div>
        <button onClick={handleKlarnaPay}>Pay</button>
    </div>
  )
}
