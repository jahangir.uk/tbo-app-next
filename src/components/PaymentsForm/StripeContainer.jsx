import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { PaymentStripe } from './PaymentStripe';

const PUBLIC_KEY = "pk_test_L4U4E3Uf9lK8x1D4kBhKigri";

const stripeTestPromise = loadStripe(PUBLIC_KEY) 

export const StripeContainer = ({amount}) => {
  return (
    <Elements stripe={stripeTestPromise}>
        <PaymentStripe amount={amount} />
    </Elements>
  )
}
