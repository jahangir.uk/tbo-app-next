import { css } from "@emotion/css";

export const usePaymentFormStyle = () => ({
    root: css `
        
    .paymentForm {
        display: flex;
        flex-direction: column;
        align-items: flex-start;

        label {
            margin-bottom: 1rem;
            font-weight: bold;
        }

        input[type="text"],
        textarea {
            padding: 0.5rem;
            border: 1px solid #ccc;
            border-radius: 0.25rem;
            font-size: 1rem;
            width: 100%;
            box-sizing: border-box;
        }

        textarea {
            height: 6rem;
        }
          
        .submitButton {
        margin-top: 1rem;
        padding: 0.5rem 1rem;
        background-color: #0070f3;
        color: #fff;
        border: none;
        border-radius: 0.25rem;
        font-size: 1rem;
        cursor: pointer;

        :hover{
            background-color: #0366d6;
        }
        }
          
         
    }

    .CardElement {
        width: 600px;
        height: 40px;
        padding: 10px 12px;
        border: 1px solid #d8d8d8;
        border-radius: 4px;
        background-color: #f5f5f5;
        font-size: 16px;
        line-height: 1.5;
        color: #212529;
        box-shadow: none;
        transition: all 0.2s ease-in-out;

        --focused {
            outline: none;
            border-color: #007bff;
            box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }

        --invalid {
            outline: none;
            border-color: #dc3545;
            box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
        }

        --webkit-autofill {
            -webkit-box-shadow: 0 0 0 30px #f5f5f5 inset !important;
            -webkit-text-fill-color: #212529 !important;
        }
    }
        
    `,
});