import { Box } from "@mui/material";
import { useEffect } from "react";

const AfterpayButton = ({total}) => {
  useEffect(() => {
    
    // if(window){
    //     function createAfterpayWidget () {
    //         window.afterpayWidget = new AfterPay.Widgets.PaymentSchedule({
    //           token: 'YOUR_TOKEN',
    //           target: '#afterpay-widget-container',
    //           locale: 'en-US', 
    //           onReady: function (event) {
    //             // Fires when the widget is ready to accept updates.  
    //           },
    //           onChange: function (event) {
    //             // Fires after each update and on any other state changes.
    //             // See "Getting the widget's state" for more details.
    //           },
    //           onError: function (event) {
    //             // See "Handling widget errors" for more details.
    //           },
    //         })
    //       }
    //       createAfterpayWidget();
    
    // }
    const loadScript = () => {
        const script = document.createElement('script');
      script.src = 'https://js.afterpay.com/afterpay-1.x.js';
      script.async = true;
      document.body.appendChild(script);
    }  
    const loadScriptAfterPay = () => {
        const script = document.createElement('script');
      script.src = 'https://portal.sandbox.afterpay.com/afterpay.js? merchant_key=YOUR-PUBLIC-KEY';
      script.async = true;
      document.body.appendChild(script);
    }

   
    loadScript();
    // loadScriptAfterPay();
  }, []);

  return (
    <Box sx={{ display: "inline-block", width: "100%" }}>
        
      {/* Render the Afterpay/Clearpay payment button */}
      {/* <afterpay-placement data-modal-link-style="none" /> */}
    
        <afterpay-placement
            data-type="price-table"
            data-amount={total}
            data-currency="en_GB"
            data-payment-amount-is-bold="true"
            data-price-table-theme="mint" 
            size="sm"
            data-cbt-enabled="true"
        />
        
    </Box>
  );
};

export default AfterpayButton;