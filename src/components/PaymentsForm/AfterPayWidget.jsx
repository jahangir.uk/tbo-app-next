import { useEffect } from 'react';

const AfterpayWidget = () => {
  useEffect(() => {
    // Load the Afterpay/Clearpay JavaScript SDK dynamically
    const loadAfterpaySDK = () => {
      const script = document.createElement('script');
      script.src = 'https://portal.afterpay.com/afterpay-async.js';
      script.async = true;
      document.body.appendChild(script);
      script.onload = initializeAfterpayWidget;
    };

    // Initialize the Afterpay/Clearpay widget
    const initializeAfterpayWidget = () => {
      window.AfterPay.initialize({
        countryCode: 'GB', // Replace with your country code, e.g., 'US', 'AU', 'GB', etc.
        configuration: {
          merchantId: '400126830', // Replace with your Afterpay/Clearpay Merchant ID
          countryCode: 'GB', // Replace with your country code, e.g., 'US', 'AU', 'GB', etc.
          size: 'medium', // Adjust the size of the widget, options: 'medium', 'large', 'responsive'
        //   targetElementId: 'afterpay-widget', // Replace with the ID of the element where you want to render the widget
        },
      });
      const token = "87658yhfvsdt678w34f87tyrhf873";

      // Redirect to Afterpay
      window.AfterPay.redirect({
        countryCode: 'GB', // Replace with your country code, e.g., 'US', 'AU', 'GB', etc.
        token: token, // Include the generated token
        redirectMode: 'lightbox', // Specify the redirect mode, options: 'lightbox', 'redirect'
      });
    };

    // Load the Afterpay/Clearpay SDK and initialize the widget
    loadAfterpaySDK();
  }, []);

  return <div id="afterpay-widget" />;
};

export default AfterpayWidget;