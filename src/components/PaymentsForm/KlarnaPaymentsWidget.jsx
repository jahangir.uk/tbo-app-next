import { useEffect } from 'react';

function KlarnaPaymentsWidget({ sessionId, token }) {
 

    console.log(sessionId, token);
  useEffect(() => {
    if (sessionId && token) {
        setTimeout(() => {
            window.Klarna.Payments.init({
                client_token: token,
              });
        
              
            window.Klarna.Payments.load(
                {
                  container: `#klarna-payments-container`,
                  session_id: sessionId,
                  payment_method_category: 'pay_later'
                },
                ({ show_form }) => {
                  if (show_form) {
                    console.log('Klarna Payments widget loaded successfully', show_form);
                  } else {
                    console.error('An error occurred loading the Klarna Payments widget');
                  }
                }
              );
            
            
        }, 500)
    }
  }, [sessionId, token]);

  return (
    <div id="klarna-payments-container">
      
    </div>
  );
}

export default KlarnaPaymentsWidget;
