import { Box } from "@mui/material";
import {
    BraintreePayPalButtons,
    PayPalScriptProvider
} from "@paypal/react-paypal-js";

// This values are the props in the UI

const initialOptions = {
    "client-id": "AcXitTbBdeholTyib8UUm3p--eKm_F2ms7TECOSkeEZgrI2U3ia96JUXzePWkrVQMhVDa7JkNUP0U5To",
    currency: "GBP",
    intent: "capture",
    components: "buttons",
   
};


export const PaypalButton = ({amount, validation}) => {
    return (
        <Box sx={{ width:'100%' }}>
            <PayPalScriptProvider options={initialOptions}>
                <BraintreePayPalButtons
                    fundingSource="paylater"
                    createOrder={(data, actions) => {
                            
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        value: amount,
                                    },
                                },
                            ],
                        });
                    }}
                    onApprove={(data, actions) => {
                        return actions.order.capture().then((details) => {
                            const name = details.payer.name.given_name;
                            alert(`Transaction completed by ${name}`);
                            // Call here server api for checkout complete
                        });
                    }}

                    
                />
            </PayPalScriptProvider>
        </Box>
    )
}