import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { useState } from 'react';
import { useStripeStyle } from './stripeStyle';


const CARD_OPTION = {
    iconStyle: 'solid',
    style: {
        base: {
            iconColor: "#c4f0ff",
            color: "#fff",
            fontWeight: 500,
            fontSize: "16px",
            fontSmoothing: "antialiased",

        },

        invalid: {
            iconColor: "#ffc7ee",
            color: "#ffc7ee"
        }
    }
}

export const PaymentStripe = ({amount}) => {
    const classes = useStripeStyle();
    const [success, setSuccess] = useState(false);
    const stripe = useStripe();
    const element = useElements();

    const handleSubmit = async (e) => {
        e.preventDefault();
 
        const {error, paymentMethod} = await stripe.createPaymentMethod({
            type: 'card',
            card: element.getElement(CardElement)
        }) 

        if(!error) {
            try{
                const {id} = paymentMethod;
                const response = await fetch("/api/create-payment-intent", {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },

                    body: JSON.stringify({
                        amount: Number(amount) * 100,
                        id
                    }),
                })
                console.log(response)
                if(response.data.success) {
                    console.log('Successful Payment')
                    setSuccess(true);
                }
            }catch(error) {
                console.log(error)
            }
        }else{
            console.log(error)
        }
    }
  return (
    <form onSubmit={handleSubmit} className={classes.root}>
        <fieldset className='FormGroup' style={{ padding: '20px 20px' }}>
            <div className='FormRow'>
                <CardElement options={CARD_OPTION} />
            </div>
        </fieldset>
        <button>Pay</button>
    </form>
  )
}
