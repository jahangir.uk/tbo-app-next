import { Box, Container, Typography } from "@mui/material";

export const HeroBanner = ({ title, image }) => {
  return (
    <Box
      sx={{
        background: `url(${image?.src})`,
        padding: "50px 80px",
        backgroundSize: "cover",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: { md: 300, sm: 200, xs: 100 },
        position: "relative"
      }}
    >
      <Box
        sx={{
          position: "absolute",
          background: "#00000094",
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          display: "flex",
          height: "100%",
          alignItems: "center"
        }}
      >
        <Container sx={{ textAlign: "center" }}>
          <Typography
            sx={{
              color: "#ffffff",
              fontSize: {md:40, xs:22},
              fontFamily: '"Open Sans", sans-serif !important',
              fontWeight: 600
            }}
          >
            {title}
          </Typography>
        </Container>
      </Box>
    </Box>
  );
};
