import { Box } from "@mui/material";
import img404 from "assets/images/img-404.png";

export const Page404 = () => {
  return (
    <Box
      sx={{
        display: "flex",
        height: "100vh",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center"
      }}
    >
      <Box>
        <Box component={"img"} src={img404} alt="404" />
        <h1>Page Not Found</h1>
        <Box
          component={"a"}
          href="/"
          sx={{
            background: "#000000",
            color: "#ffffff",
            padding: "10px 25px",
            textDecoration: "none",
            transition: ".8s",
            "&:hover": { background: "#0303f3", borderRadius: 2 }
          }}
        >
          Go Back To Home
        </Box>
      </Box>
    </Box>
  );
};
