import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import CloseIcon from "@mui/icons-material/Close";
import RemoveIcon from "@mui/icons-material/Remove";
import {
    Box,
    Container,
    Drawer,
    IconButton,
    Typography,
    styled,
} from "@mui/material";
import { useCartStyle } from "assets/stylesheets/cart/cartStylesheet";
import { IMAGE_URL } from "helpers/config";
import { capitalize, isEmpty, limitWords } from "helpers/functions";
import useSnackbar from "hooks/useSnackbar";
import { useDispatch, useSelector } from "react-redux";
import { removeToCart, updateToCart } from "redux/action/cart";


const DrawerHeader = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
}));

export const CartDrawer = ({ open, setOpen }) => {
    const { cartItems, total } = useSelector((state) => state.cart);
    const classes = useCartStyle();
    const dispatch = useDispatch();
    const snackbar = useSnackbar();

    const incrementQty = (id, qty, inventory) => {
        console.log(qty, inventory);
        if(inventory >= qty){
            dispatch(updateToCart(id, Number(qty)));
        } else {
            snackbar(`Please select qty less then or equal ${inventory}`, {
                variant: "error"
              });
        }
    };

    const decrementQty = (id, qty) => {
        
        if (qty > 0) {
            dispatch(updateToCart(id, Number(qty)));
        } else {
            dispatch(removeToCart(id));
            
        }
    };
    const updateItemQuantity = (id, qty, inventory) => {
        
        if (qty > 0) {
            if(inventory >= qty){
                dispatch(updateToCart(id, Number(qty)));
            } else{
                snackbar(`Please select qty less then or equal ${inventory}`, {
                    variant: "error"
                  });
            }
        }else {
            dispatch(removeToCart(id));
        }
    };

    const removeItem = (id) => {
        if (!isEmpty(id)) {
            dispatch(removeToCart(id));
        }
    };
    return (
        <Drawer
            anchor={"right"}
            open={open}
            onClose={setOpen}
            sx={{
                width: 450,
                flexShrink: 0,
            }}
        >
            <DrawerHeader>
                <IconButton onClick={setOpen}>
                    <ChevronRightIcon />
                </IconButton>
            </DrawerHeader>

            <Container className={classes.root}>
                {isEmpty(cartItems) ? (
                    <Box className="empty-cart">
                        <Typography component={"h2"}>
                            Your cart is empty
                        </Typography>
                        <Typography>
                            Fill me up with some unbeatable bargains! <br />
                            We're adding new styles daily from the biggest
                            brands
                            <br />
                            All with up to 75% off the RRP.
                        </Typography>
                        <Box component={"a"} href="/">
                            Continue Shopping
                        </Box>
                    </Box>
                ) : (
                    <Box className="cart" sx={{ width: 400 }}>
                        <Box className="cart-items">
                            {/* <Box className="cart-header">
                <Box sx={{ width: "50%" }}>Name</Box>
                <Box sx={{ width: "10%" }}>S&C</Box>
                <Box sx={{ width: "10%" }}>Qty</Box>
                <Box sx={{ width: "10%" }}>Total</Box>
                <Box sx={{ width: "20%" }}>Remove</Box>
              </Box> */}
                            {cartItems?.length > 0 &&
                                cartItems?.map((item, index) => (
                                    <Box className="cart-body" key={index}>
                                        <Box
                                            sx={{
                                                display: "flex",
                                                alignItems: "center",
                                                gap: "5px",
                                                width: { sm: "40%", xs: "50%" },
                                            }}
                                        > 
                                            <Box
                                                component={"img"}
                                                src={
                                                    item?.featured_image
                                                        ? IMAGE_URL +
                                                          "/variant-product-image/" +
                                                          item?.featured_image
                                                        : IMAGE_URL +
                                                          "/product-fetured-image/" +
                                                          item?.image
                                                }
                                                sx={{
                                                    width: {
                                                        md: 40,
                                                        sm: 30,
                                                        xs: 20,
                                                    },
                                                }}
                                            />
                                            <Typography sx={{ fontSize: 12, color: item?.quantity >= item?.inventory ? "red" : "#000", }}>
                                                {limitWords(capitalize(item?.name), 8)}
                                            </Typography>
                                        </Box>
                                        <Box
                                            sx={{
                                                width: { sm: "15%", xs: "25%" },
                                            }}
                                        >{`${
                                            !isEmpty(
                                                item?.producttermsArr[0]
                                                    ?.termsName
                                            )
                                                ? item?.producttermsArr[0]
                                                      ?.termsName
                                                : ""
                                        }, ${
                                            !isEmpty(item?.producttermsArr[1]?.termsName) ? item?.producttermsArr[1]?.termsName : ""
                                        } `}</Box>
                                        <Box
                                            sx={{
                                                width: { sm: "25%", xs: "25%" },
                                            }}
                                        >
                                            <Box
                                                sx={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    gap: 1,
                                                }}
                                            >
                                                <IconButton
                                                    sx={{
                                                        width: 20,
                                                        height: 20,
                                                        background: "#df2828",
                                                        color: "#fff",
                                                        "&:hover": {
                                                            background:
                                                                "#df2828",
                                                        },
                                                    }}
                                                    onClick={() =>
                                                        decrementQty(
                                                            item?.id,
                                                            item?.quantity - 1
                                                        )
                                                    }
                                                >
                                                    <RemoveIcon />
                                                </IconButton>
                                                <input
                                                    onChange={(e) =>
                                                        updateItemQuantity(
                                                            item?.id,
                                                            e.target.value,
                                                            item?.inventory
                                                        )
                                                    }
                                                    value={item?.quantity}
                                                    type="number"
                                                    style={{
                                                        width: 20,
                                                        textAlign: "center",
                                                    }}
                                                    onWheel={(e) =>
                                                        e.target.blur()
                                                    }
                                                />
                                                <IconButton
                                                    sx={{
                                                        width: 20,
                                                        height: 20,
                                                        background: "#499761",
                                                        color: "#fff",

                                                        "&:hover": {
                                                            background:
                                                                "#499761",
                                                        },
                                                    }}
                                                    onClick={() =>
                                                        incrementQty(
                                                            item?.id,
                                                            item?.quantity + 1,
                                                            item?.inventory
                                                        )
                                                    }
                                                >
                                                    +
                                                </IconButton>
                                            </Box>
                                        </Box>
                                        <Box
                                            sx={{
                                                width: "10%",
                                                textAlign: "right",
                                            }}
                                        >
                                            £{Number((item?.quantity * item?.sale_price)).toFixed(2)}
                                        </Box>
                                        <Box sx={{ width: "10%" }}>
                                            <IconButton
                                                onClick={() =>
                                                    removeItem(item?.id)
                                                }
                                                title="Remove"
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Box>
                                    </Box>
                                ))}
                            <Box className="total-box">
                                <table border={1}>
                                    <tr>
                                        <th>Total Items</th>
                                        <td>{cartItems?.length}</td>
                                    </tr>
                                    <tr>
                                        <th>Sub Total:</th>
                                        <td>£{total}</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td>£{total}</td>
                                    </tr>
                                </table>
                                {/* <Box className="list">
                  <Typography component={"h4"} sx={{ fontSize:12 }}>Total Items:</Typography>
                  <Typography>{cartItems?.length}</Typography>
                </Box>
                <Box className="list">
                  <Typography component={"h4"} sx={{ fontSize:12 }}>Sub Total:</Typography>
                  <Typography>£{total}</Typography>
                </Box>
                <Box className="list">
                  <Typography component={"h4"} sx={{ fontSize:12 }}>Grand Total:</Typography>
                  <Typography>£{total}</Typography>
                </Box> */}
                            </Box>
                        </Box>
                        <Box
                            sx={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "space-between",
                                padding: "10px",
                            }}
                        >
                            <Box
                                component={"a"}
                                href="/cart"
                                sx={{
                                    background: "#000000",
                                    color: "#ffffff",
                                    padding: "7px 30px",
                                    textDecoration: "none",
                                }}
                            >
                                Go to Cart
                            </Box>
                            <Box
                                component={"a"}
                                href="/checkout"
                                sx={{
                                    background: "#60444c",
                                    color: "#ffffff",
                                    padding: "7px 30px",
                                    textDecoration: "none",
                                }}
                            >
                                Go to Checkout
                            </Box>
                        </Box>
                    </Box>
                )}
            </Container>
        </Drawer>
    );
};
