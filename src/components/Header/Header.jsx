import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import CheckroomIcon from "@mui/icons-material/Checkroom";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import {
    AppBar,
    Autocomplete,
    Badge,
    Box,
    Container,
    Drawer,
    Grid,
    IconButton,
    InputBase,
    MenuItem,
    TextField,
    Typography,
    styled,
} from "@mui/material";
import LOGO from "assets/images/TBO_logo.png";
import { COLORS } from "assets/styles/colors/colors";
import { useHeaderStyle } from "assets/stylesheets/header/headerStylesheet";
import { CartDrawer } from "components/Drawer/CartDrawer";
import { HeaderLoader } from "components/Loader/header-loader";
import { primary_menus } from "components/dummy-data/Menu";
import { capitalize } from "helpers/functions";
import useToken from "hooks/useToken";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { logout } from "redux/action/authActions";

const CssTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.primary,
        border: "none",
    },
    "& .MuiInput-underline:after": {
        border: "none",
    },
    "& .MuiOutlinedInput-root": {
        textTransform: "uppercase",
        "& fieldset": {
            border: "none",
        },
        "&:hover fieldset": {
            border: "none",
        },
        "&.Mui-focused fieldset": {
            border: "none",
        },

        "& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input": {
            padding: "10px 14px",
        },
    },
});

export const Header = () => {
    const classes = useHeaderStyle();
    const menu_items = primary_menus;
    const [menuOpen, setMenuOpen] = useState(false);
    const [kidsMenuOpen, setKidsMenuOpen] = useState(false);
    const [brandsMenuOpen, setBrandsMenuOpen] = useState(false);
    const cartItems = useSelector((state) => state.cart.cartItems);
    const { brand_list, kids_list, men_menu_list, women_menu_list, tags } =
        useSelector((state) => state.menuState);
    const [anchor, setAnchor] = useState(false);
    const [cartOpen, setCartOpen] = useState(false);
    const [items, setItems] = useState([]);
    const [brandItems, setBrandItems] = useState([]);
    const [search, setSearch] = useState("");
    const auth = useSelector((state) => state.auth.isAuthenticated);
    const [isAuthenticated, setIsAuthenticate] = useState(false);
    const [suggestionOpen, setSuggestionOpen] = useState(false);
    const token = useToken();
    const itemPerRow = 15;
    const [loader, setLoader] = useState(true);
    const route = useRouter();
    const numRows = Math.ceil(brand_list?.length / itemPerRow);
    const toggleDrawer = (open) => {
        setAnchor(open);
    };

    const handleCloseNavMenu = () => {
        setMenuOpen(!menuOpen);
    };

    const handleLogout = () => {
        // console.log('Hello');
        logout();
    };

    const handleCloseUserMenu = () => {
        setMenuOpen(!menuOpen);
    };

    const handleSearch = () => {
        route.push({ pathname: "/search", query: { name: search } });
    };

    useEffect(() => {
        setIsAuthenticate(token);
    }, [token]);

    useEffect(() => {
        setTimeout(() => {
            setLoader(false);
        }, 500);
    }, []);

    console.log(numRows);
    return (
        <Box className={classes.root}>
            {loader ? (
                <HeaderLoader />
            ) : (
                <AppBar position="static">
                    <Box className="main-menu">
                        <Container>
                            <Box>
                                <Box
                                    sx={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <Box
                                        sx={{
                                            display: "flex",
                                            alignItems: "center",
                                        }}
                                    >
                                        <Link href="/">
                                            <Box
                                                sx={{
                                                    display: {
                                                        xs: "none",
                                                        md: "block",
                                                    },
                                                    
                                                }}
                                               
                                                width={80}
                                                component={"img"}
                                                src={LOGO.src}
                                                alt="Logo"
                                            />
                                        </Link>

                                        <Box
                                            sx={{
                                                flexGrow: 1,
                                                display: {
                                                    xs: "flex",
                                                    md: "none",
                                                    justifyContent:
                                                        "flex-start",
                                                },
                                            }}
                                        >
                                            <IconButton
                                                size="large"
                                                aria-label="account of current user"
                                                aria-controls="menu-appbar"
                                                aria-haspopup="true"
                                                onClick={() =>
                                                    toggleDrawer(true)
                                                }
                                                color="black"
                                            >
                                                <MenuIcon />
                                            </IconButton>
                                        </Box>

                                        <Link href="/">
                                            <Box
                                                sx={{
                                                    display: {
                                                        xs: "block",
                                                        md: "none",
                                                    },
                                                    width: { xs: 70 },
                                                }}
                                                component={"img"}
                                                src={LOGO.src}
                                            />
                                        </Link>
                                    </Box>
                                    <Box
                                        sx={{
                                            background: {
                                                md: "#f3f3f3",
                                                xs: "transparent",
                                            },
                                            display: { md: "flex", xs: "none" },
                                            alignItems: "center",
                                            width: "50%",
                                            borderRadius: 1,
                                            position: "relative",
                                        }}
                                    >
                                        {/* <CssTextField
                         sx={{
                             ml: 1,
                             flex: 1,
                         }}
                         placeholder="I am searching for ..."
                         onChange={(e) =>
                             setSearch(e.target.value)
                         }
                         value={search}
                         onClick={() =>
                             setSuggestionOpen(!suggestionOpen)
                         }
                         fullWidth
                         size="small"
                         InputProps={{
                             endAdornment: (
                                 <InputAdornment position="end">
                                     {search ? (
                                         <IconButton
                                             aria-label="toggle password visibility"
                                             onClick={() => {
                                                 setSuggestionOpen(
                                                     false
                                                 );
                                                 setSearch("");
                                             }}
                                             edge="end"
                                         >
                                             <CloseIcon />
                                         </IconButton>
                                     ) : (
                                         ""
                                     )}
                                 </InputAdornment>
                             ),
                         }}
                     /> */}
                                        <Autocomplete
                                            fullWidth
                                            freeSolo
                                            sx={{
                                                "& .Mui-expanded": {
                                                    background:
                                                        "red !important",
                                                },
                                            }}
                                            size="small"
                                            id="free-solo-2-demo"
                                            disableClearable
                                            options={tags?.map(
                                                (option) => option?.name
                                            )}
                                            onInputChange={(
                                                event,
                                                newInputValue
                                            ) => {
                                                setSearch(newInputValue);
                                            }}
                                            onKeyUp={(e) => {
                                                if (e?.key === "Enter") {
                                                    handleSearch();
                                                }
                                            }}
                                            renderInput={(params) => (
                                                <CssTextField
                                                    {...params}
                                                    label=""
                                                    placeholder="I am searching for ..."
                                                    InputProps={{
                                                        ...params.InputProps,
                                                        type: "search",
                                                    }}
                                                    fullWidth
                                                />
                                            )}
                                        />
                                        <IconButton
                                            type="button"
                                            sx={{ p: "10px" }}
                                            aria-label="search"
                                            onClick={handleSearch}
                                        >
                                            <SearchIcon />
                                        </IconButton>
                                    </Box>
                                    <Box
                                        sx={{
                                            display: {
                                                md: "none",
                                                xs: "block",
                                            },
                                        }}
                                    >
                                        <IconButton
                                            type="button"
                                            sx={{ p: "10px" }}
                                            aria-label="search"
                                            onClick={() => toggleDrawer(true)}
                                        >
                                            <SearchIcon />
                                        </IconButton>
                                    </Box>
                                    <Box sx={{ display: "flex", gap: 4 }}>
                                        <Box
                                            sx={{ display: "flex", gap: 1 }}
                                            component={Link}
                                            href={
                                                isAuthenticated
                                                    ? "/my-account"
                                                    : "/login"
                                            }
                                        >
                                            <Typography
                                                sx={{
                                                    display: {
                                                        md: "block",
                                                        xs: "none",
                                                    },
                                                }}
                                            >
                                                {isAuthenticated
                                                    ? "My Account"
                                                    : "Sign In"}
                                            </Typography>
                                            <AccountCircleOutlinedIcon color="primary" />
                                        </Box>

                                        <Box
                                            sx={{ cursor: "pointer" }}
                                            onClick={() => setCartOpen(true)}
                                        >
                                            <Badge
                                                badgeContent={cartItems?.length}
                                                color="error"
                                                showZero
                                            >
                                                <ShoppingCartIcon />
                                            </Badge>
                                        </Box>
                                    </Box>
                                </Box>

                                <Drawer
                                    anchor={"left"}
                                    open={anchor}
                                    onClose={() => toggleDrawer(false)}
                                >
                                    <Box
                                        sx={{
                                            display: "flex",
                                            justifyContent: "flex-start",
                                            flexDirection: "column",
                                            width: 200,
                                            padding: "20px 10px",
                                            "& a": {
                                                textDecoration: "none",
                                                margin: "8px 0",
                                                display: "flex",
                                                color: "#000000",
                                            },
                                        }}
                                    >
                                        <Box
                                            sx={{
                                                background: "#f3f3f3",
                                                display: "flex",
                                                alignItems: "center",
                                                borderRadius: 1,
                                            }}
                                        >
                                            <InputBase
                                                sx={{
                                                    ml: 1,
                                                    flex: 1,
                                                }}
                                                placeholder="I am searching for ..."
                                                inputProps={{
                                                    "aria-label":
                                                        "I am searching for ...",
                                                }}
                                            />
                                            <IconButton
                                                type="button"
                                                sx={{ p: "10px" }}
                                                aria-label="search"
                                            >
                                                <SearchIcon />
                                            </IconButton>
                                        </Box>
                                        <Link
                                            href={"/"}
                                            sx={{ my: 2, color: "black" }}
                                        >
                                            Home
                                        </Link>
                                        <Box
                                            onClick={() => {
                                                setMenuOpen(!menuOpen);
                                                setItems(men_menu_list);
                                            }}
                                            sx={{ my: 2, color: "black", display: "flex", alignItems: "center" }}
                                        >
                                            Men <KeyboardArrowDownIcon />
                                        </Box>
                                        <Box
                                            
                                            onClick={() => {
                                                setMenuOpen(!menuOpen);
                                                setItems(women_menu_list);
                                            }}
                                            sx={{ my: 2, color: "black", display: "flex", alignItems: "center" }}
                                        >
                                            Women <KeyboardArrowDownIcon />
                                        </Box>
                                        <Box
                                            
                                            onClick={() => {
                                                setKidsMenuOpen(!kidsMenuOpen);
                                                setItems(kids_list);
                                            }}
                                            sx={{ my: 2, color: "black", display: "flex", alignItems: "center" }}
                                        >
                                            Kids <KeyboardArrowDownIcon />
                                        </Box>
                                        <Link
                                            href={"/"}
                                            onClick={() => {
                                                setBrandsMenuOpen(!brandsMenuOpen);
                                                setItems(brand_list);
                                            }}
                                            sx={{ my: 2, color: "black", display: "flex", alignItems: "center" }}
                                        >
                                            Brands <KeyboardArrowDownIcon />
                                        </Link>
                                        <Link
                                            href={"/my-account"}
                                            sx={{ my: 2, color: "black" }}
                                        >
                                            My Account
                                        </Link>
                                        <Link href="/">
                                            <Badge
                                                badgeContent={cartItems?.length}
                                                color="error"
                                                showZero
                                            >
                                                <ShoppingCartIcon />
                                            </Badge>
                                        </Link>
                                        
                                    </Box>
                                </Drawer>
                            </Box>
                        </Container>
                    </Box>
                    <Box
                        className="menu-bottom"
                        sx={{
                            display: { md: "block", xs: "none" },
                            position: "relative",
                        }}
                    >
                        <Container>
                            <Box
                                sx={{
                                    height: 44,
                                    display: "flex",
                                }}
                            >
                                <Box
                                    className="menu"
                                    sx={{
                                        flexGrow: 1,
                                        gap: 3,
                                        display: { xs: "none", md: "flex" },
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <Link
                                        href={"/"}
                                        sx={{ my: 2, color: "white" }}
                                    >
                                        Home
                                    </Link>
                                    <Link
                                        href={"/shop/men"}
                                        onClick={() => {
                                            setMenuOpen(!menuOpen);
                                            setItems(men_menu_list);
                                        }}
                                        sx={{ my: 2, color: "white" }}
                                        onMouseEnter={() => {
                                            setMenuOpen(true);
                                            setItems(men_menu_list);
                                            setKidsMenuOpen(false);
                                            setBrandsMenuOpen(false);
                                        }}
                                    >
                                        Men <KeyboardArrowDownIcon />
                                    </Link>
                                    <Link
                                        href={"/shop/women"}
                                        onClick={() => {
                                            setMenuOpen(true);
                                            setItems(women_menu_list);
                                        }}
                                        onMouseEnter={() => {
                                            setMenuOpen(true);
                                            setItems(women_menu_list);
                                            setKidsMenuOpen(false);
                                            setBrandsMenuOpen(false);
                                        }}
                                        sx={{ my: 2, color: "white" }}
                                    >
                                        Women <KeyboardArrowDownIcon />
                                    </Link>
                                    <Link
                                        href={"/shop/kids"}
                                        onClick={() => {
                                            setKidsMenuOpen(!kidsMenuOpen);
                                            setItems(kids_list);
                                        }}
                                        onMouseEnter={() => {
                                            setKidsMenuOpen(!kidsMenuOpen);
                                            setItems(kids_list);
                                            setMenuOpen(false);
                                            setBrandsMenuOpen(false);
                                        }}
                                        sx={{ my: 2, color: "white" }}
                                    >
                                        Kids <KeyboardArrowDownIcon />
                                    </Link>
                                    <Link
                                        href={"#"}
                                        onClick={() => {
                                            setBrandsMenuOpen(true);
                                            setBrandItems(menu_items?.brands);
                                            setMenuOpen(false);
                                            setKidsMenuOpen(false);
                                        }}
                                        onMouseEnter={() => {
                                            setBrandsMenuOpen(true);
                                            setBrandItems(menu_items?.brands);
                                            setMenuOpen(false);
                                            setKidsMenuOpen(false);
                                        }}
                                        sx={{ my: 2, color: "white" }}
                                    >
                                        Brands <KeyboardArrowDownIcon />
                                    </Link>
                                </Box>
                            </Box>
                        </Container>
                        <Box
                            //  onMouseLeave={() => setMenuOpen(false)}
                            sx={{
                                visibility: menuOpen ? "visible" : "hidden",
                                position: "absolute",
                                opacity: menuOpen ? 1 : 0,
                                width: "100%",
                                zIndex: 999,
                                top: "44px",
                                transition: menuOpen ? "1s" : "1s",
                            }}
                        >
                            <Grid
                                container
                                sx={{
                                    width: "1200px",
                                    background: "#f5f5f5",
                                    margin: "0 auto",
                                    padding: "5px 10px",
                                    border: "1px solid #ccc",
                                    "& a": {
                                        textDecoration: "none",
                                        color: "#000000de",
                                    },
                                }}
                                className="menu-items"
                                onMouseLeave={() => setMenuOpen(false)}
                            >
                                {items?.items?.length > 0 &&
                                    items?.items?.map((item, index) => (
                                        <Grid
                                            item
                                            md={3}
                                            sm={4}
                                            xs={12}
                                            key={index}
                                        >
                                            <Link
                                                href={`/shop/${items?.slug}/${item?.slug}`}
                                            >
                                                <Box
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        gap: "10px",
                                                        padding: "5px 10px",
                                                        cursor: "pointer",
                                                        "&:hover": {
                                                            background:
                                                                "#e9e9e9",
                                                            borderRadius: "5px",
                                                        },
                                                    }}
                                                    onClick={
                                                        handleCloseUserMenu
                                                    }
                                                >
                                                    {item?.name !== "" && (
                                                        <>
                                                            <CheckroomIcon />
                                                            <Typography
                                                                component={"h2"}
                                                                sx={{
                                                                    fontWeight: 600,
                                                                }}
                                                            >
                                                                {" "}
                                                                {capitalize(
                                                                    item?.name
                                                                )}
                                                            </Typography>
                                                        </>
                                                    )}
                                                </Box>
                                            </Link>
                                            {item?.items.length > 0 &&
                                                item?.items.map(
                                                    (data, index) => (
                                                        <Link
                                                            href={`/shop/${items?.slug}/${item?.slug}/${data?.slug}`}
                                                            key={index}
                                                        >
                                                            <MenuItem
                                                                onClick={
                                                                    handleCloseUserMenu
                                                                }
                                                            >
                                                                <Typography textAlign="center">
                                                                    {capitalize(
                                                                        data?.name
                                                                    )}
                                                                </Typography>
                                                            </MenuItem>
                                                        </Link>
                                                    )
                                                )}
                                        </Grid>
                                    ))}
                            </Grid>
                        </Box>
                        <Box
                            onMouseLeave={() => setMenuOpen(false)}
                            sx={{
                                visibility: kidsMenuOpen ? "visible" : "hidden",
                                position: "absolute",
                                opacity: kidsMenuOpen ? 1 : 0,
                                width: "100%",
                                zIndex: 999,
                                top: "44px",
                                transition: kidsMenuOpen ? "1s" : "1s",
                            }}
                        >
                            <Grid
                                container
                                sx={{
                                    width: "1200px",
                                    background: "#f5f5f5",
                                    margin: "0 auto",
                                    padding: "5px 10px",
                                    border: "1px solid #ccc",
                                    "& a": {
                                        textDecoration: "none",
                                        color: "#000000de",
                                    },
                                }}
                                className="menu-items"
                                onMouseLeave={() => setKidsMenuOpen(false)}
                            >
                                {items?.length > 0 &&
                                    items.map((item, index) => (
                                        <Grid
                                            item
                                            md={3}
                                            sm={4}
                                            xs={12}
                                            key={index}
                                        >
                                            <Link href={`/shop/${item?.slug}`}>
                                                <Box
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        gap: "10px",
                                                        padding: "5px 10px",
                                                        cursor: "pointer",
                                                        "&:hover": {
                                                            background:
                                                                "#e9e9e9",
                                                            borderRadius: "5px",
                                                        },
                                                    }}
                                                    onClick={() =>
                                                        setKidsMenuOpen(false)
                                                    }
                                                >
                                                    {item?.label !== "" && (
                                                        <>
                                                            <CheckroomIcon />
                                                            <Typography
                                                                component={"h2"}
                                                                sx={{
                                                                    fontWeight: 600,
                                                                }}
                                                            >
                                                                {" "}
                                                                {capitalize(
                                                                    item?.name
                                                                )}
                                                            </Typography>
                                                        </>
                                                    )}
                                                </Box>
                                            </Link>
                                            {item?.items.length > 0 &&
                                                item?.items.map(
                                                    (data, index) => (
                                                        <Link
                                                            href={`/shop/${item?.slug}/${data?.slug}`}
                                                            key={index}
                                                        >
                                                            <MenuItem
                                                                onClick={
                                                                    handleCloseUserMenu
                                                                }
                                                            >
                                                                <Typography textAlign="center">
                                                                    {capitalize(
                                                                        data?.name
                                                                    )}
                                                                </Typography>
                                                            </MenuItem>
                                                        </Link>
                                                    )
                                                )}
                                        </Grid>
                                    ))}
                            </Grid>
                        </Box>
                     
                        <Box
                            // onMouseLeave={() => setMenuOpen(false)}
                            sx={{
                                visibility: brandsMenuOpen ? "visible" : "hidden",
                                position: "absolute",
                                opacity: brandsMenuOpen ? 1 : 0,
                                width: "100%",
                                zIndex: 999,
                                top: "44px",
                                transition: brandsMenuOpen ? "1s" : "1s",
                            }}
                            
                        >
                            <Grid
                                container
                                sx={{
                                    width: "1200px",
                                    background: "#f5f5f5",
                                    margin: "0 auto",
                                    padding: "5px 10px",
                                    border: "1px solid #ccc",
                                    "& a": {
                                        textDecoration: "none",
                                        color: "#000000de",
                                    },
                                }}
                                className="menu-items"
                                onMouseLeave={() => setBrandsMenuOpen(false)}
                            >
                                {Array.from({length: numRows}, (_, rowIndex) => (
                                    <Grid
                                        item
                                        md={3}
                                        sm={4}
                                        xs={12}
                                        key={rowIndex}
                                    >
                                        {brand_list.slice(rowIndex * itemPerRow, (rowIndex * itemPerRow) + itemPerRow).map(
                                                (data, index) => (
                                                    <Link
                                                        href={`/${data?.slug}`}
                                                        key={index}
                                                    >
                                                        <MenuItem
                                                            onClick={()=> setBrandsMenuOpen(false)}
                                                        >
                                                            <Typography textAlign="center">
                                                                {capitalize(
                                                                    data?.name
                                                                )}
                                                            </Typography>
                                                        </MenuItem>
                                                    </Link>
                                                )
                                            )}
                                    </Grid>
                                ))}
                                
                            </Grid>
                        </Box>
                    </Box>
                    <CartDrawer
                        open={cartOpen}
                        setOpen={() => setCartOpen(false)}
                    />
                </AppBar>
            )}
        </Box>
    );
};
