import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box
} from "@mui/material";
import { useAccordionStyle } from "assets/stylesheets/accordion/accordionStylesheet";
import { useState } from "react";

export const Accordions = ({ data }) => {
  const classes = useAccordionStyle();
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  console.log("Expended", expanded);
  function createMarkup(text) {
    return { __html: text };
  }
  return (
    <Box className={classes.root}>
      {data?.length > 0 &&
        data.map((item, index) => (
          <Accordion
            onChange={handleChange(index)}
            expanded={expanded === index}
            className="single-item"
            key={index}
          >
            <AccordionSummary
              expandIcon={item?.icon ?? <ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Box>{item?.title ?? "No title"}</Box>
            </AccordionSummary>
            <AccordionDetails
                
              dangerouslySetInnerHTML={createMarkup(item?.details)}
            ></AccordionDetails>
          </Accordion>
        ))}
    </Box>
  );
};
