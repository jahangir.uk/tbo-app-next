import { Box, Card, Typography } from "@mui/material";
import { useSlideProductCardStyle } from "assets/stylesheets/product-card/slideProductCardStylesheet";
import { IMAGE_URL } from "helpers/config";
import { capitalize, limitWords } from "helpers/functions";
import Image from "next/image";

export const SlideProductCard = ({ item }) => {
  const classes = useSlideProductCardStyle();
  return (
    <Card className={classes.root}>
      <Box component="a" href={`/product/${item?.slug}`} sx={{ minHeight: {md: 345, sm: 300, xs: 280} }}>
        <Box sx={{ height: {md: 250, sm:200, xs: 150}, width: "auto", position: "relative" }}>
            <Image
                layout="fill" 
                objectFit="contain"
                alt="green iguana"
                src={IMAGE_URL + 'product-fetured-image/' + item?.featured_image}
            />
        </Box>

        <Box className="content-box">
          <Typography className="price">£20</Typography>
          <Typography component="h3">{limitWords(capitalize(item?.name), 10)}</Typography>

        </Box>
      </Box>
    </Card>
  );
};
