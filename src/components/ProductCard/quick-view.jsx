import CloseIcon from "@mui/icons-material/Close";
import {
    Box,
    Button,
    Dialog,
    Grid,
    IconButton,
    Slide,
    Typography,
    useTheme
} from "@mui/material";
import { useQuickViewStyle } from "assets/stylesheets/quick-view/quickViewStylesheet";
import { ImageSliderWithBottomThumbnail } from "components/ImageSlider/ImageSliderWithBottomThumbnail";
import { QuickViewLoader } from "components/Loader/quick-view-loader";
import { AfterAddToCartModal } from "components/Modals/AfterAddToCartModal";
import { camelCaseToText, capitalize, isEmpty } from "helpers/functions";
import useSnackbar from "hooks/useSnackbar";
import Link from "next/link";
import { forwardRef, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addToCart } from "redux/action/cart";
import ProductService from "services/ProductService";

const style = {
    width: 900,
    bgcolor: "background.paper",
    borderRadius: "10px",
    boxShadow: 24,
    margin: "0 auto",
};

export const QuickView = ({ item, open, handleClose }) => {
    const classes = useQuickViewStyle();
    const snackbar = useSnackbar();
    const dispatch = useDispatch();
    const theme = useTheme();
    const [productDetails, setProductDetails] = useState();
    const [pImages, setPImages] = useState([]);
    const [variantProducts, setVariantProducts] = useState([]);
    const [terms, setTerms] = useState([]);
    const [stateStyle, setStateStyle] = useState();
    const [qty, setQty] = useState(1);
    const [cartModalOpen, setCartModalOpen] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);

    const getDetailsBySlug = (slug) => {
        ProductService.Commands.getProductDetails({ slug }).then((res) => {
            setProductDetails(res?.data);
            setPImages(res?.product_images);
            setVariantProducts(res?.variant_product);
        });
    };

    const selectTerms = (attrId) => (e) => {
        console.log(attrId, e.target.value);
        const selectTedTerms = item?.producttermsArr.find(
            (i) => i.termsId === e.target.value && i.attributeId === attrId
        );
        if (isEmpty(e.target.value)) {
            setTerms((prev) => prev.filter((i) => i.attributeId !== attrId));
        }
        console.log(selectTedTerms);
        const findIndex = terms.find(
            (data) =>
                data?.attributeId === selectTedTerms?.attributeId &&
                data?.termsId === selectTedTerms?.termsId
        );
        const hasAttr = terms.find(
            (data) => data?.attributeId === selectTedTerms?.attributeId
        );
        if (!findIndex && !hasAttr) {
            setTerms([...terms, selectTedTerms]);
        } else if (hasAttr) {
            setTerms((prev) =>
                prev.map((i) =>
                    i?.attributeId === hasAttr?.attributeId ? selectTedTerms : i
                )
            );
        }
    };

    const onChangeQty = (e) => {
        const q = e.target.value;
        if (selectedProduct?.inventory >= q) {
            if (q > 0) {
                setQty(q);
            } else {
                setQty(1);
            }
        } else {
            snackbar(
                `Please select qty less then or equal ${selectedProduct?.inventory}`,
                {
                    variant: "error",
                }
            );
        }
    };

    function createMarkup(text) {
        return { __html: text };
    }

    useEffect(() => {
        getDetailsBySlug(item?.slug);
    }, [item]);

    const handleAddToCart = () => {
        console.log(selectedProduct?.inventory, qty);
        if (parseInt(qty) < 0) {
            snackbar(`Please select quantity minimum 1`, {
                variant: "error",
            });

            return false;
        }
        if (selectedProduct?.inventory >= qty) {
            dispatch(
                addToCart({
                    ...selectedProduct,
                    quantity: Number(qty),
                    name: productDetails?.name,
                    image: productDetails?.featured_image,
                    slug: productDetails?.slug,
                    unit_price: Number(
                        (selectedProduct?.sale_price * 100).toFixed(2)
                    ),
                    total_amount: Number(
                        (
                            Number(qty) *
                            selectedProduct?.sale_price *
                            100
                        ).toFixed(2)
                    ),
                    product_id: productDetails?.id,
                    item_total: Number(
                        (Number(qty) * selectedProduct?.sale_price).toFixed(2)
                    ),
                    category_id: productDetails?.category,
                })
            );
            setQty(1);

            setCartModalOpen(true);
        } else {
            snackbar(
                `Please select qty less then or equal ${selectedProduct?.inventory}`,
                {
                    variant: "error",
                }
            );
        }
    };

    const Transition = forwardRef(function Transition(props, ref) {
        return <Slide direction="down" ref={ref} {...props} />;
      });

    useEffect(() => {
        if (
            Number(terms?.length) ===
            Number(productDetails?.productattributeArr?.length)
        ) {
            //   console.log("Equal");
            const matchedProducts = variantProducts?.find((product) => {
                // check if the product has the same number of attributes as the selected options
                if (product.producttermsArr.length !== terms.length) {
                    return false;
                }
                // check if all selected options exist in the product
                return terms.every((option) => {
                    return product.producttermsArr.some((productOption) => {
                        return (
                            productOption.attributeId === option.attributeId &&
                            productOption.termsId === option.termsId
                        );
                    });
                });
            });
            setSelectedProduct(matchedProducts);
        } else {
            setSelectedProduct(null);
        }
    }, [terms]);

    console.log();

    return (
        <Dialog
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={open}
            closeAfterTransition
            slotProps={{
                backdrop: {
                    timeout: 500,
                },
            }}
            
            maxWidth={"md"}
            sx={{ 
                "& ::-webkit-scrollbar":{
                    display: "none"
                },
                width: {md: "100%" ,xs: '100%'}
             }}
        >
            <Box className={classes.root}>
                {isEmpty(productDetails) ? (
                    <>
                        <QuickViewLoader />
                    </>
                ) : (
                    <>
                        <Box
                            sx={{
                                background: "#800",
                                position: "relative",
                                display: "flex",
                                borderRadius: "3px 3px 0px 0px",
                                color: "#ffffff",
                                alignItems: "center",
                                justifyContent: "space-between",
                                padding: "0 10px",
                                "& svg": {
                                    color: "#ffffff",
                                },
                            }}
                        >
                            <Typography>Quick product viewer</Typography>
                            <IconButton title="Close" onClick={handleClose}>
                                <CloseIcon />
                            </IconButton>
                        </Box>
                        <Box sx={{ padding: "20px 10px 0 10px", width:"90%" }}>
                            <Grid container spacing={2}>
                                <Grid item md={6} sm={6} xs={12}>
                                    <Box
                                        sx={{
                                            width: "100%",
                                            paddingRight: "10px",
                                            borderRight: "2px solid #b7b4b4",
                                        }}
                                    >
                                        <ImageSliderWithBottomThumbnail
                                            images={pImages}
                                        />
                                    </Box>
                                </Grid>
                                <Grid item md={6} sm={6} xs={12}>
                                    <Box className="content" sx={{ width: "100%" }}>
                                        <Typography component="h2">
                                            <Link
                                                href={`/product/${productDetails?.slug}`}
                                            >
                                                {" "}
                                                {camelCaseToText(
                                                    productDetails?.name
                                                )}
                                            </Link>
                                        </Typography>
                                        <Typography>
                                            <strong>Availability: </strong>
                                            {selectedProduct?.outofstock
                                                ? "Stock out"
                                                : "In Stock"}
                                        </Typography>
                                        <Typography>
                                            <strong>SKU: </strong>
                                            {selectedProduct?.sku ??
                                                productDetails?.sku}
                                        </Typography>
                                        <Box sx={{ minHeight: 42 }}>
                                            <Typography>
                                                <strong
                                                    style={{ color: "green" }}
                                                >
                                                    {selectedProduct?.sale_price
                                                        ? `£${selectedProduct?.sale_price}`
                                                        : ""}
                                                </strong>
                                            </Typography>
                                            <Typography>
                                                <strong
                                                    style={{ color: "green" }}
                                                >
                                                    {selectedProduct?.inventory
                                                        ? `Qty:${selectedProduct?.inventory}`
                                                        : ""}
                                                </strong>
                                            </Typography>
                                        </Box>
                                        <Box className="variant">
                                            {productDetails?.productattributeArr
                                                ?.length > 0 &&
                                                productDetails?.productattributeArr?.map(
                                                    (data, index) => (
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                flexDirection:
                                                                    "column",
                                                            }}
                                                            key={index}
                                                        >
                                                            <label>
                                                                {capitalize(
                                                                    data?.name
                                                                )}
                                                            </label>
                                                            <select
                                                                onChange={selectTerms(
                                                                    data?.id
                                                                )}
                                                            >
                                                                <option value="0">
                                                                    Select{" "}
                                                                    {data?.name}
                                                                </option>
                                                                {productDetails.producttermsArr
                                                                    .filter(
                                                                        (i) =>
                                                                            i.attributeId ===
                                                                            data.id
                                                                    )
                                                                    .map(
                                                                        (
                                                                            data,
                                                                            index
                                                                        ) => (
                                                                            <option
                                                                                value={
                                                                                    data?.termsId
                                                                                }
                                                                                key={
                                                                                    index
                                                                                }
                                                                            >
                                                                                {capitalize(
                                                                                    data?.termsName
                                                                                )}
                                                                            </option>
                                                                        )
                                                                    )}
                                                            </select>
                                                        </Box>
                                                    )
                                                )}
                                            <Box className="quantity">
                                                <label>QTY</label>
                                                <input
                                                    className="qty"
                                                    type="number"
                                                    onChange={onChangeQty}
                                                    value={qty}
                                                    min="1"
                                                    onWheel={(e) =>
                                                        e.target.blur()
                                                    }
                                                    disabled={isEmpty(
                                                        selectedProduct
                                                    )}
                                                />
                                            </Box>
                                        </Box>
                                        <Box>
                                            {typeof selectedProduct ===
                                                "undefined" ||
                                                (selectedProduct?.inventory ===
                                                    0 && (
                                                    <Typography
                                                        sx={{ color: "red" }}
                                                    >
                                                        Out of stock!
                                                    </Typography>
                                                ))}
                                            <Button
                                                sx={{
                                                    width: "100%",
                                                    background: "#880000d4",
                                                    marginTop: "10px",
                                                    color: "#ffffff",
                                                    padding: "10px 30px",
                                                    "&:hover": {
                                                        background: "#800",
                                                    },
                                                }}
                                                disabled={
                                                    Number(
                                                        productDetails
                                                            ?.productattributeArr
                                                            .length
                                                    ) !==
                                                        Number(terms.length) ||
                                                    typeof selectedProduct ===
                                                        "undefined" ||
                                                    selectedProduct?.inventory ===
                                                        0
                                                }
                                                onClick={handleAddToCart}
                                            >
                                                Add To Bag
                                            </Button>
                                        </Box>

                                        {/* <Box
                                            dangerouslySetInnerHTML={createMarkup(
                                                productDetails?.description
                                            )}
                                        /> */}
                                    </Box>
                                </Grid>
                            </Grid>
                        </Box>
                        <AfterAddToCartModal
                            open={cartModalOpen}
                            handleClose={() => setCartModalOpen(false)}
                            item={item}
                            addedItem={selectedProduct}
                        />
                    </>
                )}
            </Box>
        </Dialog>
    );
};
