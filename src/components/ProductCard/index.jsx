import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Box, Button, IconButton, Typography } from "@mui/material";
import productImg from "assets/images/products/products.png";
import { COLORS } from "assets/styles/colors/colors";
import { useProductCardStyle } from "assets/stylesheets/product-card/productCardStylesheet";
import { IMAGE_URL } from "helpers/config";
import { camelCaseToText, limitWords } from "helpers/functions";
import Image from 'next/image';
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { QuickView } from './quick-view';

export const ProductCard = ({ item, setOpen, setData, open, size=262 }) => {
//   const dispatch = useDispatch();
  const router = useRouter();
  const classes = useProductCardStyle();
  const [quickViewOpen, setQuickViewOpen] = useState(false);
  const [image, setImage] = useState(
    IMAGE_URL + 'product-fetured-image/' +  item?.featured_image ?? productImg
  );
  const [title, setTitle] = useState(item?.name);
  const [imageOpen, setImageOpen] = useState(false);

  const onHoverEvent = () => {
    setImageOpen(!imageOpen);
  };

  const onHoverOutEvent = () => {
    setImageOpen(!imageOpen);
  };

  return (
    <Box className={classes.root}>
        <Box
            onMouseEnter={onHoverEvent}
            onMouseLeave={() => {
            onHoverOutEvent();
            setImage(IMAGE_URL + 'product-fetured-image/' + item?.featured_image);
            setTitle(item?.name);
            }}
            className="content-box"
        >
            <Box className="image" sx={{ height: {md: size, sm: 200, xs: 200}, width:{ md: size, sm: 150, xs:150}, position: "relative", padding: "20px" }}>
                <Image src={image} alt={limitWords(item?.name, 5)} layout="fill" objectFit="contain" />
            </Box>
            <Box className="content">
            

            <Typography component={"h3"} sx={{ textTransform: 'capitalize' }}>{camelCaseToText(title) }</Typography>
            <Typography sx={{ color: "4a4a4a", fontSize: 14 }}>
                <strong style={{ color: "red", fontSize: 11 }}>SALE</strong>{" "}
                <span style={{ textDecoration: "line-through", fontSize: 10 }}>
                £{item?.regular_price}
                </span>{" "}
                <strong>£{item?.sale_price}</strong>
            </Typography>
                <Box className="overlay" onClick={() => setQuickViewOpen(!quickViewOpen)}>
                    <Box className="icons">
                        <IconButton onClick={(e)=> {
                            
                            setQuickViewOpen(!quickViewOpen);
                        }} title="Quick View"><VisibilityIcon /></IconButton>
                        <IconButton title='Product Details' onClick={(e) => {e.stopPropagation(); router.push(`/product/${item?.slug}`)}}><OpenInNewIcon/></IconButton>
                    </Box>
                    {imageOpen && (
                        <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                            {item?.groupProductArr?.map((data, index) => (
                                <Box component={Link} href={data?.slug} key={index}>
                                <Box
                                    sx={{
                                    "&:hover": {
                                        borderBottom: "2px solid #000000"
                                    }
                                    }}
                                    onMouseEnter={() => {
                                    setImage(IMAGE_URL + 'product-fetured-image/' + data?.featured_image);
                                    setTitle(data?.name);
                                    }}
                                    component={"img"}
                                    src={IMAGE_URL + 'product-fetured-image/' + data?.featured_image}
                                    width={30}
                                />
                                </Box>
                            ))}
                        </Box>
                    )}
                </Box>
            </Box>

            
                
            
        </Box>
      <Box className="button">
        <Button
          sx={{
            background: COLORS.primary,
            color: COLORS.white,
            borderRadius: "0 0 5px 5px",
            textTransform: "capitalize",
            transition: ".8s",

            "&:hover": {
              background: COLORS.black
            }
          }}
          color="primary"
          onClick={(e) => {
            e.stopPropagation();
            setOpen(!open);
            setData(item);
          }}
        >
          Add to Bag
        </Button>
      </Box>
      {quickViewOpen && (
        <QuickView open={quickViewOpen} handleClose={() => setQuickViewOpen(false)} item={item} />
      )}
      
    </Box>
  );
};
