import { Box, Typography } from '@mui/material'
import { COLORS } from 'assets/styles/colors/colors'
import Image from 'next/image'

export const SocialLoginButton = ({icon, text, onClick, color}) => {
  return (
    <Box sx={{ 
        display: 'inline-flex',
        border: `2px solid ${COLORS.gray}` ,
        borderRadius: '5px',
        padding: '8px 10px',
        flex: '1 1 0%',
        justifyContent: 'center',
        alignItems: 'center',
        color: color ?? '#000000',
        cursor: 'pointer',

        '&:hover': {
          background: COLORS.gray
        }
      }}>
        <Image alt="" src={icon} width={36} />
        <Typography>{text}</Typography>
    </Box>
  )
}
