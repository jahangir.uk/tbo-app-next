import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Box, Typography } from "@mui/material";
import { useProductTopBannerStyle } from "assets/stylesheets/productTopBanner/productTopBanner";
import { TextViewModal } from "components/Modals/TextViewModal";
import { camelCaseToText } from "helpers/functions";
import { useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { TransitionGroup } from 'react-transition-group';

export const ProductTopBanner = ({ data }) => {
    const classes = useProductTopBannerStyle();
    const maxLength = 250;
    const [textLength, setTextLength] = useState(0);
    const [open, setOpen] = useState(false);
    const [textOpen, setTextOpen] = useState(false);
    function createMarkup(text) {
        return { __html: text };
    }

    const truncatedText = useMemo(() => {
        if (!data?.description) return '';
        
        const tempElement = document.createElement('div');
        tempElement.innerHTML = data?.description;
        const text = tempElement.textContent || tempElement.innerText || '';
        setTextLength(text.length);
        // Truncate the text if it exceeds the maxLength
        if (text.length > maxLength) {
          return text.slice(0, maxLength) + '...';
        }
    
        return text;
      }, [data?.description, maxLength]);
console.log(textLength);
  return (
    <Box className={classes.root}>
      <Typography
        component={"h2"}
        sx={{ display: { md: "none", xs: "flex" } }}
        onClick={() => setOpen(!open)}
      >
        {camelCaseToText(data?.slug?.replaceAll("-", " "))}
        <KeyboardArrowDownIcon />
      </Typography>
      <TransitionGroup transitionDelay={2} >
        <Box
          className="content"
          sx={{ display: { md: "block", xs: open ? "block" : "none" } }}
        >
          <Typography
            component={"h2"}
            sx={{ display: { md: "block", xs: "none" } }}
          >
            {camelCaseToText(data?.slug?.replaceAll("-", " "))}
          </Typography>

          {/* <Typography>{data?.description}</Typography> */}
            {/* <Box sx={{ textAlign: "justify" }}
                dangerouslySetInnerHTML={createMarkup(
                    data?.description
                )}
            /> */}
            <Box >
                {truncatedText} {textLength >= maxLength ? (<span onClick={() => setTextOpen(true)} style={{ color: '#800', textDecoration: "underline", cursor: "pointer" }}>View More</span>) : ""} 
            </Box>
          <Box className="sub-items">
            {data?.items?.length > 0 &&
              data?.items.map((item, index) => (
                <Box
                  key={index}
                  component={Link}
                  to={`/${item?.link}`}
                  className="item"
                >
                  {item?.title} 
                </Box>
              ))}
          </Box>
        </Box>
      </TransitionGroup>
        <TextViewModal open={textOpen} handleClose={()=>setTextOpen(false)} text={data?.description} />
    </Box>
  );
};
