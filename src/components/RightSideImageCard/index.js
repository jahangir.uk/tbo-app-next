import { Box, Grid, Typography } from "@mui/material";
import { useRightSideImageStyle } from "assets/stylesheets/image-card/rightSideImageCardStylesheet";

export const RightSideImageCard = ({image, title, description}) => {
    const classes = useRightSideImageStyle();
  return (
    <Box className={classes.root}>
        <Grid container className="right-image-card" spacing={2}>
          <Grid item md={6}>
            <Typography component={"h2"}>{title}</Typography>
            <Typography>
             {description}
            </Typography>
          </Grid>
          <Grid item md={6}>
            <Box className="right-img">
              <Box component={"img"} src={image} alt={title} />
            </Box>
          </Grid>
        </Grid>
    </Box>
  );
}
