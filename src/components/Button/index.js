import { Button } from '@mui/material'
import { COLORS } from 'assets/styles/colors/colors'

export const CustomButton = ({text, height, ...props}) => {
  return (
    <Button 
        sx={{ 
            backgroundColor: COLORS.primary, 
            color: COLORS.white ,
            padding: height > 40 ? '12px 30px' : '8px 20px',
            fontSize: 16,
            height: height,
            margin: '15px 0',
            textTransform: 'capitalize',

            '&:hover':{
                backgroundColor: COLORS.black,  
            }
        }} 
        {...props}
    >{text}</Button>
  )
}
