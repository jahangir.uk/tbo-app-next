import { Box } from "@mui/material";
import { Puff } from "react-loader-spinner";

export const Loader = () => {
  return (
    <Box
      sx={{
        display: "flex",
        height: "85vh",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <Puff
        height="40"
        width="40"
        color="#4fa94d"
        wrapperStyle={{}}
        wrapperClass=""
        visible={true}
        radius={1}
        outerCircleColor=""
        innerCircleColor=""
        barColor=""
        ariaLabel="circles-with-bar-loading"
      />
    </Box>
  );
};
