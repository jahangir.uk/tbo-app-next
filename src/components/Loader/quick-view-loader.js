import { Box, Grid, Skeleton } from "@mui/material";

export const QuickViewLoader = () => {
    return (
        <Grid container spacing={2}>
            <Grid item md={12}>
                {" "}
                <Skeleton
                    animation="wave"
                    height={40}
                    style={{ marginBottom: 6 }}
                    variant="rectangular"
                />
            </Grid>
            <Grid item md={6}>
                <Box sx={{ padding:"10px" }}>
                    <Skeleton
                        sx={{ height: 450 }}
                        animation="wave"
                        variant="rectangular"
                    />
                    <Box
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            marginTop: "20px",
                            gap: "10px",
                            justifyContent: "space-between"
                        }}
                    >
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={60}
                            style={{ marginBottom: 6 }}
                            variant="rectangular"
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={60}
                            style={{ marginBottom: 6 }}
                            variant="rectangular"
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={60}
                            style={{ marginBottom: 6 }}
                            variant="rectangular"
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={60}
                            style={{ marginBottom: 6 }}
                            variant="rectangular"
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={60}
                            style={{ marginBottom: 6 }}
                            variant="rectangular"
                        />
                    </Box>
                </Box>
            </Grid>
            <Grid item md={6}>
                <Box sx={{ padding: "0 10px" }}>
                    <Skeleton
                        animation="wave"
                        height={60}
                        style={{ marginBottom: 6 }}
                        variant="text"
                    />

                   
                    <Skeleton
                        animation="wave"
                        height={40}
                        width={200}
                        style={{ marginBottom: 6 }}
                    />
                    <Skeleton
                        animation="wave"
                        height={40}
                        width={200}
                        style={{ marginBottom: 6 }}
                    />

                    <Box sx={{ width: "60%", display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={70}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={70}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={40}
                            width={70}
                            style={{ marginBottom: 6 }}
                        />
                    </Box>
                    
                    <Skeleton animation="wave" height={70} />
                </Box>
            </Grid>
        </Grid>
    );
};
