import { Box, Container, Grid, Skeleton } from "@mui/material";

export const HeaderLoader = () => {
    
    return (
        <Box sx={{ background: "#fff", border: "1px solid #ccc", padding: "10px" }}>
            <Container>
                <Grid container spacing={2} >
                    <Grid item md={3} >
                        <Box sx={{ maxWidth: '100%', }}>
                            <Skeleton
                                sx={{ height: 50 }}
                                animation="wave"
                                variant="rectangular"
                            />
                        </Box>
                    </Grid>
                    <Grid item md={7}>
                        <Box sx={{ maxWidth: '100%', }}>
                            <Skeleton
                                sx={{ height: 50 }}
                                animation="wave"
                                variant="rectangular"
                            />
                        </Box>
                    </Grid>
                    <Grid item md={2}>
                        <Box sx={{ maxWidth: '100%', }}>
                            <Skeleton
                                sx={{ height: 50 }}
                                animation="wave"
                                variant="rectangular"
                            />
                        </Box>
                    </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ marginTop: "2px" }}>
                    <Grid item md={12}>
                        <Skeleton
                            sx={{ height: 40 }}
                            animation="wave"
                            variant="rectangular"
                        />
                    </Grid>
                </Grid>
            </Container>
           
        </Box>
    );
};
