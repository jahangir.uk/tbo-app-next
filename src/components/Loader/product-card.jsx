import { Box, Card, CardContent, Grid, Skeleton } from "@mui/material";
import { randomArray } from "helpers/functions";

export const CardLoader = ({ total = 3, width = 280 }) => {
    const data = randomArray(total);
    return (
        <Grid container spacing={2}>
            <Grid item md={3}>
                <Box sx={{ maxWidth: width, }}>
                    <Skeleton
                        sx={{ height: 290 }}
                        animation="wave"
                        variant="rectangular"
                    />

                    <Box>
                        <Skeleton
                            animation="wave"
                            height={40}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={15}
                            style={{ marginBottom: 6 }}
                        />
                    </Box>
                    <Skeleton animation="wave" height={70} />
                </Box>
            </Grid>
            <Grid item md={3}>
                <Box sx={{ maxWidth: width }}>
                    <Skeleton
                        sx={{ height: 290 }}
                        animation="wave"
                        variant="rectangular"
                    />

                    <Box>
                        <Skeleton
                            animation="wave"
                            height={40}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={15}
                            style={{ marginBottom: 6 }}
                        />
                    </Box>
                    <Skeleton animation="wave" height={70} />
                </Box>
            </Grid>
            <Grid item md={3}>
                <Box sx={{ maxWidth: width, }}>
                    <Skeleton
                        sx={{ height: 290 }}
                        animation="wave"
                        variant="rectangular"
                    />

                    <Box>
                        <Skeleton
                            animation="wave"
                            height={40}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={15}
                            style={{ marginBottom: 6 }}
                        />
                    </Box>
                        <Skeleton animation="wave" height={70} />
                </Box>
            </Grid>
            <Grid item md={3}>
                <Box sx={{ maxWidth: width, }}>
                    <Skeleton
                        sx={{ height: 290 }}
                        animation="wave"
                        variant="rectangular"
                    />

                    <Box>
                        <Skeleton
                            animation="wave"
                            height={40}
                            style={{ marginBottom: 6 }}
                        />
                        <Skeleton
                            animation="wave"
                            height={15}
                            style={{ marginBottom: 6 }}
                        />
                    </Box>
                        <Skeleton animation="wave" height={70} />
                </Box>
            </Grid>
        </Grid>
    );
};
