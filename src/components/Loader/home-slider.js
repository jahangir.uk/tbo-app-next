import { Box, Grid, Skeleton } from "@mui/material";
import { randomArray } from "helpers/functions";

export const HomeSliderLoader = ({ total = 3 }) => {
    const data = randomArray(total);
    return (
        <Grid container spacing={2} sx={{ height:400 }}>
            <Grid item md={12}>
                <Box sx={{ maxWidth: '100%', }}>
                    <Skeleton
                        sx={{ height: 400 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
        </Grid>
    );
};
