import { Box, Grid, Skeleton } from "@mui/material";
import { randomArray } from "helpers/functions";

export const BrandSectionLoader = ({ total = 3 }) => {
    const data = randomArray(total);
    return (
        <Grid container spacing={2} marginTop={2} sx={{ padding:'80px 0', height: 100 }}>
            <Grid md={5}></Grid>
            <Grid md={2}>
                <Box>
                    <Skeleton animation="wave" height={60} />
                    <Skeleton animation="wave" height={10} />
                </Box>
            </Grid>
            <Grid md={5}></Grid>

            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            <Grid item md={2}>
                <Box sx={{ maxWidth: 150, }}>
                    <Skeleton
                        sx={{ height: 100 }}
                        animation="wave"
                        variant="rectangular"
                    />
                </Box>
            </Grid>
            
        </Grid>
    );
};
