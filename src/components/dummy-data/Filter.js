export const filter_data = [
    {label: 'GENDER', items: [
        {label: 'Mens', value: 'mens'},
        {label: 'Womens', value: 'womens'},
        {label: 'Boys', value: 'boys'},
        {label: 'Girls', value: 'girls'},
    ]},
    {label: 'PRODUCT TYPE', items: [
        {label: 'Clutch Bags', value: 'clutch-bags'},
        {label: 'Gym Equipment', value: 'gym-equipment'},
        {label: 'Hand Bags', value: 'hand-bags'},
        {label: 'Hip Bag', value: 'hip-bag'},
        {label: 'Holdalls', value: 'holdalls'},
        {label: 'Luggage', value: 'luggage'},
        {label: 'Messenger Bags', value: 'messenger-bags'},
        {label: 'Rucksacks', value: 'rucksacks'},
        {label: 'Shopper Bags', value: 'shopper-bags'},
    ]},
    {label: 'SIZE', items: [
        {label: 'One Size', value: 'one-size'},
        
    ]},
    {label: 'BRAND', items: [
        {label: 'Adidas', value: 'adidas'},
        {label: 'Ashwood', value: 'ashwood'},
        {label: 'Bench', value: 'bench'},
        {label: 'Fluid', value: 'fluid'},
        {label: 'French Connection', value: 'french-connection'},
        {label: 'Hype', value: 'hype'},
        {label: 'JACK AND JONES', value: 'jack-and-jones'},
        {label: 'Levis', value: 'levis'},
        {label: 'Nike', value: 'Nike'},
    ]},
    {label: 'PRICE', items: [
        {label: '£0 - £15', value: '£0-£15'},
        {label: '£15 - £30', value: '£15-£30'},
        {label: '£30 - £45', value: '£30-£45'},
    ]},
    {label: 'COLOUR', items: [
        {label: 'Black', value: 'black'},
        {label: 'Blue', value: 'blue'},
        {label: 'Gray', value: 'gray'},
        {label: 'Red', value: 'red'},
    ]}

]