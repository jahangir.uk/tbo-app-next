export const bannerData = {
    title: 'Womens Clothing, Footwear & Accessories',
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut leo ac arcu posuere molestie. Ut egestas malesuada eros. Curabitur in nibh eu ipsum auctor dignissim eget sed ligula. Donec tincidunt tempus bibendum. Nullam lacinia sed lacus non aliquet. Proin mattis bibendum lacus, at interdum urna. ",
    items: [{
            title: 'Trainers',
            link: '#'
        },
        {
            title: 'Jackets & Coats',
            link: '#'
        },
        {
            title: 'Hoodies & Sweatshirts',
            link: '#'
        },
        {
            title: 'T-Shirts & Vests',
            link: '#'
        },
        {
            title: 'item 1',
            link: '#'
        },
        {
            title: 'item 1',
            link: '#'
        },
        {
            title: 'item 1',
            link: '#'
        },
    ]
}