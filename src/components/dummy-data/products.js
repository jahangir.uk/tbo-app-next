export const Products = [{
        "name": "product has varient uk",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "product_type": 2,
        "slug": "product-has-varient-uk",
        "featured_image": "10202231721579.webp",
        "sku": "0202231722581",
        "productattributeArr": [{
                "id": "63b1237a0d1deb2048aa0bc1",
                "name": "color"
            },
            {
                "id": "63b123700d1deb2048aa0bc0",
                "name": "size"
            }
        ],
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63db9cfc083583b8dcf4781b"
            },
            {
                "termsId": "63b124ed0d1deb2048aa0bc7",
                "termsName": "green",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63db9cfc083583b8dcf4781b"
            },
            {
                "termsId": "63b125160d1deb2048aa0bc8",
                "termsName": "red",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63db9cfc083583b8dcf4781b"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63db9cfc083583b8dcf4781b"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63db9cfc083583b8dcf4781b"
            },
            {
                "termsId": "63b124c20d1deb2048aa0bc4",
                "termsName": "s",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63db9cfc083583b8dcf4781b"
            }
        ],
        "groupProductArr": [{
                "name": "product has varient another",
                "slug": "product-has-varient-another",
                "featured_image": "10102231712245.webp",
                "id": "63da4908fb34831584b86600"
            },
            {
                "name": "product has varient test",
                "slug": "product-has-varient-test",
                "featured_image": "10102231740997.webp",
                "id": "63da4fa5bf73ca76254e36ef"
            }
        ],
        "id": "63db9cfc083583b8dcf4781b"
    },
    {
        "name": "product has varient new",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "product_type": 2,
        "slug": "product-has-varient-new",
        "featured_image": "10102231922799.webp",
        "sku": "0102231927147",
        "productattributeArr": [{
                "id": "63b1237a0d1deb2048aa0bc1",
                "name": "color"
            },
            {
                "id": "63b123700d1deb2048aa0bc0",
                "name": "size"
            }
        ],
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da68c1e420f692255ae498"
            },
            {
                "termsId": "63b124ed0d1deb2048aa0bc7",
                "termsName": "green",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da68c1e420f692255ae498"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da68c1e420f692255ae498"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da68c1e420f692255ae498"
            }
        ],
        "groupProductArr": [{
                "name": "product has varient",
                "slug": "product-has-varient",
                "featured_image": "13101231724573.webp",
                "id": "63d8fa692dc3938bb01f9375"
            },
            {
                "name": "product has varient another",
                "slug": "product-has-varient-another",
                "featured_image": "10102231712245.webp",
                "id": "63da4908fb34831584b86600"
            }
        ],
        "id": "63da68c1e420f692255ae498"
    },
    {
        "name": "product has varient test",
        "regular_price": 33,
        "sale_price": 33,
        "available": true,
        "outofstock": false,
        "product_type": 2,
        "slug": "product-has-varient-test",
        "featured_image": "10102231740997.webp",
        "sku": "0102231740165",
        "productattributeArr": [{
                "id": "63b1237a0d1deb2048aa0bc1",
                "name": "color"
            },
            {
                "id": "63b123700d1deb2048aa0bc0",
                "name": "size"
            }
        ],
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da4fa5bf73ca76254e36ef"
            },
            {
                "termsId": "63b125160d1deb2048aa0bc8",
                "termsName": "red",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da4fa5bf73ca76254e36ef"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da4fa5bf73ca76254e36ef"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da4fa5bf73ca76254e36ef"
            }
        ],
        "groupProductArr": [],
        "id": "63da4fa5bf73ca76254e36ef"
    },
    {
        "name": "product has varient another",
        "regular_price": 55,
        "sale_price": 55,
        "available": true,
        "outofstock": false,
        "product_type": 2,
        "slug": "product-has-varient-another",
        "featured_image": "10102231712245.webp",
        "sku": "0102231712430",
        "productattributeArr": [{
                "id": "63b1237a0d1deb2048aa0bc1",
                "name": "color"
            },
            {
                "id": "63b123700d1deb2048aa0bc0",
                "name": "size"
            },
            {
                "id": "63b124060d1deb2048aa0bc2",
                "name": "outfit-type"
            }
        ],
        "producttermsArr": [{
                "termsId": "63b124ed0d1deb2048aa0bc7",
                "termsName": "green",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da4908fb34831584b86600"
            },
            {
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63da4908fb34831584b86600"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da4908fb34831584b86600"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63da4908fb34831584b86600"
            },
            {
                "termsId": "63b6b71412c9ee6c10478f9b",
                "termsName": "hoddy",
                "attributeId": "63b124060d1deb2048aa0bc2",
                "attributeName": "outfit-type",
                "productId": "63da4908fb34831584b86600"
            },
            {
                "termsId": "63b6b72212c9ee6c10478f9c",
                "termsName": "joggar",
                "attributeId": "63b124060d1deb2048aa0bc2",
                "attributeName": "outfit-type",
                "productId": "63da4908fb34831584b86600"
            }
        ],
        "groupProductArr": [],
        "id": "63da4908fb34831584b86600"
    },
    {
        "name": "simple product",
        "regular_price": 0,
        "sale_price": 0,
        "available": true,
        "outofstock": false,
        "product_type": 1,
        "slug": "simple-product",
        "featured_image": "13101231729882.webp",
        "sku": "3101231729026",
        "productattributeArr": [],
        "producttermsArr": [],
        "groupProductArr": [],
        "id": "63d8fb932dc3938bb01f93a5"
    },
    {
        "name": "product has varient",
        "regular_price": 12,
        "sale_price": 10,
        "available": true,
        "outofstock": false,
        "product_type": 2,
        "slug": "product-has-varient",
        "featured_image": "13101231724573.webp",
        "sku": "3101231724703",
        "productattributeArr": [{
                "id": "63b1237a0d1deb2048aa0bc1",
                "name": "color"
            },
            {
                "id": "63b123700d1deb2048aa0bc0",
                "name": "size"
            }
        ],
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63d8fa692dc3938bb01f9375"
            },
            {
                "termsId": "63b124ed0d1deb2048aa0bc7",
                "termsName": "green",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63d8fa692dc3938bb01f9375"
            },
            {
                "termsId": "63b125160d1deb2048aa0bc8",
                "termsName": "red",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color",
                "productId": "63d8fa692dc3938bb01f9375"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63d8fa692dc3938bb01f9375"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63d8fa692dc3938bb01f9375"
            },
            {
                "termsId": "63b124c20d1deb2048aa0bc4",
                "termsName": "s",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size",
                "productId": "63d8fa692dc3938bb01f9375"
            }
        ],
        "groupProductArr": [],
        "id": "63d8fa692dc3938bb01f9375"
    }
]