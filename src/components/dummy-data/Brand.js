import img1 from "assets/images/brands/1.webp";
import img2 from "assets/images/brands/Armani_logo.png";
import img3 from "assets/images/brands/FARAH_logo.png";
import img4 from "assets/images/brands/Gant_logo (1).png";
import img5 from "assets/images/brands/Hugo-Boss_logo.png";
import img6 from "assets/images/brands/adidas_logo.png";
import img8 from "assets/images/brands/ck_logo.png";
import img7 from "assets/images/brands/diesel_logo.png";

export const brands = [
    {image: img1, title: 'Test Slider One', link: '#' },
    {image: img2, title: 'Test Slider One', link: '#' },
    {image: img3, title: 'Test Slider One', link: '#' },
    {image: img4, title: 'Test Slider One', link: '#' },
    {image: img5, title: 'Test Slider One', link: '#' },
    {image: img6, title: 'Test Slider One', link: '#' },
    {image: img7, title: 'Test Slider One', link: '#' },
    {image: img8, title: 'Test Slider One', link: '#' },

]