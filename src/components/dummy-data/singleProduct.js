//  const item = {
//     "brand": "63b3ff86740c0c52b01de7c1",
//     "category": "63b3fffd740c0c52b01de7c2",
//     "subcategory": "63b40029740c0c52b01de7c3",
//     "subsubcategory": "63b40061740c0c52b01de7c4",
//     "meta_title": "meta title",
//     "meta_description": "Meta des",
//     "name": "product has varient uk",
//     "description": "<p>sdaas</p>\r\n",
//     "regular_price": 22,
//     "sale_price": 22,
//     "warranty": "",
//     "is_active": true,
//     "is_publish": false,
//     "is_featured": false,
//     "is_best_selling": false,
//     "is_special": false,
//     "is_clearance": false,
//     "sort": 0,
//     "min_stock_level": 0,
//     "inventory": 9,
//     "available": true,
//     "outofstock": false,
//     "stockout_msg": "",
//     "product_type": 2,
//     "is_add_multi_buy": false,
//     "add_multi_buy_opt_one_quantity": 0,
//     "add_multi_buy_opt_one_discount_percent": 0,
//     "add_multi_buy_opt_two_quantity": 0,
//     "add_multi_buy_opt_two_discount_percent": 0,
//     "add_multi_buy_opt_more_quantity": 0,
//     "add_multi_buy_opt_more_discount_percent": 0,
//     "identifier_type": 0,
//     "identifier_number": "",
//     "brandObj": {
//         "name": "addidas",
//         "image": "10301231612735.webp",
//         "slug": "addidas"
//     },
//     "categoryObj": {
//         "name": "mens",
//         "image": "10301231614963.webp",
//         "slug": "mens"
//     },
//     "subcategoryObj": {
//         "name": "mans clothing",
//         "image": "10301231615820.webp",
//         "slug": "mans-clothing"
//     },
//     "subsubcategoryObj": {
//         "name": "mans clothing",
//         "image": "10301231615820.webp",
//         "slug": "mans-clothing"
//     },
//     "productimageArr": [{
//         "id": "63db9d1a083583b8dcf4782c",
//         "title": "Image 1",
//         "alt_text": "Image 1",
//         "sort": 0,
//         "image": "10202231723657.webp"
//     }],
//     "slug": "product-has-varient-uk",
//     "featured_image": "10202231721579.webp",
//     "sku": "0202231722581",
//     "is_simple_product": false,
//     "is_variant_master_product": true,
//     "is_variant_product": false,
//     "productattributeArr": [{
//             "id": "63b1237a0d1deb2048aa0bc1",
//             "name": "color"
//         },
//         {
//             "id": "63b123700d1deb2048aa0bc0",
//             "name": "size"
//         }
//     ],
//     "producttermsArr": [{
//             "termsId": "63b124e00d1deb2048aa0bc6",
//             "termsName": "yello",
//             "attributeId": "63b1237a0d1deb2048aa0bc1",
//             "attributeName": "color",
//             "productId": "63db9cfc083583b8dcf4781b"
//         },
//         {
//             "termsId": "63b124ed0d1deb2048aa0bc7",
//             "termsName": "green",
//             "attributeId": "63b1237a0d1deb2048aa0bc1",
//             "attributeName": "color",
//             "productId": "63db9cfc083583b8dcf4781b"
//         },
//         {
//             "termsId": "63b125160d1deb2048aa0bc8",
//             "termsName": "red",
//             "attributeId": "63b1237a0d1deb2048aa0bc1",
//             "attributeName": "color",
//             "productId": "63db9cfc083583b8dcf4781b"
//         },
//         {
//             "termsId": "63b124740d1deb2048aa0bc3",
//             "termsName": "xs",
//             "attributeId": "63b123700d1deb2048aa0bc0",
//             "attributeName": "size",
//             "productId": "63db9cfc083583b8dcf4781b"
//         },
//         {
//             "termsId": "63b124cf0d1deb2048aa0bc5",
//             "termsName": "xl",
//             "attributeId": "63b123700d1deb2048aa0bc0",
//             "attributeName": "size",
//             "productId": "63db9cfc083583b8dcf4781b"
//         },
//         {
//             "termsId": "63b124c20d1deb2048aa0bc4",
//             "termsName": "s",
//             "attributeId": "63b123700d1deb2048aa0bc0",
//             "attributeName": "size",
//             "productId": "63db9cfc083583b8dcf4781b"
//         }
//     ],
//     "is_wholesale": false,
//     "local_tax_percent": 0,
//     "local_tax_amount": 0,
//     "customer_view": 0,
//     "is_new_in": true,
//     "relatedProductArr": [{
//             "name": "product has varient another",
//             "slug": "product-has-varient-another",
//             "featured_image": "10102231712245.webp",
//             "id": "63da4908fb34831584b86600"
//         },
//         {
//             "name": "product has varient new",
//             "slug": "product-has-varient-new",
//             "featured_image": "10102231922799.webp",
//             "id": "63da68c1e420f692255ae498"
//         }
//     ],
//     "suggestedProductArr": [{
//             "name": "simple product",
//             "slug": "simple-product",
//             "featured_image": "13101231729882.webp",
//             "id": "63d8fb932dc3938bb01f93a5"
//         },
//         {
//             "name": "product has varient test",
//             "slug": "product-has-varient-test",
//             "featured_image": "10102231740997.webp",
//             "id": "63da4fa5bf73ca76254e36ef"
//         }
//     ],
//     "groupProductArr": [{
//             "name": "product has varient another",
//             "slug": "product-has-varient-another",
//             "featured_image": "10102231712245.webp",
//             "id": "63da4908fb34831584b86600"
//         },
//         {
//             "name": "product has varient test",
//             "slug": "product-has-varient-test",
//             "featured_image": "10102231740997.webp",
//             "id": "63da4fa5bf73ca76254e36ef"
//         }
//     ],
//     "frbTogetherProductArr": [{
//             "name": "product has varient another",
//             "slug": "product-has-varient-another",
//             "featured_image": "10102231712245.webp",
//             "id": "63da4908fb34831584b86600"
//         },
//         {
//             "name": "product has varient test",
//             "slug": "product-has-varient-test",
//             "featured_image": "10102231740997.webp",
//             "id": "63da4fa5bf73ca76254e36ef"
//         },
//         {
//             "name": "product has varient new",
//             "slug": "product-has-varient-new",
//             "featured_image": "10102231922799.webp",
//             "id": "63da68c1e420f692255ae498"
//         }
//     ],
//     "tagArr": [{
//             "name": "brand",
//             "slug": "brand",
//             "id": "63ac268ba6f8f45c897a24ca"
//         },
//         {
//             "name": "bangla 25 xy",
//             "slug": "bangla-25-xy",
//             "id": "63ac26bba6f8f45c897a24cb"
//         }
//     ],
//     "createdAt": "2023-02-02T11:22:36.600Z",
//     "updatedAt": "2023-02-02T11:27:00.720Z",
//     "varientProductArr": [{
//             "id": "63db9cfc083583b8dcf47822",
//             "sku": "0202231722581_yello_xs",
//             "inventory": 4,
//             "termsIds": [
//                 "63b124e00d1deb2048aa0bc6",
//                 "63b124740d1deb2048aa0bc3"
//             ],
//             "image": "10202231723561.webp",
//             "slug": "product-has-varient-uk-yello-xs",
//             "is_active": true
//         },
//         {
//             "id": "63db9cfc083583b8dcf47823",
//             "sku": "0202231722581_green_xl",
//             "inventory": 5,
//             "termsIds": [
//                 "63b124ed0d1deb2048aa0bc7",
//                 "63b124cf0d1deb2048aa0bc5"
//             ],
//             "image": "10202231724036.webp",
//             "slug": "product-has-varient-uk-green-xl",
//             "is_active": true
//         },
//         {
//             "id": "63db9daa083583b8dcf47833",
//             "sku": "0202231722581_yello_xl",
//             "inventory": 0,
//             "termsIds": [
//                 "63b124e00d1deb2048aa0bc6",
//                 "63b124cf0d1deb2048aa0bc5"
//             ],
//             "image": "",
//             "slug": "product-has-varient-uk-yello-xl",
//             "is_active": true
//         },
//         {
//             "id": "63db9daa083583b8dcf47834",
//             "sku": "0202231722581_yello_s",
//             "inventory": 0,
//             "termsIds": [
//                 "63b124e00d1deb2048aa0bc6",
//                 "63b124c20d1deb2048aa0bc4"
//             ],
//             "image": "",
//             "slug": "product-has-varient-uk-yello-s",
//             "is_active": true
//         },
//         {
//             "id": "63db9daa083583b8dcf47835",
//             "sku": "0202231722581_red_s",
//             "inventory": 0,
//             "termsIds": [
//                 "63b125160d1deb2048aa0bc8",
//                 "63b124c20d1deb2048aa0bc4"
//             ],
//             "image": "",
//             "slug": "product-has-varient-uk-red-s",
//             "is_active": true
//         }
//     ],
//     "id": "63db9cfc083583b8dcf4781b"
// }

const varient_product = [{
        "sku": "0202231722581_yello_xs",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "stockout_msg": "",
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color"
            },
            {
                "termsId": "63b124740d1deb2048aa0bc3",
                "termsName": "xs",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size"
            }
        ],
        "identifier_type": 2,
        "identifier_number": "45SD45",
        "id": "63db9cfc083583b8dcf47822"
    },
    {
        "sku": "0202231722581_green_xl",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "stockout_msg": "",
        "producttermsArr": [{
                "termsId": "63b124ed0d1deb2048aa0bc7",
                "termsName": "green",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size"
            }
        ],
        "identifier_type": 3,
        "identifier_number": "45SD45",
        "id": "63db9cfc083583b8dcf47823"
    },
    {
        "sku": "0202231722581_yello_xl",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "stockout_msg": "",
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color"
            },
            {
                "termsId": "63b124cf0d1deb2048aa0bc5",
                "termsName": "xl",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size"
            }
        ],
        "identifier_type": 0,
        "identifier_number": "",
        "id": "63db9daa083583b8dcf47833"
    },
    {
        "sku": "0202231722581_yello_s",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "stockout_msg": "",
        "producttermsArr": [{
                "termsId": "63b124e00d1deb2048aa0bc6",
                "termsName": "yello",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color"
            },
            {
                "termsId": "63b124c20d1deb2048aa0bc4",
                "termsName": "s",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size"
            }
        ],
        "identifier_type": 0,
        "identifier_number": "",
        "id": "63db9daa083583b8dcf47834"
    },
    {
        "sku": "0202231722581_red_s",
        "regular_price": 22,
        "sale_price": 22,
        "available": true,
        "outofstock": false,
        "stockout_msg": "",
        "producttermsArr": [{
                "termsId": "63b125160d1deb2048aa0bc8",
                "termsName": "red",
                "attributeId": "63b1237a0d1deb2048aa0bc1",
                "attributeName": "color"
            },
            {
                "termsId": "63b124c20d1deb2048aa0bc4",
                "termsName": "s",
                "attributeId": "63b123700d1deb2048aa0bc0",
                "attributeName": "size"
            }
        ],
        "identifier_type": 0,
        "identifier_number": "",
        "id": "63db9daa083583b8dcf47835"
    }
]

const selectedOption = [{
        "termsId": "63b125160d1deb2048aa0bc8",
        "termsName": "red",
        "attributeId": "63b1237a0d1deb2048aa0bc1",
        "attributeName": "color",
        "productId": "63db9cfc083583b8dcf4781b"
    },
    {
        "termsId": "63b124c20d1deb2048aa0bc4",
        "termsName": "s",
        "attributeId": "63b123700d1deb2048aa0bc0",
        "attributeName": "size",
        "productId": "63db9cfc083583b8dcf4781b"
    }
]



const matchedProducts = varient_product.find((product) => {
    // check if the product has the same number of attributes as the selected options
    if (product.producttermsArr.length !== selectedOption.length) {
        return false;
    }
    // check if all selected options exist in the product
    return selectedOption.every((option) => {
        return product.producttermsArr.some((productOption) => {
            return (
                productOption.attributeId === option.attributeId &&
                productOption.termsId === option.termsId
            );
        });
    });
});
console.log(matchedProducts);


export const singleOrder = {
    "orderitem": [
        {
            "quantity": 1,
            "product_type": 2,
            "price": 11.99,
            "total_price": 18.99,
            "product": "64636c527bda6125cf331a5a",
            "productObj": {
                "id": "64636c527bda6125cf331a5a",
                "slug": "mens-full-tracksuits-set-gym-hoodie-joggers-casual-winter-sweatshirt-top-bottoms",
                "sku": "1605231743714",
                "name": "mens full tracksuits set gym hoodie joggers casual winter sweatshirt top bottoms",
                "featured_image": "",
                "regular_price": 49.99,
                "sale_price": 18.99
            },
            "variantProduct": "64636c527bda6125cf331a66",
            "vatiantProductObj": {
                "id": "64636c527bda6125cf331a66",
                "slug": "mens-full-tracksuits-set-gym-hoodie-joggers-casual-winter-sweatshirt-top-bottoms-gh-s-olive-hoodie",
                "sku": "GH_S_Olive_Hoodie",
                "name": "Mens Full Tracksuits Set Gym Hoodie Joggers Casual Winter Sweatshirt Top Bottoms_GH_S_Olive_Hoodie",
                "featured_image": "wmsv-2-1605231743716.webp",
                "regular_price": 49.99,
                "sale_price": 11.99
            },
            "order": "6463714c7bda6125cf331a7b",
            "is_refunded": false,
            "refunded_quantity": 0,
            "refunded_total_price": 0,
            "is_returned": false,
            "returned_quantity": 0,
            "returned_total_price": 0,
            "createdAt": "2023-05-16T12:04:28.421Z",
            "updatedAt": "2023-05-16T12:04:28.421Z",
            "id": "6463714c7bda6125cf331a7c"
        }
    ],
    "customer": "63ff2eb1d9b34c829410106b",
    "invoice": "230516r2zxje1s",
    "code": "230516td5SYwdgZNu2IHw0cx3lMux1ZV7n5GCP",
    "billing_first_name": "",
    "billing_last_name": "",
    "billing_email": "jahangir147441@gmail.com",
    "billing_street_address": "House#57",
    "billing_street_address2": "Road#19",
    "billing_street_address3": "",
    "billing_post_code": "1850",
    "billing_phone": "01778175444",
    "billing_country": "BD",
    "billing_city": "Mirpur",
    "billing_county": "",
    "shipping_method": "Free Delivery",
    "shipping_first_name": "",
    "shipping_last_name": "",
    "shipping_email": "jahangir147441@gmail.com",
    "shipping_street_address": "House#57",
    "shipping_street_address2": "Road#19",
    "shipping_street_address3": "",
    "shipping_post_code": "1850",
    "shipping_phone": "01778175444",
    "shipping_country": "BD",
    "shipping_city": "Mirpur",
    "shipping_county": "",
    "payment_method": "paypal",
    "payer_id": "KKMQ6QHDPJN7J",
    "payer_name": "John",
    "payer_email": "sb-tpfiv25490079@business.example.com",
    "status": 2,
    "sub_total": 18.99,
    "shipping_cost": 0,
    "grand_total": 18.99,
    "transaction_id": "",
    "coupon_code": "",
    "coupon_value": 0,
    "is_refunded": false,
    "is_returned": false,
    "agree_terms_condition": true,
    "createdAt": "2023-05-16T12:04:28.392Z",
    "updatedAt": "2023-05-16T12:04:28.392Z",
    "id": "6463714c7bda6125cf331a7b"
}