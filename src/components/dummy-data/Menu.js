export const primary_menus = {
    mens: [
        {label: 'MENS CLOTHING', link: '/shop/mens/mens-clothing', items: [
            {label: 'Hoodies & Sweatshirts', link: '/shop/mens/mens-clothing/hoodies-sweatshirts'},
            {label: 'Jackets & Coats', link: '/shop/mens/mens-clothing/jackets-coats'},
            {label: 'Jeans', link: '/shop/mens/mens-clothing/jeans'},
            {label: 'Jumpers & Cardigans', link: '/shop/mens/mens-clothing/jumpers-cardigans'},
            {label: 'Nightwear & Slippers', link: '/shop/mens/mens-clothing/nightwear-slippers'},
            {label: 'Polo', link: '/shop/mens/mens-clothing/polo'},
            {label: 'Shirt', link: '/shop/mens/mens-clothing/shirt'},
            {label: 'Sports', link: '/shop/mens/mens-clothing/sports'},
            {label: 'Suits & Tailoring', link: '/shop/mens/mens-clothing/suits-tailoring'},
            {label: 'Swimwear', link: '/shop/mens/mens-clothing/swimwear'},
            {label: 'T-Shirt & Vests', link: '/shop/mens/mens-clothing/t-shirt-vests'},
            {label: 'Tracksuits & Joggers', link: '/shop/mens/mens-clothing/tracksuits-joggers'},
            {label: 'Trousers & Chinos', link: '/shop/mens/mens-clothing/trousers-chinos'},
            {label: 'Underwear & Socks', link: '/shop/mens/mens-clothing/underwear-socks'},
        ]},
        {label: 'MENS FOOTWEAR', link: '/shop/mens/mens-footwear', items: [
            {label: 'Boots', link:'/shop/mens/mens-footwear/boots'},
            {label: 'Flip Flops & Sandals', link:'/shop/mens/mens-footwear/flip-flop-sandals'},
            {label: 'Shoes', link:'/shop/mens/mens-footwear/shoes'},
            {label: 'Trainers', link:'/shop/mens/mens-footwear/trainers'},

        ]},
        {label: 'MENS ACCESSORIES', link: '/shop/mens/mens-accessories', items: [
            {label:'Bags & Wallets', link:'/shop/mens/mens-accessories/bags-wallets'},
            {label:'Belts & Caps', link:'/shop/mens/mens-accessories/belts-caps'},
            {label:'Other Accessories', link:'/shop/mens/mens-accessories/other-accessories'},
        ]},
        {label: 'MENS OFFER', link: '/shop/mens/mens-offer', items: [
            {label:'Bags & Wallets', link:'/shop/mens/mens-offer/bags-wallets'},
            {label:'Belts & Caps', link:'/shop/mens/mens-offer/belts-caps'},
            {label:'Other Accessories', link:'/shop/mens/mens-offer/other-accessories'},
        ]},
    ],

    womens: [
        {label: 'WOMENS CLOTHING', link: '/shop/womens/womens-clothing', items: [
            {label: 'Hoodies & Sweatshirts', link: '/shop/womens/womens-clothing/hoodies-sweatshirts'},
            {label: 'Jackets | Coats', link: '/shop/womens/womens-clothing/jackets-coats'},
            {label: 'Jumpers | Cardigans', link: '/shop/womens/womens-clothing/jumpers-cardigans'},
            {label: 'Dresses | Skirts', link: '/shop/womens/womens-clothing/dresses-skirts'},
            {label: 'Shirts | Tops', link: '/shop/womens/womens-clothing/shirts-tops'},
            {label: 'Playsuits | Jumpsuits', link: '/shop/womens/womens-clothing/playsuits-jumpsuits'},
            {label: 'T-Shirts | Vests', link: '/shop/womens/womens-clothing/t-shirt-vests'},
            {label: 'Tracksuits | Joggers', link: '/shop/womens/womens-clothing/tracksuits-joggers'},
            {label: 'Beachwear', link: '/shop/womens/womens-clothing/beachwear'},
            {label: 'Trousers | Leggings', link: '/shop/womens/womens-clothing/trousers'},
            {label: 'Activewear', link: '/shop/womens/womens-clothing/activewear'},
            {label: 'Jeans', link: '/shop/womens/womens-clothing/jeans'},
            {label: 'Shorts', link: '/shop/womens/womens-clothing/shorts'},
        ]},
        {label: 'WOMENS FOOTWEAR', link: '/shop/womens/womens-footwear', items: [
            {label: 'Bags | Clutch', link:'/shop/womens/womens-footwear/bags-clutch'},
            {label: 'Jewellery | Watches', link:'/shop/womens/womens-footwear/jewellery-watches'},
            {label: 'Scarves | Shawl', link:'/shop/womens/womens-footwear/scarves-shawl'},
            {label: 'Caps | Hats', link:'/shop/womens/womens-footwear/caps-hats'},
            {label: 'Sunglasses | Belts', link:'/shop/womens/womens-footwear/sunglasses-belts'},

        ]},
        {label: 'WOMENS ACCESSORIES', link: '/shop/womens/womens-accessories', items: [
            {label:'Heels', link:'/shop/womens/womens-accessories/heels'},
            {label:'Trainer | Sneakers', link:'/shop/womens/womens-accessories/trainer-sneakers'},
            {label:'Boots', link:'/shop/womens/womens-accessories/boots'},
        ]},
        {label: 'WOMENS OFFER', link: '/shop/womens/womens-offer', items: [
            {label:'Bags & Wallets', link:'/shop/womens/womens-offer/bags-wallets'},
            {label:'Belts & Caps', link:'/shop/womens/womens-offer/belts-caps'},
            {label:'Other Accessories', link:'/shop/womens/womens-offer/other-accessories'},
        ]},
    ],

    kids: [
        {label: '', link:'/shop/kids', items: [
            {label: 'Boys', link: '/shop/kids/boys'},
        ]},
        {label: '', link:'#', items: [
            {label: 'Girls', link: '/shop/kids/girls'},
        ]},
    ],

    brands: {
        name: "",
        slug: "",
        items: [ {name: '', slug:'', items: [
            {
                "name": "abercrombie & fitch",
                "slug": "abercrombie-fitch",
                "id": "64323b39e91db72b89a57b69"
            },
            {
                "name": "adidas",
                "slug": "adidas",
                "id": "64324ac766dc012d3d71c47c"
            },
            {
                "name": "allsaints",
                "slug": "allsaints",
                "id": "6433bfc310f6883521a927c9"
            },
            {
                "name": "armani",
                "slug": "armani",
                "id": "6433bfe610f6883521a927cb"
            },
            {
                "name": "berna",
                "slug": "berna",
                "id": "6433c04310f6883521a927ce"
            },
            {
                "name": "calvin klein",
                "slug": "calvin-klein",
                "id": "6433c05610f6883521a927cf"
            },
            {
                "name": "comme des fuckdown",
                "slug": "comme-des-fuckdown",
                "id": "6433c07010f6883521a927d1"
            },
            {
                "name": "costume national",
                "slug": "costume-national",
                "id": "6433c0c610f6883521a927d2"
            },
            {
                "name": "diane von fürstenberg",
                "slug": "diane-von-f-rstenberg",
                "id": "6433c0da10f6883521a927d3"
            },
            {
                "name": "diesel",
                "slug": "diesel",
                "id": "6433c0ed10f6883521a927d4"
            },
            {
                "name": "duvetica",
                "slug": "duvetica",
                "id": "6433c10310f6883521a927d5"
            },
        ]},
        {name: '', link:'', items: [
            {
                "name": "ellesse",
                "slug": "ellesse",
                "id": "6433c11610f6883521a927d6"
            },
            {
                "name": "farah",
                "slug": "farah",
                "id": "6433c12b10f6883521a927d7"
            },
            {
                "name": "fv men",
                "slug": "fv-men",
                "id": "6433c15110f6883521a927d8"
            },
            {
                "name": "galliano",
                "slug": "galliano",
                "id": "6433c16610f6883521a927d9"
            },
            {
                "name": "gant",
                "slug": "gant",
                "id": "6433c18210f6883521a927da"
            },
            {
                "name": "gf ferre",
                "slug": "gf-ferre",
                "id": "6433c1a210f6883521a927db"
            },
            {
                "name": "gym king",
                "slug": "gym-king",
                "id": "6433c1bd10f6883521a927dc"
            },
            {
                "name": "henbury",
                "slug": "henbury",
                "id": "6433c1d110f6883521a927dd"
            },
            {
                "name": "henleys",
                "slug": "henleys",
                "id": "6433c1f910f6883521a927df"
            },
            {
                "name": "hugo boss",
                "slug": "hugo-boss",
                "id": "6433c20e10f6883521a927e1"
            },
            {
                "name": "just cavalli",
                "slug": "just-cavalli",
                "id": "6433c27b10f6883521a927e5"
            },
        ]},  
        {name: '', link:'', items: [
            {
                "name": "kappa",
                "slug": "kappa",
                "id": "6433c29c10f6883521a927e6"
            },
            {
                "name": "kenzo",
                "slug": "kenzo",
                "id": "6433c2b510f6883521a927e8"
            },
            {
                "name": "lacoste",
                "slug": "lacoste",
                "id": "6433c46710f6883521a927f3"
            },
            {
                "name": "lock & co london",
                "slug": "lock-co-london",
                "id": "6433c48c10f6883521a927f5"
            },
            {
                "name": "lyle & scott",
                "slug": "lyle-scott",
                "id": "6433c4fb10f6883521a927f8"
            },
            {
                "name": "miss grant",
                "slug": "miss-grant",
                "id": "6433c51a10f6883521a927fa"
            },
            {
                "name": "morley",
                "slug": "morley",
                "id": "6433c52d10f6883521a927fb"
            },
            {
                "name": "moschino",
                "slug": "moschino",
                "id": "6433c54310f6883521a927fd"
            },
            {
                "name": "nike",
                "slug": "nike",
                "id": "6433c55a10f6883521a927fe"
            },
            {
                "name": "other brands",
                "slug": "other-brands",
                "id": "6433c75d10f6883521a9280f"
            },
            {
                "name": "palm angels",
                "slug": "palm-angels",
                "id": "6433c78c10f6883521a92811"
            },
        ]},  
        {name: '', link:'', items: [
            {
                "name": "ralph lauren",
                "slug": "ralph-lauren",
                "id": "6433c7ab10f6883521a92812"
            },
            {
                "name": "ruf & tuf",
                "slug": "ruf-tuf",
                "id": "6433c7bd10f6883521a92814"
            },
            {
                "name": "samoon",
                "slug": "samoon",
                "id": "6433c7d610f6883521a92816"
            },
            {
                "name": "starter",
                "slug": "starter",
                "id": "6433c7e810f6883521a92817"
            },
            {
                "name": "the north face",
                "slug": "the-north-face",
                "id": "6433c7ff10f6883521a92819"
            },
            {
                "name": "timberland",
                "slug": "timberland",
                "id": "6433c80f10f6883521a9281a"
            },
            {
                "name": "tom tompson",
                "slug": "tom-tompson",
                "id": "6433c82310f6883521a9281b"
            },
            {
                "name": "tommy hilfiger",
                "slug": "tommy-hilfiger",
                "id": "6433c85810f6883521a9281c"
            },
            {
                "name": "true religion",
                "slug": "true-religion",
                "id": "6433c88c10f6883521a9281d"
            },
            {
                "name": "umbro",
                "slug": "umbro",
                "id": "6433c89e10f6883521a9281e"
            },
            {
                "name": "versace",
                "slug": "versace",
                "id": "6433c8ad10f6883521a9281f"
            },
        ]},]
    }
}


export const footer_menu = {
    help_menu: [
        {label: 'Contact Us', link: '/contact-us'},
        {label: 'Return Policy', link: '#'},
        {label: 'Customer Care', link: '/customer-care'},
        {label: 'Coupon Page', link: '#'},
        {label: 'Cookies Policy', link: '/cookies'},
        {label: 'Coupon Partner', link: 'https://www.couponupto.com/coupons/top-brand-outlet'},
    ],

    top_brand: [
        {label: 'Men', link: '/shop/mens'},
        {label: 'Womens', link: '/shop/womens'},
        {label: 'Kids', link: '/shop/kids'},
        {label: 'Find Us', link: '#'},
        {label: 'Track Order', link: '/order-tracking'},
        {label: 'Terms & Conditions', link: '/terms-and-condition'},
        {label: 'Privacy Policy', link: '/privacy-policy'},
    ]
}