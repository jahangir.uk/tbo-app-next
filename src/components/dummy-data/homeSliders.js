import { default as slider1, default as slider2, default as slider3 } from "assets/images/slider/banner2.jpg"
export const sliders = [{
        title: 'First Slider',
        action_link: '#',
        image_uri: slider1,
        button_text: 'Shop Now',
        details: 'Lorem Ipsome doler site amit'
    },
    {
        title: 'Second Slider',
        action_link: '#',
        image_uri: slider2,
        button_text: 'View More',
        details: 'Lorem Ipsome doler site amit'
    },
    {
        title: 'Third Slider',
        action_link: '#',
        image_uri: slider3,
        button_text: 'Click Here',
        details: 'Lorem Ipsome doler site amit'
    },
]