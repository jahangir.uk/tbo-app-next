export const Faqs = {
    delivery: [],
    orders: [],
    returns: [{
            title: 'HOW LONG DO I HAVE TO EXCHANGE/RETURN AN ITEM?',
            details: `You have 30 days from the date you receive your delivery to return goods from your order for an exchange or refund. We do allow slightly longer to return items for an exchange or credit note. Please note we do not accept any items back after 30 days from the date the order is delivered.`
        },
        {
            title: 'IS THERE A CHARGE TO MAKE A RETURN?',
            details: `We do not offer free returns, however we do have several ways for you to return your item to us. You can purchase a return label at very competitive rates with <a href="https://www.parcel2go.com/">Parcel2Go</a>. This allows us to offer an affordable returns service for all of our customers which includes tracking and insurance.`
        },
        {
            title: 'ARE THERE ANY PRODUCTS YOU DO NOT ACCEPT BACK?',
            details: `<p>We do not accept earrings or underwear back due to health and hygiene regulations. We also do not accept returns on products from Crep Protect due to the usable nature of the items. All returned goods are subject to stringent checks by our returns department and any garments with a strong defined odor such as tobacco, cooking/food smells, aftershave or deodorant will not be accepted.</p> <p>Please include your return form in the return parcel and simply fill the return form and state that you would like to receive an exchange. We require the product ID number and size you require in the exchange item also writing on the return form. If you are unable to fill the return form, please enclose a note with your name, order number and that you require a refund written on it. All goods must have original tags attached and returned in the same condition as received. If the item is in a box, such as trainers, we will not accept your return if the box is damaged or has been written on.</p>`
        },
        {
            title: 'HOW LONG DO I HAVE TO EXCHANGE/RETURN AN ITEM?',
            details: `You have 30 days from the date you receive your delivery to return goods from your order for an exchange or refund. We do allow slightly longer to return items for an exchange or credit note. Please note we do not accept any items back after 30 days from the date the order is delivered.`
        },
        {
            title: 'HOW DO I REQUEST A REFUND?',
            details: `Please include your return form in the return parcel and simply fill the return form and state that you would like to receive a refund. If you are unable to fill the return form, please enclose a note with your name, order number and that you require a refund written on it. All goods must have original tags attached and returned in the same condition as received. If the item is in a box, such as trainers, we will not accept your return if the box is damaged or has been written on.`
        },
        {
            title: 'WHAT DO I DO IF MY EXCHANGE ITEM COSTS MORE THAN MY RETURN ITEM?',
            details: `In the case of a price difference, Top Brand Outlet will automatically process a refund when this is in the customer’s favour. If the difference requires a payment to Top Brand Outlet a invoice will be sent to the customer’s email for the difference outstanding.`
        },
        {
            title: 'HOW LONG WILL MY EXCHANGE/REFUND TAKE?',
            details: `<p>We aim to process all refunds within 24 hours* of the item(s) been received. A refund to a credit/debit card takes between 3-5 working days to show in your account (dependent on your bank’s processing time). A PayPal or Clear Pay refund takes 3-6 hours and goes into the PayPal or Clear Pay account you originally paid with.</p><p>We aim to process all exchanged orders within 24 hours of the item(s) been received. Once your exchanged order has been set up you will receive an email confirmation with your new order number and upon dispatch your new tracking number.</p><p>Please note that all exchanges are delivered to you using our standard 48 Hours Royal Mail service. If you need your exchange faster please <a href="#">contact</a> us to upgrade your delivery. Fees apply.</p><p>*This can take up to 5 working days in sale periods or over weekends.</p>`
        },

        {
            title: "DO YOU CHARGE A DELIVERY FEE ON EXCHANGE ORDERS?",
            details: `We do not charge any delivery on an exchanged order. You will receive a dispatch confirmation email with the new order number and tracking information when your exchange has been processed, this will show there has been no delivery charge.`,
        },
        {
            title: "HOW DO I RETURN THE GOODS TO YOU?",
            details: `You can return your goods to us using our return form also return can be made by any delivery service from Parcel2Go which is shown at the bottom of this page. Simply right-click on the return form and save picture, print & fill the return form and attach to your return parcel. There is an upfront cost to 3-5 pounds for returns varies based on the delivery services option.  All the service in Parcel2Go includes the benefits of full tracking, insurance to cover loss by courier, various drop off points, generous weight allowance, and hassle-free reliable service.`,
        },
        {
            title: "WHAT IF I HAVE LOST RETURN FORM, CAN I STILL RETURN?",
            details: `<img src="https://topbrandoutlet.co.uk/wp-content/uploads/2020/08/returns.jpg" alt="Img" />`,
        },
        {
            title: "HOW DO I RETURN THE GOODS TO YOU IF I HAVE NO ACCESS TO A PRINTER?",
            details: `You can return your items to the address below via your chosen courier.<ul>
                <li>Top Brand Outlet Ltd</li>
                <li>Unit 10 Howletts Hall Farm</li>
                <li>Mill Lane</li>
                <li>Nevestock</li>
                <li>Romford</li>
                <li>Essex</li>
                <li>RM4 1ET</li>
                <li>United Kingdom</li>
            </ul><p>Please use a tracked service with the correct insurance, which allows cover for a return order should it go missing in transit.</p>`,
        },
        {
            title: "HOW DO I RETURN THE GOODS TO YOU IF I HAVE NO ACCESS TO A PRINTER?",
            details: `<p>We would still require you to return the item exactly the same as a normal return. The only difference is you will need to write on the return note that the item is faulty and also contact the returns department beforehand. If you are able to email contactus@topbrandoutlet.co.uk and attach any images of the fault this can be logged prior to the returned item arriving. We also ask where possible you use our pre-paid labels to make a faulty return so we are able to waive the £3.99 fee as we do not charge a return fee on any faulty items. Please be aware all faulty items will be assessed by our returns department and in some cases, we would need to return the item to the brand for a final decision.</p><p>Please Note: All faulty products must be returned to us within 30 days of you reporting the issue to us. Failure to do so may result in further delays.</p><h3>RETURNING VIA ANY DELIVERY SERVICES OF YOUR CHOICE</h3><p>Take your parcel to your local Drop Off or Book a collection.</p><ol type="i">
                <li>Easily print a  label online.</li>
                <li>Online tracking on all parcels.</li>
                <li>Buy any delivery services.</li>
                <li>Allow 1 – 3 days for the parcel to reach us.</li>
            </ol><a href="http://www.parcel2go.com/">Parcel2Go</a>`,
        },
    ],
    myAccounts: [],
    website: [{
            title: 'DOES YOUR SITE HAVE SECURE PAYMENT?',
            details: `<p>Absolutely, we use the <a href="https://stripe.com/">Stripe</a> as our payment getaway system which give the added safety step of your bank verifying the order.</p> <p>We take your security very seriously, and we’ve invested in a totally secure and safe website for you to shop. Once you checkout, we encrypt all your details using Internet standard encryption technology (TSL/SSL) which can be verified by the lock which appear left side of address bar.</p><p>If you have any questions about the online security of Top Brand Outlet please email contactus@topbrandoutlet.co.uk. Or, please feel free to visit our Privacy Policy for more information.</p>`
        }, {
            title: 'I LIVE OUTSIDE THE UK, CAN I STILL ORDER?',
            details: `<p>We are happy to announce that we offer 230 countries and territories with Royal Mail.</p><p>Please visit <a href="https://www.royalmail.com/international-zones">Royal Mail</a> for update list of countries & territories.</p><p>Unfortunately we don’t deliver to any other countries not listed on Royal Mail. If you’ve any queries ralated to  international delivery options, please drop us an email to contactus@topbrandoutlet.co.uk</p>`
        },
        {
            title: "YOUR PRICES ARE VERY CHEAP, ARE YOUR PRODUCTS AUTHENTIC?",
            details: `Everything on the Top Brand Outlet is authentic and obtained directly from brand authorised dealers. We would never sell an item of clothing that was from an unofficial source.`
        }
    ],
    contactUs: [],
}