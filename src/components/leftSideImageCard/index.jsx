import { Box, Grid, Typography } from "@mui/material";
import { useLeftSideImageStyle } from "assets/stylesheets/image-card/leftSideImageCardStylesheet";

export const LeftSideImageCard = ({ image, title, description }) => {
    const classes = useLeftSideImageStyle();
  return (
    <Box className={classes.root}>
      <Grid container className="left-image-card" spacing={2}>
        <Grid item md={6}>
          <Box className="left-img">
            <Box component={"img"} src={image} alt="Left image card" />
          </Box>
        </Grid>
        <Grid item md={6}>
          <Typography component={"h2"}>{title}</Typography>
          <Typography>{description}</Typography>
        </Grid>
      </Grid>
    </Box>
  );
};
