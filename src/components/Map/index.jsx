import { Box, Grid } from "@mui/material";

export const GoogleMap = () => {
  return (
    // Important! Always set the container height explicitly

    <Grid container>
      <Grid item md={12}>
        <Box
          sx={{
            position: "relative",
            textAlign: "right",
            height: "450px",
            width: "100%",
            border: "1px solid #bfbfbf"
          }}
        >
          <Box
            sx={{
              overflow: "hidden",
              background: "none!important",
              height: "100%",
              width: "100%"
            }}
          >
            <iframe
              title="Google Map"
              width="100%"
              height="450px"
              id="gmap_canvas"
              src="https://maps.google.com/maps?q=Greenhithe DA9&t=&z=13&ie=UTF8&iwloc=&output=embed"
              frameborder="0"
              scrolling="yes"
              marginheight="0"
              marginwidth="0"
            ></iframe>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
