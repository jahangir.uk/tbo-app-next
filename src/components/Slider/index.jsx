import Slider from "react-slick";

const Sliders = ({ children, settings, ...props }) => {
  return (
    <Slider {...settings} {...props}>
      {children}
    </Slider>
  );
};

export default Sliders;
