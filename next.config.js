/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: false,
  images: {
    formats: ['image/avif', 'image/webp'],
    remotePatterns: [
      {
        protocol: 'http',
        hostname: '20.58.20.129',
        port: '',
        pathname: '/uploads/**',
      },
    ],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  api: {
    bodyParser: {
      sizeLimit: '5mb',
    },
    externalResolver: true,
  },
  pageExtensions: ['jsx', 'js', 'ts', 'tsx'],
  distDir: 'build',
  
}

module.exports = nextConfig
